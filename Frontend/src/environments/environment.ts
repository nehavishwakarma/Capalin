// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: "http://localhost/kaplin/",
  apiUrl1: "http://localhost:4201/api/",
  bajajAllianz:
    "https://general.bajajallianz.com/Insurance/WS/new_cc_payment.jsp?requestId=",
  bajajAllianzUsername: "webservice@kapalin.com",
  shriram: "http://119.226.131.2/PC10Web/PC10Web.AgentPortal/frmWSPayment.aspx",
  // shriramUsername: '',
  futurGenerali:
    "http://fglpg001.futuregenerali.in/Ecom_NL/WEBAPPLN/UI/Common/WebAggPayNew.aspx",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
