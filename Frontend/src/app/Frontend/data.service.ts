import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class DataService {
  constructor(private http: HttpClient) {}
  baseApiUrl = environment.apiUrl;
  baseApiUrl1 = environment.apiUrl1;

  getCarModelDetails(): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "api/car-insurance-ajax/car_model/car_rto"
    );
  }

  getCarFuel(table, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "car-insurance-ajax/" + table + "/" + id
    );
  }

  getCarVariant(table, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "car-insurance-ajax/" + table + "/" + id
    );
  }

  getBikeModelDetails(): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "api/bike-insurance-ajax/bike_manufacturer/car_rto"
    );
  }

  getBikeVariant(table, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "bike-insurance-ajax/" + table + "/" + id
    );
  }

  getCarStoreJson(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/car-insurance",
      JSON.stringify(data)
    );
  }

  getBikeStoreJson(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-insurance",
      JSON.stringify(data)
    );
  }

  getHealthStoreJson(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/health-insurance",
      JSON.stringify(data)
    );
  }

  getCarPropsalData(bike_enquiry_id): Observable<any> {
    return this.http.get<any>(
      this.baseApiUrl + "api/fetch-car-proposal/" + bike_enquiry_id
    );
  }

  saveQuoteMailBike(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-proposal-form-save-quote-email",
      JSON.stringify(data)
    );
  }

  getBikePropsalData(bike_enquiry_id): Observable<any> {
    return this.http.get<any>(
      this.baseApiUrl + "api/fetch-bike-proposal/" + bike_enquiry_id
    );
  }

  getInsurerApi(id): Observable<any> {
    return this.http.post<any>(this.baseApiUrl + "api/car-quotes/" + id, "");
  }

  getBikeInsurerApi(id): Observable<any> {
    return this.http.post<any>(this.baseApiUrl + "api/bike-quotes/" + id, "");
  }

  getHealthInsurerApi(id): Observable<any> {
    return this.http.get<any>(this.baseApiUrl + "api/health-quotes/" + id);
  }

  getHDFCApiApi(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/insurer-api",
      JSON.stringify(data)
    );
  }

  getBikeHDFCApiApi(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-insurer-api",
      JSON.stringify(data)
    );
  }

  editCarQuotes(id, data: any): Observable<any> {
    console.log(data);
    return this.http.post<any>(
      this.baseApiUrl + "api/car-quotes/edit/" + id,
      JSON.stringify(data)
    );
  }

  editBikeQuotes(id, data: any): Observable<any> {
    console.log(data);
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-quotes/edit-insurer-api/" + id,
      JSON.stringify(data)
    );
  }

  editHealhQuotes(data: any): Observable<any> {
    return this.http.post(
      this.baseApiUrl + "api/health-quotes/edit",
      JSON.stringify(data)
    );
  }

  editInsurerApi(id, data: any): Observable<any> {
    console.log(data);
    return this.http.post<any>(
      this.baseApiUrl + "api/car-quotes/edit-insurer-api/" + id,
      JSON.stringify(data)
    );
  }

  newDemoFuntion(data: any): Observable<any> {
    console.log(JSON.stringify(data) + "  on DataService");
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-quotes/new-demo-function",
      JSON.stringify(data)
    );
  }

  editBikeInsurerApi(id, data: any): Observable<any> {
    console.log(data);
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-quotes/edit-insurer-api/" + id,
      JSON.stringify(data)
    );
  }

  proposalFormSave(data: any): Observable<any> {
    //Lead_id is required
    // this.http.get(this.baseApiUrl+'update-car-lead-status/298/11');
    return this.http.post<any>(
      this.baseApiUrl + "api/car-proposal-form-save",
      JSON.stringify(data)
    );
  }

  bikeProposalFormSave(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-proposal-form-save",
      JSON.stringify(data)
    );
  }

  contactus(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/contactus",
      JSON.stringify(data)
    );
  }

  healthProposalFormSave(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/health-proposal-form-save",
      JSON.stringify(data)
    );
  }

  healthProposalQuoteSaveEmail(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/health-proposal-form-save-quote-email",
      JSON.stringify(data)
    );
  }

  downloadPdf(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/health-summary-form-save-pdf",
      JSON.stringify(data)
    );
  }

  getCity(value, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl +
        "api/car-insurer-city-state-district/" +
        id +
        "/" +
        value
    );
  }

  getHealthCity(value, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl +
        "health/health-insurer-city-state-district/" +
        id +
        "/" +
        value
    );
  }

  getCityproposalFormSummary(pid, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "api/car-proposal-form-summary/" + id + "/" + pid
    );
  }

  getCarProposalForm(id): Observable<any> {
    return this.http.get(this.baseApiUrl + "api/car-proposal-form/" + id);
  }

  getBikeFormSummary(pid, id): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "api/bike-proposal-form-summary/" + id + "/" + pid
    );
  }

  LoanProvider(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/car-proposal-insurance-finance",
      JSON.stringify(data)
    );
  }

  bookNowHDFC(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/car-proposal-form",
      JSON.stringify(data)
    );
  }

  proposalFormSaveUpdate(lead_id): Observable<any> {
    //Lead_id is required
    return this.http.get(
      this.baseApiUrl + "update-car-lead-status/" + lead_id + "/11"
    );
  }

  bookNowBikeHDFC(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-proposal-form",
      JSON.stringify(data)
    );
  }

  bookNow(
    data: any,
    id,
    mastersheetId,
    premium,
    health_enquiry_id
  ): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl +
        "api/buy-health-plan-hand/" +
        id +
        "/" +
        mastersheetId +
        "/" +
        premium +
        "/" +
        health_enquiry_id,
      JSON.stringify(data)
    );
  }

  proposalIndex(enquiry_id, id): Observable<any> {
    if (id == null) {
      return this.http.get<any>(
        this.baseApiUrl + "api/health-proposal-form/" + enquiry_id + "/" + id
      );
    } else {
      return this.http.get<any>(
        this.baseApiUrl + "api/health-proposal-form/" + enquiry_id + "/" + id
      );
    }
  }

  saveInsuredProposalForm(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/health-insure-proposal-form-save",
      JSON.stringify(data)
    );
  }

  saveMedicalHistory(data: any): Observable<any> {
    //sushil
    return this.http.post<any>(
      this.baseApiUrl + "api/health-proposal-medical-history-save",
      JSON.stringify(data)
    );
  }

  getHealthProposalSummary(enquiry_id, proposal_id): Observable<any> {
    //sushil
    return this.http.get(
      this.baseApiUrl +
        "api/health-proposal-form-summary/" +
        enquiry_id +
        "/" +
        proposal_id
    );
  }

  getHelthEnquiryDetails(enquiry_id): Observable<any> {
    return this.http.get(
      this.baseApiUrl + "api/health-enquiery_details/" + enquiry_id
    );
  }

  apolloProfession(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/get-apollo-profession",
      JSON.stringify(data)
    );
  }

  getProposalRelation(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/get-proposal-relation",
      JSON.stringify(data)
    );
  }

  healthQuestions(data: any): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/health-proposal-medical-questions",
      JSON.stringify(data)
    );
  }

  downloadHealthComparePdf(data: any, enquiry_id): Observable<any> {
    const headers = new HttpHeaders({
      Accept: "application/pdf",
    });
    return this.http.post<any>(
      this.baseApiUrl + "api/health-download-pdf/" + enquiry_id,
      JSON.stringify(data),
      {
        observe: "body" as "response",
        headers: headers,
        responseType: "blob" as "json",
      }
      // JSON.stringify(data), { observe: 'body' as 'response', headers: headers}
    );
  }

  downloadBikeComparePdf(data: any, enquiry_id): Observable<any> {
    const headers = new HttpHeaders({
      Accept: "application/pdf",
    });
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-download-pdf/" + enquiry_id,
      JSON.stringify(data),
      {
        headers: headers,
        observe: "body" as "response",
        responseType: "blob" as "json",
      }
      // JSON.stringify(data), { observe: 'body' as 'response', headers: headers}
    );
  }

  downloadCarComparePdf(data: any, enquiry_id): Observable<any> {
    const headers = new HttpHeaders({
      Accept: "application/pdf",
    });
    return this.http.post<any>(
      this.baseApiUrl + "api/car-download-pdf/" + enquiry_id,
      JSON.stringify(data),
      {
        observe: "body" as "response",
        responseType: "blob" as "json",
        headers: headers,
      }
      // JSON.stringify(data), { observe: 'body' as 'response', headers: headers}
    );
  }

  car_payment_request(data): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/car-insurer-proposal-api",
      JSON.stringify(data)
    );
  }

  bike_payment_request(data): Observable<any> {
    return this.http.post<any>(
      this.baseApiUrl + "api/bike-insurer-proposal-api",
      JSON.stringify(data)
    );
  }

  shri_ram_payment_request(data): Observable<any> {
    return this.http.post<any>(environment.shriram, data);
  }
}
