import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('content', { static: false }) supportPopup: ElementRef;

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }
  openSupportModal() {
    this.modalService.open(this.supportPopup);
  }

}
