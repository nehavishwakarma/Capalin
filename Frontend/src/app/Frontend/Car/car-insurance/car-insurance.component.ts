import { Component, NgModule, OnInit } from "@angular/core";
import { CarInsuranceData } from "src/app/car-insurance-data";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { saveAs } from "file-saver";
import { data } from "jquery";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-car-insurance",
  templateUrl: "./car-insurance.component.html",
  styleUrls: ["./car-insurance.component.scss"],
})
export class CarInsuranceComponent implements OnInit {
  baseApiUrl = environment.apiUrl;
  p: number = 1;
  orderby = "company_name";
  reverseFlag = false;
  showCarInsurance: boolean = false;
  itemsPerPage = 10;

  carInsuranceArr: any = [
    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹1000 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹2500 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹5500 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹4600 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹255645600 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹566500 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹200 "
    ),

    new CarInsuranceData(
      1,
      "/assets/home_images/car_module/HDFC-Ergo-logo 1.png",
      "HDFC ERGO",
      " Cover Amount (IDV) : ₹16,435 ",
      " NCB Discount : 25%",
      "1 year ",
      " ₹254573465700 "
    ),
  ];
  radioButtonClicked: any;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService
  ) {}
  carData: any = {};
  totalPrice;
  addOnString = "";
  checked = "";
  checkbox = "";
  invoice_cover: boolean;
  engine_protector: boolean;
  ncb_protection: boolean;
  consumable: boolean;
  key_protector: boolean;
  tyre_cover: boolean;
  paid_driver_legal_liability_cover: boolean;
  pa_to_owner_driver: boolean;
  driver_cover;
  enquiry_id;
  insurerData;
  zero_depreciation;
  insurer;
  thirdPartyPlans;
  quotaSuccess = [];
  quotaSuccess1 = [];
  registerDateCar1;
  policyExpiry;
  antiTheftDevice;
  antiTheftDeviceChecked;
  autoAssoc;
  autoAssocChecked;
  zerDepp;
  rode_side_assistance;
  prev_ncb;

  compare_data = [];
  footerstatus = false;

  ngOnInit() {
    this.radioButtonClicked = true;
    // console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    localStorage.setItem("proposal_id", "0");
    this.enquiry_id = localStorage.getItem("enquiry_id_car");
    this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
      this.Apis(response);
      localStorage.setItem(
        "enquiry_data",
        response.enquiry_data ? JSON.stringify(response.enquiry_data) : ""
      );

      this.carData = response.enquiry_data;
      this.registerDateCar1 = new Date(
        `${this.carData.car_register_date.split("/")[2]}/${
          this.carData.car_register_date.split("/")[1]
        }/${this.carData.car_register_date.split("/")[0]}`
      );
      this.policyExpiry = new Date(
        `${this.carData.previous_year_policy_expirty_date.split("/")[2]}/${
          this.carData.previous_year_policy_expirty_date.split("/")[1]
        }/${this.carData.previous_year_policy_expirty_date.split("/")[0]}`
      );
      this.insurerData = response.insurer_data;
      this.zero_depreciation =
        response.enquiry_data.zero_depreciation == "true" ? true : false;
      // response.enquiry_data.zero_depreciation;
      response.enquiry_data.zero_depreciation == "true"
        ? (this.zerDepp = true)
        : false;
      this.driver_cover =
        response.enquiry_data.driver_cover == "true" ? true : false;
      // response.enquiry_data.driver_cover;

      this.invoice_cover =
        response.enquiry_data.invoice_cover == "true" ? true : false;
      // response.enquiry_data.invoice_cover;
      this.engine_protector =
        response.enquiry_data.engine_protector == "true" ? true : false;
      // response.enquiry_data.engine_protector;
      this.ncb_protection =
        response.enquiry_data.ncb_protection == "true" ? true : false;
      // response.enquiry_data.ncb_protection;
      this.consumable =
        response.enquiry_data.consumable == "true" ? true : false;
      // response.enquiry_data.consumable;
      this.key_protector =
        response.enquiry_data.key_protector == "true" ? true : false;
      // response.enquiry_data.key_protector;
      this.tyre_cover =
        response.enquiry_data.tyre_cover == "true" ? true : false;
      // response.enquiry_data.tyre_cover;
      this.paid_driver_legal_liability_cover =
        response.enquiry_data.paid_driver_legal_liability_cover == "true"
          ? true
          : false;
      // response.enquiry_data.paid_driver_legal_liability_cover;
      this.pa_to_owner_driver =
        response.enquiry_data.pa_to_owner_driver == "true" ? true : false;
      // response.enquiry_data.pa_to_owner_driver;
      this.antiTheftDeviceChecked =
        response.enquiry_data.anti_theft_device == "true" ? true : false;

      this.autoAssocChecked =
        response.enquiry_data.automobile_association == "true" ? true : false;

      this.rode_side_assistance =
        response.enquiry_data.rode_side_assistance == "true" ? true : false;

      localStorage.setItem("car_enquiry_id", this.carData.carEnquiryId);
      localStorage.setItem("car_lead_id", response.enquiry_data.leadId);
    });
  }

  pageChanged(event) {
    console.log(event);
  }
  setChecked(event, whatToSet) {
    console.log(event);
    console.log(event.target.checked);
    if (whatToSet == "antiTheftDeviceChecked") {
      this.antiTheftDeviceChecked = event.target.checked;
    } else if (whatToSet == "autoAssocChecked") {
      this.autoAssocChecked = event.target.checked;
    }
    console.log(this.antiTheftDeviceChecked);
    // if(event.checked == true && whatToSet == 'antiTheftDeviceChecked'){
    //   this.antiTheftDeviceChecked = true;
    // }else if(event.checked == true && whatToSet == 'autoAssocChecked'){
    //   this.autoAssocChecked = true
    // }else if(event.checked == false && whatToSet == 'antiTheftDeviceChecked'){

    // }
    // this.antiTheftDeviceChecked
  }

  Apis(data) {
    for (let index = 0; index < data["insurer_data"].length; index++) {
      let data3 = data;
      data3["enquiryData"] = data["enquiry_data"];
      data3["insurerData"] = data["insurer_data"][index];
      console.log("suidfisudfsuidf");
      console.log(data3);
      // if (data3["insurerData"]["company_id"] == 40) {
      this.dataService.getHDFCApiApi(data3).subscribe((response) => {
        if (response.insurerApiStatus == "Success") {
          console.log("succesFromAPi");
          this.totalPrice =
            response.insurerApiData["finalPremium"] *
            response.insurerApiData["taxRate"];
          this.insurer = response.availablePlans;
          this.thirdPartyPlans = response;
          this.quotaSuccess.push(response);
          this.quotaSuccess1.push(response);

          //calculations to display
          response.insurerApiData.display.displayPremium =
            Math.round(response.insurerApiData.totalPremium) +
            Math.round(response.insurerApiData.display.zero_depreciation) +
            Math.round(response.insurerApiData.display.rode_side_assistance) +
            Math.round(response.insurerApiData.display.invoice_cover) +
            Math.round(response.insurerApiData.display.engine_protector) +
            Math.round(response.insurerApiData.display.ncb_protection) +
            Math.round(response.insurerApiData.display.consumable) +
            Math.round(response.insurerApiData.display.key_protector) +
            Math.round(response.insurerApiData.display.tyre_cover) +
            Math.round(
              response.insurerApiData.display.paid_driver_legal_liability_cover
            ) +
            Math.round(response.insurerApiData.display.pa_to_owner_driver) +
            Math.round(response.insurerApiData.display.passengerCoverPremium) +
            Math.round(
              response.insurerApiData.display.lossOfPersonalBelongingPremium
            ) +
            Math.round(
              response.insurerApiData.display
                .emergencyTransportHotelExpensePremium
            ) -
            Math.round(response.insurerApiData.display.anti_theft_device) -
            Math.round(response.insurerApiData.display.automobile_association);

          response.enquiryData["odPremium"] =
            Math.round(response.insurerApiData.display.displayPremium) -
            Math.round(response.insurerApiData.tpPremium) -
            Math.round(response.insurerApiData.pa_to_owner_driver) -
            Math.round(response.insurerApiData.display.passengerCoverPremium) -
            Math.round(
              response.insurerApiData.display.paid_driver_legal_liability_cover
            );
          console.log(response.insurerApiData);
          console.log(response.enquiryData);
        }
      });
      // }
    }
  }

  editInsurerApi() {
    let value =
      this.carData.electrical_accessories +
      "," +
      this.carData.non_electrical_accessories +
      "," +
      this.carData.external_bi_fuel_kit_value;

    if (
      Number(this.carData.electrical_accessories) >= 50000 ||
      Number(this.carData.non_electrical_accessories) >= 50000 ||
      Number(this.carData.external_bi_fuel_kit_value) >= 50000
    ) {
      alert(
        "Electrical, Non Electrical and BI Fuel Kit each value can not be more than 50000"
      );
      return false;
    }
    const data = { parameter: "additionalCovers", id: value };
    this.dataService
      .editInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        this.carData = response.enquiry_data;
        this.editedApiCalls();
      });
  }

  editedApiCalls() {
    let totalPlans = this.quotaSuccess1.length;
    this.quotaSuccess = [];
    this.quotaSuccess1.forEach((plan, index) => {
      let data3 = {};
      data3["enquiry_data"] = this.carData;
      data3["insurer_data"] = this.quotaSuccess1[index]["availablePlans"];
      data3["enquiryData"] = this.carData;
      data3["insurerData"] = this.quotaSuccess1[index]["availablePlans"];
      console.log(data3);
      this.dataService.getHDFCApiApi(data3).subscribe((response) => {
        if (response.insurerApiStatus == "Success") {
          this.totalPrice =
            response.insurerApiData["finalPremium"] *
            response.insurerApiData["taxRate"];
          this.insurer = response.availablePlans;
          this.thirdPartyPlans = response;
          this.quotaSuccess.push(response);
          //calculations to display
          response.insurerApiData.display.displayPremium =
            Math.round(response.insurerApiData.totalPremium) +
            Math.round(response.insurerApiData.display.zero_depreciation) +
            Math.round(response.insurerApiData.display.rode_side_assistance) +
            Math.round(response.insurerApiData.display.invoice_cover) +
            Math.round(response.insurerApiData.display.engine_protector) +
            Math.round(response.insurerApiData.display.ncb_protection) +
            Math.round(response.insurerApiData.display.consumable) +
            Math.round(response.insurerApiData.display.key_protector) +
            Math.round(response.insurerApiData.display.tyre_cover) +
            Math.round(
              response.insurerApiData.display.paid_driver_legal_liability_cover
            ) +
            Math.round(response.insurerApiData.display.pa_to_owner_driver) +
            Math.round(response.insurerApiData.display.passengerCoverPremium) +
            Math.round(
              response.insurerApiData.display.lossOfPersonalBelongingPremium
            ) +
            Math.round(
              response.insurerApiData.display
                .emergencyTransportHotelExpensePremium
            ) -
            Math.round(response.insurerApiData.display.anti_theft_device) -
            Math.round(response.insurerApiData.display.automobile_association);

          response.enquiryData["odPremium"] =
            Math.round(response.insurerApiData.display.displayPremium) -
            Math.round(response.insurerApiData.tpPremium) -
            Math.round(response.insurerApiData.pa_to_owner_driver) -
            Math.round(response.insurerApiData.display.passengerCoverPremium) -
            Math.round(
              response.insurerApiData.display.paid_driver_legal_liability_cover
            );
        }
      });
    });
  }
  displayCarInsurance() {
    if (this.showCarInsurance == true) {
      this.showCarInsurance = false;
    } else {
      this.showCarInsurance = true;
    }
  }

  setOrderBy(orderVal) {
    this.orderby = orderVal;
    if (orderVal == "premium_with_tax_true") {
      this.reverseFlag = true;
      this.orderby = "premium_with_tax";
    } else {
      this.reverseFlag = false;
    }
  }
  redirectToEdit() {
    this.router.navigate(["/car"]);
  }

  addOn(bool, addOn) {
    if (bool) {
      var value = "true";
    } else {
      var value = "false";
    }

    this.editCarQuotes(addOn, value);
    let availablePlans = this.quotaSuccess.length;
    // alert(availablePlans);
    this.quotaSuccess.forEach((plan, pindex) => {
      this.hdfcErgoAddOns(value, addOn, pindex);
    });
  }

  hdfcErgoAddOns = function (bool, addOn, index) {
    if (bool == "true") {
      this.quotaSuccess[index].insurerApiData.display[addOn] =
        this.quotaSuccess[index].insurerApiData[addOn];
    } else {
      this.quotaSuccess[index].insurerApiData.display[addOn] = "0";
    }
    this.calculateDisplayPremium(index);
  };

  calculateDisplayPremium(index) {
    this.quotaSuccess[index].insurerApiData.display.displayPremium =
      Math.round(this.quotaSuccess[index].insurerApiData.totalPremium) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.zero_depreciation
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.rode_side_assistance
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.invoice_cover
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.engine_protector
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.ncb_protection
      ) +
      Math.round(this.quotaSuccess[index].insurerApiData.display.consumable) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.key_protector
      ) +
      Math.round(this.quotaSuccess[index].insurerApiData.display.tyre_cover) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display
          .paid_driver_legal_liability_cover
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.pa_to_owner_driver
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.passengerCoverPremium
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display
          .lossOfPersonalBelongingPremium
      ) +
      Math.round(
        this.quotaSuccess[index].insurerApiData.display
          .emergencyTransportHotelExpensePremium
      ) -
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.anti_theft_device
      ) -
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.automobile_association
      );
    console.log(`INNNNNNNNNNNNNN`);
    console.log(this.quotaSuccess[index].insurerApiData.display.displayPremium);
    console.log(
      +this.quotaSuccess[index].insurerApiData.display.displayPremium * 1.18
    );

    this.quotaSuccess[index].enquiryData["odPremium"] =
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.displayPremium
      ) -
      Math.round(this.quotaSuccess[index].insurerApiData.tpPremium) -
      Math.round(this.quotaSuccess[index].insurerApiData.pa_to_owner_driver) -
      Math.round(
        this.quotaSuccess[index].insurerApiData.display.passengerCoverPremium
      ) -
      Math.round(
        this.quotaSuccess[index].insurerApiData.display
          .paid_driver_legal_liability_cover
      );
    console.log(this.quotaSuccess[index].enquiryData["odPremium"]);
  }

  editCarQuotes(addOn, value) {
    const data = { parameter: addOn, id: value };
    this.dataService
      .editCarQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        // console.log(response);
      });
  }

  RecommendedAddOns(para, id) {
    if (id) {
      id = "true";
    } else {
      id = "false";
    }
    const data = { parameter: para, id: id };
    console.log(data);
    this.dataService
      .editCarQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.carData = response.enquiry_data;
      });
    this.quotaSuccess.forEach((plan, pindex) => {
      this.hdfcErgoAddOns(id, para, pindex);
    });
  }

  editDiscountsApi() {
    if (this.autoAssocChecked == true) {
      this.carData.automobile_association = "true";
    } else {
      this.carData.automobile_association = "false";
    }
    if (this.antiTheftDeviceChecked == true) {
      this.carData.anti_theft_device = "true";
    } else {
      this.carData.anti_theft_device = "false";
    }
    console.log(this.antiTheftDeviceChecked);
    console.log(this.carData.anti_theft_device);
    // return false;
    let value =
      this.carData.voluntary_deductible_value +
      "," +
      this.carData.anti_theft_device +
      "," +
      this.carData.automobile_association;
    const data = { parameter: "discounts", id: value };
    this.dataService
      .editInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.carData = response.enquiry_data;
        this.editedApiCalls();
      });
  }

  bookNowHDFC(
    leadId,
    mastersheetId,
    enquiry_id,
    idv,
    premium,
    transactionId_1,
    transactionId_2,
    odPremium,
    renewal_type,
    pa_to_owner_driver_premium,
    finalPremium,
    tpPremium,
    quota
  ) {
    localStorage.setItem("master_sheet_id", mastersheetId);

    const dataArray = [
      {
        leadId: leadId,
        mastersheetId: mastersheetId,
        enquiry_id: enquiry_id,
        idv: idv,
        premium: premium,
        transactionId: transactionId_2,
        odPremium: odPremium,
        pa_to_owner_driver_premium: pa_to_owner_driver_premium,
        finalPremium: finalPremium,
        tpPremium: tpPremium,
      },
    ];

    console.log(dataArray);
    // return false;
    this.dataService.bookNowHDFC(dataArray).subscribe((response) => {
      console.log(response);
      // this.router.navigate(["car/proposal/", this.enquiry_id]);
      // this.router.navigate(["car/CarOwner/", this.enquiry_id]);
      this.router.navigate(["car/CarOwner"], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: quota.availablePlans["company_id"],
        },
      });
    });
  }

  onClickRadioButton(value) {
    this.radioButtonClicked = value;
  }

  onKeydown(event, value: Number) {
    if (event.key === "Enter") {
      console.log({ parameter: "idv", id: value });
      const data = { parameter: "idv", id: value };
      this.dataService
        .editInsurerApi(this.enquiry_id, data)
        .subscribe((response) => {
          console.log(response);
          this.carData = response.enquiry_data;
          this.editedApiCalls();
        });
    }
  }

  onValueChange(parameter, value) {
    console.log({ parameter: parameter, id: value });
    const data = { parameter: parameter, id: value };
    this.dataService
      .editInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.carData = response.enquiry_data;
        this.editedApiCalls();
      });
  }

  checkBoxBottonClick(value, event) {
    if (this.compare_data.includes(value)) {
      this.compare_data.splice(this.compare_data.indexOf(value), 1);
      if (this.compare_data.length == 0) {
        this.footerstatus = false;
      }
      // console.log(this.compare_data.indexOf(value))
    } else {
      if (this.compare_data.length != 5) {
        this.compare_data.push(value);
        this.footerstatus = true;
        console.log(this.compare_data);
      } else {
        event.source.checked = false;
      }
    }
  }
  dataArray = [];
  downloadCarComparePdf(compare_data) {
    this.dataArray = [];
    this.dataArray.push({ selectedPlan: compare_data });
    // this.dataArray = [{"selectedPlan":[{"enquiryData":{"bikeEnquiryId":0,"leadId":623,"biModel":"Pulsar 150 As (150 CC)","biManufacturer":"Bajaj","biRto":"AN-01","biEnquiryId":"1635759851593478","biManufacturerId":8,"biRtoId":1,"policyType":"renewal_enquiry","policyTerm":0,"biModelId":143,"regDay":"15","regMonth":"10","regYear":"2020","registerDate":"15/10/2020","expiryDay":"01","expiryMonth":"11","expiryYear":"2021","prevPolicyStartDate":"02/11/2020","prevPolicyEndDate":"01/11/2021","newPolicyStartDate":"03/11/2021","newPolicyEndDate":"02/11/2022","daysLeftToPolicyExpiry":"+0","manufacturingMonth":"10","manufacturingYear":"2019","manufacturingDate":"01/10/2019","claimStatus":"false","prevNcb":20,"newNcb":"25","idv":0,"zero_depreciation":"false","passenger_cover":"false","paid_driver_legal_liability_cover":"false","rode_side_assistance":"false","consumable":"false","pa_to_owner_driver":"true","invoice_cover":"false","electrical_accessories":0,"non_electrical_accessories":0,"voluntary_deductible_value":0,"anti_theft_device":"false","automobile_association":"false","vehicleAgeInMonths":25,"existingInsurercompanyId":0,"existingInsurerPolicyNo":"","existingCarRegNo":"","royal_sundaram_idv":0},"availablePlans":{"company_id":40,"company_name":"HDFC ERGO General","logo":"public/images/hdfc-ergo.png","master_sheet_id":123,"brochure":"","policy_wordings":"","insurerModelData":[{"id":106,"bike_manufacturer_id":8,"bike_model_id":143,"vehicle_model_code":21163,"manufacturer_code":12,"vehicle_manufacturer":"BAJAJ AUTO.","vehicle_model_name":"PULSAR","no_of_wheels":2,"cubic_capacity":149,"gross_vehicle_weight":100,"seating_capacity":2,"carrying_capacity":1,"vehicle_class_code":37,"body_type_code":1,"vehicle_model_status":"Active","active_flag":"Y","no_of_axle":0,"fuel":"PETROL","segment_type":"TWO WHEELER","variant":"150 SS DTSI UG 4.5","corporate":"NC","status":1,"is_deleted":0},{"id":106,"bike_manufacturer_id":8,"bike_model_id":143,"vehicle_model_code":21163,"manufacturer_code":12,"vehicle_manufacturer":"BAJAJ AUTO.","vehicle_model_name":"PULSAR","no_of_wheels":2,"cubic_capacity":149,"gross_vehicle_weight":100,"seating_capacity":2,"carrying_capacity":1,"vehicle_class_code":37,"body_type_code":1,"vehicle_model_status":"Active","active_flag":"Y","no_of_axle":0,"fuel":"PETROL","segment_type":"TWO WHEELER","variant":"150 SS DTSI UG 4.5","corporate":"NC","status":1,"is_deleted":0},{"id":106,"bike_manufacturer_id":8,"bike_model_id":143,"vehicle_model_code":21163,"manufacturer_code":12,"vehicle_manufacturer":"BAJAJ AUTO.","vehicle_model_name":"PULSAR","no_of_wheels":2,"cubic_capacity":149,"gross_vehicle_weight":100,"seating_capacity":2,"carrying_capacity":1,"vehicle_class_code":37,"body_type_code":1,"vehicle_model_status":"Active","active_flag":"Y","no_of_axle":0,"fuel":"PETROL","segment_type":"TWO WHEELER","variant":"150 SS DTSI UG 4.5","corporate":"NC","status":1,"is_deleted":0}],"insurerRtoData":[{"id":1,"rto_code":"AN-01","rto_description":"AN-01 Port Blair (Andaman District)","status":1,"is_deleted":0,"created":"2020-07-12 17:01:14","modified":"0000-00-00 00:00:00","car_rto_id":1,"company_id":40,"insurer_rto_code":"10828","description":"PORT BLAIR ANDAMAN","state_id":"2","state_name":"ANDAMAN & NICOBAR ISLANDS","zone_code":"B","created_at":"2020-07-12 17:00:30","updated_at":"0000-00-00 00:00:00"},{"id":1,"rto_code":"AN-01","rto_description":"AN-01 Port Blair (Andaman District)","status":1,"is_deleted":0,"created":"2020-07-12 17:01:14","modified":"0000-00-00 00:00:00","car_rto_id":1,"company_id":40,"insurer_rto_code":"10828","description":"PORT BLAIR ANDAMAN","state_id":"2","state_name":"ANDAMAN & NICOBAR ISLANDS","zone_code":"B","created_at":"2020-07-12 17:00:30","updated_at":"0000-00-00 00:00:00"},{"id":1,"rto_code":"AN-01","rto_description":"AN-01 Port Blair (Andaman District)","status":1,"is_deleted":0,"created":"2020-07-12 17:01:14","modified":"0000-00-00 00:00:00","car_rto_id":1,"company_id":40,"insurer_rto_code":"10828","description":"PORT BLAIR ANDAMAN","state_id":"2","state_name":"ANDAMAN & NICOBAR ISLANDS","zone_code":"B","created_at":"2020-07-12 17:00:30","updated_at":"0000-00-00 00:00:00"}],"minIdv":0,"maxIdv":0,"status":"Success"},"insurerApiStatus":"Success","insurerApiData":{"ncbBonusDisplay":"0%","minIdv":"0","maxIdv":"0","idv":["0","0","0"],"odPremium":["0","0","0"],"tpPremium":[752,1504,2256],"pa_to_owner_driver":[375,750,1125],"ncbDiscount":["0","0","0"],"zero_depreciation":["0","0","0"],"passenger_cover":[0,0,0],"paid_driver_legal_liability_cover":["0","0","0"],"tax":[203,406,609],"finalPremium":[1330,2660,3990],"odDiscount":[0,0,0],"tpDiscount":[0,0,0],"otherDiscount":[0,0,0],"rode_side_assistance":[0,0,0],"invoice_cover":[0,0,0],"consumable":[0,0,0],"electrical_accessories":[0,0,0],"non_electrical_accessories":[0,0,0],"voluntary_deductible_value":[0,0,0],"anti_theft_device":[0,0,0],"automobile_association":[0,0,0]},"index":0}]}]
    console.log(this.dataArray);
    this.dataService
      .downloadCarComparePdf(this.dataArray, this.enquiry_id)
      .subscribe(
        (response) => {
          var blob = new Blob([response], { type: "application/pdf" });
          var link = document.createElement("a");
          link.href = window.URL.createObjectURL(blob);
          console.log(window.URL.createObjectURL(blob));
          var date = new Date();
          link.download =
            "L-_Car_Plan_Comparison_" +
            date.getDate() +
            "-" +
            (date.getMonth() + 1) +
            "-" +
            date.getFullYear() +
            ".pdf";
          link.click();
          // saveAs(blob, 'hello')
        },
        (err) => {
          var blob = new Blob([err.error.text], { type: "application/pdf" });
          saveAs(blob, "test");
        }
      );
  }
}
