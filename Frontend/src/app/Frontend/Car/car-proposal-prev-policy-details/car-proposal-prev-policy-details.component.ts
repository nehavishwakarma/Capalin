import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-car-proposal-prev-policy-details",
  templateUrl: "./car-proposal-prev-policy-details.component.html",
  styleUrls: ["./car-proposal-prev-policy-details.component.scss"],
  providers: [DatePipe],
})
export class CarProposalPrevPolicyDetailsComponent implements OnInit {
  active = 1;
  registrationForm: FormGroup;
  model: NgbDateStruct;
  enquiry_id;
  carData: any = {};
  cityDate;
  companyId;
  cityStateName;
  cityStateName1;
  car_owner1;
  communication_address = false;
  LoanProviderData;
  FinancierNameData;
  prePolicyDetails: any;
  company_name_tab;
  proposal_id;
  editData;
  regDate;
  expdate;
  isRenewal;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    // this.companyId = 40;
    this.prePolicyDetails = {
      existing_insurer_name: "",
      expiry_policy_no: "",
      third_party_expiry_date: "",
    };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .getCarPropsalData(this.enquiry_id)
      .subscribe((response) => {
        console.log(response);
        if (response["status"] == "success") {
          this.editData = response["data"][0];
          this.setData(this.editData, 1);
        } else {
          if (localStorage.getItem("proposalFormCar") != null) {
            this.editData = JSON.parse(localStorage.getItem("proposalFormCar"));
            this.setData(this.editData, 2);
          }
        }
      });
    this.dataService
      .getCarProposalForm(this.enquiry_id)
      .subscribe((response) => {
        if (response.status == "Success") {
          this.carData = response.enquiry_details[0];
          this.carData.gst = response.enquiry_details.gst;
          this.regDate = new Date(this.carData["car_register_date"]);
          this.expdate = new Date(
            this.carData["previous_year_policy_expirty_date"]
          );
          if (this.carData["policy_type"] != "new_enquiry") {
            this.isRenewal = 1;
          } else {
            this.isRenewal = 0;
          }
        } else {
        }
      });
    // this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
    //   console.log(response);
    //   this.carData = response.enquiry_data;
    //   this.regDate = new Date(
    //     `${this.carData["regYear"]}-${this.carData["regMonth"]}-${this.carData["regDay"]}`
    //   );
    //   this.expdate = new Date(
    //     `${this.carData["previous_year_policy_expirty_date"].split("/")[2]}-${
    //       this.carData["previous_year_policy_expirty_date"].split("/")[1]
    //     }-${this.carData["previous_year_policy_expirty_date"].split("/")[0]}`
    //   );
    //   if (this.carData["enquiryType"] != "new_enquiry") {
    //     this.isRenewal = 1;
    //   } else {
    //     this.isRenewal = 0;
    //   }
    // });

    const data = { company_id: this.companyId };
    this.dataService.LoanProvider(data).subscribe((response) => {
      console.log(response);
      this.LoanProviderData = response.existing_insurance;
      this.FinancierNameData = response.car_financier;
    });
  }

  setData(editData, isLocalStrorage) {
    console.log(editData);
    this.prePolicyDetails["existing_insurer_name"] =
      editData["existing_insurance"];
    this.prePolicyDetails["expiry_policy_no"] = editData["expiry_policy_no"];
    if (isLocalStrorage == 1) {
      let expPolDate = {
        year: Number(editData["third_party_expiry_date"].split("-")[0]),
        month: Number(editData["third_party_expiry_date"].split("-")[1]),
        day: Number(editData["third_party_expiry_date"].split("-")[2]),
      };
      this.prePolicyDetails["third_party_expiry_date"] = expPolDate;
    } else {
      this.prePolicyDetails["third_party_expiry_date"] =
        editData["third_party_expiry_date"];
    }
  }

  onSubmit() {
    console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    let validCounter = 0;
    if (this.isRenewal == 1) {
      if (
        this.requiredValidation(
          this.prePolicyDetails["existing_insurer_name"] || "",
          "existing_insurance_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          this.prePolicyDetails["expiry_policy_no"] || "",
          "expiry_policy_no_err"
        ) == false
      ) {
        validCounter++;
      }

      if (
        this.requiredValidation(
          this.prePolicyDetails["third_party_expiry_date"] || "",
          "third_party_expiry_date_err"
        ) == false
      ) {
        validCounter++;
      }
    }

    if (validCounter != 0) {
      return false;
    }

    let newData = JSON.parse(localStorage.getItem("proposalFormCar"));
    //set new Data
    if (this.isRenewal == 1) {
      newData["existing_insurer_name"] =
        this.prePolicyDetails["existing_insurer_name"];
      newData["expiry_policy_no"] =
        this.prePolicyDetails["expiry_policy_no"].trim();
      newData["third_party_expiry_date"] =
        this.prePolicyDetails["third_party_expiry_date"];
    } else {
      newData["existing_insurer_name"] = "";
      newData["expiry_policy_no"] = "";
      newData["third_party_expiry_date"] = "";
    }

    if (localStorage.getItem("proposalFormCar") != null) {
      localStorage.removeItem("proposalFormCar");
    }
    console.log(JSON.stringify(newData));
    localStorage.setItem("proposalFormCar", JSON.stringify(newData));
    console.log("in");
    if (localStorage.getItem("proposalFormCar") != null) {
      console.log("set local");
      let modDob = "",
        modDob1 = "";
      modDob += `${newData.dob["year"]}-`;
      if (newData.dob["month"] < 10) {
        modDob += `0${newData.dob["month"]}-`;
      } else {
        modDob += `${newData.dob["month"]}-`;
      }
      if (newData.dob["day"] < 10) {
        modDob += `0${newData.dob["day"]}`;
      } else {
        modDob += `${newData.dob["day"]}`;
      }
      newData.dob = modDob;
      if (this.isRenewal == 1) {
        modDob1 += `${newData.third_party_expiry_date["year"]}-`;
        if (newData.third_party_expiry_date["month"] < 10) {
          modDob1 += `0${newData.third_party_expiry_date["month"]}-`;
        } else {
          modDob1 += `${newData.third_party_expiry_date["month"]}-`;
        }
        if (newData.third_party_expiry_date["day"] < 10) {
          modDob1 += `0${newData.third_party_expiry_date["day"]}`;
        } else {
          modDob1 += `${newData.third_party_expiry_date["day"]}`;
        }
        newData.third_party_expiry_date = modDob1;
      }
      Object.assign(newData, {
        master_sheet_id: Number(localStorage.getItem("master_sheet_id")),
      });
      console.log(newData, "=============================");

      const index = this.LoanProviderData.findIndex(
        (element) => element.insurance_company === newData.existing_insurer_name
      );
      Object.assign(newData, {
        existing_insurance: this.LoanProviderData[index].company_code,
      });
      console.log(newData);
      let finalPolicyDetails = { ...newData };
      finalPolicyDetails.car_register_no =
        finalPolicyDetails.car_register_no.split("-")[2];
      finalPolicyDetails.dob = this.datePipe.transform(
        new Date(finalPolicyDetails.dob),
        "dd-MMM-yyyy"
      );
      finalPolicyDetails.nominee_dob = "10-Jan-2020";
      finalPolicyDetails.nominee_name = "fdd";
      finalPolicyDetails.nominee_relationship = "Other";
      // const params = {
      //   id: "247",
      //   user_id: null,
      //   car_enquiry_id: 1640599667873874,
      //   master_sheet_id: 65,
      //   company_name: "",
      //   p_first_name: "nikhat",
      //   p_last_name: "khan",
      //   dob: "17-Jul-1999",
      //   car_owner: "individual",
      //   p_aadhar: "",
      //   gst_no: "",
      //   gender: "Female",
      //   marital_status: "Single",
      //   email: "nikhat@nimapinfotech.com",
      //   mobile: "8876655443",
      //   nominee_name: "yfgdkugfa",
      //   nominee_dob: "07-Jan-2003",
      //   nominee_relationship: "Other",
      //   address: "ghddhg",
      //   area: "",
      //   area_locality: "vschgsa",
      //   landmark: "ngsghds",
      //   country: 0,
      //   state_id: "MAHARASHTRA",
      //   city_id: "PALGHAR",
      //   pincode_id: "401501",
      //   reg_district_id: "",
      //   reg_city_name: "PALGHAR",
      //   reg_state_name: "MAHARASHTRA",
      //   car_register_no: "PS7645",
      //   enginee_no: "D13558",
      //   chassis_no: "998877",
      //   expiry_policy_no: "564656",
      //   existing_insurance: "21",
      //   third_party_expiry_date: "30-Nov-0002",
      //   is_car_finance: "false",
      //   loan_provide_financier: "",
      //   loan_provide_financier_name: "",
      //   loan_city: "",
      //   loan_start_date: "",
      //   idv: "",
      //   buy_id: "",
      //   add_communication_address: "false",
      //   communication_address: "ghddhg",
      //   communication_area_locality: "vschgsa",
      //   communication_landmark: "ngsghds",
      //   communication_city: "PALGHAR",
      //   communication_district: "",
      //   communication_state: "MAHARASHTRA",
      //   communication_pincode: "401501",
      //   communication_city_name: "PALGHAR",
      //   communication_state_name: "MAHARASHTRA",
      //   existing_insurer_name: "BHARTI AXA GENERAL INSURANCE CO LTD",
      //   inspection_area: "",
      //   payment_mode: "",
      //   diff_amount: 0,
      //   proposal_no: "1219122268",
      //   car_proposal_request: "",
      //   car_proposal_response: "",
      //   insurance_status: 1,
      //   payment_status: 0,
      //   policy_no: "",
      //   policy_status: 0,
      //   inspection_no: "",
      //   quote_no: "",
      //   receipt_no: "",
      //   proposal_premium: 0,
      //   txn_id: "",
      //   contact_id: "",
      //   proposal_timestamp: "",
      //   proposal_response: "{\"status\":\"Success\",\"proposalArray\":{\"missuePolicyResponse\":{\"pRcptList_inout\":[],\"pCustDetails_inout\":{\"typtelephone1\":[],\"typdateOfBirth\":\"17-Jul-1999\",\"typtelephone2\":[],\"typstatus1\":[],\"typinstitutionName\":[],\"typprofession\":[],\"typexistingYn\":[],\"typaddLine3\":\"ngsghds\",\"typsurname\":\" khan\",\"typaddLine2\":\"vschgsa\",\"typpartId\":[],\"typaddLine1\":\"ghddhg\",\"typpassword\":[],\"typtitle\":[],\"typdelivaryOption\":[],\"typfirstName\":\"nikhat\",\"typmiddleName\":[],\"typmobileAlerts\":[],\"typloggedIn\":[],\"typpartTempId\":[],\"typavailableTime\":[],\"typpincode\":\"401501\",\"typpolAddLine1\":\"ghddhg\",\"typpolAddLine3\":\"ngsghds\",\"typpolAddLine2\":\"vschgsa\",\"typaddLine5\":\"PALGHAR MAHARASHTRA\",\"typpolAddLine5\":\"PALGHAR MAHARASHTRA\",\"typpolPincode\":\"401501\",\"typcpType\":\"P\",\"typemail\":\"nikhat@nimapinfotech.com\",\"typstatus3\":[],\"typstatus2\":[],\"typemailAlerts\":[],\"typmobile\":\"8876655443\"},\"pWeoMotPolicyIn_inout\":{\"typvehicleTypeCode\":\"22\",\"typvehicleSubtype\":\"NEW LXI (CC- 1197)\",\"typelecAccTotal\":\"0\",\"typaddLoadingOn\":[],\"typnonElecAccTotal\":\"0\",\"typpolType\":\"3\",\"typregistrationNo\":\"MH02PS7645\",\"typpartnerType\":\"P\",\"typncb\":\"0\",\"typprvClaimStatus\":\"1\",\"typregistrationDate\":\"18-AUG-2018\",\"typmiscVehType\":\"0\",\"typfuel\":\"P\",\"typchassisNo\":\"998877\",\"typcontractId\":[],\"typyearManf\":\"2017\",\"typspDiscRate\":\"0\",\"typcubicCapacity\":\"1197\",\"typregiLocOther\":\"MUMBAI(WEST)\",\"typaddLoading\":[],\"typvehicleSubtypeCode\":\"30\",\"typprvPolicyRef\":\"564656\",\"typvehicleMake\":\"MARUTI\",\"typautoMembership\":[],\"typtermStartDate\":\"28-DEC-2021\",\"typcarryingCapacity\":\"5\",\"typtermEndDate\":\"27-DEC-2022\",\"typhypo\":[],\"typbranchCode\":\"1919\",\"typprvNcb\":\"0\",\"typvehicleMakeCode\":\"110\",\"typvehicleIdv\":\"260336\",\"typprvInsCompany\":\"21\",\"typengineNo\":\"D13558\",\"typproduct4digitCode\":\"1801\",\"typcolor\":[],\"typvehicleModel\":\"SWIFT\",\"typvehicleType\":\"Private Car\",\"typprvExpiryDate\":\"27-Dec-2021\",\"typtpFinType\":[],\"typdeptCode\":\"18\",\"typvehicleModelCode\":\"29\",\"typregistrationLocation\":\"MUMBAI(WEST)\",\"typzone\":\"B\"},\"motExtraCover_inout\":{\"typsideCarValue\":[],\"typcovernoteNo\":[],\"typnoOfTrailers\":[],\"typsubImdcode\":[],\"typcovernoteDate\":[],\"typcngValue\":\"0\",\"typfibreGlassValue\":[],\"typvoluntaryExcess\":\"0\",\"typtotalTrailerValue\":[],\"typextraField1\":[],\"typsumInsuredTotalNamedPa\":[],\"typgeogExtn\":[],\"typnoOfPersonsLlo\":[],\"typnoOfEmployeesLle\":[],\"typextraField2\":[],\"typextraField3\":\"1219122268\",\"typsumInsuredPa\":[],\"typnoOfPersonsPa\":\"5\"},\"ppolicyref_out\":[],\"ppolicyissuedate_out\":[],\"ppartId_out\":[],\"pError_out\":[],\"pErrorCode_out\":\"0\"}}}",
      //   account_timestamp: "",
      //   payment_getway_response: "",
      //   created: "2021-12-27 16:58:18",
      //   modified: "0000-00-00 00:00:00",
      //   status: 1,
      //   is_deleted: 0
      // }
      this.dataService.proposalFormSave(newData).subscribe((response) => {
        this.proposal_id = response["id"];
        this.router.navigate(["car/summary"], {
          queryParams: {
            id: this.enquiry_id,
            propId: this.proposal_id,
            compId: this.companyId,
          },
        });
      });
    }
  }

  goback() {
    this.router.navigate(["car/CarDetails"], {
      queryParams: {
        id: this.enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }
  requiredValidation(element, errorId) {
    // console.log(errorId);
    // console.log(document.getElementById(errorId));
    // return false;
    if (errorId != "third_party_expiry_date_err") {
      element = element.trim();
    }
    console.log(element);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
