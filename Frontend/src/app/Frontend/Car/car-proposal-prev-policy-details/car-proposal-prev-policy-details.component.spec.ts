import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarProposalPrevPolicyDetailsComponent } from './car-proposal-prev-policy-details.component';

describe('CarProposalPrevPolicyDetailsComponent', () => {
  let component: CarProposalPrevPolicyDetailsComponent;
  let fixture: ComponentFixture<CarProposalPrevPolicyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarProposalPrevPolicyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarProposalPrevPolicyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
