import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from "@angular/forms";
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../data.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-car-proposal',
  templateUrl: './car-proposal.component.html',
  styleUrls: ['./car-proposal.component.scss'] 
})
export class CarProposalComponent implements OnInit {

  // @ViewChild('email', { static: false }) emailPopup: ElementRef;
  active = 1;
  registrationForm: FormGroup;
  model: NgbDateStruct;
  enquiry_id; 
  carData;
  cityDate;
  companyId;
  cityStateName;
  cityStateName1;
  car_owner1;
  communication_address = false;
  LoanProviderData;
  FinancierNameData;
  formData:any;
  company_name_tab;
  constructor(private router: Router,private dataService: DataService, private fb: FormBuilder,private formBuilder: FormBuilder) { }

  get f() { return this.registrationForm.controls; }
 
ngOnInit() {
  this.companyId =40;
    this.formData =  {
      id:'',
      car_owner: 'individual',
      p_first_name:'',
      p_last_name:'',
      email:'', 
      gst_no:'',
      gender:'0',
      nominee_name:'',
      marital_status:'Single',
      dob:'', 
      nominee_dob:'', 
      nominee_relationship:'', 
      address:'',
      mobile:'',
      area_locality:'',
      landmark:'',
      pincode_id:'',  
      add_communication_address:'',
      car_register_no:'',
      expiry_policy_no:'',
      enginee_no:'',
      existing_insurance:'',
      chassis_no:'',
      is_car_finance:'',
      // email_proposal:'',
      loan_start_date:'',
      communication_address  :'',      
      communication_area_locality  :'',   
      communication_landmark  :'',    
      communication_pincode  :'',   
      car_enquiry_id:'',
      master_sheet_id: ''
    };

     
    this.enquiry_id = localStorage.getItem('enquiry_id'); 
    this.dataService.getInsurerApi(this.enquiry_id ).subscribe(response => { 
      this.carData = response.enquiry_data ; 
    })  

    const data=     {company_id: this.companyId };
    this.dataService.LoanProvider(data ).subscribe(response => { 
      this.LoanProviderData = response.existing_insurance ; 
      this.FinancierNameData = response.car_financier ; 
      
    })

  } 
  submitform()  {   
    this.formData.car_enquiry_id = this.enquiry_id;         //added by sushil
    this.formData.add_communication_address = this.communication_address;   //added by sushil
    this.formData.dob=this.formData.dob.year+ '-'+this.formData.dob.month+ '-'+this.formData.dob.day;
    // this.formData.nominee_dob=this.formData.nominee_dob.year+ '-'+this.formData.nominee_dob.month+ '-'+this.formData.nominee_dob.day;
    this.formData.nominee_dob = '2020-12-10';
    this.formData.nominee_name = 'sDGSHDGDS';
    this.formData.nominee_relationship = 'other';
    this.formData.master_sheet_id = localStorage.getItem('master_sheet_id')
    console.log(this.formData);
    
    this.dataService.proposalFormSave(this.formData ).subscribe(response => { 
   console.log(response); 
   this.dataService.getInsurerApi(localStorage.getItem('car_lead_id')).subscribe();

    this.router.navigate(['car/summary']); 
    }) 
    } 
    getCity(event) {    
         
        if(event.target.value.length == 6){
          this.dataService.getCity(event.target.value ,this.companyId).subscribe(response => { 
            console.log(response); 
            this.cityStateName=response;
            }) 
        }
    } 
    getCity1(event) {  
         
      if(event.target.value.length == 6){
        this.dataService.getCity(event.target.value ,this.companyId).subscribe(response => { 
          console.log(response); 
          this.cityStateName1=response;
          }) 
      }
    } 
    changeCity(data){ 
      let myArray =  data.split(',');
       this.formData.city_id= myArray[0];
       this.formData.reg_city_name=myArray[1];
       this.formData.reg_district_id=myArray[2]; 
       this.formData.state_id=myArray[3];
       this.formData.reg_state_name=myArray[4];
       
      if(this.communication_address ==false){                                                       
          this.formData.communication_city=myArray[0];
          this.formData.communication_city_name=myArray[1];
          this.formData.communication_district=myArray[2]; 
          this.formData.communication_state=myArray[3];
          this.formData.communication_state_name=myArray[4]; 
      }
      console.log(myArray);
    }

    changeCity1(data){ 
      let myArray =  data.split(',');                                                            
        this.formData.communication_city=myArray[0];
        this.formData.communication_city_name=myArray[1];
        this.formData.communication_district=myArray[2]; 
        this.formData.communication_state=myArray[3];
        this.formData.communication_state_name=myArray[4];
      
      console.log(myArray);
    }

    CommunicationAddress(id){
      console.log(id)
      this.communication_address=id;
    }

    company_name_btn(id){ 
      this.company_name_tab=id;
    }
}
