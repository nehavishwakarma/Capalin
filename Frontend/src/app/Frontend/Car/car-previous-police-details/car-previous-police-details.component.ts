import { Component, OnInit } from "@angular/core";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-car-previous-police-details",
  templateUrl: "./car-previous-police-details.component.html",
  styleUrls: ["./car-previous-police-details.component.scss"],
})
export class CarPreviousPoliceDetailsComponent implements OnInit {
  formData;
  ncbValues = [0, 20, 25, 35, 45, 50];
  prePolicyDetailsForm: FormGroup;
  prePolicyDetails = {
    expiryDate: "",
    ncb: "",
  };
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder
  ) {}

  get f() {
    return this.prePolicyDetailsForm.controls;
  }

  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    this.prePolicyDetailsForm = this.fb.group({
      expiryDate: [""],
      ncb: [""],
      claimStatus: [""],
    });

    console.log(JSON.parse(localStorage.getItem("registerFormCar")));
    this.formData = JSON.parse(localStorage.getItem("registerFormCar"));

    if (this.formData["policy_type"] == "renew_enquiry") {
      this.prePolicyDetailsForm
        .get("expiryDate")
        .setValidators([Validators.required]);
      this.prePolicyDetailsForm.get("ncb").setValidators([Validators.required]);
      this.prePolicyDetailsForm
        .get("claimStatus")
        .setValidators([Validators.required]);
      this.prePolicyDetailsForm.controls["expiryDate"].setValue(
        this.formData["expiryDate"]
      );
      this.prePolicyDetailsForm.controls["claimStatus"].setValue(
        this.formData["claimStatus"]
      );
      this.prePolicyDetailsForm.controls["ncb"].setValue(
        this.formData["last_policy_ncb"]
      );
    } else {
      this.prePolicyDetailsForm.get("expiryDate").clearValidators();
      this.prePolicyDetailsForm.get("ncb").clearValidators();
      this.prePolicyDetailsForm.get("claimStatus").clearValidators();
      this.formData["expiryDate"] = "";
      this.formData["last_policy_ncb"] = "";
      this.formData["claimStatus"] = "";
    }
  }

  submitEnqiry() {
    this.formData["last_policy_ncb"] = this.prePolicyDetailsForm.value["ncb"];
    this.formData["expiryDate"] = this.prePolicyDetailsForm.value["expiryDate"];
    this.formData["claimStatus"] =
      this.prePolicyDetailsForm.value["claimStatus"];
    let validCounter = 0;
    if (this.formData["policy_type"] == "renew_enquiry") {
      if (
        this.requiredValidation(
          this.prePolicyDetailsForm.controls["expiryDate"].value,
          "expiryDate_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          this.prePolicyDetailsForm.controls["claimStatus"].value,
          "claimStatus_err"
        ) == false
      ) {
        validCounter++;
      }

      if(this.prePolicyDetailsForm.controls["claimStatus"].value == "no" )
      {

        if (
          this.requiredValidation(
            this.prePolicyDetailsForm.controls["claimStatus"].value == "no" &&
              this.prePolicyDetailsForm.controls["ncb"].value,
            "ncb_err"
          ) == false
        ) {
          validCounter++;
        }
      }
      
      
    }
    if (validCounter !== 0) {
      return false;
    }
    localStorage.removeItem("registerFormCar"); 
    localStorage.setItem("registerFormCar", JSON.stringify(this.formData));
    this.dataService.getCarStoreJson(this.formData).subscribe((response) => {
      if (response.status == "Success") {
        localStorage.setItem("enquiry_id_car", response.enquiry_id);
        this.router.navigate(["car/carInsurance"]);
        // this.router.navigate(["car/quotes/", response.enquiry_id]);
      } else if (response.status == "Error") {
        alert(response.ErrorMsg);
      } else {
        alert("Something Went Wrong!!");
      }
    });
  }

  requiredValidation(element, errorId) {
    if (errorId != "expiryDate_err") {
      element = element.trim();
    }
    console.log(element);
    console.log(errorId);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
