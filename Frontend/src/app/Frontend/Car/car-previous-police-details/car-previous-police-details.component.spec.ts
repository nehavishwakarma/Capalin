import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarPreviousPoliceDetailsComponent } from './car-previous-police-details.component';

describe('CarPreviousPoliceDetailsComponent', () => {
  let component: CarPreviousPoliceDetailsComponent;
  let fixture: ComponentFixture<CarPreviousPoliceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarPreviousPoliceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarPreviousPoliceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
