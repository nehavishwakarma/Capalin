import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
// import { timeStamp } from "console";

@Component({
  selector: "app-car-owner",
  templateUrl: "./car-owner.component.html",
  styleUrls: ["./car-owner.component.scss"],
})
export class CarOwnerComponent implements OnInit {
  active = 1;
  registrationForm: FormGroup;
  model: NgbDateStruct;
  enquiry_id;
  carData: any = {};
  cityDate;
  companyId;
  cityStateName;
  cityStateName1;
  car_owner1;
  communication_address = false;
  LoanProviderData;
  FinancierNameData;
  formData: any;
  company_name_tab;
  proposal_id;
  editData: any = {};
  regDate;
  expdate;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {}
  get f() {
    return this.formData.controls;
  }
  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    // this.companyId = 40;
    this.formData = {
      id: "",
      car_owner: "",
      p_first_name: "",
      p_last_name: "",
      email: "",
      gst_no: "",
      gender: "",
      nominee_name: "",
      marital_status: "",
      dob: "",
      nominee_dob: "",
      nominee_relationship: "",
      address: "",
      mobile: "",
      area_locality: "",
      landmark: "",
      pincode_id: "",
      add_communication_address: "",
      car_register_no: "",
      expiry_policy_no: "",
      enginee_no: "",
      existing_insurance: "",
      chassis_no: "",
      is_car_finance: "",
      // email_proposal:'',
      loan_start_date: "",
      communication_address: "",
      communication_area_locality: "",
      communication_landmark: "",
      communication_pincode: "",
      car_enquiry_id: "",
      company_name: "",
      third_party_expiry_date: "",
    };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .getCarPropsalData(this.enquiry_id)
      .subscribe((response) => {
        console.log(response);
        if (response["status"] == "success") {
          this.editData = response["data"][0];
        } else {
          if (localStorage.getItem("proposalFormCar") != null) {
            this.editData = JSON.parse(localStorage.getItem("proposalFormCar"));
          }
        }
        this.setData(this.editData);
      });

    // this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
    //   console.log(response);
    //   this.carData = response.enquiry_data;
    //   this.regDate = new Date(
    //     `${this.carData["regYear"]}-${this.carData["regMonth"]}-${this.carData["regDay"]}`
    //   );
    //   this.expdate = new Date(
    //     `${this.carData["previous_year_policy_expirty_date"].split("/")[2]}-${
    //       this.carData["previous_year_policy_expirty_date"].split("/")[1]
    //     }-${this.carData["previous_year_policy_expirty_date"].split("/")[0]}`
    //   );
    // });

    this.dataService
      .getCarProposalForm(this.enquiry_id)
      .subscribe((response) => {
        if (response.status == "Success") {
          this.carData = response.enquiry_details[0];

          this.carData.gst = response.enquiry_details.gst;
          this.regDate = new Date(this.carData["car_register_date"]);
          this.expdate = new Date(
            this.carData["previous_year_policy_expirty_date"]
          );
        }
      });

    // const data = { company_id: this.companyId };
    // this.dataService.LoanProvider(data).subscribe((response) => {
    //   console.log(response);
    //   this.LoanProviderData = response.existing_insurance;
    //   this.FinancierNameData = response.car_financier;
    // });
  }

  setData(editData: any) {
    this.formData["id"] = editData["id"];
    this.formData["car_owner"] = editData["car_owner"];
    this.formData["company_name"] = editData["company_name"];
    this.formData["p_first_name"] = editData["p_first_name"];
    this.formData["p_last_name"] = editData["p_last_name"];
    this.formData["email"] = editData["email"];
    this.formData["gst_no"] = editData["gst_no"];
    this.formData["car_enquiry_id"] = editData["car_enquiry_id"];
    this.formData["mobile"] = editData["mobile"];

    this.formData["gender"] = editData["gender"];
    this.formData["marital_status"] = editData["marital_status"];
    if (typeof editData["dob"] == "string") {
      editData["dob"] = {
        year: editData["dob"].split("-")[0],
        month: editData["dob"].split("-")[1],
        day: editData["dob"].split("-")[2],
      };
    }
    this.formData["dob"] = editData["dob"];
    this.formData["address"] = editData["address"];
    this.formData["area_locality"] = editData["area_locality"];
    this.formData["landmark"] = editData["landmark"];
    this.formData["pincode_id"] = editData["pincode_id"];
    this.formData["car_register_no"] = editData["car_register_no"];
    this.formData["expiry_policy_no"] = editData["expiry_policy_no"];
    this.formData["enginee_no"] = editData["enginee_no"];
    this.formData["existing_insurance"] = editData["existing_insurance"];
    this.formData["chassis_no"] = editData["chassis_no"];
    this.formData["is_car_finance"] = editData["is_car_finance"];
    this.formData["car_enquiry_id"] = editData["car_enquiry_id"];
    this.formData["company_name"] = editData["company_name"];
    if (typeof editData["third_party_expiry_date"] == "string") {
      editData["third_party_expiry_date"] = {
        year: editData["third_party_expiry_date"].split("-")[0],
        month: editData["third_party_expiry_date"].split("-")[1],
        day: editData["third_party_expiry_date"].split("-")[2],
      };
    }
    this.formData["third_party_expiry_date"] =
      editData["third_party_expiry_date"];
    console.log(editData);
    if (editData["city_id"]) {
      this.formData["city_id"] = editData["city_id"];
      this.formData["reg_city_name"] = editData["reg_city_name"];
      this.formData["reg_district_id"] = editData["reg_district_id"];
      this.formData["state_id"] = editData["state_id"];
      this.formData["reg_state_name"] = editData["reg_state_name"];
    }
  }

  submitform() {
    this.formData.car_enquiry_id = this.enquiry_id; //added by sushil
    //validations
    let validCounter = 0;
    if (
      this.requiredValidation(
        this.formData["car_owner"] || "",
        "car_owner_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.formData["car_owner"] == "company" &&
      this.requiredValidation(
        this.formData["company_name"] || "",
        "company_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.formData["car_owner"] == "company" &&
      this.alphanumericValidation(
        this.formData["company_name"] || "",
        "company_name_err"
      ) == false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(
        this.formData["p_first_name"] || "",
        "p_first_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(
        this.formData["p_first_name"] || "",
        "p_first_name_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["p_last_name"] || "",
        "p_last_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(
        this.formData["p_last_name"] || "",
        "p_last_name_err"
      ) == false
    ) {
      validCounter++;
    }
    // if (this.requiredValidation(this.formData["email"], "email_err") == false) {
    //   validCounter++;
    // } else if (
    //   this.emailValidation(this.formData["email"], "email_err") == false
    // ) {
    //   validCounter++;
    // }
    if (
      this.requiredValidation(this.formData["mobile"] || "", "mobile_err") ==
      false
    ) {
      validCounter++;
    }
    if (
      this.formData["gst_no"] != "" &&
      this.formData["gst_no"] != undefined &&
      this.gstValidation(this.formData["gst_no"] || "", "gst_no_err") == false
    ) {
      if (this.formData["gst_no"] != "") {
        validCounter++;
      }
    }

    if (validCounter != 0) {
      return false;
    }
    console.log(this.formData);
    if (localStorage.getItem("proposalFormCar") != null) {
      localStorage.removeItem("proposalFormCar");
    }
    localStorage.setItem("proposalFormCar", JSON.stringify(this.formData));
    if (localStorage.getItem("proposalFormCar") != null) {
      this.router.navigate(["car/CarPersonalDetail"], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    }
    // this.formData.add_communication_address = this.communication_address;   //added by sushil
    // this.formData.dob=this.formData.dob.year+ '-'+this.formData.dob.month+ '-'+this.formData.dob.day;
    // this.formData.nominee_dob=this.formData.nominee_dob.year+ '-'+this.formData.nominee_dob.month+ '-'+this.formData.nominee_dob.day;

    //   this.dataService.proposalFormSave(this.formData ).subscribe(response => {
    //  console.log(response);
    //  this.dataService.getInsurerApi(localStorage.getItem('car_lead_id')).subscribe();

    //   this.router.navigate(['car/summary']);
    //   })
  }

  goback() {
    this.router.navigate(["car/carInsurance"]);
  }

  requiredValidation(element, errorId) {
    if (errorId != "mobile_err") {
      element = element.trim();
    }
    console.log(element);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  onlyLettersValidation(element, errorId) {
    let pattern = /^[a-zA-Z]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphabets are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphanumericValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphanumerics are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  emailValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML = "Please enter valid email.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  gstValidation(element, errorId) {
    let pattern = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML = "Please enter valid GSTIN.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      // document.getElementById(errorId).innerHTML =
      //   "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  checkmobile(event) {
    let pattern = /^[0-9]*$/;
    let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
    console.log(event.key);
    if (event.key != "Backspace" && event.key != "Tab") {
      if (!pattern.test(event.key)) {
        return false;
      }
    }
  }
}
