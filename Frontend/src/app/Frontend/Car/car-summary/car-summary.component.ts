import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import jspdf from "jspdf";
import html2canvas from "html2canvas";
import { environment } from "src/environments/environment";
@Component({
  selector: "app-car-summary",
  templateUrl: "./car-summary.component.html",
  styleUrls: ["./car-summary.component.scss"],
})
export class CarSummaryComponent implements OnInit {
  @ViewChild("email", { static: false }) emailPopup: ElementRef;

  constructor(
    private dataService: DataService,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }
  enquiry_id;
  carData;
  proposal_id;
  SummaryData;
  car_register_date;
  previous_year_policy_expirty_date;
  policy_start_date;
  newNcb;
  dob;
  companyId;
  quote_no;
  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    // this.enquiry_id = localStorage.getItem('enquiry_id');
    // this.proposal_id = localStorage.getItem('proposal_id');
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .getCityproposalFormSummary(this.proposal_id, this.enquiry_id)
      .subscribe((response) => {
        response["car_proposal_details"]["car_proposal_details"]["gstConfig"] = response["car_proposal_details"]["car_proposal_details"]["gst"];
        this.carData = response["car_enquiry_details"]["enquiry_details"][0];
        this.car_register_date = new Date(this.carData["car_register_date"]);
        this.previous_year_policy_expirty_date = new Date(this.carData["previous_year_policy_expirty_date"]); //from enquiry form value is taken
        this.policy_start_date = new Date(this.carData["previous_year_policy_expirty_date"]).setDate(2);
        this.SummaryData = response["car_proposal_details"]["car_proposal_details"][0];
        this.quote_no = this.SummaryData.quote_no;
        // this.SummaryData.gstConfig =response["car_proposal_details"]["car_proposal_details"]["gst"];
        if (this.SummaryData !== undefined) {
          this.dob = new Date(this.SummaryData.dob);
        }
        this.newNcb = this.NewNcb(this.carData.ncb);
        // this.bike_payment_request()
      });
  }

  shriramPayment() {
    const params = {
      PolicySysID: this.quote_no,
      ReturnURL: 'http://localhost:4200/car/car-payment-response'
    }
    this.dataService.car_payment_request(params).subscribe(res => {
      console.log(res);
    })
  }

  openEmailModal() {
    this.modalService.open(this.emailPopup);
  }


  public NewNcb(value) {
    if (value == '0') return 20;
    if (value == '20') return 25;
    if (value == '25') return 35;
    if (value == '35') return 45;
    if (value == '45') return 50;
    if (value == '50') return 50;
    else return 0
  }

  public convetToPDF() {
    var data = document.getElementById("maindiv");
    window.scrollTo(0, 0);
    html2canvas(data).then((canvas) => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = (canvas.height * imgWidth) / canvas.width + 20;
      // var imgHeight = canvas.height;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jspdf("p", "mm", "a4"); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      pdf.save("Car.pdf"); // Generated PDF
    });
  }

  gotToEdit(pageLevel, url) {
    if (localStorage.getItem("proposalFormCar") !== null) {
      let setAgain = localStorage.getItem("proposalFormCar");
      localStorage.removeItem("proposalFormCar");
      localStorage.setItem("proposalFormCar", setAgain);
    }
    if (pageLevel == 1) {
      if (url == "car") {
        this.router.navigate([`car`]);
      } else {
        this.router.navigate([`car/${url}`]);
      }
    } else if (pageLevel == 2) {
      this.router.navigate([`car/${url}`], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    } else if (pageLevel == 3) {
    }
  }

  car_payment_request() {

    this.dataService.car_payment_request({
      company_id: this.companyId,
      enquiry_id: '1640599667873874',
      proposal_id: Number(this.proposal_id),
      // payment_mode: 'CC'
    }).subscribe(res => {
      console.log(res)
      if (res.status === 'Success') {
        if (this.companyId == 8) {
          const paymentURL = `${environment.bajajAllianz}${this.carData.transaction_id}&Username=${environment.bajajAllianzUsername}&sourceName=WS_MOTOR`
          window.location.assign(paymentURL);
        }
        if (this.companyId == 45) {
          // const paymentURL = `${environment.shriram}`
          // window.location.assign(paymentURL);
          this.shriramPayment();
        }
        if (this.companyId == 53) {
          const paymentURL = `${environment.futurGenerali}`
          window.location.assign(paymentURL);
        }
        if(this.companyId == 43) {
          const paymentString = `$payment_gateway_data['insurerProposalNumber'] . '|1|' . $returnUrl . '|' . 'XMPVF0136141' . '|' . $payment_gateway_data['proposalPremium'] . '|' . '60001464' . '|' . '10134025' . '|' . $payment_gateway_data['fName'] . '|' . $payment_gateway_data['lName'] . '|' . $payment_gateway_data['mobile'] . '|' . $payment_gateway_data['email'] . '|`;
        }
      }
    })
  }
}
