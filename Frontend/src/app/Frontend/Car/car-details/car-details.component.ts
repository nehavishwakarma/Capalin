import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-car-details",
  templateUrl: "./car-details.component.html",
  styleUrls: ["./car-details.component.scss"],
})
export class CarDetailsComponent implements OnInit {
  active = 1;
  registrationForm: FormGroup;
  model: NgbDateStruct;
  enquiry_id;
  carData;
  cityDate;
  companyId;
  cityStateName;
  car_owner1;
  communication_address = false;
  LoanProviderData;
  FinancierNameData;
  carDetails: any;
  company_name_tab;
  proposal_id;
  editData;
  regDate;
  expdate;
  enquiry_data = JSON.parse(localStorage.getItem("enquiry_data"))
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.window.scrollTo(0, 0);
    // this.companyId = 40;
    this.enquiry_data = JSON.parse(localStorage.getItem("enquiry_data"))
    this.carDetails = {
      car_register_no: this.enquiry_data.biRto,
      enginee_no: "",
      chassis_no: "",
      is_car_finance: null, //from backend we are getting string true or false....for by default selection of no
      loan_provide_financier_name: "",
      loan_city: "",
    };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .getCarPropsalData(this.enquiry_id)
      .subscribe((response) => {
        if (response["status"] == "success") {
          this.editData = response["data"][0];
          this.setData(this.editData, 0);
        } else {
          if (localStorage.getItem("proposalFormCar") != null) {
            this.editData = JSON.parse(localStorage.getItem("proposalFormCar"));
            this.setData(this.editData, 1);
          }
        }
      });
    this.dataService
      .getCarProposalForm(this.enquiry_id)
      .subscribe((response) => {
        if (response.status == "Success") {
          this.carData = response.enquiry_details[0];
          this.carData.gst = response.enquiry_details.gst;
          this.regDate = new Date(this.carData["car_register_date"]);
          this.expdate = new Date(
            this.carData["previous_year_policy_expirty_date"]
          );
        }
      });
    // this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
    //   this.carData = response.enquiry_data;
    //   this.regDate = new Date(
    //     `${this.carData["regYear"]}-${this.carData["regMonth"]}-${this.carData["regDay"]}`
    //   );
    //   this.expdate = new Date(
    //     `${this.carData["previous_year_policy_expirty_date"].split("/")[2]}-${
    //       this.carData["previous_year_policy_expirty_date"].split("/")[1]
    //     }-${this.carData["previous_year_policy_expirty_date"].split("/")[0]}`
    //   );
    // });

    const data = { company_id: this.companyId };
    this.dataService.LoanProvider(data).subscribe((response) => {
      this.LoanProviderData = response.existing_insurance;
      this.FinancierNameData = response.car_financier;
    });
  }

  setData(editData, isLocalStrorage) {
    if (editData) {
      // this.getCity(editData["pincode_id"], editData["city_code"]);
      this.carDetails["car_register_no"] = editData["car_register_no"] ? editData["car_register_no"] : this.enquiry_data.biRto;
      this.carDetails["enginee_no"] = editData["enginee_no"];
      this.carDetails["chassis_no"] = editData["chassis_no"];
      this.carDetails["loan_provide_financier_name"] =
        editData["loan_provide_financier_name"];
      this.carDetails["loan_city"] = editData["loan_city"];
      if (isLocalStrorage == 1) {
        this.carDetails["is_car_finance"] = editData["is_car_finance"];
      } else {
        this.carDetails["is_car_finance"] =
          editData["is_car_finance"] == "true" ? "true" : "false";
      }
    }
    // console.log(this.carDetails);
  }

  onSubmit() {
    let validCounter = 0;

    if (
      this.carDetails["car_register_no"] &&
      this.carRegNumberValidation(
        this.carDetails["car_register_no"],
        "car_register_no_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.carDetails["enginee_no"],
        "enginee_no_err"
      ) == false
    ) {
      validCounter++;
    }
    // if (
    //   this.requiredValidation(
    //     this.carDetails["chassis_no"],
    //     "chassis_no_err"
    //   ) == false
    // ) {
    //   validCounter++;
    // }
    if (
      this.requiredValidation(
        this.carDetails["is_car_finance"],
        "is_car_finance_err"
      ) == false
    ) {
      validCounter++;
    }

    if (
      this.carDetails["is_car_finance"] == "true" &&
      this.requiredValidation(
        this.carDetails["loan_provide_financier_name"],
        "loan_provide_financier_name_err"
      ) == false
    ) {
      validCounter++;
    }

    if (validCounter != 0) {
      return false;
    }

    let newData = JSON.parse(localStorage.getItem("proposalFormCar"));
    //set new Data
    newData["car_register_no"] = this.companyId == 8 ? this.carDetails["car_register_no"].trim().split(' ').join('-') : this.carDetails["car_register_no"].trim();
    newData["enginee_no"] = this.carDetails["enginee_no"].trim();
    newData["chassis_no"] = this.carDetails["chassis_no"].trim();
    newData["is_car_finance"] = this.carDetails["is_car_finance"];
    if (this.carDetails["is_car_finance"] == "true") {
      newData["loan_provide_financier_name"] =
        this.carDetails["loan_provide_financier_name"];
      newData["loan_city"] = this.carDetails["loan_city"];
    } else {
      newData["loan_provide_financier_name"] = "";
      newData["loan_city"] = "";
    }

    if (localStorage.getItem("proposalFormCar") != null) {
      localStorage.removeItem("proposalFormCar");
    }
    localStorage.setItem("proposalFormCar", JSON.stringify(newData));
    console.log(localStorage.getItem("proposalFormCar"));
    if (localStorage.getItem("proposalFormCar") !== null) {
      if (localStorage.getItem("proposalFormCar") != null) {
        this.router.navigate(["car/CarProposalPrevPolicyDetails"], {
          queryParams: {
            id: this.enquiry_id,
            propId: 0,
            compId: this.companyId,
          },
        });
      }
    }
  }

  goback() {
    this.router.navigate(["car/CarRegAddress"], {
      queryParams: {
        id: this.enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }

  requiredValidation(element, errorId) {
    // if (errorId != "dob_err") {
    //   element = element.trim();
    // }
    // console.log(element);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  carRegNumberValidation(element, errorId) {
    let pattern = /^[A-Z]{2}[-][0-9]{2}\s[A-Z]{2}[0-9]{4}$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Please Enter Valid Registration Number.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }

  }
}
