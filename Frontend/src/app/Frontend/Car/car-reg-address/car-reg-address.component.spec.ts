import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarRegAddressComponent } from './car-reg-address.component';

describe('CarRegAddressComponent', () => {
  let component: CarRegAddressComponent;
  let fixture: ComponentFixture<CarRegAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarRegAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarRegAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
