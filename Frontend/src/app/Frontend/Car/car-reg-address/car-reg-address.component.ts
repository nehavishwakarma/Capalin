import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-car-reg-address",
  templateUrl: "./car-reg-address.component.html",
  styleUrls: ["./car-reg-address.component.scss"],
})
export class CarRegAddressComponent implements OnInit {
  model: NgbDateStruct;
  enquiry_id;
  carData: any = {};
  stateName = [];
  companyId;
  LoanProviderData;
  FinancierNameData;
  addressForm: any;
  proposal_id;
  editData;
  regDate;
  expdate;
  cityStateName;
  cityOpt;
  stateOpt;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    // this.companyId = 40;
    this.addressForm = {
      address: "",
      area_locality: "",
      landmark: "",
      pincode_id: "",
    };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .getCarPropsalData(this.enquiry_id)
      .subscribe((response) => {
        if (response["status"] == "success") {
          this.editData = response["data"][0];
        } else {
          if (localStorage.getItem("proposalFormCar") != null) {
            this.editData = JSON.parse(localStorage.getItem("proposalFormCar"));
          }
        }
        this.setData(this.editData);
      });
    this.dataService
      .getCarProposalForm(this.enquiry_id)
      .subscribe((response) => {
        if (response.status == "Success") {
          this.carData = response.enquiry_details[0];
          this.carData.gst = response.enquiry_details.gst;
          this.regDate = new Date(this.carData["car_register_date"]);
          this.expdate = new Date(
            this.carData["previous_year_policy_expirty_date"]
          );
        }
      });
    // this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
    //   this.carData = response.enquiry_data;
    //   this.regDate = new Date(
    //     `${this.carData["regYear"]}-${this.carData["regMonth"]}-${this.carData["regDay"]}`
    //   );
    //   this.expdate = new Date(
    //     `${this.carData["previous_year_policy_expirty_date"].split("/")[2]}-${
    //       this.carData["previous_year_policy_expirty_date"].split("/")[1]
    //     }-${this.carData["previous_year_policy_expirty_date"].split("/")[0]}`
    //   );
    // });
  }

  setData(editData) {
    console.log(editData);
    this.getCity(editData["pincode_id"] || "", editData["city_id"]);
    this.addressForm["address"] = editData["address"];
    this.addressForm["area_locality"] = editData["area_locality"];
    this.addressForm["landmark"] = editData["landmark"];
    this.addressForm["pincode_id"] = editData["pincode_id"];
  }

  getCity(event: any, cityCode = -1) {
    if (event.length == 6) {
      console.log(event.length);
      this.dataService.getCity(event, this.companyId).subscribe((response) => {
        this.cityStateName = response;
        this.cityStateName.forEach((element) => {
          if (cityCode == element["city_code"]) {
            this.cityOpt = element;
            this.stateOpt = element;
          }
        });
      });
    } else {
      const val =
        event.target && event.target.value && event.target.value.length
          ? event.target.value.length
          : 0;
      if (val == 6) {
        console.log(event.target.value);
        this.dataService
          .getCity(event.target.value, this.companyId)
          .subscribe((response) => {
            console.log(response);
            this.cityStateName = response;
          });
      }
    }
  }

  changeCity() {
    this.addressForm.city_id = this.cityOpt["city_code"];
    this.addressForm.reg_city_name = this.cityOpt["city_name"];
    this.addressForm.reg_district_id = this.cityOpt["district_code"];
    this.addressForm.state_id = this.cityOpt["state_code"];
    this.addressForm.reg_state_name = this.cityOpt["state_name"];
    this.stateName = [];
    this.stateName.push(this.cityOpt["state_name"]);
  }

  onSubmit() {
    //validations
    let validCounter = 0;

    if (
      this.requiredValidation(this.addressForm["address"], "address_err") ==
      false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(this.addressForm["address"], "address_err") ==
      false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(
        this.addressForm["area_locality"],
        "area_locality_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(
        this.addressForm["area_locality"],
        "area_locality_err"
      ) == false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(this.addressForm["landmark"], "landmark_err") ==
      false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(
        this.addressForm["landmark"],
        "landmark_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.addressForm["pincode_id"],
        "pincode_id_err"
      ) == false
    ) {
      validCounter++;
    }
    if (this.requiredValidation(this.cityOpt, "city_err") == false) {
      validCounter++;
    }

    if (this.requiredValidation(this.stateOpt, "state_err") == false) {
      validCounter++;
    }
    if (validCounter !== 0) {
      return false;
    }

    let newData = JSON.parse(localStorage.getItem("proposalFormCar"));
    //set new Data
    newData["address"] = this.addressForm["area_locality"].trim();
    newData["area_locality"] = this.addressForm["area_locality"].trim();
    newData["landmark"] = this.addressForm["landmark"].trim();
    newData["pincode_id"] = this.addressForm["pincode_id"];
    newData["city_id"] = this.addressForm["city_id"];
    newData["reg_city_name"] = this.addressForm["reg_city_name"];
    newData["reg_district_id"] = this.addressForm["reg_district_id"];
    newData["state_id"] = this.addressForm["state_id"];
    newData["reg_state_name"] = this.addressForm["reg_state_name"];
    console.log(newData);
    if (localStorage.getItem("proposalFormCar") != null) {
      localStorage.removeItem("proposalFormCar");
    }
    localStorage.setItem("proposalFormCar", JSON.stringify(newData));
    if (localStorage.getItem("proposalFormCar") != null) {
      this.router.navigate(["car/CarDetails"], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    }
  }

  goback() {
    this.router.navigate(["car/CarPersonalDetail"], {
      queryParams: {
        id: this.enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }

  requiredValidation(element, errorId) {
    if (
      errorId != "city_err" &&
      errorId != "state_err" &&
      errorId != "pincode_id_err"
    ) {
      element = element.trim();
    }
    console.log(element);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphanumericValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9\s]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphanumerics are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  // checklength(event) {
  //   if (event.target.value.split("").length > 5) {
  //     return false;
  //   }
  //   // let pattern = /^[0-9]*$/;
  //   // let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
  //   // console.log(event.key);
  //   // if (event.key != "Backspace") {
  //   //   if (!pattern.test(event.key)) {
  //   //     return false;
  //   //   }
  //   // }
  // }
}
