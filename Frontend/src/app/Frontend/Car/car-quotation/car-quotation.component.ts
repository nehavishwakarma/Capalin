import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-car-quotation",
  templateUrl: "./car-quotation.component.html",
  styleUrls: ["./car-quotation.component.scss"],
})
export class CarQuotationComponent implements OnInit {
  // @ViewChild('content', { static: false }) addonPopup: ElementRef;
  // @ViewChild('additional', { static: false }) additionalPopup: ElementRef;
  // @ViewChild('discount', { static: false }) discountPopup: ElementRef;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService
  ) {}
  carData: any;
  totalPrice;
  addOnString = "";
  checked = "";
  checkbox = "";
  invoice_cover: boolean;
  engine_protector: boolean;
  ncb_protection: boolean;
  consumable: boolean;
  key_protector: boolean;
  tyre_cover: boolean;
  paid_driver_legal_liability_cover: boolean;
  pa_to_owner_driver: boolean;
  driver_cover;
  enquiry_id;
  insurerData;
  zero_depreciation;
  insurer;
  thirdPartyPlans;

  ngOnInit() {
    localStorage.setItem("proposal_id", "0");
    this.enquiry_id = localStorage.getItem("enquiry_id");
    this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
      this.Apis(response);

      this.carData = response.enquiry_data;
      this.insurerData = response.insurer_data;
      this.zero_depreciation =
        response.enquiry_data.zero_depreciation == "true" ? true : false;

      this.invoice_cover =
        response.enquiry_data.invoice_cover == "true" ? true : false;
      this.engine_protector =
        response.enquiry_data.engine_protector == "true" ? true : false;
      this.ncb_protection =
        response.enquiry_data.ncb_protection == "true" ? true : false;
      this.consumable =
        response.enquiry_data.consumable == "true" ? true : false;
      this.key_protector =
        response.enquiry_data.key_protector == "true" ? true : false;
      this.tyre_cover =
        response.enquiry_data.tyre_cover == "true" ? true : false;
      this.paid_driver_legal_liability_cover =
        response.enquiry_data.paid_driver_legal_liability_cover == "true"
          ? true
          : false;
      this.pa_to_owner_driver =
        response.enquiry_data.pa_to_owner_driver == "true" ? true : false;

      localStorage.setItem("car_enquiry_id", this.carData.carEnquiryId);
      localStorage.setItem("car_lead_id", response.enquiry_data.leadId);
    });
  }

  Apis(data) {
    for (let index = 0; index < data["insurer_data"].length; index++) {
      let data3 = data;
      data3["enquiryData"] = data["enquiry_data"];
      data3["insurerData"] = data["insurer_data"][index];
      if (data3["insurerData"]["company_id"] == 40) {
        this.dataService.getHDFCApiApi(data3).subscribe((response) => {
          if (response.insurerApiStatus == "Success") {
            this.totalPrice =
              response.insurerApiData["finalPremium"] *
              response.insurerApiData["taxRate"];
            this.insurer = response.availablePlans;
            this.thirdPartyPlans = response;
          }
        });
      }
    }
  }

  addOn(bool, addOn) {
    if (bool) {
      var value = "true";
    } else {
      var value = "false";
    }

    this.editCarQuotes(addOn, value);
  }

  editCarQuotes(addOn, value) {
    const data = { parameter: addOn, id: value };
    this.dataService
      .editCarQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
      });
  }

  editInsurerApi() {
    let value =
      this.carData.electrical_accessories +
      "," +
      this.carData.non_electrical_accessories +
      "," +
      this.carData.external_bi_fuel_kit_value;
    const data = { parameter: "additionalCovers", id: value };
    this.dataService
      .editInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.carData = response.enquiry_data;
      });
  }

  editDiscountsApi() {
    let value =
      this.carData.voluntary_deductible_value +
      "," +
      this.carData.anti_theft_device +
      "," +
      this.carData.automobile_association;
    const data = { parameter: "discounts", id: value };
    this.dataService
      .editInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.carData = response.enquiry_data;
      });
  }

  RecommendedAddOns(para, id) {
    const data = { parameter: para, id: id };
    this.dataService
      .editCarQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.carData = response.enquiry_data;
      });
  }

  bookNowHDFC(
    leadId,
    mastersheetId,
    enquiry_id,
    idv,
    premium,
    transactionId_1,
    transactionId_2,
    odPremium,
    renewal_type,
    pa_to_owner_driver_premium
  ) {
    localStorage.setItem("master_sheet_id", mastersheetId);
    const dataArray = [
      {
        leadId: leadId,
        mastersheetId: mastersheetId,
        enquiry_id: enquiry_id,
        idv: idv,
        premium: premium,
        transactionId: transactionId_2,
        odPremium: odPremium,
        pa_to_owner_driver_premium: pa_to_owner_driver_premium,
      },
    ];

    this.dataService.bookNowHDFC(dataArray).subscribe((response) => {
      console.log(response);
      this.router.navigate(["car/proposal/", this.enquiry_id]);
    });
  }
}
