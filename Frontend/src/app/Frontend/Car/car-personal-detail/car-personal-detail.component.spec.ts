import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarPersonalDetailComponent } from './car-personal-detail.component';

describe('CarPersonalDetailComponent', () => {
  let component: CarPersonalDetailComponent;
  let fixture: ComponentFixture<CarPersonalDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarPersonalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarPersonalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
