import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-car-personal-detail",
  templateUrl: "./car-personal-detail.component.html",
  styleUrls: ["./car-personal-detail.component.scss"],
})
export class CarPersonalDetailComponent implements OnInit {
  model: NgbDateStruct;
  enquiry_id;
  carData;
  companyId;
  LoanProviderData;
  FinancierNameData;
  personalForm: any;
  proposal_id;
  editData;
  regDate;
  expdate;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem("proposalFormCar")));
    // this.companyId = 40;
    this.personalForm = {
      gender: "",
      marital_status: "",
      dob: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .getCarPropsalData(this.enquiry_id)
      .subscribe((response) => {
        if (response["status"] == "success") {
          this.editData = response["data"][0];
          this.setData(this.editData, 1);
        } else {
          if (localStorage.getItem("proposalFormCar") != null) {
            this.editData = JSON.parse(localStorage.getItem("proposalFormCar"));
            this.setData(this.editData, 2);
          }
        }
      });
    this.dataService
      .getCarProposalForm(this.enquiry_id)
      .subscribe((response) => {
        if (response.status == "Success") {
          this.carData = response.enquiry_details[0];
          this.carData.gst = response.enquiry_details.gst;
          this.regDate = new Date(this.carData["car_register_date"]);
          this.expdate = new Date(
            this.carData["previous_year_policy_expirty_date"]
          );
        }
      });
    // this.dataService.getInsurerApi(this.enquiry_id).subscribe((response) => {
    //   this.carData = response.enquiry_data;
    //   this.regDate = new Date(
    //     `${this.carData["regYear"]}-${this.carData["regMonth"]}-${this.carData["regDay"]}`
    //   );
    //   this.expdate = new Date(
    //     `${this.carData["previous_year_policy_expirty_date"].split("/")[2]}-${
    //       this.carData["previous_year_policy_expirty_date"].split("/")[1]
    //     }-${this.carData["previous_year_policy_expirty_date"].split("/")[0]}`
    //   );
    // });

    // const data = { company_id: this.companyId };
    // this.dataService.LoanProvider(data).subscribe((response) => {
    //   this.LoanProviderData = response.existing_insurance;
    //   this.FinancierNameData = response.car_financier;
    // });
  }

  setData(editData, isLocalStrorage) {
    this.personalForm["gender"] = editData["gender"];
    this.personalForm["marital_status"] = editData["marital_status"];
    if (isLocalStrorage == 1) {
      let dob = {
        year: Number(editData["dob"].split("-")[0]),
        month: Number(editData["dob"].split("-")[1]),
        day: Number(editData["dob"].split("-")[2]),
      };
      this.personalForm["dob"] = dob;
    } else {
      this.personalForm["dob"] = editData["dob"];
    }
    console.log(this.personalForm);
  }

  onSubmit() {
    let validCounter = 0;
    if (
      this.requiredValidation(this.personalForm["gender"], "gender_err") ==
      false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.personalForm["marital_status"],
        "marital_status_err"
      ) == false
    ) {
      validCounter++;
    }
    if (this.requiredValidation(this.personalForm["dob"], "dob_err") == false) {
      validCounter++;
    }
    if (validCounter != 0) {
      return false;
    }

    let newData = JSON.parse(localStorage.getItem("proposalFormCar"));
    //set new Data
    newData["gender"] = this.personalForm["gender"];
    newData["marital_status"] = this.personalForm["marital_status"];
    newData["dob"] = this.personalForm["dob"];

    if (localStorage.getItem("proposalFormCar") != null) {
      localStorage.removeItem("proposalFormCar");
    }
    localStorage.setItem("proposalFormCar", JSON.stringify(newData));
    if (localStorage.getItem("proposalFormCar") != null) {
      if (localStorage.getItem("proposalFormCar") != null) {
        this.router.navigate(["car/CarRegAddress"], {
          queryParams: {
            id: this.enquiry_id,
            propId: 0,
            compId: this.companyId,
          },
        });
      }
    }
  }

  goback() {
    this.router.navigate(["car/CarOwner"], {
      queryParams: {
        id: this.enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }

  requiredValidation(element, errorId) {
    if (errorId != "dob_err") {
      element = element.trim();
    }
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
