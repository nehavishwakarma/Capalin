import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-car", 
  templateUrl: "./car.component.html",
  styleUrls: ["./car.component.scss"],
})
export class CarComponent implements OnInit {
  showRenewalTab: boolean;

  active = 1;
  isedit = 0;
  registrationForm: FormGroup;
  newForm: FormGroup;
  carModelDetails;
  fuelTypeDetails = [];
  variantTypeDetails = [];
  carFuel;
  carDetails;
  M_Year;
  M_Month;
  MonthYear;
  model: NgbDateStruct;
  enquiryOption: string = "renewal";

  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder
  ) {}

  // convenience getter for easy access to form fields
  get f() {
    return this.registrationForm.controls;
  }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      policy_type: ["renew_enquiry", Validators.required],
      lead_id: [""],
      enquiryType: ["renew_enquiry", Validators.required],
      makeModel: ["", Validators.required],
      fuelType: ["", Validators.required],
      variant: ["", Validators.required],
      rto: [""],
      manufactYear: ["", Validators.required],
      manufactMonth: ["", Validators.required],
      regtDate: ["", Validators.required],
      expiryDate: [""],
      claimStatus: [""],
      last_policy_ncb: [""],
      selectedMonthYear: "",
    });
    this.newForm = this.fb.group({
      policy_type: ["new_enquiry"],
      lead_id: [""],
      enquiryType: ["new_enquiry", Validators.required],
      makeModel: ["", Validators.required],
      fuelType: ["", Validators.required],
      variant: ["", Validators.required],
      rto: ["", Validators.required],
      manufactYear: ["", Validators.required],
      manufactMonth: ["", Validators.required],
      regtDate: ["", Validators.required],
      expiryDate: ["", Validators.required],
      claimStatus: [""],
    });

    this.registrationForm.controls.regtDate.valueChanges.subscribe(res => {
      if(this.registrationForm.controls.regtDate.valid) {
        document.getElementById('regtDate_err').style.display = "none";
      } else {
        document.getElementById('regtDate_err').style.display = "block";
      }
    })

    this.dataService.getCarModelDetails().subscribe((response) => {
      console.log(response);
      this.carModelDetails = response.response;
      this.M_Year = response.M_Year;
      this.M_Month = response.M_Month;
      this.MonthYear = [];
      this.M_Year.forEach((year) => {
        this.M_Month.forEach((month) => {
          this.MonthYear.push(`${month["name"]} ${year}`);
        });
      });
      if (localStorage.getItem("registerFormCar") !== null) {
        let preStoredData = JSON.parse(localStorage.getItem("registerFormCar"));
        this.M_Month.forEach((month) => {
          if (month["id"] == preStoredData["manufactMonth"]) {
            this.M_Year.forEach((year) => {
              if (year == preStoredData["manufactYear"]) {
                this.registrationForm.controls["selectedMonthYear"].setValue(
                  `${month["name"]} ${year}`
                );
              } 
            });
          }
        });
      }
    });

    //set data to edit the form
    if (localStorage.getItem("registerFormCar") !== null) {
      let preStoredData = JSON.parse(localStorage.getItem("registerFormCar"));
      this.getFuelType("car_fuel_type", preStoredData["makeModel"]);
      this.getVariantType("car_variant", preStoredData["fuelType"]);
      this.isedit = 1;
      this.registrationForm.controls["policy_type"].setValue(
        preStoredData["policy_type"]
      );
      this.registrationForm.controls["lead_id"].setValue(
        preStoredData["lead_id"]
      );
      this.registrationForm.controls["enquiryType"].setValue(
        preStoredData["enquiryType"]
      );
      this.registrationForm.controls["makeModel"].setValue(
        preStoredData["makeModel"]
      );
      this.registrationForm.controls["fuelType"].setValue(
        preStoredData["fuelType"]
      );
      this.registrationForm.controls["variant"].setValue(
        preStoredData["variant"]
      );
      this.registrationForm.controls["rto"].setValue(preStoredData["rto"]);
      this.registrationForm.controls["manufactYear"].setValue(
        preStoredData["manufactYear"]
      );
      this.registrationForm.controls["manufactMonth"].setValue(
        preStoredData["manufactMonth"]
      );
      this.registrationForm.controls["regtDate"].setValue(
        preStoredData["regtDate"]
      );
      this.registrationForm.controls["last_policy_ncb"].setValue(
        preStoredData["last_policy_ncb"]
      );
      this.registrationForm.controls["expiryDate"].setValue(
        preStoredData["expiryDate"]
      );
      this.registrationForm.controls["claimStatus"].setValue(
        preStoredData["claimStatus"]
      );
      this.registrationForm.controls["selectedMonthYear"].setValue(
        preStoredData["selectedMonthYear"]
      );
    }
  }

  getFuelType(table, id) {
    this.dataService.getCarFuel(table, id).subscribe((res) => {
      this.fuelTypeDetails = res.response;
    });
  }

  getVariantType(table, id) {
    this.dataService.getCarVariant(table, id).subscribe((response) => {
      console.log(response);
      this.variantTypeDetails = response.response;
    });
  }

  onSubmit() {
    this.registrationForm.controls["enquiryType"].setValue(
      this.registrationForm.value["policy_type"]
    );
    let validCounter = 0;
    if (
      this.requiredValidation(
        this.registrationForm.controls["regtDate"].value,
        "regtDate_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["selectedMonthYear"].value,
        "selectedMonthYear_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["rto"].value,
        "rto_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["variant"].value,
        "variant_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["fuelType"].value,
        "fuelType_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["makeModel"].value,
        "makeModel_err"
      ) == false
    ) {
      validCounter++;
    }

    if (validCounter != 0) {
      return false;
    }
    localStorage.removeItem("registerFormCar");
    localStorage.setItem(
      "registerFormCar",
      JSON.stringify(this.registrationForm.value)
    );
    console.log(this.registrationForm.value);
    if (localStorage.getItem("registerFormCar") !== null) {
      this.router.navigate(["car/carPreviousPoliceDetails"]);
    }
  }

  onSubmit1() {
    console.log();
    this.dataService
      .getCarStoreJson(this.newForm.value)
      .subscribe((response) => {
        console.log(response);
        localStorage.setItem("enquiry_id", response.enquiry_id);
        this.router.navigate(["car/quotes/", response.enquiry_id]);
      });
  }
  //showRenewalTab
  displayTab(data) {
    if (data == "renewal") {
      this.showRenewalTab = true;
    }
    if (data == "new_car") {
      this.showRenewalTab = false;
    }
  }

  setMonthYear(value) {
    let month = value.split(" ")[0];
    let year = value.split(" ")[1];
    this.registrationForm.controls["manufactYear"].setValue(year);
    this.M_Month.forEach((mnth) => {
      if (mnth["name"] == month) {
        this.registrationForm.controls["manufactMonth"].setValue(
          mnth["id"].toString()
        );
      }
    });
  }

  gotoHome() {
    if (localStorage.getItem("registerFormCar") !== null) {
      localStorage.removeItem("registerFormCar");
      this.router.navigate([""]);
    }
  }

  requiredValidation(element, errorId) {
    if (errorId == "selectedMonthYear_err") {
      element = element.trim();
    }
    console.log(element);
    console.log(errorId);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}

// }
