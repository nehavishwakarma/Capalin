import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderLiteComponent } from './header-lite.component';

describe('HeaderLiteComponent', () => {
  let component: HeaderLiteComponent;
  let fixture: ComponentFixture<HeaderLiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderLiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderLiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
