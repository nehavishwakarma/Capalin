import { Component, OnInit } from "@angular/core";
import { BikeInsurance } from "src/app/bike-insurance";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ConditionalExpr } from "@angular/compiler";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { environment } from "src/environments/environment";

@Component({
  selector: "app-bike-insurance",
  templateUrl: "./bike-insurance.component.html", 
  styleUrls: ["./bike-insurance.component.scss"],
})
export class BikeInsuranceComponent implements OnInit {
  baseApiUrl = environment.apiUrl;
  bikeData: any = [];
  totalPrice;
  totalPrice1;
  totalPrice2;
  addOnString = "";
  checked = "";
  checkbox = "";
  invoice_cover: boolean;
  consumable: boolean;
  paid_driver_legal_liability_cover: boolean;
  pa_to_owner_driver: boolean;
  enquiry_id;
  insurerData;
  quotaSuccess = [];
  quotaSuccess1 = [];
  zero_depreciation: boolean;
  passenger_cover: boolean;
  anti_theft_device: boolean;
  automobile_association: boolean;
  insurer;
  thirdPartyPlans;
  rode_side_assistance: boolean;
  itemsPerPage = 10;
  p = 1;
  numOfYears = 1;
  showBikeInsurance: boolean = false;
  expiryDate;
  radioButtonClicked: boolean;
  prev_ncb;
  expiry_date;
  register_date;

  compare_data = [];
  footerstatus = false;

  //   bikeInsuranceArr : any =
  // [
  //   new BikeInsurance(1, '/assets/home_images/HDFC-Ergo-logo 1.png' , 'HDFC ERGO' , ' Cover Amount (IDV) : ₹16,435 ',
  // ' NCB Discount : 25%', '1 year ',' ₹2500 ', '/assets/home_images/rupees.png'),

  // new BikeInsurance(1, '/assets/home_images/HDFC-Ergo-logo 1.png' , 'HDFC ERGO' , ' Cover Amount (IDV) : ₹16,435 ',
  // ' NCB Discount : 25%', '1 year ',' ₹2500 ', '/assets/home_images/rupees.png'),

  // new BikeInsurance(1, '/assets/home_images/HDFC-Ergo-logo 1.png' , 'HDFC ERGO' , ' Cover Amount (IDV) : ₹16,435 ',
  // ' NCB Discount : 25%', '1 year ',' ₹2500 ', '/assets/home_images/rupees.png'),

  // new BikeInsurance(1, '/assets/home_images/HDFC-Ergo-logo 1.png' , 'HDFC ERGO' , ' Cover Amount (IDV) : ₹16,435 ',
  // ' NCB Discount : 25%', '1 year ',' ₹2500 ', '/assets/home_images/rupees.png'),

  // new BikeInsurance(1, '/assets/home_images/HDFC-Ergo-logo 1.png' , 'HDFC ERGO' , ' Cover Amount (IDV) : ₹16,435 ',
  // ' NCB Discount : 25%', '1 year ',' ₹2500 ', '/assets/home_images/rupees.png'),

  // ]

  constructor(private router: Router, private dataService: DataService, private modalService: NgbModal) { }

  ngOnInit() {
    this.radioButtonClicked = true
    localStorage.setItem("proposal_id", "0");
    this.enquiry_id = localStorage.getItem("enquiry_id_bike");
    this.dataService
      .getBikeInsurerApi(this.enquiry_id)
      .subscribe((response) => {
        this.setData(response);
        this.Apis(response);
        this.bikeData = response.enquiry_data;
        this.insurerData = response.insurer_data;
        // console.log(response.insurer_data);
        // this.insurerData.forEach((quota) => {
        //   if (quota["status"] == "Success") {
        //     console.log("suc");
        //     this.quotaSuccess.push(quota);
        //   } else {
        //     console.log(quota["status"] + "****");
        //   }
        // });
        // console.log(this.quotaSuccess);
        // localStorage.setItem("bike_enquiry_id", this.bikeData.bikeEnquiryId);
      });
  }

  setPremium(value) {
    let data = value.split("_");
    let quotaData =
      this.quotaSuccess[data[1]].insurerApiData.finalPremium[+data[0] - 1];
    // plan_{{i}}_premium
    document.getElementById(`plan_${data[1]}_premium`).innerHTML = quotaData;
  }
  setData(response) {
    this.expiryDate = `${response.enquiry_data.expiryDay}-${response.enquiry_data.expiryMonth}-${response.enquiry_data.expiryYear}`;
    this.zero_depreciation =
      response.enquiry_data.zero_depreciation == "true" ? true : false;
    this.passenger_cover =
      response.enquiry_data.passenger_cover == "true" ? true : false;
    this.anti_theft_device =
      response.enquiry_data.anti_theft_device == "true" ? true : false;
    this.automobile_association =
      response.enquiry_data.automobile_association == "true" ? true : false;
    this.paid_driver_legal_liability_cover =
      response.enquiry_data.paid_driver_legal_liability_cover == "true"
        ? true
        : false;
    this.invoice_cover =
      response.enquiry_data.invoice_cover == "true" ? true : false;
    this.rode_side_assistance =
      response.enquiry_data.rode_side_assistance == "true" ? true : false;
    this.consumable = response.enquiry_data.consumable == "true" ? true : false;
    this.pa_to_owner_driver =
      response.enquiry_data.pa_to_owner_driver == "true" ? true : false;
    this.pa_to_owner_driver =
      response.enquiry_data.pa_to_owner_driver == "true" ? true : false;
  }
  displayBikeInsurance() {
    if (this.showBikeInsurance == true) {
      this.showBikeInsurance = false;
    } else {
      this.showBikeInsurance = true;
    }
  }
  Apis(data) {
    for (let index = 0; index < data["insurer_data"].length; index++) {
      let data3 = data;
      data3["enquiryData"] = data["enquiry_data"];
      data3["insurerData"] = data["insurer_data"][index];
      // if (data3["insurerData"]["company_id"] == 40) {
      this.dataService.getBikeHDFCApiApi(data3).subscribe((response) => {
        if (response.insurerApiStatus == "Success") {
          this.totalPrice = response.insurerApiData["finalPremium"][0];
          this.totalPrice1 = response.insurerApiData["finalPremium"][1];
          this.totalPrice2 = response.insurerApiData["finalPremium"][2];
          this.insurer = response.availablePlans;
          this.thirdPartyPlans = response;
          this.quotaSuccess.push(response);
          this.quotaSuccess1.push(response);
          //  *response.insurerApiData["tax"][0]
        }
      });
      // }
    }
    console.log("INNNNNN");
    console.log(this.quotaSuccess);
  }

  addOn(bool, addOn) {
    console.log(bool);
    if (bool) {
      var value = "true";
    } else {
      var value = "false";
    }

    this.editBikeQuotes(addOn, value);
  }

  editBikeQuotes(addOn, value) {
    const data = { parameter: addOn, id: value };
    this.dataService
      .editBikeQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  editBikeInsurerApi() {
    let value =
      this.bikeData.electrical_accessories +
      "," +
      this.bikeData.non_electrical_accessories;
    const data = { parameter: "additionalCovers", id: value };
    console.log(data);
    this.dataService
      .editBikeInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  editDiscountsApi() {
    let value = "";
    value += this.bikeData.voluntary_deductible_value;
    value += ",";

    if (this.anti_theft_device == true) {
      value += "true,";
    } else {
      value += "false,";
    }

    if (this.automobile_association == true) {
      value += "true,";
    } else {
      value += "false,";
    }
    const data = { parameter: "discounts", id: value };
    this.dataService
      .editBikeInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  RecommendedAddOns(para, id) {
    let val = "";
    if (id) {
      val = "true";
    } else {
      val = "false";
    }
    const data = { parameter: para, id: val };
    console.log(data);
    // return false;
    this.dataService
      .editBikeQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  bookNowBikeHDFC(
    leadId,
    mastersheetId,
    enquiry_id,
    idv,
    premium,
    transactionId_1,
    transactionId_2,
    odPremium,
    renewal_type,
    pa_to_owner_driver_premium,
    insurerData,
    index
  ) {
    localStorage.setItem("master_sheet_id", mastersheetId);
    let odPremium_multiyear = [];
    console.log(insurerData);
    let getpremium = +document
      .getElementById(`plan_${index}_premium`)
      .textContent.trim();
    let year = premium.indexOf(getpremium) + 1;
    for (var i = 0; i < 3; i++) {
      odPremium_multiyear[i] =
        parseInt(insurerData.insurerApiData.finalPremium[i]) -
        parseInt(insurerData.insurerApiData.tax[i]) -
        parseInt(insurerData.insurerApiData.tpPremium[i]) -
        parseInt(insurerData.insurerApiData.pa_to_owner_driver[i]) -
        parseInt(insurerData.insurerApiData.passenger_cover[i]) -
        parseInt(
          insurerData.insurerApiData.paid_driver_legal_liability_cover[i]
        );
    }
    const dataArray = [
      {
        leadId: leadId,
        mastersheetId: mastersheetId,
        enquiry_id: enquiry_id,
        idv: idv,
        premium: premium,
        transactionId: transactionId_1,
        odPremium: odPremium,
        pa_to_owner_driver_premium: pa_to_owner_driver_premium,
        premium_multiyear:
          insurerData.insurerApiData.finalPremium[0] +
          "," +
          insurerData.insurerApiData.finalPremium[1] +
          "," +
          insurerData.insurerApiData.finalPremium[2],
        idv_multiyear:
          insurerData.insurerApiData.idv[0] +
          "," +
          insurerData.insurerApiData.idv[1] +
          "," +
          insurerData.insurerApiData.idv[2],
        odPremium_multiyear:
          odPremium_multiyear[0] +
          "," +
          odPremium_multiyear[1] +
          "," +
          odPremium_multiyear[2],
        policy_term: year,
        selectedPremium: getpremium,
        selectedIdv: idv[premium.indexOf(getpremium)],
      },
    ];
    console.log(dataArray);
    this.dataService.bookNowBikeHDFC(dataArray).subscribe((response) => {
      console.log(response);
      // this.router.navigate(["bike/proposal/", this.enquiry_id]);
      this.router.navigate(["bike/proposal"], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: insurerData.availablePlans["company_id"],
        },
      });
    });
  }


  onClickRadioButton(value) {
    this.radioButtonClicked = value
  }

  onKeydown(event, value: Number) {
    if (event.key === "Enter") {
      this.dataService
        .editBikeInsurerApi(this.enquiry_id, { parameter: "idv", id: value })
        .subscribe((response) => {
          this.bikeData = response.enquiry_data;
          this.setData(response);
        });
    }
  }

  onValueChange(parameter, value) {
    console.log({ parameter: parameter, id: value })
    this.dataService
      .editBikeInsurerApi(this.enquiry_id, { parameter: parameter, id: value })
      .subscribe((response) => {
        this.bikeData = response.enquiry_data;
        this.setData(response);
        // console.log(response);
      });
  }


  openScrollableContent(longContent) {
    this.modalService.open(longContent, { scrollable: true });
  }

  checkBoxBottonClick(value, event) {
    if (this.compare_data.includes(value)) {
      this.compare_data.splice(this.compare_data.indexOf(value), 1)
      if(this.compare_data.length == 0)
      {
        this.footerstatus = false;
      }
      // console.log(this.compare_data.indexOf(value))
    }
    else {
      if (this.compare_data.length != 5) {
        this.compare_data.push(value)
        this.footerstatus = true;
        console.log(this.compare_data)
      } else {
        event.source.checked = false;
      }
    }
  }
  dataArray = []
  downloadBikeComparePdf(compare_data) {
    this.dataArray = []
    this.dataArray.push({ selectedPlan: compare_data });
    // this.dataArray = [{"selectedPlan":[{"enquiryData":{"bikeEnquiryId":0,"leadId":623,"biModel":"Pulsar 150 As (150 CC)","biManufacturer":"Bajaj","biRto":"AN-01","biEnquiryId":"1635759851593478","biManufacturerId":8,"biRtoId":1,"policyType":"renewal_enquiry","policyTerm":0,"biModelId":143,"regDay":"15","regMonth":"10","regYear":"2020","registerDate":"15/10/2020","expiryDay":"01","expiryMonth":"11","expiryYear":"2021","prevPolicyStartDate":"02/11/2020","prevPolicyEndDate":"01/11/2021","newPolicyStartDate":"03/11/2021","newPolicyEndDate":"02/11/2022","daysLeftToPolicyExpiry":"+0","manufacturingMonth":"10","manufacturingYear":"2019","manufacturingDate":"01/10/2019","claimStatus":"false","prevNcb":20,"newNcb":"25","idv":0,"zero_depreciation":"false","passenger_cover":"false","paid_driver_legal_liability_cover":"false","rode_side_assistance":"false","consumable":"false","pa_to_owner_driver":"true","invoice_cover":"false","electrical_accessories":0,"non_electrical_accessories":0,"voluntary_deductible_value":0,"anti_theft_device":"false","automobile_association":"false","vehicleAgeInMonths":25,"existingInsurercompanyId":0,"existingInsurerPolicyNo":"","existingCarRegNo":"","royal_sundaram_idv":0},"availablePlans":{"company_id":40,"company_name":"HDFC ERGO General","logo":"public/images/hdfc-ergo.png","master_sheet_id":123,"brochure":"","policy_wordings":"","insurerModelData":[{"id":106,"bike_manufacturer_id":8,"bike_model_id":143,"vehicle_model_code":21163,"manufacturer_code":12,"vehicle_manufacturer":"BAJAJ AUTO.","vehicle_model_name":"PULSAR","no_of_wheels":2,"cubic_capacity":149,"gross_vehicle_weight":100,"seating_capacity":2,"carrying_capacity":1,"vehicle_class_code":37,"body_type_code":1,"vehicle_model_status":"Active","active_flag":"Y","no_of_axle":0,"fuel":"PETROL","segment_type":"TWO WHEELER","variant":"150 SS DTSI UG 4.5","corporate":"NC","status":1,"is_deleted":0},{"id":106,"bike_manufacturer_id":8,"bike_model_id":143,"vehicle_model_code":21163,"manufacturer_code":12,"vehicle_manufacturer":"BAJAJ AUTO.","vehicle_model_name":"PULSAR","no_of_wheels":2,"cubic_capacity":149,"gross_vehicle_weight":100,"seating_capacity":2,"carrying_capacity":1,"vehicle_class_code":37,"body_type_code":1,"vehicle_model_status":"Active","active_flag":"Y","no_of_axle":0,"fuel":"PETROL","segment_type":"TWO WHEELER","variant":"150 SS DTSI UG 4.5","corporate":"NC","status":1,"is_deleted":0},{"id":106,"bike_manufacturer_id":8,"bike_model_id":143,"vehicle_model_code":21163,"manufacturer_code":12,"vehicle_manufacturer":"BAJAJ AUTO.","vehicle_model_name":"PULSAR","no_of_wheels":2,"cubic_capacity":149,"gross_vehicle_weight":100,"seating_capacity":2,"carrying_capacity":1,"vehicle_class_code":37,"body_type_code":1,"vehicle_model_status":"Active","active_flag":"Y","no_of_axle":0,"fuel":"PETROL","segment_type":"TWO WHEELER","variant":"150 SS DTSI UG 4.5","corporate":"NC","status":1,"is_deleted":0}],"insurerRtoData":[{"id":1,"rto_code":"AN-01","rto_description":"AN-01 Port Blair (Andaman District)","status":1,"is_deleted":0,"created":"2020-07-12 17:01:14","modified":"0000-00-00 00:00:00","car_rto_id":1,"company_id":40,"insurer_rto_code":"10828","description":"PORT BLAIR ANDAMAN","state_id":"2","state_name":"ANDAMAN & NICOBAR ISLANDS","zone_code":"B","created_at":"2020-07-12 17:00:30","updated_at":"0000-00-00 00:00:00"},{"id":1,"rto_code":"AN-01","rto_description":"AN-01 Port Blair (Andaman District)","status":1,"is_deleted":0,"created":"2020-07-12 17:01:14","modified":"0000-00-00 00:00:00","car_rto_id":1,"company_id":40,"insurer_rto_code":"10828","description":"PORT BLAIR ANDAMAN","state_id":"2","state_name":"ANDAMAN & NICOBAR ISLANDS","zone_code":"B","created_at":"2020-07-12 17:00:30","updated_at":"0000-00-00 00:00:00"},{"id":1,"rto_code":"AN-01","rto_description":"AN-01 Port Blair (Andaman District)","status":1,"is_deleted":0,"created":"2020-07-12 17:01:14","modified":"0000-00-00 00:00:00","car_rto_id":1,"company_id":40,"insurer_rto_code":"10828","description":"PORT BLAIR ANDAMAN","state_id":"2","state_name":"ANDAMAN & NICOBAR ISLANDS","zone_code":"B","created_at":"2020-07-12 17:00:30","updated_at":"0000-00-00 00:00:00"}],"minIdv":0,"maxIdv":0,"status":"Success"},"insurerApiStatus":"Success","insurerApiData":{"ncbBonusDisplay":"0%","minIdv":"0","maxIdv":"0","idv":["0","0","0"],"odPremium":["0","0","0"],"tpPremium":[752,1504,2256],"pa_to_owner_driver":[375,750,1125],"ncbDiscount":["0","0","0"],"zero_depreciation":["0","0","0"],"passenger_cover":[0,0,0],"paid_driver_legal_liability_cover":["0","0","0"],"tax":[203,406,609],"finalPremium":[1330,2660,3990],"odDiscount":[0,0,0],"tpDiscount":[0,0,0],"otherDiscount":[0,0,0],"rode_side_assistance":[0,0,0],"invoice_cover":[0,0,0],"consumable":[0,0,0],"electrical_accessories":[0,0,0],"non_electrical_accessories":[0,0,0],"voluntary_deductible_value":[0,0,0],"anti_theft_device":[0,0,0],"automobile_association":[0,0,0]},"index":0}]}]
    this.dataService
      .downloadBikeComparePdf(this.dataArray, this.enquiry_id)
      .subscribe((response) => {
        var blob = new Blob([response], { type: 'application/pdf' });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        console.log(window.URL.createObjectURL(blob))
        var date = new Date();
        link.download = 'L-_Bike_Plan_Comparison_' + date.getDate() + '-' + (date.getMonth() + 1) + '-' + 
        date.getFullYear() + '.pdf';
        link.click();
        // saveAs(blob, 'hello')
      }, err => {
        var blob = new Blob([err.error.text], { type: 'application/pdf' });
        saveAs(blob, 'test')
      })
  }
}
