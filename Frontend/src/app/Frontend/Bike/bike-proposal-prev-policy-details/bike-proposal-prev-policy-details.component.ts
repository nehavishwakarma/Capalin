import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-bike-proposal-prev-policy-details",
  templateUrl: "./bike-proposal-prev-policy-details.component.html",
  styleUrls: ["./bike-proposal-prev-policy-details.component.scss"],
})
export class BikeProposalPrevPolicyDetailsComponent implements OnInit {
  companyId;
  cityStateName;
  cityStateName1;
  communication_address = false;
  formData: any;
  bike_enquiry_id;
  bikeData;
  LoanProviderData;
  FinancierNameData;
  proposal_id;
  cityOpt;
  cityOpt1;
  stateOpt;
  stateOpt1;
  isedit = 0;
  gst;
  bikeDataSummary;
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.companyId = 40;
    this.formData = {
      id: "",
      bike_owner: "",
      company_name: "",
      first_name: "",
      last_name: "",
      email: "",
      gst_no: "",
      gender: "",
      nominee_name: "",
      marital_status: "",
      dob: "",
      nominee_dob: "",
      nominee_relationship: "",
      reg_address: "",
      mobile: "",
      reg_locality: "",
      reg_landmark: "",
      reg_pincode: "",
      add_communication_address: "",
      registeration_no: "",
      expiry_policy_no: "",
      enginee_no: "",
      existing_insurance: "",
      existing_insurer_name: "",
      chassis_no: "",
      is_bike_finance: "",
      loan_start_date: "",
      communication_address: "",
      communication_locality: "",
      communication_landmark: "",
      communication_pincode: "",
      bike_enquiry_id: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.bike_enquiry_id = params["id"];
      this.companyId = params["compId"];
    });
    this.dataService
      .getBikeInsurerApi(this.bike_enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        response.summaryData["status"] == "Success"
          ? (this.bikeDataSummary = response.summaryData["enquiry_details"][0])
          : "";
        response.summaryData["status"] == "Success"
          ? (this.gst = response.summaryData["gst"])
          : "";
        console.log(this.bikeData);
      });

    const data = { company_id: this.companyId };
    this.dataService.LoanProvider(data).subscribe((response) => {
      this.LoanProviderData = response.existing_insurance;
      this.FinancierNameData = response.car_financier;
    });

    if (localStorage.getItem("proposalFormBike") !== null) {
      this.formData = JSON.parse(localStorage.getItem("proposalFormBike"));
      // this.getCity(this.formData["reg_pincode"], this.formData["reg_city_id"]);
    }
  }

  back() {
    this.router.navigate(["bike/BikeDetails"], {
      queryParams: {
        id: this.bike_enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }

  submitform() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }
    if (this.bikeData["policyType"] == "new_enquiry") {
      this.formData["expiry_policy_no"] = "";
      this.formData["existing_insurance"] = "";
    }

    console.log(this.formData);
    // return false;

    if (localStorage.getItem("proposalFormBike") != null) {
      localStorage.removeItem("proposalFormBike");
    }
    localStorage.setItem("proposalFormBike", JSON.stringify(this.formData));
    // if (localStorage.getItem("proposalFormBike") != null) {
    //   this.router.navigate(["bike/BikePersonalDetails"], {
    //     queryParams: {
    //       id: this.bike_enquiry_id,
    //       propId: 0,
    //     },
    //   });
    // }

    this.formData.bike_enquiry_id = this.bike_enquiry_id;
    let modDob = "",
      modDob1 = "";
    modDob += `${this.formData.dob["year"]}-`;
    if (this.formData.dob["month"] < 10) {
      modDob += `0${this.formData.dob["month"]}-`;
    } else {
      modDob += `${this.formData.dob["month"]}-`;
    }
    if (this.formData.dob["day"] < 10) {
      modDob += `0${this.formData.dob["day"]}`;
    } else {
      modDob += `${this.formData.dob["day"]}`;
    }
    this.formData.dob = modDob;

    // modDob1 += `${this.formData.nominee_dob["year"]}-`;
    // if (this.formData.nominee_dob["month"] < 10) {
    //   modDob1 += `0${this.formData.nominee_dob["month"]}-`;
    // } else {
    //   modDob1 += `${this.formData.nominee_dob["month"]}-`;
    // }
    // if (this.formData.nominee_dob["day"] < 10) {
    //   modDob1 += `0${this.formData.nominee_dob["day"]}`;
    // } else {
    //   modDob1 += `${this.formData.nominee_dob["day"]}`;
    // }
    // this.formData.nominee_dob = modDob1;

    console.log(this.formData);
    this.dataService
      .bikeProposalFormSave(this.formData)
      .subscribe((response) => {
        console.log(response);
        this.proposal_id = response["id"];
        this.router.navigate(["bike/BikeFinalSummary"], {
          queryParams: {
            enquiry_id: this.bike_enquiry_id,
            proposal_id: this.proposal_id,
            compId: this.companyId,
          },
        });
      });
  }

  validations() {
    let validCounter = 0;
    if (this.bikeData["policyType"] !== "new_enquiry") {
      if (
        this.requiredValidation(
          this.formData["existing_insurance"],
          "existing_insurance_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          this.formData["expiry_policy_no"],
          "expiry_policy_no_err"
        ) == false
      ) {
        validCounter++;
      }

      // if (
      //   this.requiredValidation(
      //     this.formData["third_party_expiry_date"],
      //     "third_party_expiry_date_err"
      //   ) == false
      // ) {
      //   validCounter++;
      // }
    }
    return validCounter;
  }

  requiredValidation(element, errorId) {
    if (errorId != "third_party_expiry_date_err") {
      element = element.trim();
    }
    console.log(element);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
