import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeProposalPrevPolicyDetailsComponent } from './bike-proposal-prev-policy-details.component';

describe('BikeProposalPrevPolicyDetailsComponent', () => {
  let component: BikeProposalPrevPolicyDetailsComponent;
  let fixture: ComponentFixture<BikeProposalPrevPolicyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeProposalPrevPolicyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeProposalPrevPolicyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
