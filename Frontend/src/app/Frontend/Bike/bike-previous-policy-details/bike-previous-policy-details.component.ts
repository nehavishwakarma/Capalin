import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validator,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-bike-previous-policy-details",
  templateUrl: "./bike-previous-policy-details.component.html",
  styleUrls: ["./bike-previous-policy-details.component.scss"],
})
export class BikePreviousPolicyDetailsComponent implements OnInit {
  formData;
  ncbValues = [0, 20, 25, 35, 45, 50];
  prePolicyDetailsForm: FormGroup;
  prePolicyDetails = {
    last_policy_expiry_date: "",
    last_policy_claim_status: "",
    last_policy_ncb: "",
  };
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder
  ) {}
  get f() {
    return this.prePolicyDetailsForm.controls;
  }
  ngOnInit() {
    this.prePolicyDetailsForm = this.fb.group({
      last_policy_expiry_date: [""],
      last_policy_ncb: [""],
      last_policy_claim_status: [""],
    });

    this.formData = JSON.parse(localStorage.getItem("registerFormBike"));

    if (this.formData["enquiryOption"] == "renewal") {
      this.prePolicyDetailsForm.controls["last_policy_expiry_date"].setValue(
        this.formData["last_policy_expiry_date"]
      );
      this.prePolicyDetailsForm.controls["last_policy_ncb"].setValue(
        this.formData["last_policy_ncb"]
      );
      this.prePolicyDetailsForm.controls["last_policy_claim_status"].setValue(
        this.formData["last_policy_claim_status"]
      );
    } else {
      this.formData["last_policy_expiry_date"] = "";
      this.formData["last_policy_ncb"] = "";
      this.formData["last_policy_claim_status"] = "";
    }
  }
  submitEnqiry() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }
    this.formData["last_policy_expiry_date"] =
      this.prePolicyDetailsForm.value["last_policy_expiry_date"];
    this.formData["last_policy_ncb"] =
      this.prePolicyDetailsForm.value["last_policy_ncb"];
    this.formData["last_policy_claim_status"] =
      this.prePolicyDetailsForm.value["last_policy_claim_status"];

    if (this.formData["enquiryOption"] != "renewal") {
      this.formData["last_policy_expiry_date"] = "";
      this.formData["last_policy_ncb"] = "";
      this.formData["last_policy_claim_status"] = "false";
    }
    console.log(this.formData);
    localStorage.removeItem("registerFormBike");
    localStorage.setItem("registerFormBike", JSON.stringify(this.formData));
    this.dataService.getBikeStoreJson(this.formData).subscribe(
      (response) => {
        console.log(response);
        if (response.status == "Success") {
          localStorage.setItem("enquiry_id_bike", response.enquiry_id);
          this.router.navigate(["/bike/BikeInsurance"]);
        } else if (response.status == "Error") {
          alert(response.ErrorMsg);
        } else {
          alert("Something Went Wrong!!");
        }
      },
      (error) => {
        alert("Something Went Wrong!!");
        console.error(error);
      }
    );
  }

  back() {}
  validations() {
    let validCounter = 0;
    if (this.formData["enquiryOption"] == "renewal") {
      if (
        this.requiredValidation(
          this.prePolicyDetailsForm.controls["last_policy_expiry_date"].value,
          "last_policy_expiry_date_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          this.prePolicyDetailsForm.controls["last_policy_claim_status"].value,
          "last_policy_claim_status_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.prePolicyDetailsForm.controls["last_policy_claim_status"].value ==
          "yes" &&
        this.requiredValidation(
          this.prePolicyDetailsForm.controls["last_policy_ncb"].value,
          "last_policy_ncb_err"
        ) == false
      ) {
        validCounter++;
      }
    }
    return validCounter;
  }
  requiredValidation(element, errorId) {
    if (errorId != "last_policy_expiry_date_err") {
      element = element.trim();
    }
    console.log(element);
    console.log(errorId);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
