import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikePreviousPolicyDetailsComponent } from './bike-previous-policy-details.component';

describe('BikePreviousPolicyDetailsComponent', () => {
  let component: BikePreviousPolicyDetailsComponent;
  let fixture: ComponentFixture<BikePreviousPolicyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikePreviousPolicyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikePreviousPolicyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
