import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeRegAddressComponent } from './bike-reg-address.component';

describe('BikeRegAddressComponent', () => {
  let component: BikeRegAddressComponent;
  let fixture: ComponentFixture<BikeRegAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeRegAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeRegAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
