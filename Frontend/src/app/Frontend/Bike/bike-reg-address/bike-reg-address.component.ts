import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bike-reg-address",
  templateUrl: "./bike-reg-address.component.html",
  styleUrls: ["./bike-reg-address.component.scss"],
})
export class BikeRegAddressComponent implements OnInit {
  companyId;
  cityStateName;
  cityStateName1;
  stateName = [];
  communication_address = false;
  formData: any;
  bike_enquiry_id;
  bikeData;
  LoanProviderData;
  FinancierNameData;
  proposal_id;
  cityOpt;
  cityOpt1;
  stateOpt;
  stateOpt1;
  isedit = 0;
  gst;
  bikeDataSummary;
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.companyId = 40;
    this.formData = {
      id: "",
      bike_owner: "",
      company_name: "",
      first_name: "",
      last_name: "",
      email: "",
      gst_no: "",
      gender: "",
      nominee_name: "",
      marital_status: "",
      dob: "",
      nominee_dob: "",
      nominee_relationship: "",
      reg_address: "",
      mobile: "",
      reg_locality: "",
      reg_landmark: "",
      reg_pincode: "",
      add_communication_address: "",
      registeration_no: "",
      expiry_policy_no: "",
      enginee_no: "",
      existing_insurance: "",
      existing_insurer_name: "",
      chassis_no: "",
      is_bike_finance: "",
      loan_start_date: "",
      communication_address: "",
      communication_locality: "",
      communication_landmark: "",
      communication_pincode: "",
      bike_enquiry_id: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.bike_enquiry_id = params["id"];
      this.companyId = params["compId"];
    });
    this.dataService
      .getBikeInsurerApi(this.bike_enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        response.summaryData["status"] == "Success"
          ? (this.bikeDataSummary = response.summaryData["enquiry_details"][0])
          : "";
        response.summaryData["status"] == "Success"
          ? (this.gst = response.summaryData["gst"])
          : "";
      });

    if (localStorage.getItem("proposalFormBike") !== null) {
      this.formData = JSON.parse(localStorage.getItem("proposalFormBike"));
      this.getCity(this.formData["reg_pincode"], this.formData["reg_city_id"]);
    }
  }
  submitform() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }
    console.log(this.formData);
    if (localStorage.getItem("proposalFormBike") != null) {
      localStorage.removeItem("proposalFormBike");
    }

    localStorage.setItem("proposalFormBike", JSON.stringify(this.formData));
    if (localStorage.getItem("proposalFormBike") != null) {
      this.router.navigate(["bike/BikeDetails"], {
        queryParams: {
          id: this.bike_enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    }
  }

  validations() {
    let validCounter = 0;
    if (
      this.requiredValidation(
        this.formData["reg_address"],
        "reg_address_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(
        this.formData["reg_address"],
        "reg_address_err"
      ) == false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(
        this.formData["reg_locality"],
        "reg_locality_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(
        this.formData["reg_locality"],
        "reg_locality_err"
      ) == false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(
        this.formData["reg_landmark"],
        "reg_landmark_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(
        this.formData["reg_landmark"],
        "reg_landmark_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["reg_pincode"],
        "reg_pincode_err"
      ) == false
    ) {
      validCounter++;
    }
    if (this.requiredValidation(this.cityOpt, "city_err") == false) {
      validCounter++;
    }

    if (this.requiredValidation(this.stateOpt, "state_err") == false) {
      validCounter++;
    }
    return validCounter;
  }

  getCity(event, cityCode = 0) {
    if (cityCode != 0) {
      this.dataService.getCity(event, this.companyId).subscribe((response) => {
        this.cityStateName = response;
        this.cityStateName.forEach((element) => {
          if (cityCode == element["city_code"]) {
            this.cityOpt = element;
            this.stateOpt = element;
          }
        });
      });
    } else {
      if (event.target.value.length == 6) {
        console.log(event.target.value);
        this.dataService
          .getCity(event.target.value, this.companyId)
          .subscribe((response) => {
            console.log(response);
            this.cityStateName = response;
          });
      }
    }
  }

  changeCity() {
    this.formData.reg_city_id = this.cityOpt["city_code"];
    this.formData.reg_city_name = this.cityOpt["city_name"];
    this.formData.reg_district_id = this.cityOpt["district_code"];
    this.formData.reg_state_id = this.cityOpt["state_code"];
    this.formData.reg_state_name = this.cityOpt["state_name"];
    this.stateName = [];
    this.stateName.push(this.cityOpt["state_name"]);
    // console.log(myArray);
    // if (this.communication_address == false) {
    //   alert("communication");
    //   this.formData.communication_city_id = myArray[0];
    //   this.formData.communication_city_name = myArray[1];
    //   this.formData.communication_district_id = myArray[2];
    //   this.formData.communication_state_id = myArray[3];
    //   this.formData.communication_state_name = myArray[4];
    // }
    // console.log(myArray);
  }

  requiredValidation(element, errorId) {
    console.log(`${element}****${errorId}`);
    if (
      errorId != "city_err" &&
      errorId != "state_err" &&
      errorId != "reg_pincode_err"
    ) {
      element = element.trim();
    }
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphanumericValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9\s]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphanumerics are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  back() {
    this.router.navigate(["bike/BikePersonalDetails"], {
      queryParams: {
        id: this.bike_enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }

  checklength(event) {
    if (event.target.value.split("").length > 5 && event.key != "Backspace") {
      return false;
    }
  }
}
