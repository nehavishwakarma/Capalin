import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bike-personal-details",
  templateUrl: "./bike-personal-details.component.html",
  styleUrls: ["./bike-personal-details.component.scss"],
})
export class BikePersonalDetailsComponent implements OnInit {
  companyId;
  cityStateName;
  cityStateName1;
  communication_address = false;
  formData: any;
  bike_enquiry_id;
  bikeData;
  LoanProviderData;
  FinancierNameData;
  proposal_id;
  cityOpt;
  cityOpt1;
  stateOpt;
  stateOpt1;
  isedit = 0;
  gst;
  bikeDataSummary;
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.companyId = 40;
    this.formData = {
      id: "",
      bike_owner: "",
      company_name: "",
      first_name: "",
      last_name: "",
      email: "",
      gst_no: "",
      gender: "",
      nominee_name: "",
      marital_status: "",
      dob: "",
      nominee_dob: "",
      nominee_relationship: "",
      reg_address: "",
      mobile: "",
      reg_locality: "",
      reg_landmark: "",
      reg_pincode: "",
      add_communication_address: "",
      registeration_no: "",
      expiry_policy_no: "",
      enginee_no: "",
      existing_insurance: "",
      existing_insurer_name: "",
      chassis_no: "",
      is_bike_finance: "",
      loan_start_date: "",
      communication_address: "",
      communication_locality: "",
      communication_landmark: "",
      communication_pincode: "",
      bike_enquiry_id: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.bike_enquiry_id = params["id"];
      this.companyId = params["compId"];
    });
    this.dataService
      .getBikeInsurerApi(this.bike_enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        response.summaryData["status"] == "Success"
          ? (this.bikeDataSummary = response.summaryData["enquiry_details"][0])
          : "";
        response.summaryData["status"] == "Success"
          ? (this.gst = response.summaryData["gst"])
          : "";
      });

    if (localStorage.getItem("proposalFormBike") !== null) {
      this.formData = JSON.parse(localStorage.getItem("proposalFormBike"));
    }
  }

  submitform() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }
    console.log(this.formData);
    if (localStorage.getItem("proposalFormBike") != null) {
      localStorage.removeItem("proposalFormBike");
    }

    localStorage.setItem("proposalFormBike", JSON.stringify(this.formData));
    if (localStorage.getItem("proposalFormBike") != null) {
      this.router.navigate(["bike/BikeRegAddress"], {
        queryParams: {
          id: this.bike_enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    }
  }

  validations() {
    let validCounter = 0;
    if (
      this.requiredValidation(this.formData["gender"], "gender_err") == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["marital_status"],
        "marital_status_err"
      ) == false
    ) {
      validCounter++;
    }
    if (this.requiredValidation(this.formData["dob"], "dob_err") == false) {
      validCounter++;
    }
    return validCounter;
  }

  requiredValidation(element, errorId) {
    if (errorId != "dob_err") {
      element = element.trim();
    }
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  back() {
    this.router.navigate(["bike/proposal"], {
      queryParams: {
        id: this.bike_enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }
}
