import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikePersonalDetailsComponent } from './bike-personal-details.component';

describe('BikePersonalDetailsComponent', () => {
  let component: BikePersonalDetailsComponent;
  let fixture: ComponentFixture<BikePersonalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikePersonalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikePersonalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
