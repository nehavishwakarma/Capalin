import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import jspdf from "jspdf";
import html2canvas from "html2canvas";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bike-summary",
  templateUrl: "./bike-summary.component.html",
  styleUrls: ["./bike-summary.component.scss"],
})
export class BikeSummaryComponent implements OnInit {
  @ViewChild("email", { static: false }) emailPopup: ElementRef;

  constructor(
    private dataService: DataService,
    private modalService: NgbModal,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}
  enquiry_id;
  bikeData;
  proposal_id;
  SummaryData;
  valid_email = true;
  mail_success = null;
  saveQuotePayLaterForm = "";
  ngOnInit() {
    // this.enquiry_id = localStorage.getItem("enquiry_id");
    // this.proposal_id = localStorage.getItem("proposal_id");

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["proposal_id"];
      this.enquiry_id = params["enquiry_id"];
    });
    if (!this.proposal_id || !this.enquiry_id) {
      alert("Something Went Wrong");
    }
    this.dataService
      .getBikeFormSummary(this.proposal_id, this.enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response["bike_enquiry_details"]["enquiry_details"][0];
        console.log(this.bikeData);
        this.SummaryData =
          response["bike_proposal_details"]["bike_proposal_details"][0];
        this.saveQuotePayLaterForm = this.SummaryData["email"];
        console.log(this.SummaryData);
      });
  }

  openEmailModal() {
    this.modalService.open(this.emailPopup);
  }

  public convetToPDF() {
    var data = document.getElementById("maindiv");
    html2canvas(data).then((canvas) => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = (canvas.height * imgWidth) / canvas.width + 20;
      // var imgHeight = canvas.height;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jspdf("p", "mm", "a4"); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      pdf.save("Bike.pdf"); // Generated PDF
    });
  }

  redirectToEdit() {
    // this.router.navigate(["bike/proposal/", this.enquiry_id]);
    this.router.navigate(["bike/proposal"], {
      queryParams: {
        id: this.enquiry_id,
        propId: this.proposal_id,
      },
    });
  }

  saveQuoteMail() {
    let dataObj = [];
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(this.saveQuotePayLaterForm).toLowerCase())) {
      this.valid_email = false;
      return false;
    } else {
      this.valid_email = true;
    }
    dataObj.push(this.SummaryData, this.bikeData, this.saveQuotePayLaterForm);
    console.log(dataObj);
    this.dataService.saveQuoteMailBike(dataObj).subscribe((response) => {
      response.status == "Success"
        ? (this.mail_success = true)
        : (this.mail_success = false);
    });
  }
}
