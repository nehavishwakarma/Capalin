import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeFinalSummaryComponent } from './bike-final-summary.component';

describe('BikeFinalSummaryComponent', () => {
  let component: BikeFinalSummaryComponent;
  let fixture: ComponentFixture<BikeFinalSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeFinalSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeFinalSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
