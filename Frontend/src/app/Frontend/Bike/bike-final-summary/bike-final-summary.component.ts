import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import jspdf from "jspdf";
import html2canvas from "html2canvas";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-bike-final-summary",
  templateUrl: "./bike-final-summary.component.html",
  styleUrls: ["./bike-final-summary.component.scss"],
})
export class BikeFinalSummaryComponent implements OnInit {
  constructor(
    private dataService: DataService,
    private modalService: NgbModal,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}
  enquiry_id;
  bikeData;
  proposal_id;
  SummaryData;
  valid_email = true;
  mail_success = null;
  saveQuotePayLaterForm = "";
  regDate;
  expiryDate;
  dob;
  bikeQuotes;
  newPolicyStart;
  companyId;

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["proposal_id"];
      this.enquiry_id = params["enquiry_id"];
      this.companyId = params["compId"];
    });
    
    // if (!this.proposal_id || !this.enquiry_id) {
    //   alert("Something Went Wrong");
    // }
    this.dataService
      .getBikeFormSummary(this.proposal_id, this.enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response["bike_enquiry_details"]["enquiry_details"][0];
        this.regDate = new Date(this.bikeData.register_date);
        this.expiryDate = new Date(this.bikeData.expiry_date);

        this.SummaryData =
          response["bike_proposal_details"]["bike_proposal_details"][0];
        this.saveQuotePayLaterForm = this.SummaryData["email"];
        this.dob = new Date(this.SummaryData.dob);

        this.bikeQuotes = response["bikeQuotes"]["enquiry_data"];
        this.newPolicyStart = new Date(
          `${this.bikeQuotes["newPolicyStartDate"].split("/")[2]}-${
            this.bikeQuotes["newPolicyStartDate"].split("/")[1]
          }-${this.bikeQuotes["newPolicyStartDate"].split("/")[0]}`
        );
      });
  }

  public convetToPDF() {
    var data = document.getElementById("maindiv");
    html2canvas(data).then((canvas) => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = (canvas.height * imgWidth) / canvas.width + 20;
      // var imgHeight = canvas.height;
      var heightLeft = imgHeight;
      window.scrollTo(0, 0);
      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jspdf("p", "mm", "a4"); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      pdf.save("Bike.pdf"); // Generated PDF
    });
  }

  goToEdit(goto, isParam) {
    if (isParam == 1) {
      this.router.navigate([`bike/${goto}`], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    } else {
      this.router.navigate([`bike/${goto}`]);
    }
  }

  bike_payment_request(){
    
    this.dataService.bike_payment_request({
      company_id: this.companyId,
      enquiry_id: this.enquiry_id,
      proposal_id: Number(this.proposal_id),
      payment_mode: 'CC'
    }).subscribe(res => {
      console.log(res)
      if (res.status === 'Success') {
        if (this.companyId == 8) {
          const paymentURL = `${environment.bajajAllianz}${this.bikeData.transaction_id}&Username=${environment.bajajAllianzUsername}&sourceName=WS_MOTOR`
          window.location.assign(paymentURL);
        }
        if (this.companyId == 45) {
          const paymentURL = `${environment.shriram}`
          window.location.assign(paymentURL);
        }
        if (this.companyId == 53) {
          const paymentURL = `${environment.futurGenerali}`
          window.location.assign(paymentURL);
        }
        if(this.companyId == 43) {
          const paymentString = `$payment_gateway_data['insurerProposalNumber'] . '|1|' . $returnUrl . '|' . 'XMPVF0136141' . '|' . $payment_gateway_data['proposalPremium'] . '|' . '60001464' . '|' . '10134025' . '|' . $payment_gateway_data['fName'] . '|' . $payment_gateway_data['lName'] . '|' . $payment_gateway_data['mobile'] . '|' . $payment_gateway_data['email'] . '|`;
        }
      }
    })
  }
}

