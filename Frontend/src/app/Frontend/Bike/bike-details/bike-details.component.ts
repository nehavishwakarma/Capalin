import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bike-details",
  templateUrl: "./bike-details.component.html",
  styleUrls: ["./bike-details.component.scss"],
})
export class BikeDetailsComponent implements OnInit {
  companyId;
  cityStateName;
  cityStateName1;
  communication_address = false;
  formData: any;
  bike_enquiry_id;
  bikeData;
  LoanProviderData;
  FinancierNameData;
  proposal_id;
  cityOpt;
  cityOpt1;
  stateOpt;
  stateOpt1;
  isedit = 0;
  gst;
  bikeDataSummary;
  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.companyId = 40;
    this.formData = {
      id: "",
      bike_owner: "",
      company_name: "",
      first_name: "",
      last_name: "",
      email: "",
      gst_no: "",
      gender: "",
      nominee_name: "",
      marital_status: "",
      dob: "",
      nominee_dob: "",
      nominee_relationship: "",
      reg_address: "",
      mobile: "",
      reg_locality: "",
      reg_landmark: "",
      reg_pincode: "",
      add_communication_address: "",
      registeration_no: "",
      expiry_policy_no: "",
      enginee_no: "",
      existing_insurance: "",
      existing_insurer_name: "",
      chassis_no: "",
      is_bike_finance: "",
      loan_start_date: "",
      communication_address: "",
      communication_locality: "",
      communication_landmark: "",
      communication_pincode: "",
      bike_enquiry_id: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.bike_enquiry_id = params["id"];
      this.companyId = params["compId"];
    });
    this.dataService
      .getBikeInsurerApi(this.bike_enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        response.summaryData["status"] == "Success"
          ? (this.bikeDataSummary = response.summaryData["enquiry_details"][0])
          : "";
        response.summaryData["status"] == "Success"
          ? (this.gst = response.summaryData["gst"])
          : "";
      });

    const data = { company_id: this.companyId };
    this.dataService.LoanProvider(data).subscribe((response) => {
      this.LoanProviderData = response.existing_insurance;
      this.FinancierNameData = response.car_financier;
    });

    if (localStorage.getItem("proposalFormBike") !== null) {
      this.formData = JSON.parse(localStorage.getItem("proposalFormBike"));
      // this.getCity(this.formData["reg_pincode"], this.formData["reg_city_id"]);
    }
  }

  submitform() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }
    if (this.formData["is_bike_finance"] == "false") {
      this.formData["existing_insurer_name"] = "";
      this.formData["loan_city"] = "";
    }
    console.log(this.formData);
    if (localStorage.getItem("proposalFormBike") != null) {
      localStorage.removeItem("proposalFormBike");
    }

    localStorage.setItem("proposalFormBike", JSON.stringify(this.formData));
    if (localStorage.getItem("proposalFormBike") != null) {
      this.router.navigate(["bike/BikeProposalPrevPolicyDetails"], {
        queryParams: {
          id: this.bike_enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    }
  }

  validations() {
    let validCounter = 0;
    if (
      this.requiredValidation(this.formData["enginee_no"], "enginee_no_err") ==
      false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(
        this.formData["is_bike_finance"],
        "is_bike_finance_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.formData["registeration_no"] !== "" &&
      this.indVehicleNumberValidation(
        this.formData["registeration_no"],
        "registeration_no_err"
      ) == false
    ) {
      validCounter++;
    } else {
      document.getElementById("registeration_no_err").style.display = "none";
    }
    // if (
    //   this.requiredValidation(
    //     this.formData["reg_pincode"],
    //     "reg_pincode_err"
    //   ) == false
    // ) {
    //   validCounter++;
    // }
    // if (this.requiredValidation(this.cityOpt, "city_err") == false) {
    //   validCounter++;
    // }

    // if (this.requiredValidation(this.stateOpt, "state_err") == false) {
    //   validCounter++;
    // }
    return validCounter;
  }

  requiredValidation(element, errorId) {
    console.log(`${element}****${errorId}`);
    if (
      errorId != "city_err" &&
      errorId != "state_err" &&
      errorId != "reg_pincode_err"
    ) {
      element = element.trim();
    }
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  indVehicleNumberValidation(element, errorId) {
    let pattern = /^[A-Z]{2}\s[0-9]{2}\s[A-Z]{2}\s[0-9]{4}$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Please Enter Valid Registration Number.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }

    // [A-Z]{2} means 2 characters in the range of A through Z
    // \s means white space
    // [0-9]{2} means 2 characters in the range of 0 through 9
    // \s means white space
    // [A-Z]{2} means 2 characters in the range of A through Z
    // \s means white space
    // [0-9]{4} means 4 characters in the range of 0 through 9
  }
  back() {
    this.router.navigate(["bike/BikeRegAddress"], {
      queryParams: {
        id: this.bike_enquiry_id,
        propId: 0,
        compId: this.companyId,
      },
    });
  }
}
