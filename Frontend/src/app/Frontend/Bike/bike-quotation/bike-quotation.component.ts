import { Component, OnInit } from "@angular/core";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ConditionalExpr } from "@angular/compiler";

@Component({
  selector: "app-bike-quotation",
  templateUrl: "./bike-quotation.component.html",
  styleUrls: ["./bike-quotation.component.scss"],
})
export class BikeQuotationComponent implements OnInit {
  constructor(private router: Router, private dataService: DataService) {}
  bikeData: any = [];
  totalPrice;
  totalPrice1;
  totalPrice2;
  addOnString = "";
  checked = "";
  checkbox = "";
  invoice_cover: boolean;
  consumable: boolean;
  paid_driver_legal_liability_cover: boolean;
  pa_to_owner_driver: boolean;
  enquiry_id;
  insurerData;
  quotaSuccess = [];
  zero_depreciation: boolean;
  passenger_cover: boolean;
  anti_theft_device: boolean;
  automobile_association: boolean;
  insurer;
  thirdPartyPlans;
  rode_side_assistance: boolean;

  ngOnInit() {
    localStorage.setItem("proposal_id", "0");
    this.enquiry_id = localStorage.getItem("enquiry_id");
    this.dataService
      .getBikeInsurerApi(this.enquiry_id)
      .subscribe((response) => {
        this.Apis(response);

        this.bikeData = response.enquiry_data;
        this.insurerData = response.insurer_data;
        console.log(response.insurer_data);
        this.insurerData.forEach((quota) => {
          if (quota["status"] == "Success") {
            console.log("suc");
            this.quotaSuccess.push(quota);
          } else {
            console.log(quota["status"] + "****");
          }
        });
        console.log(this.quotaSuccess);
        localStorage.setItem("bike_enquiry_id", this.bikeData.bikeEnquiryId);
        this.setData(response);
      });
  }

  setData(response) {
    this.zero_depreciation =
      response.enquiry_data.zero_depreciation == "true" ? true : false;
    this.passenger_cover =
      response.enquiry_data.passenger_cover == "true" ? true : false;
    this.anti_theft_device =
      response.enquiry_data.anti_theft_device == "true" ? true : false;
    this.automobile_association =
      response.enquiry_data.automobile_association == "true" ? true : false;
    this.paid_driver_legal_liability_cover =
      response.enquiry_data.paid_driver_legal_liability_cover == "true"
        ? true
        : false;
    this.invoice_cover =
      response.enquiry_data.invoice_cover == "true" ? true : false;
    this.rode_side_assistance =
      response.enquiry_data.rode_side_assistance == "true" ? true : false;
    this.consumable = response.enquiry_data.consumable == "true" ? true : false;
    this.pa_to_owner_driver =
      response.enquiry_data.pa_to_owner_driver == "true" ? true : false;
    this.paid_driver_legal_liability_cover =
      response.enquiry_data.paid_driver_legal_liability_cover == "true"
        ? true
        : false;
    this.pa_to_owner_driver =
      response.enquiry_data.pa_to_owner_driver == "true" ? true : false;
  }

  Apis(data) {
    for (let index = 0; index < data["insurer_data"].length; index++) {
      let data3 = data;
      data3["enquiryData"] = data["enquiry_data"];
      data3["insurerData"] = data["insurer_data"][index];
      if (data3["insurerData"]["company_id"] == 40) {
        this.dataService.getBikeHDFCApiApi(data3).subscribe((response) => {
          if (response.insurerApiStatus == "Success") {
            this.totalPrice = response.insurerApiData["finalPremium"][0];
            this.totalPrice1 = response.insurerApiData["finalPremium"][1];
            this.totalPrice2 = response.insurerApiData["finalPremium"][2];
            this.insurer = response.availablePlans;
            this.thirdPartyPlans = response;
            //  *response.insurerApiData["tax"][0]
          }
        });
      }
    }
  }

  addOn(bool, addOn) {
    console.log(bool);
    if (bool) {
      var value = "true";
    } else {
      var value = "false";
    }

    this.editBikeQuotes(addOn, value);
  }

  editBikeQuotes(addOn, value) {
    const data = { parameter: addOn, id: value };
    this.dataService
      .editBikeQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  editBikeInsurerApi() {
    let value =
      this.bikeData.electrical_accessories +
      "," +
      this.bikeData.non_electrical_accessories;
    const data = { parameter: "additionalCovers", id: value };
    console.log(data);
    this.dataService
      .editBikeInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  editDiscountsApi() {
    let value = "";
    value += this.bikeData.voluntary_deductible_value;
    value += ",";

    if (this.anti_theft_device == true) {
      value += "true,";
    } else {
      value += "false,";
    }

    if (this.automobile_association == true) {
      value += "true,";
    } else {
      value += "false,";
    }
    const data = { parameter: "discounts", id: value };
    this.dataService
      .editBikeInsurerApi(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  RecommendedAddOns(para, id) {
    let val = "";
    if (id == "false") {
      val = "true";
    } else if (id == "true") {
      val = "false";
    }
    const data = { parameter: para, id: val };
    this.dataService
      .editBikeQuotes(this.enquiry_id, data)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        this.setData(response);
      });
  }

  bookNowBikeHDFC(
    leadId,
    mastersheetId,
    enquiry_id,
    idv,
    premium,
    transactionId_1,
    transactionId_2,
    odPremium,
    renewal_type,
    pa_to_owner_driver_premium
  ) {
    localStorage.setItem("master_sheet_id", mastersheetId);
    const dataArray = [
      {
        leadId: leadId,
        mastersheetId: mastersheetId,
        enquiry_id: enquiry_id,
        idv: idv,
        premium: premium,
        transactionId: transactionId_1,
        odPremium: odPremium,
        pa_to_owner_driver_premium: pa_to_owner_driver_premium,
      },
    ];
    console.log(dataArray);
    this.dataService.bookNowBikeHDFC(dataArray).subscribe((response) => {
      console.log(response);
      // this.router.navigate(["bike/proposal/", this.enquiry_id]);
      this.router.navigate(["bike/proposal"], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
        },
      });
    });
  }
}
