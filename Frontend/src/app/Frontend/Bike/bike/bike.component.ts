import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validator,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-bike",
  templateUrl: "./bike.component.html",
  styleUrls: ["./bike.component.scss"],
})
export class BikeComponent implements OnInit {
  registrationForm: FormGroup = this.fb.group({
    enquiryOption: ["renewal"],
    bike_manufacturer: [""],
    varientmodel: [""],
    rto: [""],
    manufacturing_year: [""],
    registration_date: [""],
    last_policy_expiry_date: [""],
    last_policy_claim_status: [""],
    last_policy_ncb: [""],
  });
  newForm: FormGroup = this.fb.group({
    enquiryOption: ["new_enquiry"],
    bike_manufacturer: [""],
    varientmodel: [""],
    rto: [""],
    manufacturing_year: [""],
    registration_date: [""],
    last_policy_expiry_date: [""],
  });
  bikeModelDetails;
  variantTypeDetails;
  model: NgbDateStruct;
  enquiryOption: string = "renewal";
  M_Year;
  active = 1;

  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder
  ) {}
  get f() {
    return this.registrationForm.controls;
  }

  ngOnInit() {
    if (localStorage.getItem("registerFormBike") !== null) {
      let preData = JSON.parse(localStorage.getItem("registerFormBike"));
      console.log(preData);
      this.registrationForm.controls["enquiryOption"].setValue(
        preData["enquiryOption"]
      );
      this.registrationForm.controls["bike_manufacturer"].setValue(
        preData["bike_manufacturer"]
      );
      this.getVariantType("bike_model", preData["bike_manufacturer"]);
      this.registrationForm.controls["varientmodel"].setValue(
        preData["varientmodel"]
      );
      this.registrationForm.controls["rto"].setValue(preData["rto"]);
      this.registrationForm.controls["manufacturing_year"].setValue(
        preData["manufacturing_year"]
      );
      this.registrationForm.controls["registration_date"].setValue(
        preData["registration_date"]
      );
      this.registrationForm.controls["last_policy_expiry_date"].setValue(
        preData["last_policy_expiry_date"]
      );
      this.registrationForm.controls["last_policy_claim_status"].setValue(
        preData["last_policy_claim_status"]
      );
      this.registrationForm.controls["last_policy_ncb"].setValue(
        preData["last_policy_ncb"]
      );
    }

    this.dataService.getBikeModelDetails().subscribe((response) => {
      console.log(response);
      this.M_Year = response.M_Year;
      this.bikeModelDetails = response.response;
    });
  }

  getVariantType(table, id) {
    this.dataService.getBikeVariant(table, id).subscribe((response) => {
      this.variantTypeDetails = response.response;
    });
  }

  onSubmit() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }

    localStorage.removeItem("registerFormBike");
    localStorage.setItem(
      "registerFormBike",
      JSON.stringify(this.registrationForm.value)
    );
    console.log(this.registrationForm.value);
    if (localStorage.getItem("registerFormBike") !== null) {
      this.router.navigate(["bike/BikePreviousPolicyDetails"]);
    }
    // this.dataService
    //   .getBikeStoreJson(this.registrationForm.value)
    //   .subscribe((response) => {
    //     // routerLink="/bike/BikePreviousPolicyDetails"
    //     console.log(response);

    //     localStorage.setItem("enquiry_id", response.enquiry_id);
    //     this.router.navigate(["bike/quotes/", response.enquiry_id]);
    //   });
  }

  validations() {
    let validCounter = 0;
    if (
      this.requiredValidation(
        this.registrationForm.controls["bike_manufacturer"].value,
        "bike_manufacturer_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["varientmodel"].value,
        "varientmodel_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["rto"].value,
        "rto_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["manufacturing_year"].value,
        "manufacturing_year_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["registration_date"].value,
        "registration_date_err"
      ) == false
    ) {
      validCounter++;
    }

    return validCounter;
  }

  requiredValidation(element, errorId) {
    // if (errorId != "registration_date_err") {
    //   element = element.trim();
    // }
    console.log(element);
    console.log(errorId);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  onSubmit1() {
    console.log(this.newForm.value);
    this.dataService
      .getBikeStoreJson(this.newForm.value)
      .subscribe((response) => {
        console.log(response);
        localStorage.setItem("enquiry_id", response.enquiry_id);
        this.router.navigate(["bike/quotes/", response.enquiry_id]);
      });
  }
}
