import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-bike-proposal",
  templateUrl: "./bike-proposal.component.html",
  styleUrls: ["./bike-proposal.component.scss"],
})
export class BikeProposalComponent implements OnInit {
  companyId;
  cityStateName;
  cityStateName1;
  communication_address = false;
  formData: any;
  bike_enquiry_id;
  bikeData;
  bikeDataSummary;
  LoanProviderData;
  FinancierNameData;
  proposal_id;
  cityOpt;
  cityOpt1;
  stateOpt;
  stateOpt1;
  isedit = 0;
  gst;

  @ViewChild("email", { static: false }) emailPopup: ElementRef;

  active = 1;
  model;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.companyId = 40;
    this.formData = {
      id: "",
      bike_owner: "",
      company_name: "",
      first_name: "",
      last_name: "",
      email: "",
      gst_no: "",
      gender: "",
      nominee_name: "",
      marital_status: "",
      dob: "",
      nominee_dob: "",
      nominee_relationship: "",
      reg_address: "",
      mobile: "",
      reg_locality: "",
      reg_landmark: "",
      reg_pincode: "",
      add_communication_address: "",
      registeration_no: "",
      expiry_policy_no: "",
      enginee_no: "",
      existing_insurance: "",
      existing_insurer_name: "",
      chassis_no: "",
      is_bike_finance: "",
      loan_start_date: "",
      communication_address: "",
      communication_locality: "",
      communication_landmark: "",
      communication_pincode: "",
      bike_enquiry_id: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.bike_enquiry_id = params["id"];
      this.companyId = params["compId"];
    });
    this.dataService
      .getBikeInsurerApi(this.bike_enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.bikeData = response.enquiry_data;
        response.summaryData["status"] == "Success"
          ? (this.bikeDataSummary = response.summaryData["enquiry_details"][0])
          : "";
        response.summaryData["status"] == "Success"
          ? (this.gst = response.summaryData["gst"])
          : "";
      });

    if (localStorage.getItem("proposalFormBike") !== null) {
      this.formData = JSON.parse(localStorage.getItem("proposalFormBike"));
    }
    // if (this.proposal_id != 0) {
    //   this.isedit = 1;
    //   this.dataService
    //     .getBikePropsalData(this.bike_enquiry_id)
    //     .subscribe((response) => {
    //       console.log(response["data"][0]);
    //       if (response["status"] == "success") {
    //         this.setBikeData(response["data"][0]);
    //       }
    //     });
    // }

    // const data = { company_id: this.companyId };
    // this.dataService.LoanProvider(data).subscribe((response) => {
    //   this.LoanProviderData = response.existing_insurance;
    //   this.FinancierNameData = response.car_financier;
    // });
  }

  // setBikeData(data) {
  //   this.formData.bike_owner = data["bike_owner"];
  //   this.formData.id = data["id"];
  //   this.formData.company_name = data["company_name"];
  //   this.formData.first_name = data["first_name"];
  //   this.formData.last_name = data["last_name"];
  //   this.formData.email = data["email"];
  //   this.formData.gst_no = data["gst_no"];
  //   this.formData.gender = data["gender"];
  //   this.formData.nominee_name = data["nominee_name"];
  //   this.formData.marital_status = data["marital_status"];
  //   let dob = { year: 0, month: 0, day: 0 };
  //   dob.year = Number(data["dob"].split("-")[0]);
  //   dob.month = Number(data["dob"].split("-")[1]);
  //   dob.day = Number(data["dob"].split("-")[2]);
  //   this.formData.dob = dob;
  //   let nomdob = { year: 0, month: 0, day: 0 };
  //   nomdob.year = Number(data["nominee_dob"].split("-")[0]);
  //   nomdob.month = Number(data["nominee_dob"].split("-")[1]);
  //   nomdob.day = Number(data["nominee_dob"].split("-")[2]);
  //   this.formData.nominee_dob = nomdob;
  //   this.formData.nominee_relationship = data["nominee_relationship"];
  //   this.formData.reg_address = data["reg_address"];
  //   this.formData.mobile = data["mobile"];
  //   this.formData.reg_locality = data["reg_locality"];
  //   this.formData.reg_landmark = data["reg_landmark"];
  //   this.formData.reg_pincode = data["reg_pincode"];
  //   this.formData.reg_city_id = data["reg_city_id"];
  //   this.formData.reg_city_name = data["reg_city_name"];
  //   this.formData.reg_district_id = data["reg_district_id"];
  //   this.formData.reg_state_id = data["reg_state_id"];
  //   this.formData.reg_state_name = data["reg_state_name"];

  //   this.formData.add_communication_address =
  //     data["add_communication_address"] == "0" ? false : true;
  //   this.communication_address =
  //     data["add_communication_address"] == "0" ? false : true;

  //   this.formData.registeration_no = data["registeration_no"];
  //   this.formData.expiry_policy_no = data["expiry_policy_no"];
  //   this.formData.enginee_no = data["enginee_no"];
  //   this.formData.existing_insurance = data["existing_insurance"];
  //   this.formData.existing_insurer_name = data["existing_insurer_name"];
  //   this.formData.chassis_no = data["chassis_no"];
  //   this.formData.is_bike_finance = data["is_bike_finance"];
  //   this.formData.loan_financier_name = data["loan_financier_name"];
  //   this.formData.loan_city = data["loan_city"];
  //   // this.formData.loan_start_date = data["loan_start_date"];
  //   this.formData.communication_address = data["communication_address"];
  //   this.formData.communication_locality = data["communication_locality"];
  //   this.formData.communication_landmark = data["communication_landmark"];
  //   this.formData.communication_pincode = data["communication_pincode"];
  //   this.formData.communication_city_id = data["communication_city_id"];
  //   this.formData.communication_city_name = data["communication_city_name"];
  //   this.formData.communication_district_id = data["communication_district_id"];
  //   this.formData.communication_state_id = data["communication_state_id"];
  //   this.formData.communication_state_name = data["communication_state_name"];

  //   this.formData.bike_enquiry_id = data["bike_enquiry_id"];
  //   this.getCity(data["reg_pincode"], data["reg_city_id"]);
  //   this.getCity1(data["communication_pincode"], data["communication_city_id"]);
  // }

  submitform() {
    let valid = this.validations();
    if (valid !== 0) {
      return false;
    }

    if (localStorage.getItem("proposalFormBike") != null) {
      localStorage.removeItem("proposalFormBike");
    }
    localStorage.setItem("proposalFormBike", JSON.stringify(this.formData));
    if (localStorage.getItem("proposalFormBike") != null) {
      this.router.navigate(["bike/BikePersonalDetails"], {
        queryParams: {
          id: this.bike_enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    }
    // return false;
    // if (this.formData.add_communication_address == false) {
    //   this.formData.communication_city_id = this.formData.reg_city_id;
    //   this.formData.communication_city_name = this.formData.reg_city_name;
    //   this.formData.communication_district_id = this.formData.reg_district_id;
    //   this.formData.communication_state_id = this.formData.reg_state_id;
    //   this.formData.communication_state_name = this.formData.reg_state_name;
    //   this.formData.communication_landmark = this.formData.reg_landmark;
    //   this.formData.communication_locality = this.formData.reg_locality;
    //   this.formData.communication_address = this.formData.reg_address;
    //   this.formData.communication_pincode = this.formData.reg_pincode;
    // }
    // if (this.formData.is_bike_finance == "false") {
    //   this.formData.loan_financier_name = "";
    //   this.formData.loan_city = "";
    // }
    // this.formData.bike_enquiry_id = this.bike_enquiry_id;
    // this.formData.add_communication_address = this.communication_address;
    // let modDob = "",
    //   modDob1 = "";
    // modDob += `${this.formData.dob["year"]}-`;
    // if (this.formData.dob["month"] < 10) {
    //   modDob += `0${this.formData.dob["month"]}-`;
    // } else {
    //   modDob += `${this.formData.dob["month"]}-`;
    // }
    // if (this.formData.dob["day"] < 10) {
    //   modDob += `0${this.formData.dob["day"]}`;
    // } else {
    //   modDob += `${this.formData.dob["day"]}`;
    // }
    // this.formData.dob = modDob;

    // modDob1 += `${this.formData.nominee_dob["year"]}-`;
    // if (this.formData.nominee_dob["month"] < 10) {
    //   modDob1 += `0${this.formData.nominee_dob["month"]}-`;
    // } else {
    //   modDob1 += `${this.formData.nominee_dob["month"]}-`;
    // }
    // if (this.formData.nominee_dob["day"] < 10) {
    //   modDob1 += `0${this.formData.nominee_dob["day"]}`;
    // } else {
    //   modDob1 += `${this.formData.nominee_dob["day"]}`;
    // }
    // this.formData.nominee_dob = modDob1;

    // console.log(this.formData);
    // this.dataService
    //   .bikeProposalFormSave(this.formData)
    //   .subscribe((response) => {
    //     console.log(response["id"]);
    //     // localStorage.setItem("proposal_id", response["id"]);
    //     this.proposal_id = response["id"];
    //     // this.dataService.getBikeInsurerApi(localStorage.getItem('car_lead_id')).subscribe();
    //     // this.router.navigate(["bike/summary"]);
    //     this.router.navigate(["bike/summary"], {
    //       queryParams: {
    //         enquiry_id: this.bike_enquiry_id,
    //         proposal_id: this.proposal_id,
    //       },
    //     });
    //   });
  }

  validations() {
    let validCounter = 0;
    if (
      this.requiredValidation(this.formData["bike_owner"], "bike_owner_err") ==
      false
    ) {
      validCounter++;
    }
    if (
      this.formData["bike_owner"] == "company" &&
      this.requiredValidation(
        this.formData["company_name"],
        "company_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.formData["bike_owner"] == "company" &&
      this.alphanumericValidation(
        this.formData["company_name"],
        "company_name_err"
      ) == false
    ) {
      validCounter++;
    }

    if (
      this.requiredValidation(this.formData["first_name"], "first_name_err") ==
      false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(
        this.formData["first_name"],
        "first_name_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(this.formData["last_name"], "last_name_err") ==
      false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(this.formData["last_name"], "last_name_err") ==
      false
    ) {
      validCounter++;
    }
    // if (this.requiredValidation(this.formData["email"], "email_err") == false) {
    //   validCounter++;
    // } else if (
    //   this.emailValidation(this.formData["email"], "email_err") == false
    // ) {
    //   validCounter++;
    // }
    if (
      this.requiredValidation(this.formData["mobile"], "mobile_err") == false
    ) {
      validCounter++;
    } else if (
      this.mobile10digits(this.formData["mobile"], "mobile_err") == false
    ) {
      validCounter++;
    }
    if (
      this.formData["gst_no"] != "" &&
      this.gstValidation(this.formData["gst_no"], "gst_no_err") == false
    ) {
      validCounter++;
    } else {
      document.getElementById("gst_no_err").style.display = "none";
    }
    return validCounter;
  }

  openEmailModal() {
    this.modalService.open(this.emailPopup);
  }

  getCity(event, cityCode = 0) {
    if (this.isedit == 1) {
      this.dataService.getCity(event, this.companyId).subscribe((response) => {
        this.cityStateName = response;
        this.cityStateName.forEach((element) => {
          if (cityCode == element["city_code"]) {
            this.cityOpt = element;
            this.stateOpt = element;
          }
        });
      });
    } else {
      if (event.target.value.length == 6) {
        console.log(event.target.value);
        this.dataService
          .getCity(event.target.value, this.companyId)
          .subscribe((response) => {
            console.log(response);
            this.cityStateName = response;
          });
      }
    }
  }

  getCity1(event, cityid = 0) {
    if (this.isedit == 1) {
      this.dataService.getCity(event, this.companyId).subscribe((response) => {
        this.cityStateName1 = response;
        this.cityStateName1.forEach((element) => {
          if (cityid == element["city_code"]) {
            this.cityOpt1 = element;
            this.stateOpt1 = element;
          }
        });
      });
    } else {
      if (event.target.value.length == 6) {
        this.dataService
          .getCity(event.target.value, this.companyId)
          .subscribe((response) => {
            console.log(response);
            this.cityStateName1 = response;
          });
      }
    }
  }

  changeCity() {
    this.formData.reg_city_id = this.cityOpt["city_code"];
    this.formData.reg_city_name = this.cityOpt["city_name"];
    this.formData.reg_district_id = this.cityOpt["district_code"];
    this.formData.reg_state_id = this.cityOpt["state_code"];
    this.formData.reg_state_name = this.cityOpt["state_name"];
    // console.log(myArray);
    // if (this.communication_address == false) {
    //   alert("communication");
    //   this.formData.communication_city_id = myArray[0];
    //   this.formData.communication_city_name = myArray[1];
    //   this.formData.communication_district_id = myArray[2];
    //   this.formData.communication_state_id = myArray[3];
    //   this.formData.communication_state_name = myArray[4];
    // }
    // console.log(myArray);
  }

  changeCity1() {
    // let myArray = data.split(",");
    this.formData.communication_city_id = this.cityOpt1["city_code"];
    // myArray[0];
    this.formData.communication_city_name = this.cityOpt1["city_name"];
    this.formData.communication_district_id = this.cityOpt1["district_code"];
    this.formData.communication_state_id = this.cityOpt1["state_code"];
    this.formData.communication_state_name = this.cityOpt1["state_name"];

    // this.formData.communication_city_id = myArray[0];
    // this.formData.communication_city_name = myArray[1];
    // this.formData.communication_district_id = myArray[2];
    // this.formData.communication_state_id = myArray[3];
    // this.formData.communication_state_name = myArray[4];
    // console.log(myArray);
  }

  CommunicationAddress(id) {
    console.log(id);
    this.communication_address = id;
  }

  setInsurerName(event) {
    let text = event.target.options[event.target.options.selectedIndex].text;
    this.formData.existing_insurer_name = text;
  }

  requiredValidation(element, errorId) {
    if (errorId != "mobile_err") {
      element = element.trim();
    }
    console.log(element);
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  onlyLettersValidation(element, errorId) {
    let pattern = /^[a-zA-Z]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphabets are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphanumericValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphanumerics are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  emailValidation(element, errorId) {
    let pattern =/^[a-zA-Z0-9]*$/;
     if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML = "Please enter valid email.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  gstValidation(element, errorId) {
    let pattern = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML = "Please enter valid GSTIN.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      // document.getElementById(errorId).innerHTML =
      //   "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  checkmobile(event) {
    let pattern = /^[0-9]*$/;
    let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
    console.log(event.key);
    if (event.key != "Backspace" && event.key != "Tab") {
      if (!pattern.test(event.key)) {
        return false;
      }
    }
  }

  mobile10digits(element, errorId) {
    if (element.split("").length < 10) {
      document.getElementById(errorId).children[0].innerHTML =
        "Please enter 10 digit mobile number.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
