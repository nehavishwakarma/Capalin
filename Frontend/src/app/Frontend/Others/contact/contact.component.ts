import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validator,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"],
})
export class ContactComponent implements OnInit {
  formData;
  issubmitted = false;
  isChecked: boolean = false;
  constructor(private router: Router, private dataService: DataService) {}

  ngOnInit() {
    this.formData = {
      fullName: "",
      companyName: "",
      email: "",
      contact: "",
      message: "",
    };
  }

  submitForm() {
    if (!this.isValid()) {
      return;
    }

    this.formData["fullName"] = this.formData["fullName"].trim();
    this.formData["companyName"] = this.formData["companyName"].trim();
    this.formData["email"] = this.formData["email"].trim();
    this.formData["contact"] = this.formData["contact"].trim();
    this.formData["message"] = this.formData["message"].trim();
    this.showProgressBar();
    this.issubmitted = true;
    this.dataService.contactus(this.formData).subscribe(
      (response) => {
        this.issubmitted = false;
        console.log(response);
        if (response.status == 1) {
          this.successMsg(
            "Message received. We will get back to you soon.Thank you."
          );
          this.clearFormData();
        } else {
          this.hideSuccessMessage();
          alert("something Went Wrong!! Please Try Again.");
        }
        this.hideProgressBar();
        // response.status == 1
        //   ? alert("Message received. We will get back to you soon.Thank you.")
        //   : alert("something Went Wrong!! Please Try Again.");
      },
      (error) => {
        this.hideProgressBar();
        this.issubmitted = false;
        console.log(error);
        alert("something Went Wrong!! Please Try Again.");
      }
    );
  }

  fullNameValidator() {
    return this.requiredValidation(this.formData["fullName"], "fullName_err");
  }

  companyNameValidator() {
    return this.requiredValidation(
      this.formData["companyName"],
      "companyName_err"
    );
  }

  emailValidator() {
    return (
      this.requiredValidation(this.formData["email"], "email_err") &&
      this.emailValidation(this.formData["email"], "email_err")
    );
  }

  contactValidator() {
    return (
      this.requiredValidation(this.formData["contact"], "contact_err") &&
      this.mobile10digits(this.formData["contact"], "contact_err")
    );
  }

  messageValidator() {
    return this.requiredValidation(this.formData["message"], "message_err");
  }

  termsAndConditionValidator() {
    return this.requiredValidationForTermsAndCondition(
      this.isChecked ? "ok" : "",
      "message_err"
    );
  }

  isValid() {
    this.fullNameValidator();
    this.companyNameValidator();
    this.emailValidator();
    this.contactValidator();
    this.messageValidator();
    return (
      this.fullNameValidator() &&
      this.companyNameValidator() &&
      this.emailValidator() &&
      this.contactValidator() &&
      this.messageValidator() &&
      this.termsAndConditionValidator()
    );
  }

  requiredValidation(element, errorId) {
    if (!element.trim()) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  requiredValidationForTermsAndCondition(element, errorId) {
    if (!element.trim()) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).innerHTML =
        "Please indicate that you agree to the Terms and Conditions.";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphaOnly(e) {
    // Accept only alpha numerics, not numbers & special characters
    var regex = new RegExp("^[a-zA-Z ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      return true;
    }

    e.preventDefault();
    return false;
  }

  smallAlphaOnly(e) {
    var regex = new RegExp("^[a-z!@#$%^&*)(+=._-]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      return true;
    }

    e.preventDefault();
    return false;
  }

  onPaste(e) {
    e.preventDefault();
    return false;
  }

  emailValidation(element, errorId) {
    let pattern = /^[a-z]+@[a-z]+\.[a-z]+$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML = "Please enter valid email.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  checkmobile(event) {
    let pattern = /^[0-9]*$/;
    let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
    console.log(event.key);
    if (event.key != "Backspace" && event.key != "Tab") {
      if (!pattern.test(event.key)) {
        return false;
      }
    }
  }

  mobile10digits(element, errorId) {
    if (element.split("").length < 10) {
      document.getElementById(errorId).innerHTML =
        "Please enter 10 digit mobile number.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphanumericValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).innerHTML =
        "Only alphanumerics are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  clearFormData() {
    this.formData = {
      fullName: "",
      companyName: "",
      email: "",
      contact: "",
      message: "",
    };
  }

  successMsg(message) {
    document.getElementById("message_success").style.display = "block";
    document.getElementById("message_success").innerHTML = message;
  }

  hideSuccessMessage() {
    document.getElementById("message_success").style.display = "none";
  }

  showProgressBar() {
    document.getElementById(
      "submitBtn"
    ).innerHTML = `WAIT <i class="fa fa-spinner fa-spin"></i>`;
  }

  hideProgressBar() {
    document.getElementById("submitBtn").innerHTML = `SUBMIT`;
  }
}
