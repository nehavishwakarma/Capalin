import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder,FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-digital-partner',
  templateUrl: './digital-partner.component.html',
  styleUrls: ['./digital-partner.component.scss']
})
export class DigitalPartnerComponent implements OnInit {

registrationForm: FormGroup;
ImagePath: string;
captha:any = Math.random().toString(36).slice(7);
  constructor( private formBuilder: FormBuilder,private http:HttpClient) //private service:DataService) 
  { 
      this.ImagePath = 'assets/home_images/Group-12.png';
  }




  ngOnInit() {
   
    this.registrationForm = this.formBuilder.group({
    firstName : ["",[Validators.required]],
    lastName : ["",[Validators.required]],
    email : ["",[Validators.required, Validators.email]],
    mobile_no : ["",[Validators.required, Validators.pattern('[0-9]{10}$')]],
    password_s : ["",[Validators.required, Validators.minLength(6)]],
    verify_password : ["",[Validators.required]],
    captchCode : ["",[Validators.required]],
  }, {validator: this.mustMatch('password_s', 'verify_password')}
  );

  this.valueChangesCaptcha();

  }//ngonitEnd


  valueChangesCaptcha()
  {
    this.registrationForm.get('captchCode').valueChanges
      .subscribe(enterdValue => 
      {
         if (this.registrationForm.get('captchCode').errors != null)  //if has any error already
          {
            return;
          }
          else if (enterdValue != this.captha)  //if value is not matched
           {
             this.registrationForm.controls['captchCode'].setErrors({notMatched: true});
             return;
           }
         else
           {
             this.registrationForm.controls['captchCode'].setErrors(null);
            }
      });
  }


  mustMatch(controlName: string, matchingControlName: string) 
  {
      return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }
        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
} // fn mustMatch end






onSubmit()
{
  // if has any error
  if(this.registrationForm.invalid)
  {
    return;
  }
  else
  { 
    var formData: any = new FormData();
    formData.append("email_a", this.registrationForm.get('email').value);
    formData.append("firstName_a", this.registrationForm.get('firstName').value);
    formData.append("lastName_a", this.registrationForm.get('lastName').value);
    formData.append("mobile_no_a", this.registrationForm.get('mobile_no').value);
    formData.append("password_a", this.registrationForm.get('password_s').value);
    this.http.post('http://localhost/kaplin/api/registration_api',formData).subscribe(
      function(res){
        console.log(res);
      },
      function(error){
       console.log(error);
      },
	  
      );
  }
} //fn onSubmit end


checkDuplicate(event)
{
 
  let inputFormName = event.currentTarget.attributes[1].nodeValue;
  if (this.registrationForm.get(inputFormName).errors != null)  //if has any error already
    {
      return;
    }
  else
  {
      let inputValue = event.target.value;
      this.http.get(`http://localhost/kaplin/api/checkDuplicate?${inputFormName}=${inputValue}`).subscribe(
      (response) => 
        {
          if (!response)  //if value is taken
            {
              this.registrationForm.controls[inputFormName].setErrors({alreadyTaken: true});
              return;
            }
          else
            {
              this.registrationForm.controls[inputFormName].setErrors(null);
            }
        },
      (error) => console.log(error)
      );//subscribe end
   }
}// fn checkDuplicate finish


refershCapth()
{
  this.captha = Math.random().toString(36).slice(7);
  this.registrationForm.controls['captchCode'].setErrors({notMatched: true});
}






}