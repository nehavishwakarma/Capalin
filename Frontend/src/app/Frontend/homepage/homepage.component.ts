import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.scss"],
})
export class HomepageComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    if (localStorage.getItem("registerFormCar") !== null) {
      localStorage.removeItem("registerFormCar");
    }
    if (localStorage.getItem("proposalFormCar") !== null) {
      localStorage.removeItem("proposalFormCar");
    }
    if (localStorage.getItem("enquiry_id") !== null) {
      localStorage.removeItem("enquiry_id");
    }
    if (localStorage.getItem("healthFormData") !== null) {
      localStorage.removeItem("healthFormData");
    }
    if (localStorage.getItem("healthFormDataInsured") !== null) {
      localStorage.removeItem("healthFormDataInsured");
    }
    if (localStorage.getItem("medicalHistoryFormData") !== null) {
      localStorage.removeItem("medicalHistoryFormData");
    }
    if (localStorage.getItem("Adult_array") !== null) {
      localStorage.removeItem("Adult_array");
    }
    if (localStorage.getItem("Adult_array1") !== null) {
      localStorage.removeItem("Adult_array1");
    }
    if (localStorage.getItem("Child_array") !== null) {
      localStorage.removeItem("Child_array");
    }
    if (localStorage.getItem("Child_array1") !== null) {
      localStorage.removeItem("Child_array1");
    }
    if (localStorage.getItem("proposalFormBike") !== null) {
      localStorage.removeItem("proposalFormBike");
    }
    if (localStorage.getItem("registerFormBike") !== null) {
      localStorage.removeItem("registerFormBike");
    }
  }
}
