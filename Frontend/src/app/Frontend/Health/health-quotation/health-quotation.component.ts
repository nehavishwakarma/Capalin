import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { saveAs } from 'file-saver';



@Component({
  selector: "app-health-quotation",
  templateUrl: "./health-quotation.component.html",
  styleUrls: ["./health-quotation.component.scss"],
})
export class HealthQuotationComponent implements OnInit {
  baseApiUrl = environment.apiUrl
  itemsPerPage = 10;
  p: number = 1;
  orderby = "company_name";
  reverseFlag = false;
  dataArray: any; 
  constructor(private router: Router, private dataService: DataService, private modalService: NgbModal, private http: HttpClient) { }
  enquiry_id;
  healthData: any;
  insurerData;
  quotesData = [];
  leadId;
  mastersheetId;
  premium;
  sum_assured;
  area_code;
  dataEnquiry;
  ages = [];
  enquiryAge;
  enquiryCover;
  enquiryMembers;
  // healthEnquiryId:any;

  compare_data = [];
  footerstatus = false;

  demoArray = [1, 2, 3, 4]

  ngOnInit() {
    localStorage.setItem("proposal_id", "0");
    this.enquiry_id = localStorage.getItem("enquiry_id");

    // this.dataService
    //   .getHelthEnquiryDetails(this.enquiry_id)
    //   .subscribe((response) => {
    //     console.log(response);
    //     if (response) {
    //       this.dataEnquiry = response[0];
    //       this.setValues(response[0]);
    //     }
    //   });

    this.dataService
      .getHealthInsurerApi(this.enquiry_id)
      .subscribe((response) => {
        console.log(response);
        this.healthData = response;
        this.quotesData = this.healthData.healthQuotes;
        //for sorting we need numeric data
        this.quotesData.forEach((element, index) => {
          this.quotesData[index]["sumAssuredNumeric"] = +element["sum_assured"];
        });
        this.enquiryAge = this.healthData["age"];
        this.enquiryCover = `${this.healthData["min_insurance_cover"]}-${this.healthData["max_insurance_cover"]}`;
        this.enquiryMembers = `${this.healthData["adults"]},${this.healthData["children"]}`;
        localStorage.setItem(
          "health_enquiry_id",
          JSON.stringify(this.healthData)
        );
        let tempData = localStorage.getItem("health_enquiry_id");
        tempData = JSON.parse(tempData);
        this.leadId = tempData["lead_id"];
      });

    for (let index = 18; index < 90; index++) {
      this.ages.push(index);
    }

  }

  editHealthQuotes() {
    let data = {
      min_insurance_cover: this.enquiryCover.split("-")[0],
      max_insurance_cover: this.enquiryCover.split("-")[1],
      adults: this.enquiryMembers.split(",")[0],
      children: this.enquiryMembers.split(",")[1],
      age: this.enquiryAge,
      enquiry_id: this.enquiry_id,
    };
    this.dataService.editHealhQuotes(data).subscribe((response) => {
      this.healthData = response;
      this.quotesData = this.healthData.healthQuotes;
      this.quotesData.forEach((element, index) => {
        this.quotesData[index]["sumAssuredNumeric"] = +element["sum_assured"];
      });
      this.enquiryAge = this.healthData["age"];
      this.enquiryCover = `${this.healthData["min_insurance_cover"]}-${this.healthData["max_insurance_cover"]}`;
      this.enquiryMembers = `${this.healthData["adults"]},${this.healthData["children"]}`;
      localStorage.setItem(
        "health_enquiry_id",
        JSON.stringify(this.healthData)
      );
      let tempData = localStorage.getItem("health_enquiry_id");
      tempData = JSON.parse(tempData);
      this.leadId = tempData["lead_id"];
      if (localStorage.getItem("Adult_array") !== null) {
        localStorage.removeItem("Adult_array");
      }
      if (localStorage.getItem("Adult_array1") !== null) {
        localStorage.removeItem("Adult_array1");
      }
      if (localStorage.getItem("Child_array1") !== null) {
        localStorage.removeItem("Child_array1");
      }
      if (localStorage.getItem("medicalHistoryFormData") !== null) {
        localStorage.removeItem("medicalHistoryFormData");
      }
    });
  }

  setOrderBy(orderVal) {
    this.orderby = orderVal;
    if (orderVal == "premium_with_tax_true") {
      this.reverseFlag = true;
      this.orderby = "premium_with_tax";
    } else {
      this.reverseFlag = false;
    }
  }
  redirectToEdit() {
    this.router.navigate(["/health"]);
  }

  bookNow(
    companyId,
    leadId,
    mastersheetId,
    sum_assured,
    premium,
    area_code,
    health_enquiry_id
  ) {
    localStorage.setItem("master_sheet_id", mastersheetId);
    const dataArray = {
      leadId: leadId,
      mastersheetId: mastersheetId,
      premium: premium,
      sum_assured: sum_assured,
      area_code: area_code,
      health_enquiry_id: health_enquiry_id,
    };
    this.dataService
      .bookNow(dataArray, leadId, mastersheetId, premium, health_enquiry_id)
      .subscribe((response) => {
        if (response["status"] == "Success") {
          this.router.navigate(["health/proposal-details"], {
            queryParams: { id: this.enquiry_id, propId: 0, compId: companyId },
          });
        }
        // this.router.navigate(["health/proposal/" + this.enquiry_id + "/0"]);
        // // this.router.navigate(["health/proposal/", this.enquiry_id, 0]);
      });
  }

  openScrollableContent(longContent) {
    this.modalService.open(longContent, { scrollable: true, size: 'lg' });
  }

  checkBoxBottonClick(value, event) {
    if (this.compare_data.includes(value)) {
      this.compare_data.splice(this.compare_data.indexOf(value), 1)
      if (this.compare_data.length == 0) {
        this.footerstatus = false;
      }
      // console.log(this.compare_data.indexOf(value))
    }
    else {
      if (this.compare_data.length != 5) {
        this.compare_data.push(value)
        this.footerstatus = true;
        console.log(this.compare_data)
      } else {
        event.source.checked = false;
      }
    }
  }



  downloadHealthComparePdf(compare_data) {
    this.dataArray = []
    this.dataArray.push({ selectedPlan: compare_data });
    this.dataService
      .downloadHealthComparePdf(this.dataArray, this.enquiry_id)
      .subscribe((response) => {
        var blob = new Blob([response], { type: 'application/pdf' });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        console.log(window.URL.createObjectURL(blob))
        var date = new Date();
        link.download = 'L-_Health_Plan_Comparison_' + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '.pdf';
        link.click();
        // saveAs(blob, 'hello')
      }, err => {
        var blob = new Blob([err.error.text], { type: 'application/pdf' });
        saveAs(blob, 'test')
      })
  }
}
