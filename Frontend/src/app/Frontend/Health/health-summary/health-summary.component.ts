import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import jspdf from "jspdf";
import html2canvas from "html2canvas";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-health-summary",
  templateUrl: "./health-summary.component.html",
  styleUrls: ["./health-summary.component.scss"],
})
export class HealthSummaryComponent implements OnInit {
  @ViewChild("emailModal", { static: false }) emailPopup: ElementRef;
  proposal_id;
  enquiry_id;
  proposal_summary;
  medical_questions_history;
  medical_sub_questions_history;
  medical_sub_form_history;
  master_sheet_id;
  insured_members_array;
  proposal_form;
  health_enquiry_id;
  health_enquiry_details;
  company_id;
  payment_status;
  isChecked: any;
  star_social_status_info;
  mail_success = null;
  valid_email = true;
  saveQuotePayLaterForm;
  newquehistory = [];
  uniquQArr = [];
  sameQArr = [];
  medicalHistoryFormData = {};
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  Adult_array1 = [];
  Child_array1 = [];
  companyId;
  proposalDob;
  questionCount = {};
  submitted = false;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute
  ) {
    this.saveQuotePayLaterForm = {
      email: "",
    };
  }

  ngOnInit() {
    this.isChecked = "true";
    this.star_social_status_info = {
      bpl: "",
      handicaped: "",
      informal: "",
      unorganized: "",
    };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["proposal_id"];
      this.enquiry_id = params["enquiry_id"];
      this.companyId = params["compId"];
    });
    if (this.proposal_id && this.enquiry_id) {
      this.dataService
        .getHealthProposalSummary(this.enquiry_id, this.proposal_id)
        .subscribe((response) => {
          console.log(response);
          if (response["health_enquiry_details"]["status"] == "Success") {
            this.health_enquiry_details =
              response["health_enquiry_details"]["enquiry_details"][0];
            this.company_id = this.health_enquiry_details["company_id"];
            this.health_enquiry_id = this.health_enquiry_details["enquiry_id"];
          }

          this.insured_members_array =
            response["health_insured_members_details"];
          if (response["health_proposal_details"]["status"] == "Success") {
            this.proposal_form =
              response["health_proposal_details"]["health_proposal_details"][0];
            this.proposalDob = new Date(this.proposal_form["dob"]);
            // console.log(this.proposal_form);
            this.saveQuotePayLaterForm.email = this.proposal_form["email"];
            this.payment_status = this.proposal_form["payment_status"];
            if (this.proposal_form.star_social_status_info != null) {
              let social_status_info =
                this.proposal_form.star_social_status_info.split(",");
              this.star_social_status_info["bpl"] = social_status_info[0];
              this.star_social_status_info["handicaped"] =
                social_status_info[1];
              this.star_social_status_info["informal"] = social_status_info[2];
              this.star_social_status_info["unorganized"] =
                social_status_info[3];
            }
          }

          this.medical_questions_history =
            response["medical_questions_history"];
          this.medical_sub_questions_history =
            response["medical_sub_questions_history"];
          this.medical_sub_form_history =
            response["medical_sub_questions_form_history"];
          this.medical_sub_questions_history.forEach((element) => {
            if (this.questionCount[element["question_id"]] == undefined) {
              this.questionCount[element["question_id"]] = 1;
            } else {
              this.questionCount[element["question_id"]] =
                this.questionCount[element["question_id"]] + 1;
            }
          });

          console.log(this.questionCount);
        });
    }
  }

  openEmailModal() {
    this.modalService.open(this.emailPopup);
    // this.mail_success = null;
    // // $('#emailModal').modal('show');
    // this.displayModalEmail = "block";
  }

  redirectToEdit() {
    // this.router.navigate([
    //   "health/proposal/" +
    //     this.health_enquiry_id +
    //     "/" +
    //     this.proposal_form[0]["id"],
    // ]);
    this.router.navigate(["health/proposal"], {
      queryParams: {
        id: this.health_enquiry_id,
        propId: this.proposal_form["id"],
      },
    });
    // window.location.href = API_URL + "health-proposal-form/" + this.health_enquiry_id + '/' + this.proposal_form[0]['id'];
  }

  saveQuotePayLater() {
    let dataObj = [];
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(this.saveQuotePayLaterForm.email).toLowerCase())) {
      this.valid_email = false;
      return false;
    } else {
      this.valid_email = true;
    }
    dataObj.push(
      this.proposal_form,
      this.health_enquiry_details,
      this.saveQuotePayLaterForm.email,
      3 //health
    );
    // console.log(dataObj);
    this.dataService
      .healthProposalQuoteSaveEmail(dataObj)
      .subscribe((response) => {
        // console.log(response);
        response.status == "Success"
          ? (this.mail_success = true)
          : (this.mail_success = false);
      });
  }

  downloadpdf() {
    window.scrollTo(0, 0);
    // alert(this.health_enquiry_id);
    // alert(this.proposal_form[0]["id"]);
    this.convetToPDF();
    // let dataObj = {
    //   enqId: this.health_enquiry_id,
    //   propId: this.proposal_form[0]["id"],
    // };
    // this.dataService.downloadPdf(dataObj).subscribe((response) => {
    //   console.log(response);

    // });
  }

  public convetToPDF() {
    // this.submitted = true;
    document.getElementById("savePdf").innerHTML =
      "Please Wait <i class='fa fa-spinner fa-spin'></i>";
    var data = document.getElementById("maindiv");
    html2canvas(data).then((canvas) => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = (canvas.height * imgWidth) / canvas.width + 20;
      // var imgHeight = canvas.height;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jspdf("p", "mm", "a4"); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;
      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        pdf.addPage();
        pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      // this.submitted = false;
      pdf.save("Health.pdf"); // Generated PDF
      document.getElementById("savePdf").innerHTML = "Save as PDF";
    });
  }

  goToEdit(goto, isParam) {
    if (isParam == 1) {
      this.router.navigate([`health/${goto}`], {
        queryParams: {
          id: this.enquiry_id,
          propId: 0,
          compId: this.companyId,
        },
      });
    } else if (goto == "health") {
      this.router.navigate([`health`]);
    } else {
      this.router.navigate([`health/${goto}`]);
    }
  }
}
