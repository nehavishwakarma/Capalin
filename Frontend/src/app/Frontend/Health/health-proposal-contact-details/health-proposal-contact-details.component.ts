import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-health-proposal-contact-details",
  templateUrl: "./health-proposal-contact-details.component.html",
  styleUrls: ["./health-proposal-contact-details.component.scss"],
})
export class HealthProposalContactDetailsComponent implements OnInit {
  active = 1;
  stateName = [];
  valueOption: any;
  model;
  apollo_profession;
  registrationForm: FormGroup;
  companyId;
  enquiry_id;
  proposal_id;
  proposal_relation;
  formData: any;
  formData1: any;
  master_sheet_id: any;
  health_proposal_questions = [];
  health_proposal_sub_questions = [];
  proposalIndex;
  adults = 0;
  children = 0;
  cityStateName;
  Adult_array: any;
  Child_array: any;
  Adult_array1: any;
  Child_array1: any;
  insured_member_form: any;
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  medicalHistoryFormData = {};
  medicalHistoryFormDataEdit = {};
  // insuredIds = [];
  proposalFormEdit = "";
  isedit = 0;
  medical_questions_history;
  medical_sub_questions_history;
  insured_members_arrayEdit;
  adultsEdit = [];
  cityOpt;
  stateOpt;
  enquiryDetails;
  gst;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.formData = JSON.parse(localStorage.getItem("healthFormData"));
    this.insured_member_form = JSON.parse(
      localStorage.getItem("healthFormDataInsured")
    );

    this.master_sheet_id = { master_sheet_id: 0 };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
    });

    this.dataService
      .proposalIndex(this.enquiry_id, this.proposal_id)
      .subscribe((res) => {
        if (res.status == "success") {
          this.gst = res.response.health_enquiry_details.gst;
          // this.proposalIndex = res.proposalIndex;
          this.enquiryDetails =
            res.response.health_enquiry_details.enquiry_details[0];
          this.proposalIndex = res.proposalIndex;
          this.adults =
            res.response.health_enquiry_details.enquiry_details[0].adults;
          this.children =
            res.response.health_enquiry_details.enquiry_details[0].children;
          // this.getProposalRelation([
          //   res.response.health_enquiry_details.enquiry_details[0]
          //     .master_sheet_id,
          // ]);
          this.companyId =
            res.response.health_enquiry_details.enquiry_details[0].company_id;
          this.master_sheet_id.master_sheet_id =
            res.response.health_enquiry_details.enquiry_details[0].master_sheet_id;
          if (this.formData["pincode_id"] != "") {
            this.getHealthCity(this.formData["pincode_id"], 1);
          }
        } else {
          alert("Something Went Wrong!!");
        }
      });
  }

  redirect() {
    this.router.navigate(["health/proposal-insured-members"], {
      queryParams: { id: this.enquiry_id, propId: 0, compId: this.companyId },
    });
  }

  getHealthCity(event, editOrNot) {
    if (editOrNot == 0) {
      if (event.target.value.length == 6) {
        const data = { company_id: this.companyId };
        this.dataService
          .getHealthCity(event.target.value, this.companyId)
          .subscribe((response) => {
            this.cityStateName = response.get_state;
          });
      }
    } else {
      console.log(event);
      console.log(this.companyId);
      this.dataService
        .getHealthCity(event, this.companyId)
        .subscribe((response) => {
          this.cityStateName = response.get_state;
          this.cityStateName.forEach((city) => {
            if (city.id == this.formData["city_id"]) {
              this.cityOpt = city;
              this.stateOpt = city;
            }
          });
        });
    }
  }

  changeHealthCity() {
    this.formData.city_id = this.cityOpt["city_code"];
    this.formData.city_name = this.cityOpt["city"];
    this.formData.district_id = this.cityOpt["district_code"];
    this.formData.state_id = this.cityOpt["state_code"];
    this.formData.state_name = this.cityOpt["state_name"];
    this.stateName = [];
    this.stateName.push(this.cityOpt["state_name"]); 
  }

  submitform() {
    let validCounter = this.onchangeValidations();

    if (validCounter !== 0) {
      return false;
    }
    if (localStorage.getItem("healthFormData") !== null) {
      localStorage.removeItem("healthFormData");
    }
    localStorage.setItem("healthFormData", JSON.stringify(this.formData));
    console.log(this.formData);
    let modDob = "";
    modDob += `${this.formData.dob["year"]}-`;
    if (this.formData.dob["month"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["month"]}-`;
    } else {
      modDob += `${this.formData.dob["month"]}-`;
    }
    if (this.formData.dob["day"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["day"]}`;
    } else {
      modDob += `${this.formData.dob["day"]}`;
    }
    this.formData.dob = modDob;

    console.log(this.formData);

    // return false;
    this.formData.health_enquiry_id = this.enquiry_id;
    this.dataService.healthProposalFormSave(this.formData).subscribe(
      (response) => {
        console.log(response);
        if (this.isedit == 0) {
          // this.insured_member_form.proposal_id = response.id;
          // this.formData.id = response.id;
        }
        this.router.navigate(["health/proposal-medical-history"], {
          queryParams: {
            id: this.enquiry_id,
            propId: 0,
            compId: this.companyId,
          },
        });
      },
      (error) => {
        alert("Something Went Wrong!!");
        console.error(error);
      }
    );
  }

  onchangeValidations() {
    let validCounter = 0;
    if (
      this.requiredValidation(this.formData["address"], "address_err") == false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(this.formData["address"], "address_err") ==
      false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["area_locality"],
        "area_locality_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(
        this.formData["area_locality"],
        "area_locality_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(this.formData["landmark"], "landmark_err") ==
      false
    ) {
      validCounter++;
    } else if (
      this.alphanumericValidation(this.formData["landmark"], "landmark_err") ==
      false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(this.formData["pincode_id"], "pincode_id_err") ==
      false
    ) {
      validCounter++;
    }
    // if (
    //   this.requiredValidation(this.formData["city_id"], "city_err") == false
    // ) {
    //   validCounter++;
    // }
    if (this.requiredValidation(this.stateOpt, "state_err") == false) {
      validCounter++;
    }
    if (
      this.requiredValidation(this.formData["mobile"], "mobile_err") == false
    ) {
      validCounter++;
    } else if (
      this.mobile10digits(this.formData["mobile"], "mobile_err") == false
    ) {
      validCounter++;
    }
    // if (this.requiredValidation(this.formData["email"], "email_err") == false) {
    //   validCounter++;
    // } else if (
    //   this.emailValidation(this.formData["email"], "email_err") == false
    // ) {
    //   validCounter++;
    // }
    // if (
    //   this.formData["alt_mobile"] != "" &&
    //   this.requiredValidation(this.formData["alt_mobile"], "alt_mobile_err") ==
    //     false
    // ) {
    //   validCounter++;
    // } else if (
    //   this.formData["alt_mobile"] != "" &&
    //   this.mobile10digits(this.formData["alt_mobile"], "alt_mobile_err") ==
    //     false
    // ) {
    //   validCounter++;
    // }
    return validCounter;
  }

  requiredValidation(element, errorId) {
    // if (errorId != "dob_err") {
    //   element = element.trim();
    // }
    if (!element) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  alphanumericValidation(element, errorId) {
    let pattern = /^[a-zA-Z0-9]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).children[0].innerHTML =
        "Only alphanumerics are allowed.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  checkNumbers(event) {
    let pattern = /^[0-9]*$/;
    // let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
    console.log(event.key);
    if (
      event.key != "Backspace" &&
      event.key != "Tab" &&
      event.key != "ArrowLeft" &&
      event.key != "ArrowRight"
    ) {
      if (!pattern.test(event.key)) {
        return false;
      }
    }
  }

  emailValidation(element, errorId) {
    let pattern =
    /^[a-zA-Z0-9]*$/;
        if (!pattern.test(element.trim())) {
      document.getElementById(errorId).children[0].innerHTML =
        "Please enter valid email.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  mobile10digits(element, errorId) {
    if (element.split("").length < 10) {
      document.getElementById(errorId).children[0].innerHTML =
        "Please enter 10 digit mobile number.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  checklength(event) {
    if (event.target.value.split("").length > 5 && event.key != "Backspace") {
      return false;
    }
  }
}
