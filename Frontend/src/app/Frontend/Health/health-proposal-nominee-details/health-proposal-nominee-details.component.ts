import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";
// import { Console } from "console";
@Component({
  selector: "app-health-proposal-nominee-details",
  templateUrl: "./health-proposal-nominee-details.component.html",
  styleUrls: ["./health-proposal-nominee-details.component.scss"],
})
export class HealthProposalNomineeDetailsComponent implements OnInit {
  active = 1;
  valueOption: any;
  model;
  apollo_profession;
  registrationForm: FormGroup;
  companyId;
  enquiry_id;
  proposal_id;
  proposal_relation;
  formData: any;
  formData1: any;
  master_sheet_id: any;
  health_proposal_questions = [];
  health_proposal_sub_questions = [];
  proposalIndex;
  adults = 0;
  children = 0;
  cityStateName;
  Adult_array: any;
  Child_array: any;
  Adult_array1: any;
  Child_array1: any;
  insured_member_form: any;
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  medicalHistoryFormData = {};
  medicalHistoryFormDataEdit = {};
  // insuredIds = [];
  proposalFormEdit = "";
  isedit = 0;
  medical_questions_history;
  medical_sub_questions_history;
  insured_members_arrayEdit;
  adultsEdit = [];
  cityOpt;
  stateOpt;
  enquiryDetails;
  gst;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.formData = JSON.parse(localStorage.getItem("healthFormData"));
    this.insured_member_form = JSON.parse(
      localStorage.getItem("healthFormDataInsured")
    );

    this.master_sheet_id = { master_sheet_id: 0 };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
    });

    this.dataService
      .proposalIndex(this.enquiry_id, this.proposal_id)
      .subscribe((res) => {
        if (res.status == "success") {
          this.gst = res.response.health_enquiry_details.gst;
          // this.proposalIndex = res.proposalIndex;
          this.enquiryDetails =
            res.response.health_enquiry_details.enquiry_details[0];
          // this.proposalIndex = res.proposalIndex;
          // this.adults =
          //   res.response.health_enquiry_details.enquiry_details[0].adults;
          // this.children =
          //   res.response.health_enquiry_details.enquiry_details[0].children;
          // this.getProposalRelation([
          //   res.response.health_enquiry_details.enquiry_details[0]
          //     .master_sheet_id,
          // ]);
          this.companyId =
            res.response.health_enquiry_details.enquiry_details[0].company_id;
          this.master_sheet_id.master_sheet_id =
            res.response.health_enquiry_details.enquiry_details[0].master_sheet_id;
          // this.medical_questions_history = res.response.medical_questions_history;
          // this.medical_sub_questions_history =
          //   res.response.medical_sub_questions_history;
          // this.insured_members_arrayEdit =
          //   res.response.health_insured_members_details;
          // const data = { company_id: this.companyId };
          // this.dataService.apolloProfession(data).subscribe((response) => {
          //   this.apollo_profession = response.apollo_occupation;
          // });
          // this.Adult_array = [];
          // for (let index = 0; index < this.adults; index++) {
          //   this.Adult_array.push({
          //     id: "",
          //     relationship: "",
          //     i_first_name: "",
          //     i_middle_name: "",
          //     i_last_name: "",
          //     dob: "",
          //     gender: "",
          //     height_feet: "",
          //     height_inch: "",
          //     weight: "",
          //     insured_type: "",
          //     i_marital_status: "",
          //     insured_occupation: "",
          //     i_mobile: "",
          //   });
          // }
          // this.Child_array = [];
          // for (let index = 0; index < this.children; index++) {
          //   this.Child_array.push({
          //     id: "",
          //     relationship: "",
          //     i_first_name: "",
          //     i_middle_name: "",
          //     i_last_name: "",
          //     dob: "",
          //     gender: "",
          //     height_feet: "",
          //     height_inch: "",
          //     weight: "",
          //     insured_type: "",
          //     i_marital_status: "",
          //     insured_occupation: "",
          //     i_mobile: "",
          //   });
          // }

          // if (localStorage.getItem("Adult_array") !== null) {
          //   let preAdultData = JSON.parse(localStorage.getItem("Adult_array"));
          //   this.Adult_array.forEach((element, index) => {
          //     this.Adult_array[index] = preAdultData[index];
          //   });
          // }
          // if (localStorage.getItem("Child_array") !== null) {
          //   let preChildData = JSON.parse(localStorage.getItem("Child_array"));
          //   this.Child_array.forEach((element, index) => {
          //     this.Child_array[index] = preChildData[index];
          //   });
          // }
        } else {
          alert("Something Went Wrong!!");
        }
      });
  }

  submitform() {
    let validCounter = this.onchangeValidations();

    if (validCounter !== 0) {
      return false;
    }
    if (localStorage.getItem("healthFormData") !== null) {
      localStorage.removeItem("healthFormData");
    }
    localStorage.setItem("healthFormData", JSON.stringify(this.formData));
    let modDob = "";
    modDob += `${this.formData.dob["year"]}-`;
    if (this.formData.dob["month"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["month"]}-`;
    } else {
      modDob += `${this.formData.dob["month"]}-`;
    }
    if (this.formData.dob["day"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["day"]}`;
    } else {
      modDob += `${this.formData.dob["day"]}`;
    }
    this.formData.dob = modDob;

    console.log(this.formData);

    // return false;
    this.formData.health_enquiry_id = this.enquiry_id;
    this.dataService.healthProposalFormSave(this.formData).subscribe(
      (response) => {
        console.log(response.id);
        if (this.isedit == 0) {
          // this.insured_member_form.proposal_id = response.id;
          // this.formData.id = response.id;
        }
        this.router.navigate(["health/summary"], {
          queryParams: {
            enquiry_id: this.enquiry_id,
            proposal_id: response.id,
            compId: this.companyId,
          },
        });
      },
      (error) => {
        alert("Something Went Wrong!!");
        console.error(error);
      }
    );
  }

  back() {
    this.router.navigate(["health/proposal-medical-history"], {
      queryParams: { id: this.enquiry_id, propId: 0 },
    });
  }

  onchangeValidations() {
    let validCounter = 0;
    if (
      this.requiredValidation(this.formData["Existing"], "Existing_err") ==
      false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["nominee_relationship"],
        "nominee_relationship_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["nominee_name"],
        "nominee_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(
        this.formData["nominee_name"],
        "nominee_name_err"
      ) == false
    ) {
      validCounter++;
    }

    return validCounter;
  }

  requiredValidation(element, errorId) {
    // if (errorId != "dob_err") {
    //   element = element.trim();
    // }
    if (!element.trim()) {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  onlyLettersValidation(element, errorId) {
    let pattern = /^[a-zA-Z\s]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Only alphabets are allowed.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
