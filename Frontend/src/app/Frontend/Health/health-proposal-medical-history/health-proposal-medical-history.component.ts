import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-health-proposal-medical-history",
  templateUrl: "./health-proposal-medical-history.component.html",
  styleUrls: ["./health-proposal-medical-history.component.scss"],
})
export class HealthProposalMedicalHistoryComponent implements OnInit {
  active = 1;
  valueOption: any;
  model;
  apollo_profession;
  registrationForm: FormGroup;
  companyId;
  enquiry_id;
  proposal_id;
  proposal_relation;
  formData: any;
  formData1: any;
  master_sheet_id: any;
  health_proposal_questions = [];
  health_proposal_sub_questions = [];
  proposalIndex;
  adults = 0;
  children = 0;
  cityStateName;
  Adult_array: any;
  Child_array: any;
  Adult_array1: any;
  Child_array1: any;
  insured_member_form: any;
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  medicalHistoryFormData = {};
  medicalHistoryFormDataEdit = {};
  // insuredIds = [];
  proposalFormEdit = "";
  isedit = 0;
  medical_questions_history;
  medical_sub_questions_history;
  insured_members_arrayEdit;
  adultsEdit = [];
  cityOpt;
  stateOpt;
  enquiryDetails;
  gst;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    // this.formData1 = {
    //   Diabetes: "0",
    //   Thyroid: "0",
    //   Nervous: "0",
    //   Respiratory: "0",
    //   Heart: "0",
    //   stomach: "0",
    //   Spine: "0",
    //   Tumour: "0",
    //   ongoing: "0",
    //   surgical: "0",
    //   pregnant: "0",
    //   LiquorPeg: "0",
    //   Beer: "0",
    //   Wine: "0",
    //   smoke: "0",
    //   gutkha: "0",
    // };

    this.formData = JSON.parse(localStorage.getItem("healthFormData"));
    this.insured_member_form = JSON.parse(
      localStorage.getItem("healthFormDataInsured")
    );

    if (localStorage.getItem("medicalHistoryFormData") !== null) {
      let preMedData = JSON.parse(
        localStorage.getItem("medicalHistoryFormData")
      );
      console.log(preMedData);
      this.medicalHistoryFormData = preMedData[0];
      this.selectedOption = preMedData[1];
      this.selectedAnsFromQuestions = preMedData[2];
      this.selectedOptionFromQuestions = preMedData[3];
      this.selectedAnsFromChild = preMedData[4];
      this.selectedOptionFromChild = preMedData[5];
    }
    console.log(JSON.parse(localStorage.getItem("Adult_array")));
    this.master_sheet_id = { master_sheet_id: 0 };
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
    });

    this.dataService
      .proposalIndex(this.enquiry_id, this.proposal_id)
      .subscribe((res) => {
        if (res.status == "success") {
          this.gst = res.response.health_enquiry_details.gst;
          // this.proposalIndex = res.proposalIndex;
          this.enquiryDetails =
            res.response.health_enquiry_details.enquiry_details[0];
          this.proposalIndex = res.proposalIndex;
          this.adults =
            res.response.health_enquiry_details.enquiry_details[0].adults;
          this.children =
            res.response.health_enquiry_details.enquiry_details[0].children;
          // this.getProposalRelation([
          //   res.response.health_enquiry_details.enquiry_details[0]
          //     .master_sheet_id,
          // ]);
          this.companyId =
            res.response.health_enquiry_details.enquiry_details[0].company_id;
          this.master_sheet_id.master_sheet_id =
            res.response.health_enquiry_details.enquiry_details[0].master_sheet_id;

          this.getMedicalDetails();

          this.medical_questions_history =
            res.response.medical_questions_history;
          this.medical_sub_questions_history =
            res.response.medical_sub_questions_history;
          this.insured_members_arrayEdit =
            res.response.health_insured_members_details;
          // const data = { company_id: this.companyId };
          // this.dataService.apolloProfession(data).subscribe((response) => {
          //   this.apollo_profession = response.apollo_occupation;
          // });
          this.Adult_array = [];
          for (let index = 0; index < this.adults; index++) {
            this.Adult_array.push({
              id: "",
              relationship: "",
              i_first_name: "",
              i_middle_name: "",
              i_last_name: "",
              dob: "",
              gender: "",
              height_feet: "",
              height_inch: "",
              weight: "",
              insured_type: "",
              i_marital_status: "",
              insured_occupation: "",
              i_mobile: "",
            });
          }
          this.Child_array = [];
          for (let index = 0; index < this.children; index++) {
            console.log(index);
            this.Child_array.push({
              id: "",
              relationship: "",
              i_first_name: "",
              i_middle_name: "",
              i_last_name: "",
              dob: "",
              gender: "",
              height_feet: "",
              height_inch: "",
              weight: "",
              insured_type: "",
              i_marital_status: "",
              insured_occupation: "",
              i_mobile: "",
            });
          }

          this.Adult_array1 = [];
          this.Child_array1 = [];
          let adultCounter = 0;
          let childCounter = 0;
          if (
            localStorage.getItem("Adult_array") &&
            JSON.parse(localStorage.getItem("Adult_array")).length > 0
          ) {
            let preAdultData = JSON.parse(localStorage.getItem("Adult_array"));
            this.Adult_array.forEach((element, index) => {
              this.Adult_array[index] = preAdultData[index];
            });
            this.Adult_array1 = JSON.parse(
              localStorage.getItem("Adult_array1")
            );
          }
          if (
            localStorage.getItem("Child_array") &&
            JSON.parse(localStorage.getItem("Child_array")).length > 0
          ) {
            let preChildData = JSON.parse(localStorage.getItem("Child_array"));
            this.Child_array.forEach((element, index) => {
              this.Child_array[index] = preChildData[index];
            });
            this.Child_array1 = JSON.parse(
              localStorage.getItem("Child_array1")
            );
          }
        } else {
          alert("Something Went Wrong!!");
        }
      });
  }

  getMedicalDetails() {
    this.dataService
      .healthQuestions(this.master_sheet_id)
      .subscribe((response) => {
        this.health_proposal_questions = response.health_proposal_questions;
        this.health_proposal_sub_questions =
          response.health_proposal_sub_questions;
        this.health_proposal_questions.forEach((element, index) => {
          this.health_proposal_questions[index]["sub_question"] = [];
          // this.Adult_array1['healthQ'].push(element.id);
          this.health_proposal_sub_questions.forEach((element2, index2) => {
            if (element.id == element2.health_questions_id) {
              this.health_proposal_questions[index]["sub_question"].push(
                element2
              );
            }
          });
        });

        console.log(this.health_proposal_questions);
      });
  }

  createadultsChildren(id, mode, i) {
    if (this.medicalHistoryFormData["mainQues"] == undefined) {
      // this.medicalHistoryFormData[this.enquiry_id] = {};
      this.medicalHistoryFormData["mainQues"] = {};
      this.medicalHistoryFormData["subQues"] = {};
      this.medicalHistoryFormData["subQuesForm"] = {};
      this.medicalHistoryFormData["enquiryId"] = this.enquiry_id;
    }
    this.medicalHistoryFormData["mainQues"][id] = mode;
    console.log(this.medicalHistoryFormData);

    if (mode == "Yes") {
      if (!this.selectedOption.includes(id)) {
        this.selectedOption.push(id);
      } else {
      }
    } else {
      if (this.selectedOption.includes(id)) {
        const index = this.selectedOption.indexOf(id);
        this.selectedOption.splice(index, 1);
      }
      if (this.medicalHistoryFormData["subQues"] != undefined) {
        this.Adult_array1.forEach((element) => {
          for (const i in this.medicalHistoryFormData["subQues"][element]) {
            if (i == id) {
              delete this.medicalHistoryFormData["subQues"][element][id];
            }
          }
        });
        this.Child_array1.forEach((element) => {
          for (const i in this.medicalHistoryFormData["subQues"][element]) {
            if (i == id) {
              delete this.medicalHistoryFormData["subQues"][element][id];
            }
          }
        });
      }
      //for sub form
      // if (this.medicalHistoryFormData["subQuesForm"] != undefined) {
      //   console.log(id);
      //   console.log(i);
      //   console.log(this.health_proposal_questions);
      //   if (this.health_proposal_questions[i]["sub_question"].length > 0) {
      //     let sub_id =
      //       this.health_proposal_questions[i]["sub_question"][0]["id"];
      //     this.Adult_array1.forEach((element) => {
      //       // console.log(element);
      //       for (const i in this.medicalHistoryFormData["subQuesForm"][
      //         element
      //       ]) {
      //         // console.log(`${i}****${sub_id}`);
      //         if (i == sub_id) {
      //           delete this.medicalHistoryFormData["subQuesForm"][element][
      //             sub_id
      //           ];
      //         }
      //         if (
      //           Object.keys(this.medicalHistoryFormData["subQuesForm"][element])
      //             .length == 0
      //         ) {
      //           delete this.medicalHistoryFormData["subQuesForm"][element];
      //         }
      //       }
      //     });
      //     this.Child_array1.forEach((element) => {
      //       for (const i in this.medicalHistoryFormData["subQues"][element]) {
      //         if (i == sub_id) {
      //           delete this.medicalHistoryFormData["subQuesForm"][element][
      //             sub_id
      //           ];
      //         }
      //         if (
      //           Object.keys(this.medicalHistoryFormData["subQuesForm"][element])
      //             .length == 0
      //         ) {
      //           delete this.medicalHistoryFormData["subQuesForm"][element];
      //         }
      //       }
      //     });
      //   }
      // }
    }
  }

  sunquestions(id, mode, i, a) {
    // console.log(this.medicalHistoryFormData);
    let insuredId = this.Adult_array1[a];
    // return false;
    let subQus = this.health_proposal_questions[i]["sub_question"];
    if (this.medicalHistoryFormData["subQues"][insuredId] == undefined) {
      this.medicalHistoryFormData["subQues"][insuredId] = {};
      // this.medicalHistoryFormData["subQues"][insuredId][id] = 'No';
    }
    this.medicalHistoryFormData["subQues"][insuredId][id] = mode;

    this.Adult_array1.forEach((element, aindex) => {
      if (this.medicalHistoryFormData["subQues"][element] == undefined) {
        this.medicalHistoryFormData["subQues"][element] = {};
        this.medicalHistoryFormData["subQues"][element][id] = "No";
      } else {
        if (this.medicalHistoryFormData["subQues"][element][id] == undefined) {
          this.medicalHistoryFormData["subQues"][element][id] = "No";
        }
      }
    });
    this.Child_array1.forEach((element, aindex) => {
      if (this.medicalHistoryFormData["subQues"][element] == undefined) {
        this.medicalHistoryFormData["subQues"][element] = {};
        this.medicalHistoryFormData["subQues"][element][id] = "No";
      } else {
        if (this.medicalHistoryFormData["subQues"][element][id] == undefined) {
          this.medicalHistoryFormData["subQues"][element][id] = "No";
        }
      }
    });

    if (mode == "Yes") {
      if (!this.selectedOptionFromQuestions.includes(id)) {
        this.selectedOptionFromQuestions.push(id);
      }
      // } else if (from != "input") {
    } else {
      if (this.selectedOptionFromQuestions.includes(id)) {
        const index = this.selectedOptionFromQuestions.indexOf(id);
        this.selectedOptionFromQuestions.splice(index, 1);
        if (
          this.medicalHistoryFormData["subQuesForm"][insuredId] != undefined
        ) {
          for (const subq in this.medicalHistoryFormData["subQuesForm"][
            insuredId
          ]) {
            delete this.medicalHistoryFormData["subQuesForm"][insuredId][subq];
            if (
              Object.keys(this.medicalHistoryFormData["subQuesForm"][insuredId])
                .length == 0
            ) {
              delete this.medicalHistoryFormData["subQuesForm"][insuredId];
            }
          }
        }
      }
    }
  }
  //for children
  sunquestions1(id, mode, i, chldIndex) {
    let insuredId = this.Child_array1[chldIndex];
    let subQus = this.health_proposal_questions[i]["sub_question"];
    if (this.medicalHistoryFormData["subQues"][insuredId] == undefined) {
      this.medicalHistoryFormData["subQues"][insuredId] = {};
    }
    this.medicalHistoryFormData["subQues"][insuredId][id] = mode;

    this.Adult_array1.forEach((element, aindex) => {
      if (this.medicalHistoryFormData["subQues"][element] == undefined) {
        this.medicalHistoryFormData["subQues"][element] = {};
        this.medicalHistoryFormData["subQues"][element][id] = "No";
      } else {
        if (this.medicalHistoryFormData["subQues"][element][id] == undefined) {
          this.medicalHistoryFormData["subQues"][element][id] = "No";
        }
      }
    });
    this.Child_array1.forEach((element, aindex) => {
      if (this.medicalHistoryFormData["subQues"][element] == undefined) {
        this.medicalHistoryFormData["subQues"][element] = {};
        this.medicalHistoryFormData["subQues"][element][id] = "No";
      } else {
        if (this.medicalHistoryFormData["subQues"][element][id] == undefined) {
          this.medicalHistoryFormData["subQues"][element][id] = "No";
        }
      }
    });

    if (mode == "Yes") {
      if (!this.selectedOptionFromChild.includes(id)) {
        this.selectedOptionFromChild.push(id);
      }
      // } else if (from != "input") {
    } else {
      // console.log(this.selectedOptionFromChild);
      if (this.selectedOptionFromChild.includes(id)) {
        const index = this.selectedOptionFromChild.indexOf(id);
        this.selectedOptionFromChild.splice(index, 1);
        // console.log(this.selectedOptionFromChild);
        for (const subq in this.medicalHistoryFormData["subQuesForm"][
          insuredId
        ]) {
          delete this.medicalHistoryFormData["subQuesForm"][insuredId][subq];
          if (
            Object.keys(this.medicalHistoryFormData["subQuesForm"][insuredId])
              .length == 0
          ) {
            delete this.medicalHistoryFormData["subQuesForm"][insuredId];
          }
        }
      }
    }
    // console.log(this.medicalHistoryFormData);
  }

  sunquestions2(id, mode, i, personIndex, subAnswer, mainQid) {
    let insuredId = "";
    if (mode == "adult") {
      insuredId = this.Adult_array1[personIndex];
      // insuredId = this.Child_array[personIndex]["id"];
    } else {
      // insuredId = this.Adult_array[personIndex]["id"];
      insuredId = this.Child_array1[personIndex];
    }
    // alert(insuredId);

    // let subQus = this.health_proposal_questions[i]["sub_question"];
    if (this.medicalHistoryFormData["subQuesForm"][insuredId] == undefined) {
      this.medicalHistoryFormData["subQuesForm"][insuredId] = {};
    }
    // console.log(this.medicalHistoryFormData["subQuesForm"]);
    // if (!this.medicalHistoryFormData["subQuesForm"][insuredId][id]) {
    //   this.medicalHistoryFormData["subQuesForm"][insuredId][id] = [];
    // }
    let newVals = [id, subAnswer.toString(), mainQid];
    // console.log(newVals);
    // if(this.medicalHistoryFormData["subQuesForm"][insuredId][id]){

    // }
    // this.medicalHistoryFormData["subQuesForm"][insuredId][id] =
    //   subAnswer.toString();
    this.medicalHistoryFormData["subQuesForm"][insuredId][id] = newVals;
    // console.log(this.medicalHistoryFormData);
  }

  submitform2() {
    let valid = this.medicalQVals();
    if (valid !== 0) {
      return false;
    }

    this.medicalHistoryFormData["enquiryId"] = this.enquiry_id;
    let data = this.medicalHistoryFormData;
    console.log(data);
    if (localStorage.getItem("medicalHistoryFormData") !== null) {
      localStorage.removeItem("medicalHistoryFormData");
    }
    localStorage.setItem(
      "medicalHistoryFormData",
      JSON.stringify([
        this.medicalHistoryFormData,
        this.selectedOption,
        this.selectedAnsFromQuestions,
        this.selectedOptionFromQuestions,
        this.selectedAnsFromChild,
        this.selectedOptionFromChild,
      ])
    );
    // return false;
    let lastData = { data: data, isedit: this.isedit };
    // console.log(this.medicalHistoryFormData);
    this.dataService.saveMedicalHistory(lastData).subscribe((response) => {
      console.log(response);
      response.status == "Success"
        ? this.goNext()
        : alert("Something Went Wrong!!");
      // this.proposal_relation = response.health_proposal_relation;
    });
  }

  medicalQVals() {
    let validCounter = 0;
    console.log(this.medicalHistoryFormData);
    this.health_proposal_questions.forEach((question, qIndex) => {
      if (
        !this.medicalHistoryFormData["mainQues"].hasOwnProperty(question.id)
      ) {
        document.getElementById(`${question.id}_err`).style.display = "block";
        validCounter++;
      } else {
        document.getElementById(`${question.id}_err`).style.display = "none";
        document.getElementById(`${question.id}_none_err`).style.display =
          "none";
        let memberCounter = 0;
        if (this.medicalHistoryFormData["mainQues"][question.id] == "Yes") {
          for (let subQ in this.medicalHistoryFormData["subQues"]) {
            for (let subQId in this.medicalHistoryFormData["subQues"][subQ]) {
              if (subQId == question.id) {
                memberCounter++;
              }
            }
          }
          if (memberCounter == 0) {
            validCounter++;
            document.getElementById(`${question.id}_none_err`).style.display =
              "block";
          }
          // else {
          //   document.getElementById(`${question.id}_none_err`).style.display =
          //     "none";
          // }
        }
      }
    });
    return validCounter;
  }

  goback() {
    this.router.navigate(["health/proposal-contact-details"], {
      queryParams: { id: this.enquiry_id, propId: 0, compId: this.companyId },
    });
  }

  goNext() {
    this.router.navigate(["health/proposal-nominee-details"], {
      queryParams: { id: this.enquiry_id, propId: 0, compId: this.companyId },
    });
  }
}
