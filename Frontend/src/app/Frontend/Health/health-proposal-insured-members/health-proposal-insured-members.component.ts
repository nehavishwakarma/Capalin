import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";
import { stringify } from "querystring";
// import { Console } from "console";

@Component({
  selector: "app-health-proposal-insured-members",
  templateUrl: "./health-proposal-insured-members.component.html",
  styleUrls: ["./health-proposal-insured-members.component.scss"],
})
export class HealthProposalInsuredMembersComponent implements OnInit {
  active = 1;
  valueOption: any;
  model;
  apollo_profession;
  registrationForm: FormGroup;
  companyId;
  enquiry_id;
  proposal_id;
  proposal_relation;
  formData: any;
  formData1: any;
  master_sheet_id: any;
  health_proposal_questions = [];
  health_proposal_sub_questions = [];
  proposalIndex;
  adults = 0;
  children = 0;
  cityStateName;
  Adult_array: any;
  Child_array: any;
  Adult_array1 = [];
  Child_array1 = [];
  insured_member_form: any;
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  medicalHistoryFormData = {};
  medicalHistoryFormDataEdit = {};
  // insuredIds = [];
  proposalFormEdit = "";
  isedit = 0;
  medical_questions_history;
  medical_sub_questions_history;
  insured_members_arrayEdit;
  adultsEdit = [];
  cityOpt;
  stateOpt;
  enquiryDetails;
  gst;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    // this.insured_member_form = {
    //   insured_members_array: [],
    //   proposal_id: "",
    // };
    this.formData = JSON.parse(localStorage.getItem("healthFormData"));
    this.insured_member_form = JSON.parse(
      localStorage.getItem("healthFormDataInsured")
    );

    this.master_sheet_id = { master_sheet_id: 0 };

    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
    });
    // this.proposal_id = localStorage.getItem('proposal_id');
    this.dataService
      .proposalIndex(this.enquiry_id, this.proposal_id)
      .subscribe((res) => {
        if (res.status == "success") {
          this.gst = res.response.health_enquiry_details.gst;
          // this.proposalIndex = res.proposalIndex;
          this.enquiryDetails =
            res.response.health_enquiry_details.enquiry_details[0];
          this.proposalIndex = res.proposalIndex;
          this.adults =
            res.response.health_enquiry_details.enquiry_details[0].adults;
          this.children =
            res.response.health_enquiry_details.enquiry_details[0].children;
          this.getProposalRelation([
            res.response.health_enquiry_details.enquiry_details[0]
              .master_sheet_id,
          ]);
          this.companyId =
            res.response.health_enquiry_details.enquiry_details[0].company_id;
          this.master_sheet_id.master_sheet_id =
            res.response.health_enquiry_details.enquiry_details[0].master_sheet_id;
          // this.medical_questions_history = res.response.medical_questions_history;
          // this.medical_sub_questions_history =
          //   res.response.medical_sub_questions_history;
          // this.insured_members_arrayEdit =
          //   res.response.health_insured_members_details;
          const data = { company_id: this.companyId };
          this.dataService.apolloProfession(data).subscribe((response) => {
            this.apollo_profession = response.apollo_occupation;
          });
          this.Adult_array = [];
          for (let index = 0; index < this.adults; index++) {
            this.Adult_array.push({
              id: "",
              relationship: "",
              i_first_name: "",
              i_middle_name: "",
              i_last_name: "",
              dob: "",
              gender: "",
              height_feet: "",
              height_inch: "",
              weight: "",
              insured_type: "",
              i_marital_status: "",
              insured_occupation: "",
              i_mobile: "",
            });
          }
          this.Child_array = [];
          for (let index = 0; index < this.children; index++) {
            console.log(index);
            this.Child_array.push({
              id: "",
              relationship: "",
              i_first_name: "",
              i_middle_name: "",
              i_last_name: "",
              dob: "",
              gender: "",
              height_feet: "",
              height_inch: "",
              weight: "",
              insured_type: "",
              i_marital_status: "",
              insured_occupation: "",
              i_mobile: "",
            });
          }

          if (
            localStorage.getItem("Adult_array") &&
            JSON.parse(localStorage.getItem("Adult_array")).length > 0
          ) {
            let preAdultData = JSON.parse(localStorage.getItem("Adult_array"));
            this.Adult_array.forEach((element, index) => {
              this.Adult_array[index] = preAdultData[index];
            });
          }
          if (
            localStorage.getItem("Child_array") &&
            JSON.parse(localStorage.getItem("Child_array")).length > 0
          ) {
            let preChildData = JSON.parse(localStorage.getItem("Child_array"));
            this.Child_array.forEach((element, index) => {
              this.Child_array[index] = preChildData[index];
            });
            console.log(this.Child_array);
          }
        } else {
          alert("Something Went Wrong!!");
        }
      });
  }

  setdata(data) {}
  
  setSelfData(selectedData) {
    if(selectedData == 'SELF' && this.formData != undefined){
      let data = {
        id: this.formData.id,
        relationship: selectedData,
        i_first_name: this.formData.p_first_name,
        i_middle_name: this.formData.p_mid_name,
        i_last_name: this.formData.p_last_name,
        dob:  this.formData.dob,
        gender:  this.formData.gender,
        height_feet:  "",
        height_inch:  "",
        weight:  "",
        insured_type:  "",
        i_marital_status:  this.formData.marital_status,
        insured_occupation:  "",
        i_mobile:  this.formData.mobile,
      }
      this.Adult_array = []
      this.Adult_array[0] = data;   
      console.log('data',this.formData)
    }
  }
  
  getProposalRelation(data) {
    this.dataService.getProposalRelation(data).subscribe((response) => {
      this.proposal_relation = response.health_proposal_relation;
    });
  }

  submitform1() {
    //validations
    let validCounter = this.validations();

    if (validCounter !== 0) {
      alert("Data Is Not Valid!!");
      return false;
    }

    //set to localstorage for edit

    this.Adult_array.forEach((element) => {
      if (element.dob) {
        let modDob = "";
        modDob += `${element.dob["year"]}-`;
        if (element.dob["month"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["month"]}-`;
        } else {
          modDob += `${element.dob["month"]}-`;
        }
        if (element.dob["day"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["day"]}`;
        } else {
          modDob += `${element.dob["day"]}`;
        }
        element.dob = modDob;
        element.insured_type = "adult";
        // element.dob = `${if() ? '0'+element.dob["month"] : }-${element.dob["day"]}`;
      }
      this.insured_member_form.insured_members_array.push(element);
    });
    this.Child_array.forEach((element) => {
      if (element.dob) {
        let modDob = "";
        modDob += `${element.dob["year"]}-`;
        if (element.dob["month"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["month"]}-`;
        } else {
          modDob += `${element.dob["month"]}-`;
        }
        if (element.dob["day"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["day"]}`;
        } else {
          modDob += `${element.dob["day"]}`;
        }
        element.dob = modDob;
        element.insured_type = "child";
        // element.dob = `${element.dob["year"]}-${element.dob["month"]}-${element.dob["day"]}`;
      }
      this.insured_member_form.insured_members_array.push(element);
    });
    // if (this.isedit == 1) {
    //   this.insured_member_form.proposal_id = this.proposal_id;
    // }
    console.log(this.insured_member_form);
    // return false;
    this.dataService
      .saveInsuredProposalForm(this.insured_member_form)
      .subscribe(
        (response) => {
          console.info("Success");
          console.log(response);
          // if (this.isedit == 0) {
          // this.Adult_array1 = [];
          // this.Child_array1 = [];
          response["adultData"].forEach((element, index) => {
            this.Adult_array[index]["id"] = element[0];
            this.Adult_array[index]["dob"] = {
              year: Number(this.Adult_array[index]["dob"].split("-")[0]),
              month: Number(this.Adult_array[index]["dob"].split("-")[1]),
              day: Number(this.Adult_array[index]["dob"].split("-")[2]),
            };
            if (this.Adult_array1.indexOf(element[0]) == -1) {
              this.Adult_array1.push(element[0]);
            }
          });
          response["childData"].forEach((element, index) => {
            this.Child_array[index]["id"] = element[0];
            this.Child_array[index]["dob"] = {
              year: Number(this.Child_array[index]["dob"].split("-")[0]),
              month: Number(this.Child_array[index]["dob"].split("-")[1]),
              day: Number(this.Child_array[index]["dob"].split("-")[2]),
            };
            if (this.Child_array1.indexOf(element[0]) == -1) {
              this.Child_array1.push(element[0]);
            }
          });

          // response["health_insured_members_id"].forEach((element) => {
          //   if (element[1] == "adult") {
          //       this.Adult_array1.push(element[0]);
          //   } else {
          //       this.Child_array1.push(element[0]);
          //   }
          // });
          if (localStorage.getItem("Adult_array") !== null) {
            localStorage.removeItem("Adult_array");
          }
          localStorage.setItem("Adult_array", JSON.stringify(this.Adult_array));

          if (localStorage.getItem("Child_array") !== null) {
            localStorage.removeItem("Child_array");
          }
          localStorage.setItem("Child_array", JSON.stringify(this.Child_array));
          localStorage.setItem(
            "Adult_array1",
            JSON.stringify(this.Adult_array1)
          );
          localStorage.setItem(
            "Child_array1",
            JSON.stringify(this.Child_array1)
          );
          // return false;
          this.router.navigate(["health/proposal-contact-details"], {
            queryParams: {
              id: this.enquiry_id,
              propId: 0,
              compId: this.companyId,
            },
          });
          // }
        },
        (error) => {
          alert("Something Went Wrong!!");
          console.error(error);
        }
      );
    // this.getMedicalDetails();
  }

  validations() {
    let validCounter = 0;
    //adult validation
    this.Adult_array.forEach((element, index) => {
      if (
        this.requiredValidation(
          element["relationship"],
          `relationship_err_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["i_first_name"],
          `i_first_name_err_adult_${index}`
        ) == false
      ) {
        validCounter++;
      } else if (
        this.onlyLettersValidation(
          element["i_first_name"],
          `i_first_name_err_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      // if (
      //   this.requiredValidation(
      //     element["i_middle_name"],
      //     `i_middle_name_err_adult_${index}`
      //   ) == false
      // ) {
      //   validCounter++;
      // } else if (
      //   this.onlyLettersValidation(
      //     element["i_middle_name"],
      //     `i_middle_name_err_adult_${index}`
      //   ) == false
      // ) {
      //   validCounter++;
      // }
      if (
        this.requiredValidation(
          element["i_last_name"],
          `i_last_name_err_adult_${index}`
        ) == false
      ) {
        validCounter++;
      } else if (
        this.onlyLettersValidation(
          element["i_last_name"],
          `i_last_name_err_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(element["dob"], `dob_err_adult_${index}`) ==
        false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(element["gender"], `gender_adult_${index}`) ==
        false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["height_feet"],
          `height_feet_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["height_inch"],
          `height_inch_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(element["weight"], `weight_adult_${index}`) ==
        false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["i_marital_status"],
          `i_marital_status_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["insured_occupation"],
          `insured_occupation_adult_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["i_mobile"],
          `i_mobile_adult_${index}`
        ) == false
      ) {
        validCounter++;
      } else if (
        this.mobile10digits(element["i_mobile"], `i_mobile_adult_${index}`) ==
        false
      ) {
        validCounter++;
      }
    });

    //child validations
    this.Child_array.forEach((element, index) => {
      if (
        this.requiredValidation(
          element["relationship"],
          `relationship_err_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["i_first_name"],
          `i_first_name_err_child_${index}`
        ) == false
      ) {
        validCounter++;
      } else if (
        this.onlyLettersValidation(
          element["i_first_name"],
          `i_first_name_err_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      // if (
      //   this.requiredValidation(
      //     element["i_middle_name"],
      //     `i_middle_name_err_child_${index}`
      //   ) == false
      // ) {
      //   validCounter++;
      // } else if (
      //   this.onlyLettersValidation(
      //     element["i_middle_name"],
      //     `i_middle_name_err_child_${index}`
      //   ) == false
      // ) {
      //   validCounter++;
      // }
      if (
        this.requiredValidation(
          element["i_last_name"],
          `i_last_name_err_child_${index}`
        ) == false
      ) {
        validCounter++;
      } else if (
        this.onlyLettersValidation(
          element["i_last_name"],
          `i_last_name_err_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(element["dob"], `dob_child_${index}`) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(element["gender"], `gender_child_${index}`) ==
        false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["height_feet"],
          `height_feet_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["height_inch"],
          `height_inch_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(element["weight"], `weight_child_${index}`) ==
        false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["i_marital_status"],
          `i_marital_status_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          element["insured_occupation"],
          `insured_occupation_child_${index}`
        ) == false
      ) {
        validCounter++;
      }
      // if (
      //   this.requiredValidation(
      //     element["i_mobile"],
      //     `i_mobile_child_${index}`
      //   ) == false
      // ) {
      //   validCounter++;
      // } else if (
      //   this.mobile10digits(element["i_mobile"], `i_mobile_child_${index}`) ==
      //   false
      // ) {
      //   validCounter++;
      // }
    });

    return validCounter;
  }

  redirect() {
    // this.router.navigate(["health/quotes/", this.enquiry_id]);
    this.router.navigate(["health/proposal-details"], {
      queryParams: { id: this.enquiry_id, propId: 0, compId: this.companyId },
    });
  }

  requiredValidation(element, errorId) {
    if (!errorId.startsWith("dob")) {
      element = element.trim();
    }
    if (!element) {
      console.log(element);
      console.log(errorId);
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  onlyLettersValidation(element, errorId) {
    let pattern = /^[a-zA-Z]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Only alphabets are allowed.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
  checkNumbers(event) {
    let pattern = /^[0-9]*$/;
    // let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
    if (
      event.key != "Backspace" &&
      event.key != "Tab" &&
      event.key != "ArrowLeft" &&
      event.key != "ArrowRight"
    ) {
      if (!pattern.test(event.key)) {
        return false;
      }
    }
  }

  mobile10digits(element, errorId) {
    if (element.split("").length < 10) {
      document.getElementById(errorId).children[0].innerHTML =
        "Please enter 10 digit mobile number.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
