import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-health",
  templateUrl: "./health.component.html",
  styleUrls: ["./health.component.scss"],
})
export class HealthComponent implements OnInit {
  active = 1;
  registrationForm: FormGroup;
  ages = [];
  preSelectionAdult = "";
  preSelectionChild = "";

  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private formBuilder: FormBuilder
  ) {}

  get f() {
    return this.registrationForm.controls;
  }

  ngOnInit() {
    let preEnquiryId =
      localStorage.getItem("enquiry_id") !== null
        ? localStorage.getItem("enquiry_id")
        : "0";
    this.dataService
      .getHelthEnquiryDetails(preEnquiryId)
      .subscribe((response) => {
        console.log(response);
        if (response) {
          this.setValues(response[0]);
        }
      });
    this.registrationForm = this.fb.group({
      lead_id: [""],
      gender: [""],
      adults: [""],
      children: [""],
      age: [""],
      mediclaim_sa: [""],
      pincode: [""],
    });

    for (let index = 18; index < 90; index++) {
      this.ages.push(index);
    }
  }

  setValues(editData) {
    this.registrationForm.controls["adults"].setValue(editData["adults"]);
    this.preSelectionAdult = editData["adults"];
    this.registrationForm.controls["children"].setValue(editData["children"]);
    this.preSelectionChild = editData["children"];
    this.registrationForm.controls["age"].setValue(editData["age"]);
    this.registrationForm.controls["mediclaim_sa"].setValue(
      `${editData["min_insurance_cover"]}-${editData["max_insurance_cover"]} Lacs`
    );
    this.registrationForm.controls["pincode"].setValue(editData["pincode"]);
    console.log(this.registrationForm);
  }
  onSubmit() {
    //validations
    let validCounter = 0;
    if (
      this.requiredValidation(
        this.registrationForm.controls["adults"].value,
        "adults_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["children"].value,
        "children_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["age"].value,
        "age_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["mediclaim_sa"].value,
        "mediclaim_sa_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.registrationForm.controls["pincode"].value,
        "pincode_err"
      ) == false
    ) {
      validCounter++;
    }
    if (validCounter !== 0) {
      return false;
    }

    this.dataService
      .getHealthStoreJson(this.registrationForm.value)
      .subscribe((response) => {
        console.log(response);
        localStorage.setItem("enquiry_id", response.enquiry_id);
        this.router.navigate(["health/quotes/", response.enquiry_id]);
      });
  }

  requiredValidation(element, errorId) {
    // if (errorId != "regtDate_err") {
    //   element = element.trim();
    // }
    console.log(element);
    console.log(typeof element);
    console.log(errorId);
    if (!element || element == "") {
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }
}
