import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";
@Component({
  selector: "app-health-proposal-details",
  templateUrl: "./health-proposal-details.component.html",
  styleUrls: ["./health-proposal-details.component.scss"],
})
export class HealthProposalDetailsComponent implements OnInit {
  active = 1;
  valueOption: any;
  model;
  apollo_profession;
  registrationForm: FormGroup;
  companyId;
  enquiry_id;
  proposal_id;
  proposal_relation;
  formData: any;
  formData1: any;
  master_sheet_id: any;
  health_proposal_questions = [];
  health_proposal_sub_questions = [];
  proposalIndex;
  adults = 0;
  children = 0;
  cityStateName;
  Adult_array: any;
  Child_array: any;
  Adult_array1: any;
  Child_array1: any;
  insured_member_form: any;
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  medicalHistoryFormData = {};
  medicalHistoryFormDataEdit = {};
  // insuredIds = [];
  proposalFormEdit = "";
  isedit = 0;
  medical_questions_history;
  medical_sub_questions_history;
  insured_members_arrayEdit;
  adultsEdit = [];
  cityOpt;
  stateOpt;
  enquiryDetails;
  gst;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.insured_member_form = {
      insured_members_array: [],
      proposal_id: "",
    };

    this.formData = {
      id: "",
      p_first_name: "",
      p_mid_name: "",
      p_last_name: "",
      dob: "",
      gender: "0",
      marital_status: "",
      gst_id_number: "",
      p_aadhar: "",
      apollo_annual_income: "",
      apollo_id_proof_type: "",
      apollo_id_proof_no: "",
      apollo_profession: "",
      address: "",
      area_locality: "",
      landmark: "",
      pincode_id: "",
      email: "",
      mobile: "",
      // alt_mobile:'',
      Existing: "0",
      nominee_relationship: "",
      nominee_name: "",
      health_enquiry_id: "",
      apollo_question: "",
      eia_no: "",
    };
    // this.formData1 = {
    //   Diabetes: "0",
    //   Thyroid: "0",
    //   Nervous: "0",
    //   Respiratory: "0",
    //   Heart: "0",
    //   stomach: "0",
    //   Spine: "0",
    //   Tumour: "0",
    //   ongoing: "0",
    //   surgical: "0",
    //   pregnant: "0",
    //   LiquorPeg: "0",
    //   Beer: "0",
    //   Wine: "0",
    //   smoke: "0",
    //   gutkha: "0",
    // };

    this.master_sheet_id = { master_sheet_id: 0 };
    // this.enquiry_id = localStorage.getItem("enquiry_id");
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
      this.enquiry_id = params["id"];
      this.companyId = params["compId"];
    });

    this.dataService
      .proposalIndex(this.enquiry_id, this.proposal_id)
      .subscribe((res) => {
        console.log(res);
        if (res.status == "success") {
          this.gst = res.response.health_enquiry_details.gst;
          // this.proposalIndex = res.proposalIndex;
          this.enquiryDetails =
            res.response.health_enquiry_details.enquiry_details[0];
          // this.adults =
          //   res.response.health_enquiry_details.enquiry_details[0].adults;
          // this.children =
          //   res.response.health_enquiry_details.enquiry_details[0].children;
          this.getProposalRelation([
            res.response.health_enquiry_details.enquiry_details[0]
              .master_sheet_id,
          ]);
          this.master_sheet_id.master_sheet_id =
            res.response.health_enquiry_details.enquiry_details[0].master_sheet_id;
          // this.medical_questions_history = res.response.medical_questions_history;
          // this.medical_sub_questions_history =
          //   res.response.medical_sub_questions_history;
          // this.insured_members_arrayEdit =
          //   res.response.health_insured_members_details;
          const data = { company_id: this.companyId };
          this.dataService.apolloProfession(data).subscribe((response) => {
            this.apollo_profession = response.apollo_occupation;
          });
        } else {
          alert("Something Went Wrong!!");
        }
      });

    if (localStorage.getItem("healthFormData") !== null) {
      this.setData(localStorage.getItem("healthFormData"));
    }
  }

  setData(data) {
    data = JSON.parse(data);
    this.formData = data;
    this.formData["dob"] = {
      year: Number(data["dob"].split("-")[0]),
      month: Number(data["dob"].split("-")[1]),
      day: Number(data["dob"].split("-")[2]),
    };
  }

  getProposalRelation(data) {
    this.dataService.getProposalRelation(data).subscribe((response) => {
      this.proposal_relation = response.health_proposal_relation;
    });
  }

  submitform() {
    let validCounter = this.onchangeValidations();
    if (validCounter !== 0) {
      return false;
    }
    let modDob = "";
    modDob += `${this.formData.dob["year"]}-`;
    if (this.formData.dob["month"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["month"]}-`;
    } else {
      modDob += `${this.formData.dob["month"]}-`;
    }
    if (this.formData.dob["day"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["day"]}`;
    } else {
      modDob += `${this.formData.dob["day"]}`;
    }
    this.formData.dob = modDob;
    console.log(this.formData);
    // return false;
    this.formData.health_enquiry_id = this.enquiry_id;
    this.dataService.healthProposalFormSave(this.formData).subscribe(
      (response) => {
        console.log(response);
        if (this.isedit == 0) {
          this.insured_member_form.proposal_id = response.id;
          this.formData.id = response.id;
          if (localStorage.getItem("healthFormData") !== null) {
            localStorage.removeItem("healthFormData");
          }
          if (localStorage.getItem("healthFormDataInsured") !== null) {
            localStorage.removeItem("healthFormDataInsured");
          }
          localStorage.setItem("healthFormData", JSON.stringify(this.formData));
          localStorage.setItem(
            "healthFormDataInsured",
            JSON.stringify(this.insured_member_form)
          );
        }
        this.router.navigate(["health/proposal-insured-members"], {
          queryParams: {
            id: this.enquiry_id,
            propId: 0,
            compId: this.companyId,
          },
        });
      },
      (error) => {
        alert("Something Went Wrong!!");
        console.error(error);
      }
    );
  }

  onchangeValidations() {
    let validCounter = 0;
    if (
      this.requiredValidation(
        this.formData["p_first_name"],
        "p_first_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(
        this.formData["p_first_name"],
        "p_first_name_err"
      ) == false
    ) {
      validCounter++;
    }
    // if (
    //   this.requiredValidation(this.formData["p_mid_name"], "p_mid_name_err") ==
    //   false
    // ) {
    //   validCounter++;
    // } else if (
    //   this.onlyLettersValidation(
    //     this.formData["p_mid_name"],
    //     "p_mid_name_err"
    //   ) == false
    // ) {
    //   validCounter++;
    // }
    if (
      this.requiredValidation(
        this.formData["p_last_name"],
        "p_last_name_err"
      ) == false
    ) {
      validCounter++;
    } else if (
      this.onlyLettersValidation(
        this.formData["p_last_name"],
        "p_last_name_err"
      ) == false
    ) {
      validCounter++;
    }
    if (this.requiredValidation(this.formData["dob"], "dob_err") == false) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["marital_status"],
        "marital_status_err"
      ) == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(this.formData["p_aadhar"], "p_aadhar_err") ==
      false
    ) {
      validCounter++;
    } else if (
      this.aadharVal(this.formData["p_aadhar"], "p_aadhar_err") == false
    ) {
      validCounter++;
    }
    if (
      this.requiredValidation(
        this.formData["apollo_annual_income"],
        "apollo_annual_income_err"
      ) == false
    ) {
      validCounter++;
    }


    // start from this
    if (this.companyId == 3) {
      if (
        this.requiredValidation(
          this.formData["apollo_id_proof_type"],
          "apollo_id_proof_type_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          this.formData["apollo_id_proof_no"],
          "apollo_id_proof_no_err"
        ) == false
      ) {
        validCounter++;
      } else if (
        this.formData["apollo_id_proof_type"] == "DriveLicense" &&
        this.drivingLicsVal(
          this.formData["apollo_id_proof_no"],
          "apollo_id_proof_no_err"
        ) == false
      ) {
        validCounter++;
      } else if (
        this.formData["apollo_id_proof_type"] == "PAN" &&
        this.panCardVal(
          this.formData["apollo_id_proof_no"],
          "apollo_id_proof_no_err"
        ) == false
      ) {
        validCounter++;
      } else if (
        this.formData["apollo_id_proof_type"] == "Passport" &&
        this.passportVal(
          this.formData["apollo_id_proof_no"],
          "apollo_id_proof_no_err"
        ) == false
      ) {
        validCounter++;
      } else if (
        this.formData["apollo_id_proof_type"] == "VoterId" &&
        this.voterIdIndiaVal(
          this.formData["apollo_id_proof_no"],
          "apollo_id_proof_no_err"
        ) == false
      ) {
        validCounter++;
      }
      if (
        this.requiredValidation(
          this.formData["apollo_profession"],
          "apollo_profession_err"
        ) == false
      ) {
        validCounter++;
      }
    }

    //ends at this

    if (
      this.formData["gst_id_number"].trim() !== "" &&
      this.gstValidation(this.formData["gst_id_number"], "gst_id_number_err")
    ) {
      validCounter++;
    }
    // if (
    //   this.requiredValidation(
    //     this.formData["apollo_question"],
    //     "apollo_question_err"
    //   ) == false
    // ) {
    //   validCounter++;
    // }
    console.log(this.formData);
    return validCounter;
  }

  requiredValidation(element, errorId) {
    if (errorId != "dob_err") {
      element = element.trim();
    }
    if (!element) {
      console.log("empty");
      console.log(errorId);
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  onlyLettersValidation(element, errorId) {
    let pattern = /^[a-zA-Z]*$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Only alphabets are allowed.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  checkNumbers(event) {
    let pattern = /^[0-9]*$/;
    // let arrayAllowedBtns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "Backspace", "Tab"];
    console.log(event.key);
    if (event.key != "Backspace" && event.key != "Tab") {
      if (!pattern.test(event.key)) {
        return false;
      }
    }
  }

  aadharVal(element, errorId) {
    let pattern = /^\d{4}\s\d{4}\s\d{4}$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Please Enter Valid Aadhar Number.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  drivingLicsVal(element, errorId) {
    let pattern =
      /^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Please Enter Valid Driving License Number.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  panCardVal(element, errorId) {
    let pattern = /[A-Z]{5}[0-9]{4}[A-Z]{1}/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Please Enter Valid PAN Number.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  passportVal(element, errorId) {
    let pattern = /^[A-PR-WYa-pr-wy][1-9]\\d\\s?\\d{4}[1-9]$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Please Enter Valid Passport Number.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  voterIdIndiaVal(element, errorId) {
    let pattern = /^([a-zA-Z]){3}([0-9]){7}?$/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).style.display = "block";
      document.getElementById(errorId).children[0].innerHTML =
        "Please Enter Valid Voter Id Number.";
      return false;
    } else {
      document.getElementById(errorId).children[0].innerHTML =
        "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  gstValidation(element, errorId) {
    let pattern = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
    if (!pattern.test(element.trim())) {
      document.getElementById(errorId).children[0].innerHTML =
        "Please enter valid GSTIN.";
      document.getElementById(errorId).style.display = "block";
      return false;
    } else {
      // document.getElementById(errorId).innerHTML =
      //   "Please fill out this field.";
      document.getElementById(errorId).style.display = "none";
      return true;
    }
  }

  redirect() {
    this.router.navigate(["health/quotes/", this.enquiry_id]);
  }
}
