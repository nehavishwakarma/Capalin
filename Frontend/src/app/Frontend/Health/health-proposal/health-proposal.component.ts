import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  ReactiveFormsModule,
} from "@angular/forms";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DataService } from "../../data.service";
import { Router } from "@angular/router";
import { hostViewClassName } from "@angular/compiler";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";

let arrayValues = "";
@Component({
  selector: "app-health-proposal",
  templateUrl: "./health-proposal.component.html",
  styleUrls: ["./health-proposal.component.scss"],
})
export class HealthProposalComponent implements OnInit {
  @ViewChild("email", { static: false }) emailPopup: ElementRef;

  active = 1;
  valueOption: any;
  model;
  apollo_profession;
  registrationForm: FormGroup;
  companyId;
  enquiry_id;
  proposal_id;
  proposal_relation;
  formData: any;
  formData1: any;
  master_sheet_id: any;
  health_proposal_questions = [];
  health_proposal_sub_questions = [];
  proposalIndex;
  adults = 0;
  children = 0;
  cityStateName;
  Adult_array: any;
  Child_array: any;
  Adult_array1: any;
  Child_array1: any;
  insured_member_form: any;
  selectedOption = [];
  selectedOptionFromQuestions = [];
  selectedAnsFromQuestions = [];
  selectedOptionFromChild = [];
  selectedAnsFromChild = [];
  medicalHistoryFormData = {};
  medicalHistoryFormDataEdit = {};
  // insuredIds = [];
  proposalFormEdit = "";
  isedit = 0;
  medical_questions_history;
  medical_sub_questions_history;
  insured_members_arrayEdit;
  adultsEdit = [];
  cityOpt;
  stateOpt;
  constructor(
    private router: Router,
    private dataService: DataService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  get f() {
    return this.registrationForm.controls;
  }

  ngOnInit() {
    this.insured_member_form = {
      insured_members_array: [],
      proposal_id: "",
    };

    this.formData = {
      id: "",
      p_first_name: "",
      p_mid_name: "",
      p_last_name: "",
      dob: "",
      gender: "0",
      marital_status: "",
      gst_id_number: "",
      p_aadhar: "",
      apollo_annual_income: "",
      apollo_id_proof_type: "",
      apollo_id_proof_no: "",
      apollo_profession: "",
      address: "",
      area_locality: "",
      landmark: "",
      pincode_id: "",
      email: "",
      mobile: "",
      // alt_mobile:'',
      Existing: "0",
      nominee_relationship: "",
      nominee_name: "",
      health_enquiry_id: "",
    };
    this.formData1 = {
      Diabetes: "0",
      Thyroid: "0",
      Nervous: "0",
      Respiratory: "0",
      Heart: "0",
      stomach: "0",
      Spine: "0",
      Tumour: "0",
      ongoing: "0",
      surgical: "0",
      pregnant: "0",
      LiquorPeg: "0",
      Beer: "0",
      Wine: "0",
      smoke: "0",
      gutkha: "0",
    };

    this.master_sheet_id = { master_sheet_id: 0 };

    // localStorage.setItem('proposal_id','0');
    this.enquiry_id = localStorage.getItem("enquiry_id");
    this.activatedRoute.queryParams.subscribe((params) => {
      this.proposal_id = params["propId"];
    });
    // this.proposal_id = localStorage.getItem('proposal_id');
    this.dataService
      .proposalIndex(this.enquiry_id, this.proposal_id)
      .subscribe((res) => {
        this.proposalIndex = res.proposalIndex;
        this.adults =
          res.response.health_enquiry_details.enquiry_details[0].adults;
        this.children =
          res.response.health_enquiry_details.enquiry_details[0].children;
        this.getProposalRelation([
          res.response.health_enquiry_details.enquiry_details[0]
            .master_sheet_id,
        ]);
        this.master_sheet_id.master_sheet_id =
          res.response.health_enquiry_details.enquiry_details[0].master_sheet_id;
        this.medical_questions_history = res.response.medical_questions_history;
        this.medical_sub_questions_history =
          res.response.medical_sub_questions_history;
        this.insured_members_arrayEdit =
          res.response.health_insured_members_details;
        const data = { company_id: this.companyId };
        this.dataService.apolloProfession(data).subscribe((response) => {
          this.apollo_profession = response.apollo_occupation;
        });
        this.Adult_array = [];
        for (let index = 0; index < this.adults; index++) {
          this.Adult_array.push({
            id: "",
            relationship: "",
            i_first_name: "",
            i_middle_name: "",
            i_last_name: "",
            dob: "",
            gender: "Male",
            height_feet: "",
            height_inch: "",
            weight: "",
            insured_type: "",
          });
        }
        this.Child_array = [];
        for (let index = 0; index < this.children; index++) {
          this.Child_array.push({
            id: "",
            relationship: "",
            i_first_name: "",
            i_middle_name: "",
            i_last_name: "",
            dob: "",
            gender: "Male",
            height_feet: "",
            height_inch: "",
            weight: "",
            insured_type: "",
          });
        }
        this.companyId =
          res.response.health_enquiry_details.enquiry_details[0].company_id;
        //added by sushil for edit
        if (
          res.response.health_proposal_details.health_proposal_details[0] &&
          this.proposal_id != 0
        ) {
          this.isedit = 1;
          this.proposalFormEdit =
            res.response.health_proposal_details.health_proposal_details[0];
          this.formData.id = this.proposalFormEdit["id"];
          this.formData.p_first_name = this.proposalFormEdit["p_first_name"];
          this.formData.p_mid_name = this.proposalFormEdit["p_mid_name"];
          this.formData.p_last_name = this.proposalFormEdit["p_last_name"];
          let date = {
            year: Number(this.proposalFormEdit["dob"].split("-")[0]),
            month: Number(this.proposalFormEdit["dob"].split("-")[1]),
            day: Number(this.proposalFormEdit["dob"].split("-")[2]),
          };
          this.formData.dob = date;
          this.formData.gender =
            this.proposalFormEdit["gender"] == "Male" ? "0" : "1";
          this.formData.marital_status =
            this.proposalFormEdit["marital_status"];
          this.formData.gst_id_number = this.proposalFormEdit["gst_id_number"];
          this.formData.p_aadhar = this.proposalFormEdit["p_aadhar"];
          this.formData.apollo_annual_income =
            this.proposalFormEdit["apollo_annual_income"];
          this.formData.apollo_id_proof_type =
            this.proposalFormEdit["apollo_id_proof_type"];
          this.formData.apollo_id_proof_no =
            this.proposalFormEdit["apollo_id_proof_no"];
          this.formData.apollo_profession =
            this.proposalFormEdit["apollo_profession"];
          this.formData.address = this.proposalFormEdit["address"];
          this.formData.area_locality = this.proposalFormEdit["area_locality"];
          this.formData.landmark = this.proposalFormEdit["landmark"];
          this.formData.pincode_id = this.proposalFormEdit["pincode_id"];
          this.formData.email = this.proposalFormEdit["email"];
          this.formData.mobile = this.proposalFormEdit["mobile"];
          this.formData.health_enquiry_id =
            this.proposalFormEdit["health_enquiry_id"];
          this.formData.Existing = this.proposalFormEdit["Existing"].toString();
          this.formData.nominee_relationship =
            this.proposalFormEdit["nominee_relationship"];
          this.formData.nominee_name = this.proposalFormEdit["nominee_name"];
          this.getHealthCity(this.proposalFormEdit["pincode_id"], 1);

          //set adult data when clicked on edit button
          // for (let index = 0; index < this.adults; index++) {
          //   this.adultsEdit.push(
          //     res.response.health_insured_members_details[index]["id"]
          //   );
          //   // this.insuredIds.push(
          //   //   res.response.health_insured_members_details[index]["id"]
          //   // );
          //   this.Adult_array[index]["id"] =
          //     res.response.health_insured_members_details[index]["id"];
          //   this.Adult_array[index]["relationship"] =
          //     res.response.health_insured_members_details[index][
          //       "relationship"
          //     ];
          //   this.Adult_array[index]["i_first_name"] =
          //     res.response.health_insured_members_details[index][
          //       "i_first_name"
          //     ];
          //   this.Adult_array[index]["i_middle_name"] =
          //     res.response.health_insured_members_details[index][
          //       "i_middle_name"
          //     ];
          //   this.Adult_array[index]["i_last_name"] =
          //     res.response.health_insured_members_details[index]["i_last_name"];
          //   let dob = {
          //     year: Number(
          //       res.response.health_insured_members_details[index]["dob"].split(
          //         "-"
          //       )[0]
          //     ),
          //     month: Number(
          //       res.response.health_insured_members_details[index]["dob"].split(
          //         "-"
          //       )[1]
          //     ),
          //     day: Number(
          //       res.response.health_insured_members_details[index]["dob"].split(
          //         "-"
          //       )[2]
          //     ),
          //   };
          //   this.Adult_array[index]["dob"] = dob;
          //   this.Adult_array[index]["gender"] =
          //     res.response.health_insured_members_details[index]["gender"];
          //   this.Adult_array[index]["height_feet"] =
          //     res.response.health_insured_members_details[index]["height_feet"];
          //   this.Adult_array[index]["height_inch"] =
          //     res.response.health_insured_members_details[index]["height_inch"];
          //   this.Adult_array[index]["weight"] =
          //     res.response.health_insured_members_details[index]["weight"];
          // }

          // for (let index = 0; index < this.children; index++) {
          //   // this.insuredIds.push(this.Adult_array[index]["id"]);
          //   this.Child_array[index]["id"] =
          //     res.response.health_insured_members_details[index]["id"];
          //   this.Child_array[index]["relationship"] =
          //     res.response.health_insured_members_details[index][
          //       "relationship"
          //     ];
          //   this.Child_array[index]["i_first_name"] =
          //     res.response.health_insured_members_details[index][
          //       "i_first_name"
          //     ];
          //   this.Child_array[index]["i_middle_name"] =
          //     res.response.health_insured_members_details[index][
          //       "i_middle_name"
          //     ];
          //   this.Child_array[index]["i_last_name"] =
          //     res.response.health_insured_members_details[index]["i_last_name"];
          //   let dobChild = {
          //     year: Number(
          //       res.response.health_insured_members_details[index]["dob"].split(
          //         "-"
          //       )[0]
          //     ),
          //     month: Number(
          //       res.response.health_insured_members_details[index]["dob"].split(
          //         "-"
          //       )[1]
          //     ),
          //     day: Number(
          //       res.response.health_insured_members_details[index]["dob"].split(
          //         "-"
          //       )[2]
          //     ),
          //   };
          //   this.Child_array[index]["dob"] = dobChild;
          //   this.Child_array[index]["gender"] =
          //     res.response.health_insured_members_details[index]["gender"];
          //   this.Child_array[index]["height_feet"] =
          //     res.response.health_insured_members_details[index]["height_feet"];
          //   this.Child_array[index]["height_inch"] =
          //     res.response.health_insured_members_details[index]["height_inch"];
          //   this.Child_array[index]["weight"] =
          //     res.response.health_insured_members_details[index]["weight"];
          // }

          this.Adult_array1 = [];
          this.Child_array1 = [];
          let adultCounter = 0;
          let childCounter = 0;
          res.response.health_insured_members_details.forEach(
            (insMember, insindex) => {
              if (insMember["insured_type"] == "adult") {
                this.Adult_array1.push(insMember["id"]);

                this.Adult_array[adultCounter]["id"] = insMember["id"];
                this.Adult_array[adultCounter]["insured_type"] =
                  insMember["insured_type"];
                this.Adult_array[adultCounter]["relationship"] =
                  insMember["relationship"];
                this.Adult_array[adultCounter]["i_first_name"] =
                  insMember["i_first_name"];
                this.Adult_array[adultCounter]["i_middle_name"] =
                  insMember["i_middle_name"];
                this.Adult_array[adultCounter]["i_last_name"] =
                  insMember["i_last_name"];
                let dob = {
                  year: Number(insMember["dob"].split("-")[0]),
                  month: Number(insMember["dob"].split("-")[1]),
                  day: Number(insMember["dob"].split("-")[2]),
                };
                this.Adult_array[adultCounter]["dob"] = dob;
                this.Adult_array[adultCounter]["gender"] = insMember["gender"];
                this.Adult_array[adultCounter]["height_feet"] =
                  insMember["height_feet"];
                this.Adult_array[adultCounter]["height_inch"] =
                  insMember["height_inch"];
                this.Adult_array[adultCounter]["weight"] = insMember["weight"];

                adultCounter++;
              } else if (insMember["insured_type"] == "child") {
                this.Child_array1.push(insMember["id"]);

                this.Child_array[childCounter]["id"] = insMember["id"];
                this.Child_array[childCounter]["insured_type"] =
                  insMember["insured_type"];
                this.Child_array[childCounter]["relationship"] =
                  insMember["relationship"];
                this.Child_array[childCounter]["i_first_name"] =
                  insMember["i_first_name"];
                this.Child_array[childCounter]["i_middle_name"] =
                  insMember["i_middle_name"];
                this.Child_array[childCounter]["i_last_name"] =
                  insMember["i_last_name"];
                let dobChild = {
                  year: Number(insMember["dob"].split("-")[0]),
                  month: Number(insMember["dob"].split("-")[1]),
                  day: Number(insMember["dob"].split("-")[2]),
                };
                this.Child_array[childCounter]["dob"] = dobChild;
                this.Child_array[childCounter]["gender"] = insMember["gender"];
                this.Child_array[childCounter]["height_feet"] =
                  insMember["height_feet"];
                this.Child_array[childCounter]["height_inch"] =
                  insMember["height_inch"];
                this.Child_array[childCounter]["weight"] = insMember["weight"];

                childCounter++;
              }
            }
          );

          //to show medical history edit
          this.medicalHistoryFormData["mainQues"] = {};
          this.medicalHistoryFormData["subQues"] = {};
          this.medicalHistoryFormData["subQuesForm"] = {};
          res.response.medical_questions_history.forEach((mainQ) => {
            if (this.medicalHistoryFormData["mainQues"] == undefined) {
              this.medicalHistoryFormData["mainQues"] = {};
            }
            // this.selectedOption.push(mainQ["question_id"]);
            this.medicalHistoryFormData["mainQues"][mainQ["question_id"]] =
              mainQ["question_answer"];
          });

          res.response.medical_sub_questions_history.forEach((subQ) => {
            if (this.medicalHistoryFormData["subQues"] == undefined) {
              this.medicalHistoryFormData["subQues"] = {};
            }
            if (
              this.medicalHistoryFormData["subQues"][subQ["insured_id"]] ==
              undefined
            ) {
              this.medicalHistoryFormData["subQues"][subQ["insured_id"]] = {};
            }
            this.medicalHistoryFormData["subQues"][subQ["insured_id"]][
              subQ["question_id"]
            ] = subQ["question_answer"];
            this.selectedOption.push(subQ["question_id"]);
            res.response.health_insured_members_details.forEach((insMember) => {
              res.response.medical_sub_questions_form_history.forEach(
                (subQF) => {
                  if (subQF["insured_id"] == insMember["id"]) {
                    if (insMember["insured_type"] == "adult") {
                      if (
                        !this.selectedOptionFromQuestions.includes(
                          subQ["question_id"]
                        )
                      ) {
                        this.selectedOptionFromQuestions.push(
                          subQ["question_id"]
                        );
                      }
                    } else if (insMember["insured_type"] == "child") {
                      if (
                        !this.selectedOptionFromChild.includes(
                          subQ["question_id"]
                        )
                      ) {
                        this.selectedOptionFromChild.push(subQ["question_id"]);
                      }
                    }
                  }
                }
              );

              // if (insMember["id"] == subQ["insured_id"]) {
              //   if (insMember["insured_type"] == "adult") {
              //     this.selectedOptionFromQuestions.push(subQ["question_id"]);
              //   } else if (insMember["insured_type"] == "child") {
              //     this.selectedOptionFromChild.push(subQ["question_id"]);
              //   }
              // }
            });
          });

          res.response.medical_sub_questions_form_history.forEach(
            (subQForm) => {
              this.selectedOption.push(subQForm["question_id"]);
              if (this.medicalHistoryFormData["subQuesForm"] == undefined) {
                this.medicalHistoryFormData["subQuesForm"] = {};
              }
              if (
                this.medicalHistoryFormData["subQuesForm"][
                  subQForm["insured_id"]
                ] == undefined
              ) {
                this.medicalHistoryFormData["subQuesForm"][
                  subQForm["insured_id"]
                ] = {};
              }
              this.medicalHistoryFormData["subQuesForm"][
                subQForm["insured_id"]
              ][subQForm["sub_question_id"]] = [
                subQForm["sub_question_id"],
                subQForm["sub_question_answer"].toString(),
                subQForm["question_id"],
              ];
              // subQForm["sub_question_answer"].toString();
            }
          );
          // console.log(this.medicalHistoryFormData);
        }
      });
  }

  submitform() {
    let modDob = "";
    modDob += `${this.formData.dob["year"]}-`;
    if (this.formData.dob["month"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["month"]}-`;
    } else {
      modDob += `${this.formData.dob["month"]}-`;
    }
    if (this.formData.dob["day"] < 10 && this.isedit == 0) {
      modDob += `0${this.formData.dob["day"]}`;
    } else {
      modDob += `${this.formData.dob["day"]}`;
    }
    this.formData.dob = modDob;

    // return false;
    this.formData.health_enquiry_id = this.enquiry_id;
    this.dataService
      .healthProposalFormSave(this.formData)
      .subscribe((response) => {
        if (this.isedit == 0) {
          this.insured_member_form.proposal_id = response.id;
          this.formData.id = response.id;
        }
        // document.getElementsByClassName('.saveformContinue').click();
      });
  }

  submitform1() {
    // this.insured_members_array.id = response.id;
    this.Adult_array.forEach((element) => {
      if (element.dob) {
        let modDob = "";
        modDob += `${element.dob["year"]}-`;
        if (element.dob["month"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["month"]}-`;
        } else {
          modDob += `${element.dob["month"]}-`;
        }
        if (element.dob["day"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["day"]}`;
        } else {
          modDob += `${element.dob["day"]}`;
        }
        element.dob = modDob;
        element.insured_type = "adult";
        // element.dob = `${if() ? '0'+element.dob["month"] : }-${element.dob["day"]}`;
      }
      this.insured_member_form.insured_members_array.push(element);
    });
    // return false;
    this.Child_array.forEach((element) => {
      if (element.dob) {
        let modDob = "";
        modDob += `${element.dob["year"]}-`;
        if (element.dob["month"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["month"]}-`;
        } else {
          modDob += `${element.dob["month"]}-`;
        }
        if (element.dob["day"] < 10 && this.isedit == 0) {
          modDob += `0${element.dob["day"]}`;
        } else {
          modDob += `${element.dob["day"]}`;
        }
        element.dob = modDob;
        element.insured_type = "child";
        // element.dob = `${element.dob["year"]}-${element.dob["month"]}-${element.dob["day"]}`;
      }
      this.insured_member_form.insured_members_array.push(element);
    });
    if (this.isedit == 1) {
      this.insured_member_form.proposal_id = this.proposal_id;
    }
    // console.log(this.insured_member_form);
    // return false;
    this.dataService
      .saveInsuredProposalForm(this.insured_member_form)
      .subscribe((response) => {
        // console.log(response);
        if (this.isedit == 0) {
          this.Adult_array1 = [];
          this.Child_array1 = [];
          response["health_insured_members_id"].forEach((element) => {
            if (element[1] == "adult") {
              this.Adult_array1.push(element[0]);
            } else {
              this.Child_array1.push(element[0]);
            }
          });
          // this.insuredIds = response["health_insured_members_id"];
        }
      });
    // this.Adult_array.forEach((adlt, index) => {
    //   // this.Adult_array1.push(index);
    //   // adlt['insuredId'] = response['health_insured_members_id'][index];
    //   // response['health_insured_members_id'].splice(index,1);
    // });
    // this.Child_array.forEach((chld, chind) => {
    //   // this.Adult_array1.push(chind);
    //   // chld['insuredId'] = response['health_insured_members_id'][chind];
    // });
    // this.Adult_array1 = this.Adult_array;
    // this.Child_array1 = this.Child_array;
    this.getMedicalDetails();
  }

  getMedicalDetails() {
    this.dataService
      .healthQuestions(this.master_sheet_id)
      .subscribe((response) => {
        this.health_proposal_questions = response.health_proposal_questions;
        this.health_proposal_sub_questions =
          response.health_proposal_sub_questions;
        this.health_proposal_questions.forEach((element, index) => {
          this.health_proposal_questions[index]["sub_question"] = [];
          // this.Adult_array1['healthQ'].push(element.id);
          this.health_proposal_sub_questions.forEach((element2, index2) => {
            if (element.id == element2.health_questions_id) {
              this.health_proposal_questions[index]["sub_question"].push(
                element2
              );
            }
          });
        });
      });
  }

  getProposalRelation(data) {
    this.dataService.getProposalRelation(data).subscribe((response) => {
      this.proposal_relation = response.health_proposal_relation;
    });
  }

  getHealthCity(event, editOrNot = 0) {
    if (editOrNot == 0) {
      if (event.target.value.length == 6) {
        // const data = { company_id: this.companyId };
        this.dataService
          .getHealthCity(event.target.value, this.companyId)
          .subscribe((response) => {
            this.cityStateName = response.get_state;
          });
      }
    } else {
      this.dataService
        .getHealthCity(event, this.companyId)
        .subscribe((response) => {
          //city.city+','+city.id+','+city.pincode+','+city.state_code+','+city.state_name
          // this.selectedCity = `${this.proposalFormEdit["city_name"]}`;
          this.cityStateName = response.get_state;
          this.cityStateName.forEach((city) => {
            if (city.id == this.proposalFormEdit["city_id"]) {
              this.cityOpt = city;
              this.stateOpt = city;
            }
          });
        });
    }
  }

  changeHealthCity(data) {
    this.formData.city_id = this.cityOpt["id"];
    this.formData.city_name = this.cityOpt["city"];
    this.formData.state_id = this.cityOpt["state_code"];
    this.formData.state_name = this.cityOpt["state_name"];
  }

  openEmailModal() {
    this.modalService.open(this.emailPopup);
  }

  createadultsChildren(id, mode, i) {
    if (this.medicalHistoryFormData["mainQues"] == undefined) {
      // this.medicalHistoryFormData[this.enquiry_id] = {};
      this.medicalHistoryFormData["mainQues"] = {};
      this.medicalHistoryFormData["subQues"] = {};
      this.medicalHistoryFormData["subQuesForm"] = {};
      this.medicalHistoryFormData["enquiryId"] = this.enquiry_id;
    }
    this.medicalHistoryFormData["mainQues"][id] = mode;
    // console.log(this.medicalHistoryFormData);

    if (mode == "Yes") {
      if (!this.selectedOption.includes(id)) {
        this.selectedOption.push(id);
      } else {
      }
    } else {
      if (this.selectedOption.includes(id)) {
        const index = this.selectedOption.indexOf(id);
        this.selectedOption.splice(index, 1);
      }
      if (this.medicalHistoryFormData["subQues"] != undefined) {
        this.Adult_array1.forEach((element) => {
          for (const i in this.medicalHistoryFormData["subQues"][element]) {
            if (i == id) {
              delete this.medicalHistoryFormData["subQues"][element][id];
            }
          }
        });
        this.Child_array1.forEach((element) => {
          for (const i in this.medicalHistoryFormData["subQues"][element]) {
            if (i == id) {
              delete this.medicalHistoryFormData["subQues"][element][id];
            }
          }
        });
      }
      // console.log(this.health_proposal_questions[i]["sub_question"]);
      if (this.medicalHistoryFormData["subQuesForm"] != undefined) {
        // console.log(this.health_proposal_questions[i]["sub_question"]);
        let sub_id = this.health_proposal_questions[i]["sub_question"][0]["id"];

        this.Adult_array1.forEach((element) => {
          // console.log(element);
          for (const i in this.medicalHistoryFormData["subQuesForm"][element]) {
            // console.log(`${i}****${sub_id}`);
            if (i == sub_id) {
              delete this.medicalHistoryFormData["subQuesForm"][element][
                sub_id
              ];
            }
            if (
              Object.keys(this.medicalHistoryFormData["subQuesForm"][element])
                .length == 0
            ) {
              delete this.medicalHistoryFormData["subQuesForm"][element];
            }
          }
        });
        this.Child_array1.forEach((element) => {
          for (const i in this.medicalHistoryFormData["subQues"][element]) {
            if (i == sub_id) {
              delete this.medicalHistoryFormData["subQuesForm"][element][
                sub_id
              ];
            }
            if (
              Object.keys(this.medicalHistoryFormData["subQuesForm"][element])
                .length == 0
            ) {
              delete this.medicalHistoryFormData["subQuesForm"][element];
            }
          }
        });

        // for (const subq in this.medicalHistoryFormData["subQuesForm"]) {
        //   delete this.medicalHistoryFormData["subQuesForm"][insuredId][subq];
        //   if (
        //     Object.keys(this.medicalHistoryFormData["subQuesForm"][insuredId])
        //       .length == 0
        //   ) {
        //     delete this.medicalHistoryFormData["subQuesForm"][insuredId];
        //   }
        // }
        // console.log(this.medicalHistoryFormData);
      }
    }
  }

  sunquestions(id, mode, i, a) {
    // console.log(this.medicalHistoryFormData);
    let insuredId = this.Adult_array1[a];
    // return false;
    let subQus = this.health_proposal_questions[i]["sub_question"];
    if (this.medicalHistoryFormData["subQues"][insuredId] == undefined) {
      this.medicalHistoryFormData["subQues"][insuredId] = {};
    }
    this.medicalHistoryFormData["subQues"][insuredId][id] = mode;

    if (mode == "Yes") {
      if (!this.selectedOptionFromQuestions.includes(id)) {
        this.selectedOptionFromQuestions.push(id);
      }
      // } else if (from != "input") {
    } else {
      if (this.selectedOptionFromQuestions.includes(id)) {
        const index = this.selectedOptionFromQuestions.indexOf(id);
        this.selectedOptionFromQuestions.splice(index, 1);
        if (
          this.medicalHistoryFormData["subQuesForm"][insuredId] != undefined
        ) {
          for (const subq in this.medicalHistoryFormData["subQuesForm"][
            insuredId
          ]) {
            delete this.medicalHistoryFormData["subQuesForm"][insuredId][subq];
            if (
              Object.keys(this.medicalHistoryFormData["subQuesForm"][insuredId])
                .length == 0
            ) {
              delete this.medicalHistoryFormData["subQuesForm"][insuredId];
            }
          }
        }
      }
    }
  }
  //for children
  sunquestions1(id, mode, i, chldIndex) {
    let insuredId = this.Child_array1[chldIndex];
    let subQus = this.health_proposal_questions[i]["sub_question"];
    if (this.medicalHistoryFormData["subQues"][insuredId] == undefined) {
      this.medicalHistoryFormData["subQues"][insuredId] = {};
    }
    this.medicalHistoryFormData["subQues"][insuredId][id] = mode;

    if (mode == "Yes") {
      if (!this.selectedOptionFromChild.includes(id)) {
        this.selectedOptionFromChild.push(id);
      }
      // } else if (from != "input") {
    } else {
      // console.log(this.selectedOptionFromChild);
      if (this.selectedOptionFromChild.includes(id)) {
        const index = this.selectedOptionFromChild.indexOf(id);
        this.selectedOptionFromChild.splice(index, 1);
        // console.log(this.selectedOptionFromChild);
        for (const subq in this.medicalHistoryFormData["subQuesForm"][
          insuredId
        ]) {
          delete this.medicalHistoryFormData["subQuesForm"][insuredId][subq];
          if (
            Object.keys(this.medicalHistoryFormData["subQuesForm"][insuredId])
              .length == 0
          ) {
            delete this.medicalHistoryFormData["subQuesForm"][insuredId];
          }
        }
      }
    }
    // console.log(this.medicalHistoryFormData);
  }

  sunquestions2(id, mode, i, personIndex, subAnswer, mainQid) {
    // this.medicalHistoryFormData["subQuesForm"];
    // console.log(this.Adult_array1);
    // console.log(personIndex);
    // console.log(mode);
    // console.log(id);
    let insuredId = "";
    if (mode == "adult") {
      insuredId = this.Adult_array1[personIndex];
      // insuredId = this.Child_array[personIndex]["id"];
    } else {
      // insuredId = this.Adult_array[personIndex]["id"];
      insuredId = this.Child_array1[personIndex];
    }
    // alert(insuredId);

    // let subQus = this.health_proposal_questions[i]["sub_question"];
    if (this.medicalHistoryFormData["subQuesForm"][insuredId] == undefined) {
      this.medicalHistoryFormData["subQuesForm"][insuredId] = {};
    }
    // console.log(this.medicalHistoryFormData["subQuesForm"]);
    // if (!this.medicalHistoryFormData["subQuesForm"][insuredId][id]) {
    //   this.medicalHistoryFormData["subQuesForm"][insuredId][id] = [];
    // }
    let newVals = [id, subAnswer.toString(), mainQid];
    // console.log(newVals);
    // if(this.medicalHistoryFormData["subQuesForm"][insuredId][id]){

    // }
    // this.medicalHistoryFormData["subQuesForm"][insuredId][id] =
    //   subAnswer.toString();
    this.medicalHistoryFormData["subQuesForm"][insuredId][id] = newVals;
    // console.log(this.medicalHistoryFormData);
  }

  submitform2() {
    if (this.isedit == 1) {
      this.medicalHistoryFormData["enquiryId"] = this.enquiry_id;
    }
    let data = this.medicalHistoryFormData;
    // console.log(data);
    let lastData = { data: data, isedit: this.isedit };
    // console.log(this.medicalHistoryFormData);
    this.dataService.saveMedicalHistory(lastData).subscribe((response) => {
      // this.proposal_relation = response.health_proposal_relation;
    });
  }
}
