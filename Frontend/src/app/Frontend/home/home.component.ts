import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    if (localStorage.getItem("registerFormCar") !== null) {
      localStorage.removeItem("registerFormCar");
    }
    if (localStorage.getItem("proposalFormCar") !== null) {
      localStorage.removeItem("proposalFormCar");
    }
    if (localStorage.getItem("enquiry_id") !== null) {
      localStorage.removeItem("enquiry_id");
    }
    if (localStorage.getItem("healthFormData") !== null) {
      localStorage.removeItem("healthFormData");
    }
    if (localStorage.getItem("healthFormDataInsured") !== null) {
      localStorage.removeItem("healthFormDataInsured");
    }
    if (localStorage.getItem("medicalHistoryFormData") !== null) {
      localStorage.removeItem("medicalHistoryFormData");
    }
  }

  // CAR() {
  //   this.router.navigate(['/car']);
  // };
}
