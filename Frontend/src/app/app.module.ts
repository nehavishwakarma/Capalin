import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HeaderComponent } from "./Frontend/header/header.component";
import { FooterComponent } from "./Frontend/footer/footer.component";
import { HomeComponent } from "./Frontend/home/home.component";
import { CarComponent } from "./Frontend/Car/car/car.component";
import { CarQuotationComponent } from "./Frontend/Car/car-quotation/car-quotation.component";
import { CarProposalComponent } from "./Frontend/Car/car-proposal/car-proposal.component";
import { CarSummaryComponent } from "./Frontend/Car/car-summary/car-summary.component";
import { BikeComponent } from "./Frontend/Bike/bike/bike.component";
import { BikeProposalComponent } from "./Frontend/Bike/bike-proposal/bike-proposal.component";
import { BikeSummaryComponent } from "./Frontend/Bike/bike-summary/bike-summary.component";
import { BikeQuotationComponent } from "./Frontend/Bike/bike-quotation/bike-quotation.component";
import { HealthComponent } from "./Frontend/Health/health/health.component";
import { HealthQuotationComponent } from "./Frontend/Health/health-quotation/health-quotation.component";
import { HealthProposalComponent } from "./Frontend/Health/health-proposal/health-proposal.component";
import { HealthSummaryComponent } from "./Frontend/Health/health-summary/health-summary.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ModalModule } from "ngb-modal";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AdvisorRegComponent } from "./Frontend/Others/advisor-reg/advisor-reg.component";
import { HttpClientModule } from "@angular/common/http";
import { AdvisorVerifyComponent } from "./Frontend/Others/advisor-verify/advisor-verify.component";
import { CustomerRegComponent } from "./Frontend/Others/customer-reg/customer-reg.component";
import { CustomerVerifyComponent } from "./Frontend/Others/customer-verify/customer-verify.component";
import { PrivacyComponent } from "./Frontend/Others/privacy/privacy.component";
import { CancellationComponent } from "./Frontend/Others/cancellation/cancellation.component";
import { FaqComponent } from "./Frontend/Others/faq/faq.component";
import { MatButtonModule } from "@angular/material";
import { CarPreviousPoliceDetailsComponent } from "./Frontend/Car/car-previous-police-details/car-previous-police-details.component";
import { CarInsuranceComponent } from "./Frontend/Car/car-insurance/car-insurance.component";
import { DatePipe } from "@angular/common";
import { HealthProposalDetailsComponent } from "./Frontend/Health/health-proposal-details/health-proposal-details.component";
import { HealthProposalInsuredMembersComponent } from "./Frontend/Health/health-proposal-insured-members/health-proposal-insured-members.component";
import { HealthProposalContactDetailsComponent } from "./Frontend/Health/health-proposal-contact-details/health-proposal-contact-details.component";
import { HealthProposalMedicalHistoryComponent } from "./Frontend/Health/health-proposal-medical-history/health-proposal-medical-history.component";
import { HealthProposalNomineeDetailsComponent } from "./Frontend/Health/health-proposal-nominee-details/health-proposal-nominee-details.component";
import { LoginComponent } from "./Frontend/Others/login/login.component";
import { DigitalPartnerComponent } from "./Frontend/Others/digital-partner/digital-partner.component";
import { IrdaiLicenseComponent } from "./Frontend/Others/irdai-license/irdai-license.component";
import { HomepageComponent } from "./Frontend/homepage/homepage.component";
import { NgxPaginationModule } from "ngx-pagination";
import { MatFormFieldModule } from "@angular/material/form-field";
import { CarOwnerComponent } from "./Frontend/Car/car-owner/car-owner.component";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { CarPersonalDetailComponent } from "./Frontend/Car/car-personal-detail/car-personal-detail.component";
import { CarRegAddressComponent } from "./Frontend/Car/car-reg-address/car-reg-address.component";
import { CarDetailsComponent } from "./Frontend/Car/car-details/car-details.component";
import { CarProposalPrevPolicyDetailsComponent } from "./Frontend/Car/car-proposal-prev-policy-details/car-proposal-prev-policy-details.component";
import { MatDatepickerModule, MatNativeDateModule } from "@angular/material";
import { MatIconModule } from "@angular/material/icon";
import { MatRadioModule } from "@angular/material/radio";
import { BikePreviousPolicyDetailsComponent } from "./Frontend/Bike/bike-previous-policy-details/bike-previous-policy-details.component";
import { BikeInsuranceComponent } from "./Frontend/Bike/bike-insurance/bike-insurance.component";
import { BikePersonalDetailsComponent } from "./Frontend/Bike/bike-personal-details/bike-personal-details.component";
import { BikeRegAddressComponent } from "./Frontend/Bike/bike-reg-address/bike-reg-address.component";
import { BikeDetailsComponent } from "./Frontend/Bike/bike-details/bike-details.component";
import { BikeProposalPrevPolicyDetailsComponent } from "./Frontend/Bike/bike-proposal-prev-policy-details/bike-proposal-prev-policy-details.component";
import { OrderModule } from "ngx-order-pipe";
import { BikeFinalSummaryComponent } from "./Frontend/Bike/bike-final-summary/bike-final-summary.component";
import { AboutUsComponent } from "./Frontend/Others/about-us/about-us.component";
import { ContactComponent } from "./Frontend/Others/contact/contact.component";
import { HeaderLiteComponent } from './Frontend/header-lite/header-lite.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CarPaymentResponseComponent } from './Frontend/Car/car-payment-response/car-payment-response.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CarComponent,
    CarQuotationComponent,
    CarProposalComponent,
    CarSummaryComponent,
    BikeComponent,
    BikeProposalComponent,
    BikeSummaryComponent,
    BikeQuotationComponent,
    HealthComponent,
    HealthQuotationComponent,
    HealthProposalComponent,
    HealthSummaryComponent,
    AdvisorRegComponent,
    AdvisorVerifyComponent,
    CustomerRegComponent,
    CustomerVerifyComponent,
    PrivacyComponent,
    CancellationComponent,
    FaqComponent,
    CarPreviousPoliceDetailsComponent,
    CarInsuranceComponent,
    CarOwnerComponent,
    CarPersonalDetailComponent,
    CarRegAddressComponent,
    CarDetailsComponent,
    CarProposalPrevPolicyDetailsComponent,
    HealthProposalDetailsComponent,
    HealthProposalInsuredMembersComponent,
    HealthProposalContactDetailsComponent,
    HealthProposalMedicalHistoryComponent,
    HealthProposalNomineeDetailsComponent,
    LoginComponent,
    DigitalPartnerComponent,
    IrdaiLicenseComponent,
    HomepageComponent,
    BikePreviousPolicyDetailsComponent,
    BikeInsuranceComponent,
    BikePersonalDetailsComponent,
    BikeRegAddressComponent,
    BikeDetailsComponent,
    BikeProposalPrevPolicyDetailsComponent,
    BikeFinalSummaryComponent,
    AboutUsComponent,
    ContactComponent,
    HeaderLiteComponent,
    CarPaymentResponseComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ModalModule,
    HttpClientModule,
    MatButtonModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatIconModule,
    MatNativeDateModule,
    NgxPaginationModule,
    OrderModule,
    NgSelectModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
