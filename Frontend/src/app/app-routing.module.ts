import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HeaderComponent } from "./Frontend/header/header.component";
import { FooterComponent } from "./Frontend/footer/footer.component";
import { HomeComponent } from "./Frontend/home/home.component";
import { CarComponent } from "./Frontend/Car/car/car.component";
import { CarQuotationComponent } from "./Frontend/Car/car-quotation/car-quotation.component";
import { CarProposalComponent } from "./Frontend/Car/car-proposal/car-proposal.component";
import { CarSummaryComponent } from "./Frontend/Car/car-summary/car-summary.component";
import { BikeComponent } from "./Frontend/Bike/bike/bike.component";
import { BikeProposalComponent } from "./Frontend/Bike/bike-proposal/bike-proposal.component";
import { BikeSummaryComponent } from "./Frontend/Bike/bike-summary/bike-summary.component";
import { BikeQuotationComponent } from "./Frontend/Bike/bike-quotation/bike-quotation.component";
import { HealthComponent } from "./Frontend/Health/health/health.component";
import { HealthQuotationComponent } from "./Frontend/Health/health-quotation/health-quotation.component";
import { HealthProposalComponent } from "./Frontend/Health/health-proposal/health-proposal.component";
import { HealthSummaryComponent } from "./Frontend/Health/health-summary/health-summary.component";
import { HealthProposalDetailsComponent } from "./Frontend/Health/health-proposal-details/health-proposal-details.component";
import { HealthProposalInsuredMembersComponent } from "./Frontend/Health/health-proposal-insured-members/health-proposal-insured-members.component";
import { AdvisorRegComponent } from "./Frontend/Others/advisor-reg/advisor-reg.component";
import { AdvisorVerifyComponent } from "./Frontend/Others/advisor-verify/advisor-verify.component";
import { CustomerRegComponent } from "./Frontend/Others/customer-reg/customer-reg.component";
import { CustomerVerifyComponent } from "./Frontend/Others/customer-verify/customer-verify.component";
import { PrivacyComponent } from "./Frontend/Others/privacy/privacy.component";
import { CancellationComponent } from "./Frontend/Others/cancellation/cancellation.component";
import { FaqComponent } from "./Frontend/Others/faq/faq.component";
import { CarPreviousPoliceDetailsComponent } from "./Frontend/Car/car-previous-police-details/car-previous-police-details.component";
import { CarInsuranceComponent } from "./Frontend/Car/car-insurance/car-insurance.component";
import { CarOwnerComponent } from "./Frontend/Car/car-owner/car-owner.component";
import { CarPersonalDetailComponent } from "./Frontend/Car/car-personal-detail/car-personal-detail.component";
import { CarRegAddressComponent } from "./Frontend/Car/car-reg-address/car-reg-address.component";
import { CarDetailsComponent } from "./Frontend/Car/car-details/car-details.component";
import { CarProposalPrevPolicyDetailsComponent } from "./Frontend/Car/car-proposal-prev-policy-details/car-proposal-prev-policy-details.component";
import { HealthProposalContactDetailsComponent } from "./Frontend/Health/health-proposal-contact-details/health-proposal-contact-details.component";
import { HealthProposalMedicalHistoryComponent } from "./Frontend/Health/health-proposal-medical-history/health-proposal-medical-history.component";
import { HealthProposalNomineeDetailsComponent } from "./Frontend/Health/health-proposal-nominee-details/health-proposal-nominee-details.component";
import { LoginComponent } from "./Frontend/Others/login/login.component";
import { DigitalPartnerComponent } from "./Frontend/Others/digital-partner/digital-partner.component";
import { IrdaiLicenseComponent } from "./Frontend/Others/irdai-license/irdai-license.component";
import { HomepageComponent } from "./Frontend/homepage/homepage.component";
import { BikePreviousPolicyDetailsComponent } from "./Frontend/Bike/bike-previous-policy-details/bike-previous-policy-details.component";
import { BikeInsuranceComponent } from "./Frontend/Bike/bike-insurance/bike-insurance.component";
import { BikePersonalDetailsComponent } from "./Frontend/Bike/bike-personal-details/bike-personal-details.component";
import { BikeRegAddressComponent } from "./Frontend/Bike/bike-reg-address/bike-reg-address.component";
import { BikeDetailsComponent } from "./Frontend/Bike/bike-details/bike-details.component";
import { BikeProposalPrevPolicyDetailsComponent } from "./Frontend/Bike/bike-proposal-prev-policy-details/bike-proposal-prev-policy-details.component";
import { BikeFinalSummaryComponent } from "./Frontend/Bike/bike-final-summary/bike-final-summary.component";
import { AboutUsComponent } from "./Frontend/Others/about-us/about-us.component";
import { ContactComponent } from "./Frontend/Others/contact/contact.component";
import { CarPaymentResponseComponent } from "./Frontend/Car/car-payment-response/car-payment-response.component";

const routes: Routes = [
  { path: "header", component: HeaderComponent },
  // { path: "", component: HomeComponent },    //previous rutuja homepage // by sushil
  { path: "", component: HomepageComponent }, //avadhoot homepage
  { path: "footer", component: FooterComponent },
  { path: "car", component: CarComponent },
  { path: "car/quotes/:id", component: CarQuotationComponent },
  { path: "car/proposal/:id", component: CarProposalComponent },
  { path: "car/summary", component: CarSummaryComponent },
  {
    path: "car/carPreviousPoliceDetails",
    component: CarPreviousPoliceDetailsComponent,
  },
  { path: "car/carInsurance", component: CarInsuranceComponent },
  { path: "car/CarOwner", component: CarOwnerComponent },
  { path: "car/CarPersonalDetail", component: CarPersonalDetailComponent },
  { path: "car/CarRegAddress", component: CarRegAddressComponent },
  { path: "car/CarDetails", component: CarDetailsComponent },
  {
    path: "car/CarProposalPrevPolicyDetails",
    component: CarProposalPrevPolicyDetailsComponent,
  },
  { path: "bike", component: BikeComponent },
  { path: "bike/quotes/:id", component: BikeQuotationComponent },
  {
    path: "bike/BikePreviousPolicyDetails",
    component: BikePreviousPolicyDetailsComponent,
  },
  { path: "bike/BikeInsurance", component: BikeInsuranceComponent },
  { path: "bike/BikePersonalDetails", component: BikePersonalDetailsComponent },
  { path: "bike/BikeRegAddress", component: BikeRegAddressComponent },
  { path: "bike/BikeDetails", component: BikeDetailsComponent },
  {
    path: "bike/BikeProposalPrevPolicyDetails",
    component: BikeProposalPrevPolicyDetailsComponent,
  },
  { path: "bike/BikeFinalSummary", component: BikeFinalSummaryComponent },

  { path: "bike/proposal", component: BikeProposalComponent },
  { path: "bike/summary", component: BikeSummaryComponent },
  { path: "health", component: HealthComponent },
  { path: "health/quotes/:id", component: HealthQuotationComponent },
  { path: "health/proposal/:id", component: HealthProposalComponent },
  { path: "health/summary", component: HealthSummaryComponent },
  {
    path: "health/proposal-details",
    component: HealthProposalDetailsComponent,
  },
  {
    path: "health/proposal-insured-members",
    component: HealthProposalInsuredMembersComponent,
  },
  {
    path: "health/proposal-contact-details",
    component: HealthProposalContactDetailsComponent,
  },
  {
    path: "health/proposal-medical-history",
    component: HealthProposalMedicalHistoryComponent,
  },
  {
    path: "health/proposal-nominee-details",
    component: HealthProposalNomineeDetailsComponent,
  },
  { path: "login", component: LoginComponent },
  { path: "digital-partner", component: DigitalPartnerComponent },
  { path: "irdai-license", component: IrdaiLicenseComponent },
  { path: "register", component: AdvisorRegComponent },
  { path: "verify", component: AdvisorVerifyComponent },
  { path: "customer/register", component: CustomerRegComponent },
  { path: "customer/verify", component: CustomerVerifyComponent },
  { path: "privacy-policy", component: PrivacyComponent },
  { path: "Insurance-FAQs", component: FaqComponent },
  { path: "cancellation-refund", component: CancellationComponent },
  { path: "about-us", component: AboutUsComponent },
  { path: "contact", component: ContactComponent},
  { path: "car/car-payment-response", component: CarPaymentResponseComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
