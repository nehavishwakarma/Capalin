const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const { parseString } = require('xml2js')
const axios = require('axios');
const { resolve } = require('path');
const xml = {}

fs
.readdirSync(__dirname)
.filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
})
.forEach(file => {
    const model = require(path.join(__dirname, file))
    file = file.substring(0, file.length - 3);
    xml[file] = model;
});

xml.header = undefined
xml.payload = undefined 
xml.url = undefined

xml.call = function(){
    return new Promise((resolve, reject) => {
        if(this.url && this.payload && this.url){
            axios.post(this.url, this.payload, this.header).then(result => {
                resolve(result)
            }).catch(ex => {
                reject(ex)
            })
        }else{
            reject("Please enter payload , url and headers")
        }
    })
}
xml.toJson = (data) =>{
    return new Promise((resolve,reject)=>{
        if(data){
            parseString(data,(err,newData)=>{
                if(err){
                    reject(err)
                }else{
                   resolve(newData)
                }
            })
        }else{
            reject('Please Enter Data')
        }
    })
}

module.exports = xml