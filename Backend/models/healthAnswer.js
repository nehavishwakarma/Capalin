module.exports = (sequelize, DataTypes) => {
    const healthAnswer = sequelize.define('healthAnswer', {
        enquiryId: {
            type: DataTypes.STRING(100),
            field: 'enquiry_id'
        },
        isMainQuestion: {
            type: DataTypes.BIGINT(2),  
            field: 'isMainQuestion',
            defaultValue: '0'
        },
        insuredId: {
            type: DataTypes.BIGINT(20),
            field: 'insured_id'
        },
        questionId: {
            type: DataTypes.BIGINT(20),
            field: 'question_id'
        },
        subQuestionId: {
            type: DataTypes.BIGINT(20),
            field: 'sub_question_id'
        },
        questionAnswer: {
            type: DataTypes.TEXT,
            field: 'question_answer'
        },
        subQuestionAnswer: {
            type: DataTypes.TEXT,
            field: 'sub_question_answer'
        },
        apolloDiagnosisDate: {
            type: DataTypes.STRING(255),
            field: 'apollo_diagnosis_date'
        },
        apolloDateOfLastConsultation: {
            type: DataTypes.STRING(255),
            field: 'apollo_date_of_last_consultation'
        },
        apolloTreatmentOutpatient: {
            type: DataTypes.STRING(255),
            field: 'apollo_treatment_outpatient',
            defaultValue: '1',
        },
        apolloDoctorHospitalName: {
            type: DataTypes.STRING(255),
            field: 'apollo_doctor_hospital_name'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN(4),
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_answer'
    })
    healthAnswer.associate = (models) => {
        healthAnswer.belongsTo(models.healthQuestions, { foreignKey: 'questionId', as: 'healthQuestions' })
        healthAnswer.belongsTo(models.healthSubQuestions, { foreignKey: 'subQuestionId', as: 'healthSubQuestions' })
    }
    return healthAnswer
}