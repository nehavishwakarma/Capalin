module.exports = (sequelize, DataTypes) => {
    const bikeDigitGiMasterCode = sequelize.define('bikeDigitGiMasterCode', {
        bikeManufacturerId: {
            type: DataTypes.INTEGER,
            field: 'bike_manufacturer_id'
        },
        bikeModelId: {
            type: DataTypes.INTEGER,
            field: 'bike_model_id'
        },
        vehicleCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        seatingCapacity: {
            type: DataTypes.INTEGER,
            field: 'seating_capacity'
        },
        bodyType: {
            type: DataTypes.STRING,
            field: 'body_type'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_digit_gi_master_code'
    })
    return bikeDigitGiMasterCode
}