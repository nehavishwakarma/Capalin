module.exports = (sequelize, DataTypes) => {
    const paytmPaymentLink = sequelize.define('paytmPaymentLink', {
        emailListId: {
            type: DataTypes.BIGINT(20),
            field: 'email_list_id'
        },
        mobileListId: {
            type: DataTypes.BIGINT(20),
            field: 'mobile_list_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        fname: {
            type: DataTypes.STRING(255),
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING(255),
            field: 'lname'
        },
        amount: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'amount'
        },
        transctionDateTime: {
            type: DataTypes.DATE,
            field: 'transction_date_time'
        },
        customerId: {
            type: DataTypes.STRING(255),
            field: 'customer_id'
        },
        orderId: {
            type: DataTypes.STRING(255),
            field: 'order_id'
        },
        comments: {
            type: DataTypes.TEXT,
            field: 'comments'
        },
        paymentStatus: {
            type: DataTypes.INTEGER(11),
            field: 'payment_status'
        },
        linkSentStatus: {
            type: DataTypes.INTEGER(11),
            field: 'link_sent_status'
        },
        paymentLink: {
            type: DataTypes.STRING(255),
            field: 'payment_link'
        },
        transctionId: {
            type: DataTypes.STRING(255),
            field: 'transction_id'
        },
        allResponse: {
            type: DataTypes.TEXT,
            field: 'all_response'
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted',
            defaultValue: 0
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'           
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'paytm_payment_link'
    })
    return paytmPaymentLink
}       