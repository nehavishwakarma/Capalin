module.exports = (sequelize, DataTypes) => {
    const serviceRemarks = sequelize.define("serviceRemarks", {
        serviceId: {
            type: DataTypes.BIGINT(20),
            field: 'service_id'
        },
        createdBy: {
            type: DataTypes.BIGINT(20),
            field: 'created_by'
        },
        serviceStatus: {
            type: DataTypes.BIGINT(20),
            field: 'service_status'
        },
        remarks: {
            type: DataTypes.STRING,
            field: 'remarks'
        },
        serviceMailContent: {
            type: DataTypes.STRING,
            field: 'service_mail_content'
        },
        followupDate:{
            type:DataTypes.DATE,
            field:'followup_date'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'service_remarks'
    })
    return serviceRemarks
}
