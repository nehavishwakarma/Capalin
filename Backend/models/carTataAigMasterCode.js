module.exports = (sequelize, DataTypes) => {
    const carTataAigMasterCode = sequelize.define('carTataAigMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT(20),
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT(20),
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT(20),
            field: 'car_variant_id'
        },
        vehicleclasscode: {
            type: DataTypes.INTEGER(11),
            field: 'vehicleclasscode'
        },
        manufacturer: {
            type: DataTypes.STRING(255),
            field: 'manufacturer'
        },
        vehiclemodelcode: {
            type: DataTypes.INTEGER(11),
            field: 'vehiclemodelcode'
        },
        vehiclemodel: {
            type: DataTypes.STRING(255),
            field: 'vehiclemodel'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER(11),
            field: 'cubic_capacity'
        },
        grossvehicleweight: {
            type: DataTypes.INTEGER(11),
            field: 'grossvehicleweight'
        },
        seatingcapacity: {
            type: DataTypes.INTEGER(11),
            field: 'seatingcapacity'
        },
        carryingcapacity: {
            type: DataTypes.INTEGER(11),
            field: 'carryingcapacity',
        },
        tabrowindx: {
            type: DataTypes.INTEGER(11),
            field: 'tabrowindx'
        },
        bodytypecode: {
            type: DataTypes.INTEGER(11),
            field: 'bodytypecode'
        },
        vehiclemodelstatus: {
            type: DataTypes.STRING(255),
            field: 'vehiclemodelstatus'
        },
        numInsertTransId: {
            type: DataTypes.INTEGER(11),
            field: 'num_insert_trans_id'
        },
        numVehicleSubclassCode: {
            type: DataTypes.INTEGER(11),
            field: 'num_vehicle_subclass_code'
        },
        activeFlag: {
            type: DataTypes.STRING(255),
            field: 'active_flag'
        },
        txtFuel: {
            type: DataTypes.STRING(255),
            field: 'txt_fuel'
        },
        txtSegmenttype: {
            type: DataTypes.STRING(255),
            field: 'txt_segmenttype'
        },
        txtVariant: {
            type: DataTypes.STRING(255),
            field: 'txt_variant'
        },
        manufacturercode: {
            type: DataTypes.INTEGER(11),
            field: 'manufacturercode',
        },
        numVehAge: {
            type: DataTypes.INTEGER(11),
            field: 'num_veh_age'
        },
        numVehId: {
            type: DataTypes.INTEGER(11),
            field: 'num_veh_id'
        },
        numExshwrmPrce: {
            type: DataTypes.INTEGER(11),
            field: 'num_exshwrm_prce'
        },
        txtDeclined: {
            type: DataTypes.STRING(255),
            field: 'txt_declined'
        },
        txtReferred: {
            type: DataTypes.STRING(255),
            field: 'txt_referred'
        },
        txtTheftTarget: {
            type: DataTypes.STRING(255),
            field: 'txt_theft_target'
        },
        txtAutoTran: {
            type: DataTypes.STRING(255),
            field: 'txt_auto_tran'
        },
        txtAbs: {
            type: DataTypes.STRING(255),
            field: 'txt_abs'
        },
        txtVehicleRefNo: {
            type: DataTypes.INTEGER(11),
            field: 'txt_vehicle_ref_no'
        },
        txtExcessGroupCd: {
            type: DataTypes.STRING(255),
            field: 'txt_excess_group_cd'
        },
        txtVehicleType: {
            type: DataTypes.INTEGER(11),
            field: 'txt_vehicle_type'
        },
        numParentModelCode: {
            type: DataTypes.INTEGER(11),
            field: 'num_parent_model_code'
        },
        numLevel: {
            type: DataTypes.INTEGER(11),
            field: 'num_level'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_tata_aig_master_code'
    })
    return carTataAigMasterCode
}

