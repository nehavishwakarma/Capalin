module.exports = (sequelize, DataTypes) => {
    const policyDocumentUser = sequelize.define('policyDocumentUser', {
        insuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'insurance_id'
        },
        insuranceType: {
            type: DataTypes.STRING(255),
            field: 'insurance_type'
        },
        documentName: {
            type: DataTypes.STRING(255),
            field: 'document_name'
        },
        documentType: {
            type: DataTypes.STRING(255),
            field: 'document_type'
        },
        otherDocName: {
            type: DataTypes.STRING(255),
            field: 'other_doc_name'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        }
    }, {
        freezeTableName: true,
        tableName: 'policy_documents_user'
    })
    return policyDocumentUser
}       