module.exports = (sequelize, DataTypes) => {
    const carUnitedIndiaMasterCode = sequelize.define('carUnitedIndiaMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT(20),
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT(20),
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT(20),
            field: 'car_variant_id'
        },
        insurerMakeCode: {
            type: DataTypes.INTEGER(11),
            field: 'insurer_make_code'
        },
        insurerMake: {
            type: DataTypes.STRING(255),
            field: 'insurer_make'
        },
        modelIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'model_id_pk'
        },
        makeModel: {
            type: DataTypes.STRING(255),
            field: 'make_model'
        },
        fuel: {
            type: DataTypes.STRING(255),
            field: 'fuel'
        },
        variant: {
            type: DataTypes.STRING(255),
            field: 'variant'
        },
        cubicCapacity: {
            type: DataTypes.STRING(255),
            field: 'cubic_capacity'
        },
        seatingCapacity: {
            type: DataTypes.STRING(255),
            field: 'seating_capacity'
        },
        carryingCapacity: {
            type: DataTypes.STRING(255),
            field: 'carrying_capacity'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_united_india_master_code'
    })
    return carUnitedIndiaMasterCode
}

