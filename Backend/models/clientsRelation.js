module.exports = (sequelize, DataTypes) => {
    const clientsRelation = sequelize.define('clientsRelation', {
        relationshipName: {
            type: DataTypes.STRING(255),
            field: 'relationship_name'
        }
    }, {
        freezeTableName: true,
        tableName: 'clients_relation'
    })
    return clientsRelation
}