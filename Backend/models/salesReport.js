module.exports = (sequelize, DataTypes) => {
    const salesReport = sequelize.define("salesReport", {
        salesReportId: {
            type: DataTypes.BIGINT(20),
            field: 'sales_report_id'
        },
        emailListId: {
            type: DataTypes.BIGINT(20),
            field: 'email_list_id'
        },
        mobileListId: {
            type: DataTypes.BIGINT(20),
            field: 'mobile_list_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        leadId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_id'
        },
        teamLeaderId: {
            type: DataTypes.INTEGER,
            field: 'team_leader_id',
            defaultValue: 0
        },
        soldById: {
            type: DataTypes.BIGINT(20),
            field: 'sold_by_id'
        },
        businessType: {
            type: DataTypes.ENUM('1', '2', '3', '4'),
            field: 'business_type'
        },
        sourceOfPolicy: {
            type: DataTypes.ENUM('0', '1', '2', '3', '4', '5', '6', '7', '8'),
            field: 'source_of_policy',
            defaultValue: '0'
        },
        policyIssueDate: {
            type: DataTypes.DATE,
            field: 'policy_issue_date'
        },
        paymentDate: {
            type: DataTypes.DATE,
            field: 'payment_date'
        },
        fname: {
            type: DataTypes.STRING,
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING,
            field: 'lname'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        sumAssured: {
            type: DataTypes.INTEGER,
            field: 'sum_assured'
        },
        grossPremium: {
            type: DataTypes.INTEGER,
            field: 'gross_premium'
        },
        premium: {
            type: DataTypes.DECIMAL(10, 0),
            field: 'premium'
        },
        motorRegistrationDate: {
            type: DataTypes.DATE,
            field: 'motor_registration_date'
        },
        motorCubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'motor_cubic_capacity'
        },
        renewalPremium: {
            type: DataTypes.INTEGER,
            field: 'renewal_premium',
            defaultValue: 0
        },
        payoutPremium: {
            type: DataTypes.INTEGER,
            field: 'payout_premium'
        },
        payoutRate: {
            type: DataTypes.DECIMAL(10, 2),
            field: 'payout_rate',
            defaultValue: 0.00
        },
        payinRate: {
            type: DataTypes.DECIMAL(10, 2),
            field: 'payin_rate',
            defaultValue: 0.00
        },
        policyId: {
            type: DataTypes.STRING,
            field: 'policy_id'
        },
        term: {
            type: DataTypes.INTEGER,
            field: 'term'
        },
        policyStartDate: {
            type: DataTypes.DATE,
            field: 'policy_start_date'
        },
        policyEndDate: {
            type: DataTypes.DATE,
            field: 'policy_end_date'
        },
        policyRenewalDate: {
            type: DataTypes.DATE,
            field: 'policy_renewal_date'
        },
        policyReceivedByClient: {
            type: DataTypes.ENUM('0', '1', '2'),
            field: 'policy_received_by_client',
            defaultValue: '0'
        },
        policyStatus: {
            type: DataTypes.ENUM('0', '1', '2', '3'),
            field: 'policy_status',
            defaultValue: '0'
        },
        renewalStatus: {
            type: DataTypes.ENUM('0', '1', '2', '3'),
            field: 'renewal_status',
            defaultValue: '0'
        },
        saleMode: {
            type: DataTypes.STRING,
            field: 'sale_mode'
        },
        remarks: {
            type: DataTypes.TEXT,
            field: 'remarks'
        },
        payoutMonth: {
            type: DataTypes.DATE,
            field: 'payout_month'
        },
        payoutDate: {
            type: DataTypes.DATE,
            field: 'payout_date'
        },
        payoutStatus: {
            type: DataTypes.INTEGER,
            field: 'payout_status',
            defaultValue: 2
        },
        downloadPolicy: {
            type: DataTypes.STRING,
            field: 'download_policy'
        },
        commRecDate: {
            type: DataTypes.DATE,
            field: 'comm_rec_date'
        },
        orcRecDate: {
            type: DataTypes.DATE,
            field: 'orc_rec_date'
        },
        commissionStatus: {
            type: DataTypes.ENUM('0', '1', '2'),
            field: 'commission_status'
        },
        orcStatus: {
            type: DataTypes.ENUM('0', '1', '2'),
            field: 'orc_status'
        },
        issuedPolicySentCount: {
            type: DataTypes.INTEGER,
            field: 'issued_policy_sent_count'
        },
        welcomeCallDate: {
            type: DataTypes.DATE,
            field: 'welcome_call_date'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'sales_report'
    })
    return salesReport
}
