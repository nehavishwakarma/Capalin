module.exports = (sequelize, DataTypes) => {
    const emailList = sequelize.define('emailList', {
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        pageUrl: {
            type: DataTypes.STRING,
            field: 'page_url'
        },
        source: {
            type: DataTypes.STRING,
            field: 'source'
        },
        emailConfirm: {
            type: DataTypes.STRING,
            field: 'email_confirm'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted',
            defaultValue: '0'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'email_list'
    })
    return emailList
}