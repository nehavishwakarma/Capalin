module.exports = (sequelize, DataTypes) => {
    const operationComment = sequelize.define('operationComment', {
        operationId: {
            type: DataTypes.BIGINT(20),
            field: 'operation_id'
        },
        commentEntryDate: {
            type: DataTypes.DATE,
            field: 'comment_entry_date'
        },
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        adminEmail: {
            type: DataTypes.STRING(100),
            field: 'admin_email'
        },
        adminFname: {
            type: DataTypes.STRING(100),
            field: 'admin_fname'
        },
        adminLname: {
            type: DataTypes.STRING(100),
            field: 'admin_lname'
        },
        adminDate: {
            type: DataTypes.DATE,
            field: 'admin_date'
        },
        queryMode: {
            type: DataTypes.STRING(100),
            field: 'query_mode'
        },
        comment: {
            type: DataTypes.TEXT,
            field: 'comment'
        },
        followUp: {
            type: DataTypes.DATE,
            field: 'follow_up'
        }
    }, {
        freezeTableName: true,
        tableName: 'operation_comment'
    })
    return operationComment
}       