const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const carFuelType = sequelize.define('carFuelType', {
        carModelId: {
            type: DataTypes.BIGINT,
            field: 'car_model_id'
        },
        fuelName: {
            type: DataTypes.STRING,
            field: 'fuel_name'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_fuel_type'
    })
    return carFuelType
}