module.exports = (sequelize, DataTypes) => {
    const pincode = sequelize.define('pincode', {
        stateId: {
            type: DataTypes.BIGINT(20),
            field: 'state_id'
        },
        pincode: {
            type: DataTypes.STRING,
            field: 'pincode'
        },
        areaCode: {
            type: DataTypes.STRING,
            field: 'area_code'
        },
        starHealthFho: {
            type: DataTypes.STRING,
            field: 'star_health_fho'
        },
        starHealthMediclassic: {
            type: DataTypes.STRING,
            field: 'star_health_mediclassic'
        },
        apolloMunich: {
            type: DataTypes.STRING,
            field: 'apollo_munich'
        },
        bajajHealthGuard: {
            type: DataTypes.STRING,
            field: 'bajaj_health_guard'
        },
        maxbupaHc: {
            type: DataTypes.STRING,
            field: 'maxbupa_hc'
        },
        hdfcTierId: {
            type: DataTypes.STRING,
            field: 'hdfc_tier_id'
        }
    }, {
        freezeTableName: true,
        tableName: 'pincode'
    })
    pincode.associate = (models) => {
        pincode.belongsTo(models.states, { foreignKey: 'stateId', as: 'states' })   
    }
    return pincode
}