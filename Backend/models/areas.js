module.exports = (sequelize, DataTypes) => {
    const areas = sequelize.define('areas', {
        companyId: {
            type: DataTypes.BIGINT,
            field: 'company_id'
        },
        citiesId: {
            type: DataTypes.BIGINT,
            field: 'cities_id'
        },
        area: {
            type: DataTypes.STRING,
            field: 'area'
        },
        areaCode: {
            type: DataTypes.STRING,
            field: 'area_code'
        }
    }, {
        freezeTableName: true,
        tableName: 'areas'
    })
    return areas
}