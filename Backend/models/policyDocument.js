module.exports = (sequelize, DataTypes) => {
    const policyDocument = sequelize.define('policyDocument', {
        emailListId: {
            type: DataTypes.BIGINT(20),
            field: 'email_list_id'
        },
        salesReportId: {
            type: DataTypes.BIGINT(20),
            field: 'sales_report_id'
        },
        documentName: {
            type: DataTypes.STRING(255),
            field: 'document_name'
        },
        documentType: {
            type: DataTypes.INTEGER(11),
            field: 'document_type'
        },
        uploadType: {
            type: DataTypes.ENUM('1', '2'),
            field: 'upload_type',
            defaultValue: '1'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status',
            defaultValue: 1
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        }
    }, {
        freezeTableName: true,
        tableName: 'policy_document'
    })
    return policyDocument
}       