module.exports = (sequelize, DataTypes) => {
    const feedbackUserData = sequelize.define('feedbackUserData', {
        leadId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_id'
        },
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        questionId: {
            type: DataTypes.BIGINT(20),
            field: 'question_id'
        },
        answerId: {
            type: DataTypes.BIGINT(20),
            field: 'answer_id'
        },
        answerDescription: {
            type: DataTypes.STRING(255),
            field: 'answer_description'
        },
        source: {
            type: DataTypes.STRING(255),
            field: 'source'
        },
        ipAddress: {
            type: DataTypes.STRING(255),
            field: 'ip_address'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'feedback_user_data'
    })
    return feedbackUserData
}