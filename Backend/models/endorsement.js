module.exports = (sequelize, DataTypes) => {
    const endorsement = sequelize.define('endorsement', {
        policyNo: {
            type: DataTypes.INTEGER(50),
            field: 'policy_no'
        },
        changeType: {
            type: DataTypes.INTEGER(50),
            field: 'change_type'
        },
        address: {
            type: DataTypes.STRING(50),
            field: 'address'
        },
        state: {
            type: DataTypes.STRING(20),
            field: 'state'
        },
        city: {
            type: DataTypes.STRING(15),
            field: 'city'
        },
        pincode: {
            type: DataTypes.INTEGER(15),
            field: 'pincode'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        sumInsured: {
            type: DataTypes.STRING(15),
            field: 'sum_insured',
            defaultValue: '1'
        },
        otherChanges: {
            type: DataTypes.STRING(150),
            field: 'other_changes'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'endorsement'
    })
    return endorsement
}