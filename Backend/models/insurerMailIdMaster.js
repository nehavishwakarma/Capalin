module.exports = (sequelize, DataTypes) => {
    const insurerMailIdMaster = sequelize.define('insurerMailIdMaster', {
        company_id: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        email_id: {
            type: DataTypes.STRING(255),
            field: 'email_id'
        },
        field_type: {
            type: DataTypes.ENUM('1', '2', '3'),
            field: 'field_type',
            defaultValue: '1'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        }
    }, {
        freezeTableName: true,
        tableName: 'insurer_mail_id_master'
    })
    return insurerMailIdMaster
}   