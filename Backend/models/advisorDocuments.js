module.exports = (sequelize, DataTypes) => {
    const advisorDocuments = sequelize.define('advisorDocuments', {
        advisorRegistration_form_id: {
            type: DataTypes.BIGINT,
            field: 'advisor_registration_form_id'
        },
        docType: {
            type: DataTypes.INTEGER,
            field: 'doc_type'
        },
        docName: {
            type: DataTypes.STRING,
            field: 'doc_name'
        },
        details: {
            type: DataTypes.TEXT,
            field: 'details'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'advisor_documents',
    })
    return advisorDocuments
}