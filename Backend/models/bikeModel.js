module.exports = (sequelize, DataTypes) => {
    const bikeModel = sequelize.define('bikeModel', {
        manufacturerId: {
            type: DataTypes.BIGINT,
            field: 'manufacturer_id'
        },
        model: {
            type: DataTypes.STRING,
            field: 'model'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_model'
    })
    return bikeModel
}
