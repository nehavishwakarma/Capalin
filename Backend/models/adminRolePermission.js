module.exports = (sequelize, DataTypes) => {
    const adminRolePermissions = sequelize.define('adminRolePermissions', {
        adminGroupId: {
            type: DataTypes.BIGINT,
            field: 'admin_group_id'
        },
        moduleId: {
            type: DataTypes.BIGINT,
            field: 'module_id'
        },
        permission: {
            type: DataTypes.ENUM('0', '1'),
            field: 'permission',
            defaultvalue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        status: {
            type: DataTypes.ENUM('active', 'disabled'),
            field: 'status',
            defaultvalue: 'active'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultvalue: '0'
        },
        deletedAt: {
            type: DataTypes.DATE,
            field: 'deleted_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'admin_role_permissions'
    })
    return adminRolePermissions
}