module.exports = (sequelize, DataTypes) => {
    const bikeProposalForm = sequelize.define('bikeProposalForm', {
        bikeEnquiryId: {
            type: DataTypes.BIGINT(20),
            field: 'bike_enquiry_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        bikeOwner: {
            type: DataTypes.STRING(200),
            field: 'bike_owner',
            defaultValue: 'individual'
        },
        companyName: {
            type: DataTypes.TEXT,
            field: 'company_name'
        },
        firstName: {
            type: DataTypes.TEXT,
            field: 'first_name'
        },
        lastName: {
            type: DataTypes.TEXT,
            field: 'last_name'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        aadhar: {
            type: DataTypes.TEXT,
            field: 'aadhar'
        },
        gstNo: {
            type: DataTypes.TEXT,
            field: 'gst_no'
        },
        gender: {
            type: DataTypes.STRING(50),
            field: 'gender'
        },
        maritalStatus: {
            type: DataTypes.STRING(50),
            field: 'marital_status',
        },
        email: {
            type: DataTypes.TEXT,
            field: 'email'
        },
        mobile: {
            type: DataTypes.TEXT,
            field: 'mobile'
        },
        nomineeName: {
            type: DataTypes.TEXT,
            field: 'nominee_name'
        },
        nomineeDob: {
            type: DataTypes.DATE,
            field: 'nominee_dob'
        },
        nomineeRelationship: {
            type: DataTypes.TEXT,
            field: 'nominee_relationship'
        },
        regAddress: {
            type: DataTypes.TEXT,
            field: 'reg_address'
        },
        regLocality: {
            type: DataTypes.TEXT,
            field: 'reg_locality'
        },
        regLandmark: {
            type: DataTypes.TEXT,
            field: 'reg_landmark'
        },
        regCityId: {
            type: DataTypes.BIGINT(20),
            field: 'reg_city_id'
        },
        regDistrict_id: {
            type: DataTypes.TEXT,
            field: 'reg_district_id'
        },
        regStateId: {
            type: DataTypes.TEXT,
            field: 'reg_state_id'
        },
        regPincode: {
            type: DataTypes.TEXT,
            field: 'reg_pincode'
        },
        regCityName: {
            type: DataTypes.TEXT,
            field: 'reg_city_name'
        },
        regStateName: {
            type: DataTypes.TEXT,
            field: 'reg_state_name'
        },
        addCommunicationAddress: {
            type: DataTypes.TEXT,
            field: 'add_communication_address'
        },
        communicationAddress: {
            type: DataTypes.TEXT,
            field: 'communication_address'
        },
        communicationLocality: {
            type: DataTypes.TEXT,
            field: 'communication_locality'
        },
        communicationLandmark: {
            type: DataTypes.TEXT,
            field: 'communication_landmark'
        },
        communicationCityId: {
            type: DataTypes.TEXT,
            field: 'communication_city_id'
        },
        communicationDistrictId: {
            type: DataTypes.TEXT,
            field: 'communication_district_id',
        },
        communicationStateId: {
            type: DataTypes.TEXT,
            field: 'communication_state_id'
        },
        communicationPincode: {
            type: DataTypes.TEXT,
            field: 'communication_pincode'
        },
        communicationCityName: {
            type: DataTypes.TEXT,
            field: 'communication_city_name'
        },
        communicationStateName: {
            type: DataTypes.TEXT,
            field: 'communication_state_name'
        },
        registerationNo: {
            type: DataTypes.TEXT,
            field: 'registeration_no'
        },
        engineeNo: {
            type: DataTypes.TEXT,
            field: 'enginee_no'
        },
        chassisNo: {
            type: DataTypes.TEXT,
            field: 'chassis_no'
        },
        expiryPolicyNo: {
            type: DataTypes.TEXT,
            field: 'expiry_policy_no'
        },
        existingInsurerId: {
            type: DataTypes.TEXT,
            field: 'existing_insurer_id'
        },
        existingInsurerName: {
            type: DataTypes.TEXT,
            field: 'existing_insurer_name'
        },
        isBikeFinance: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'is_bike_finance'
        },
        loanFinancier: {
            type: DataTypes.TEXT,
            field: 'loan_financier'
        },
        loanFinancierName: {
            type: DataTypes.TEXT,
            field: 'loan_financier_name'
        },
        loanCity: {
            type: DataTypes.STRING(250),
            field: 'loan_city'
        },
        loanStartDate: {
            type: DataTypes.DATE,
            field: 'loan_start_date'
        },
        paymentMode: {
            type: DataTypes.TEXT,
            field: 'payment_mode'
        },
        proposalNo: {
            type: DataTypes.TEXT,
            field: 'proposal_no'
        },
        policyNo: {
            type: DataTypes.TEXT,
            field: 'policy_no'
        },
        proposalStatus: {
            type: DataTypes.INTEGER(11),
            field: 'proposal_status',
            defaultValue: 0
        },
        paymentStatus: {
            type: DataTypes.INTEGER(11),
            field: 'payment_status',
            defaultValue: 0
        },
        policyStatus: {
            type: DataTypes.INTEGER(11),
            field: 'policy_status',
            defaultValue: 0
        },
        proposalResponse: {
            type: DataTypes.TEXT,
            field: 'proposal_response'
        },
        paymentGatewayResponse: {
            type: DataTypes.TEXT,
            field: 'payment_gateway_response'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        existingInsurance: {
            type: DataTypes.STRING(50),
            field: 'existing_insurance'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_proposal_form'
    })
    bikeProposalForm.associate = (models) => {
        bikeProposalForm.belongsTo(models.bikeEnquiry, { foreignKey: 'bikeEnquiryId', as: 'bikeProposalFormToBikeEnquiry', targetKey: 'enquiryId' })
    }
    return bikeProposalForm
}