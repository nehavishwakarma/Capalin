module.exports = (sequelize, DataTypes) => {
    const newIndiaStates = sequelize.define('newIndiaStates', {
        stateName: {
            type: DataTypes.STRING(255),
            field: 'state_name'
        },
        stateCode: {
            type: DataTypes.STRING(255),
            field: 'state_code'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'new_india_states'
    })
    return newIndiaStates
}       