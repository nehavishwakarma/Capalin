module.exports = (sequelize, DataTypes) => {
    const healthQuestions = sequelize.define('healthQuestions', {
        masterSheetHealthId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_health_id'
        },
        questions: {
            type: DataTypes.TEXT,
            field: 'questions'
        },
        questionDescribe: {
            type: DataTypes.TEXT,
            field: 'question_describe'
        },
        questionCode: {
            type: DataTypes.STRING(255),
            field: 'question_code'
        },
        questionSetCode: {
            type: DataTypes.STRING(255),
            field: 'question_set_code'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isSubquestions: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_subquestions',
            defaultvalue: '0'
        },
        isQuestionAns: {
            type: DataTypes.INTEGER(11),
            field: 'is_question_ans',
            defaultvalue: 0
        },
        apolloAlcholStatus: {
            type: DataTypes.INTEGER(11),
            field: 'apollo_alchol_status',
            defaultvalue: 0
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_questions'
    })
    return healthQuestions
}   