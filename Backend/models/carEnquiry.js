module.exports = (sequelize, DataTypes) => {
    const carEnquiry = sequelize.define('carEnquiry', {
        enquiryId: {
            type: DataTypes.STRING,
            field: 'enquiry_id'
        },
        leadInsuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_insurance_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        modelId: {
            type: DataTypes.BIGINT(20),
            field: 'model_id'
        },
        fuelTypeId: {
            type: DataTypes.BIGINT(20),
            field: 'fuel_type_id'
        },
        varientId: {
            type: DataTypes.BIGINT(20),
            field: 'varient_id'
        },
        cityRegistrationId: {
            type: DataTypes.BIGINT(20),
            field: 'city_registration_id'
        },
        cityId: {
            type: DataTypes.BIGINT(20),
            field: 'city_id'
        },
        policyType: {
            type: DataTypes.STRING,
            field: 'policy_type'
        },
        carRegisterDate: {
            type: DataTypes.DATE,
            field: 'car_register_date'
        },
        carRegistrationNo: {
            type: DataTypes.STRING,
            field: 'car_registration_no',
        },
        existingPolicyNo: {
            type: DataTypes.STRING,
            field: 'existing_policy_no'
        },
        existingInsuranceCompanyId: {
            type: DataTypes.STRING,
            field: 'existing_insurance_company_id'
        },
        previousYearPolicyExpirtyDate: {
            type: DataTypes.DATE,
            field: 'previous_year_policy_expirty_date'
        },
        manufacturingDate: {
            type: DataTypes.DATE,
            field: 'manufacturing_date'
        },
        claimPastYear: {
            type: DataTypes.STRING,
            field: 'claim_past_year'
        },
        ncb: {
            type: DataTypes.INTEGER,
            field: 'ncb'
        },
        idv: {
            type: DataTypes.STRING,
            field: 'idv'
        },
        royalSundaramIdv: {
            type: DataTypes.INTEGER,
            field: 'royal_sundaram_idv'
        },
        zeroDepreciation: {
            type: DataTypes.STRING,
            field: 'zero_depreciation'
        },
        relianceNilDepApplicableRate: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'reliance_nil_dep_applicable_rate',
        },
        invoiceCover: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'invoice_cover',           
            defaultValue: 'false'
        },
        rodeSideAssistance: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'rode_side_assistance',            
            defaultValue: 'false'
        },
        engineProtector: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'engine_protector',
            defaultValue: 'false'
        },
        ncbProtection: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'ncb_protection',
            defaultValue: 'false'
        },
        consumable: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'consumable',            
            defaultValue: 'false'
        },
        keyProtector: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'key_protector',
            defaultValue: 'false'
        },
        paidDriverLegalLiabilityCover: {
            type: DataTypes.STRING,
            field: 'paid_driver_legal_liability_cover',
            defaultValue: 'false'
        },
        paToOwnerDriver: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'pa_to_owner_driver',
            defaultValue: 'true'
        },
        driverCover: {
            type: DataTypes.STRING,
            field: 'driver_cover',
            defaultValue: 'false'
        },
        tyreCover: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'tyre_cover',
            defaultValue: 'false'
        },
        electricalAccessories: {
            type: DataTypes.INTEGER,
            field: 'electrical_accessories',
            defaultValue: 0
        },
        nonElectricalAccessories: {
            type: DataTypes.INTEGER,
            field: 'non_electrical_accessories',
            defaultValue: 0
        },
        externalBiFuelKit: {
            type: DataTypes.STRING,
            field: 'external_bi_fuel_kit'
        },
        externalBiFuelKitValue: {
            type: DataTypes.INTEGER,
            field: 'external_bi_fuel_kit_value'
        },
        voluntaryDeductibleValue: {
            type: DataTypes.INTEGER,
            field: 'voluntary_deductible_value'
        },
        antiTheftDevice: {
            type: DataTypes.STRING,
            field: 'anti_theft_device'
        },
        automobileAssociation: {
            type: DataTypes.STRING,
            field: 'automobile_association'
        },
        finalPremium: {
            type: DataTypes.STRING,
            field: 'final_premium'
        },
        odPremium: {
            type: DataTypes.STRING,
            field: 'od_premium'
        },
        paToOwnerDriverPremium: {
            type: DataTypes.STRING,
            field: 'pa_to_owner_driver_premium'
        },
        includingAllPremium: {
            type: DataTypes.INTEGER,
            field: 'including_all_premium'
        },
        depreciationAmount: {
            type: DataTypes.STRING,
            field: 'depreciation_amount'
        },
        calculatedDep: {
            type: DataTypes.INTEGER,
            field: 'calculated_dep'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        mobileOtp: {
            type: DataTypes.INTEGER,
            field: 'mobile_otp'
        },
        mobileStatus: {
            type: DataTypes.INTEGER,
            field: 'mobile_status'
        },
        firstName: {
            type: DataTypes.STRING,
            field: 'first_name'
        },
        lastName: {
            type: DataTypes.STRING,
            field: 'last_name'
        },
        pincode: {
            type: DataTypes.STRING,
            field: 'pincode'
        },
        transactionId: {
            type: DataTypes.STRING,
            field: 'transaction_id'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        tpPremium: {
            type: DataTypes.STRING,
            field: 'tpPremium'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_enquiry',
    })
    carEnquiry.associate = (models) => {
        carEnquiry.belongsTo(models.leadInsurance, { foreignKey: 'leadInsuranceId', as: 'leadInsurance' })
        carEnquiry.belongsTo(models.carModel, { foreignKey: 'modelId', as: 'carModel' })
        carEnquiry.belongsTo(models.carFuelType, { foreignKey: 'fuelTypeId', as: 'carFuelType' })
        carEnquiry.belongsTo(models.carVariant, { foreignKey: 'varientId', as: 'carVariant' })
        carEnquiry.belongsTo(models.carRto, { foreignKey: 'cityRegistrationId', as: 'carRto' })     

    }
    return carEnquiry
}