module.exports = (sequelize, DataTypes) => {
    const bikeInsurerRto = sequelize.define('bikeInsurerRto', {
        carRtoId: {
            type: DataTypes.INTEGER,
            field: 'car_rto_id'
        },
        companyId: {
            type: DataTypes.INTEGER,
            field: 'company_id'
        },
        insurerRtoCode: {
            type: DataTypes.STRING,
            field: 'insurer_rto_code'
        },
        description: {
            type: DataTypes.STRING,
            field: 'description'
        },
        stateId: {
            type: DataTypes.STRING,
            field: 'state_id'
        },
        stateName: {
            type: DataTypes.STRING,
            field: 'state_name'
        },
        zoneCode: {
            type: DataTypes.STRING,
            field: 'zone_code'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        createdAt: {
            type: DataTypes.STRING,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.STRING,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_insurer_rto'
    })
    return bikeInsurerRto
}
