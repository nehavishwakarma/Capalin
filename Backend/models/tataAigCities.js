module.exports = (sequelize, DataTypes) => {
    const tataAigCities = sequelize.define("tataAigCities", {
        numStateCd: {
            type: DataTypes.INTEGER,
            field: 'num_state_cd'
        },
        numCityistrictCd: {
            type: DataTypes.INTEGER,
            field: 'num_citydistrict_cd'
        },
        pincode: {
            type: DataTypes.INTEGER,
            field: 'pincode'
        },
        txtPincodeLocality:{
            type:DataTypes.STRING,
            field:'txt_pincode_locality'
        },
        numCityCd: {
            type: DataTypes.INTEGER,
            field: 'num_city_cd'
        },
        status:{
            type:DataTypes.INTEGER,
            field:'status'
        },
        isDeleted :{
            type:DataTypes.INTEGER,
            field:'is_deleted'
        }    
    }, {
        freezeTableName: true,
        tableName: 'tata_aig_cities'
    })
    return tataAigCities
}
