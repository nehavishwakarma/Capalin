module.exports = (sequelize, DataTypes) => {
    const leadInsurance = sequelize.define("leadInsurance", {
        emailListId: {
            type: DataTypes.BIGINT(20),
            field: 'email_list_id'
        },
        mobileListId: {
            type: DataTypes.BIGINT(20),
            field: 'mobile_list_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        premium: {
            type: DataTypes.DECIMAL(10, 0),
            field: 'premium'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        enquiryType: {
            type: DataTypes.STRING,
            field: 'enquiry_type'
        },
        policyType: {
            type: DataTypes.STRING,
            field: 'policy_type'
        },
        fname: {
            type: DataTypes.STRING,
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING,
            field: 'lname'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        city: {
            type: DataTypes.STRING,
            field: 'city'
        },
        state: {
            type: DataTypes.STRING,
            field: 'state'
        },
        pincode: {
            type: DataTypes.STRING,
            field: 'pincode'
        },
        country: {
            type: DataTypes.STRING,
            field: 'country'
        },
        leadMessage: {
            type: DataTypes.TEXT,
            field: 'lead_message'
        },
        ratedByClient: {
            type: DataTypes.INTEGER,
            field: 'rated_by_client'
        },
        source: {
            type: DataTypes.STRING,
            field: 'source'
        },
        medium: {
            type: DataTypes.INTEGER,
            field: 'medium',
            defaultValue: 1
        },
        bestDisposition: {
            type: DataTypes.BIGINT,
            field: 'best_disposition'
        },
        bestDispositionGroup: {
            type: DataTypes.INTEGER,
            field: 'best_disposition_group'
        },
        status: {
            type: DataTypes.STRING,
            field: 'status',
            defaultValue: '1'
        },
        emailStatus: {
            type: DataTypes.INTEGER,
            field: 'email_status',
            defaultValue: 0
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultValue: 0
        },
        quotePageVisited: {
            type: DataTypes.INTEGER,
            field: 'quote_page_visited',
            defaultValue: 0
        },
        reason: {
            type: DataTypes.STRING,
            field: 'reason'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_insurance'
    })
    leadInsurance.associate = (models) => {
        leadInsurance.hasMany(models.carEnquiry, { foreignKey: 'lead_insurance_id', as: 'leadInsurance' })
        leadInsurance.belongsTo(models.masterSheet, { foreignKey: 'masterSheetId', as: 'msId' })
        leadInsurance.belongsTo(models.leadInsuranceStatus, { foreignKey: 'status', targetKey: 'statusCode', as: 'leadInsuranceToLeadInsuranceStatus' })
    }
    return leadInsurance
}