module.exports = (sequelize, DataTypes) => {
    const feedbackAnswers = sequelize.define('feedbackAnswers', {
        answer_description: {
            type: DataTypes.STRING(255),
            field: 'answer_description'
        },
        question_id: {
            type: DataTypes.BIGINT(20),
            field: 'question_id'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'feedback_answers'
    })
    return feedbackAnswers
}