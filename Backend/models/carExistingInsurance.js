module.exports = (sequelize, DataTypes) => {
    const carExistingInsurance = sequelize.define('carExistingInsurance', {
        companyId: {
            type: DataTypes.BIGINT,
            field: 'company_id'
        },
        insuranceCompany: {
            type: DataTypes.STRING,
            field: 'insurance_company'
        },
        companyCode: {
            type: DataTypes.STRING,
            field: 'company_code'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultValue: 0
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_existing_insurance'
    })
    return carExistingInsurance
}