module.exports = (sequelize, DataTypes) => {
    const taskScheduleAttachments = sequelize.define("taskScheduleAttachments", {
        taskScheduleId: {
            type: DataTypes.INTEGER,
            field: 'task_schedule_id'
        },
        fileName: {
            type: DataTypes.STRING,
            field: 'file_name'
        },
        originalFileName: {
            type: DataTypes.STRING,
            field: 'original_file_name'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        createdAt:{
            type:DataTypes.DATE,
            field:'created_at'
        },
        updatedAt:{
            type:DataTypes.DATE,
            field:'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'task_schedule_attachments'
    })
    return taskScheduleAttachments
}
