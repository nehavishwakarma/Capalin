module.exports = (sequelize, DataTypes) => {
    const leadCaimpaignDetails = sequelize.define('leadCaimpaignDetails', {
        leadId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_id'
        },
        campaignType: {
            type: DataTypes.STRING,
            field: 'campaign_type'
        },
        vendorId: {
            type: DataTypes.BIGINT(20),
            field: 'vendor_id'
        },
        campaignDate: {
            type: DataTypes.DATE,
            field: 'caimpaign_date'
        },
        utmCampaign: {
            type: DataTypes.STRING,
            field: 'utm_campaign'
        },
        ipAddress: {
            type: DataTypes.STRING,
            field: 'ip_address'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultValue: 0
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_caimpaign_details'
    })
    return leadCaimpaignDetails
}