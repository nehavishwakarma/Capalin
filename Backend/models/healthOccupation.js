module.exports = (sequelize, DataTypes) => {
    const healthOccupation = sequelize.define('healthOccupation', {
        companyId: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        masterSheetId: {
            type: DataTypes.INTEGER,
            field: 'master_sheet_id'
        },
        occupationCode: {
            type: DataTypes.STRING(255),
            field: 'occupation_code'
        },
        occupation: {
            type: DataTypes.STRING(255),
            field: 'occupation'
        },
        level: {
            type: DataTypes.STRING(255),
            field: 'level'
        },
        category: {
            type: DataTypes.STRING(255),
            field: 'category'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultvalue: '1'
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_occupation'
    })
    return healthOccupation
}   