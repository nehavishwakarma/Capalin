module.exports = (sequelize, DataTypes) => {
    const leadAssignManagementSystems = sequelize.define('leadAssignManagementSystems', {
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        policyCategory: {
            type: DataTypes.BIGINT(20),
            field: 'policy_category'
        },
        leadPerDay: {
            type: DataTypes.INTEGER(11),
            field: 'lead_per_day'
        },
        campaignBy: {
            type: DataTypes.STRING(255),
            field: 'campaign_by'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultvalue: '1'
        },
        currentAssignStatus: {
            type: DataTypes.ENUM('0', '1'),
            field: 'current_assign_status',
            defaultvalue: '0'
        },
        pageRefreshTime: {
            type: DataTypes.INTEGER(11),
            field: 'page_refresh_time'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted',
            defaultvalue: '0'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_assign_management_systems'
    })
    return leadAssignManagementSystems
}   