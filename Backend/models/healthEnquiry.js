module.exports = (sequelize, DataTypes) => {
    const healthEnquiry = sequelize.define('healthEnquiry', {
        enquiryId: {
            type: DataTypes.STRING(255),
            field: 'enquiry_id'
        },
        leadInsuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_insurance_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        healthInsuranceId: {
            type: DataTypes.INTEGER(11),
            field: 'health_insurance_id'
        },
        adults: {
            type: DataTypes.INTEGER(11),
            field: 'adults'
        },
        children: {
            type: DataTypes.INTEGER(11),
            field: 'children'
        },
        gender: {
            type: DataTypes.ENUM('Male', 'Female'),
            field: 'gender'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        age: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'age'
        },
        minInsuranceCover: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'min_insurance_cover',
            defaultValue: 1
        },
        maxInsuranceCover: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'max_insurance_cover'
        },
        healthPremium: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'health_premium'
        },
        policyTerm: {
            type: DataTypes.INTEGER(11),
            field: 'policy_term'
        },
        pincode: {
            type: DataTypes.INTEGER(11),
            field: 'pincode'
        },
        firstName: {
            type: DataTypes.STRING(255),
            field: 'first_name'
        },
        lastName: {
            type: DataTypes.STRING(255),
            field: 'last_name'
        },
        email: {
            type: DataTypes.STRING(255),
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING(255),
            field: 'mobile'
        },
        mobileOtp: {
            type: DataTypes.INTEGER(11),
            field: 'mobile_otp'
        },
        mobileStatus: {
            type: DataTypes.INTEGER(11),
            field: 'mobile_status'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status',
            defaultValue: 1
        },
        enquiryUrl: {
            type: DataTypes.STRING(255),
            field: 'enquiry_url'
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_enquiry'
    })
    healthEnquiry.associate = (models) => {
        healthEnquiry.belongsTo(models.leadInsurance, { foreignKey: 'leadInsuranceId', as: 'leadInsurance' })
        healthEnquiry.belongsTo(models.masterSheet, { foreignKey: 'masterSheetId', as: 'masterSheet' })
        healthEnquiry.belongsTo(models.healthInsurance, { foreignKey: 'healthInsuranceId', as: 'healthInsurance' })
    }
    return healthEnquiry
}   