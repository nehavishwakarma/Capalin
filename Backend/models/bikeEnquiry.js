module.exports = (sequelize, DataTypes) => {
    const bikeEnquiry = sequelize.define('bikeEnquiry', {
        enquiryId: {
            type: DataTypes.STRING,
            field: 'enquiry_id'
        },
        leadInsuranceId: {
            type: DataTypes.BIGINT,
            field: 'lead_insurance_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT,
            field: 'master_sheet_id'
        },
        modelId: {
            type: DataTypes.BIGINT,
            field: 'model_id'
        },
        manufacturerId: {
            type: DataTypes.BIGINT,
            field: 'manufacturer_id'
        },
        rtoId: {
            type: DataTypes.BIGINT,
            field: 'rto_id'
        },
        existingInsuranceCompanyId: {
            type: DataTypes.INTEGER,
            field: 'existing_insurance_company_id'
        },
        existingPolicyNo: {
            type: DataTypes.STRING,
            field: 'existing_policy_no'
        },
        bikeRegistrationNo: {
            type: DataTypes.STRING,
            field: 'bike_registration_no'
        },
        policyType: {
            type: DataTypes.STRING,
            field: 'policy_type'
        },
        zeroDepreciation: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'zero_depreciation',
            defaultValue: 'false'
        },
        paToOwnerDriver: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'pa_to_owner_driver',
            defaultValue: 'true'
        },
        passengerCover: {
            type: DataTypes.STRING,
            field: 'passenger_cover',
            defaultValue: 'false'
        },
        paidDriverLegalLiabilityCover: {
            type: DataTypes.STRING,
            field: 'paid_driver_legal_liability_cover',
            defaultValue: 'false'
        },
        rodeSideAssistance: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'rode_side_assistance',
            values: ['true', 'false'],
            defaultValue: 'false'
        },
        electricalAccessories: {
            type: DataTypes.INTEGER,
            field: 'electrical_accessories'
        },
        nonElectricalAccessories: {
            type: DataTypes.INTEGER,
            field: 'non_electrical_accessories'
        },
        voluntaryDeductibleValue: {
            type: DataTypes.INTEGER,
            field: 'voluntary_deductible_value'
        },
        antiTheftDevice: {
            type: DataTypes.STRING,
            field: 'anti_theft_device',
            defaultValue: 'false'
        },
        automobileAssociation: {
            type: DataTypes.STRING,
            field: 'automobile_association',
            defaultValue: 'false'
        },
        consumable: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'consumable',
            defaultValue: 'false'
        },
        invoiceCover: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'invoice_cover',
            defaultValue: 'false'
        },
        thirdParty: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'third_party',            
            defaultValue: 'false'
        },
        registerDate: {
            type: DataTypes.DATE,
            field: 'register_date'
        },
        expiryDate: {
            type: DataTypes.DATE,
            field: 'expiry_date'
        },
        manufacturingDate: {
            type: DataTypes.DATE,
            field: 'manufacturing_date'
        },
        claimStatus: {
            type: DataTypes.STRING,
            field: 'claim_status'
        },
        prevNcb: {
            type: DataTypes.INTEGER,
            field: 'prev_ncb',
            defaultValue: 0
        },
        idv: {
            type: DataTypes.INTEGER,
            field: 'idv'
        },
        royalSundaramIdv: {
            type: DataTypes.INTEGER,
            field: 'royal_sundaram_idv'
        },
        premium: {
            type: DataTypes.INTEGER,
            field: 'premium'
        },
        odPremiumMultiyear: {
            type: DataTypes.STRING,
            field: 'od_premium_multiyear'
        },
        policyTerm: {
            type: DataTypes.INTEGER,
            field: 'policy_term'
        },
        idvMultiyear: {
            type: DataTypes.STRING,
            field: 'idv_multiyear'
        },
        premiumMultiyear: {
            type: DataTypes.STRING,
            field: 'premium_multiyear'
        },
        mobileOtp: {
            type: DataTypes.INTEGER,
            field: 'mobile_otp'
        },
        mobileStatus: {
            type: DataTypes.INTEGER,
            field: 'mobile_status'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
    }, {
        freezeTableName: true,
        tableName: 'bike_enquiry',
    })
    bikeEnquiry.associate = (models) => {
        bikeEnquiry.belongsTo(models.leadInsurance, { foreignKey: 'leadInsuranceId', as: 'leId' })
        bikeEnquiry.belongsTo(models.masterSheet, { foreignKey: 'masterSheetId', as: 'msId' })       
        bikeEnquiry.belongsTo(models.carRto, { foreignKey: 'rtoId', as: 'crId' })
        bikeEnquiry.belongsTo(models.bikeModel, { foreignKey: 'modelId', as: 'bmId' })
        bikeEnquiry.belongsTo(models.bikeManufacturer, { foreignKey: 'manufacturerId', as: 'bikeManufacturerId' })
    }
    return bikeEnquiry
}
