module.exports = (sequelize, DataTypes) => {
    const carFutureGeneraliGiMasterCode = sequelize.define('carFutureGeneraliGiMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT,
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT,
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT,
            field: 'car_variant_id'
        },
        vehicleCode: {
            type: DataTypes.STRING,
            field: 'vehicle_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        seatingCapacity: {
            type: DataTypes.INTEGER,
            field: 'seating_capacity'
        },
        bodyType: {
            type: DataTypes.STRING,
            field: 'body_type'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_future_generali_gi_master_code'
    })
    return carFutureGeneraliGiMasterCode
}