module.exports = (sequelize, DataTypes) => {
    const policyCategories = sequelize.define('policyCategories', {
        category: {
            type: DataTypes.STRING(255),
            field: 'category'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '0'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'policy_categories'
    })
    return policyCategories
}   