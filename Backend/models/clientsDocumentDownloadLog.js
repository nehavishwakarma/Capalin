module.exports = (sequelize, DataTypes) => {
    const clientsDocumentDownloadLog = sequelize.define('clientsDocumentDownloadLog', {
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        clientId: {
            type: DataTypes.BIGINT(20),
            field: 'client_id'
        },
        documentCategory: {
            type: DataTypes.BIGINT(20),
            field: 'document_category'
        },
        ipAddress: {
            type: DataTypes.STRING(255),
            field: 'ip_address'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'clients_document_download_log'
    })
    return clientsDocumentDownloadLog
}