const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const carRto = sequelize.define('carRto', {
        rtoCode: {
            type: DataTypes.STRING,
            field: 'rto_code'
        },
        rtoDescription: {
            type: DataTypes.TEXT,
            field: 'rto_description'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_rto'
    })
    carRto.associate = (models) => {
        carRto.hasMany(models.carRtoCode, { foreignKey: 'rtoCodeId', as: 'RtoCode' })
    }

    return carRto
}