module.exports = (sequelize, DataTypes) => {
    const healthProposalForm = sequelize.define('healthProposalForm', {
        userId: {
            type: DataTypes.BIGINT(20),
            field: 'user_id'
        },
        healthEnquiryId: {
            type: DataTypes.BIGINT(20),
            field: 'health_enquiry_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        pFirstName: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_first_name'
        },
        pMidName: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_mid_name'
        },
        pLastName: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_last_name'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        gender: {
            type: DataTypes.TEXT('tiny'),
            field: 'gender'
        },
        maritalStatus: {
            type: DataTypes.TEXT('tiny'),
            field: 'marital_status'
        },
        gstIdNumber: {
            type: DataTypes.TEXT('tiny'),
            field: 'gst_id_number'
        },
        pAadhar: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_aadhar'
        },
        eiaNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'eia_no'
        },
        insRepositoryName: {
            type: DataTypes.TEXT('tiny'),
            field: 'ins_repository_name'
        },
        apolloQuestion: {
            type: DataTypes.TEXT('tiny'),
            field: 'apollo_question'
        },
        apolloMedicalQuestion: {
            type: DataTypes.INTEGER(11),
            field: 'apollo_medical_question',
            defaultValue: 0
        },
        starMedicalQuestion: {
            type: DataTypes.INTEGER(11),
            field: 'star_medical_question',
            defaultValue: 0
        },
        apolloNstpProcess: {
            type: DataTypes.INTEGER(11),
            field: 'apollo_nstp_process'
        },
        apolloDetails: {
            type: DataTypes.TEXT,
            field: 'apollo_details'
        },
        apolloAnnualIncome: {
            type: DataTypes.TEXT('tiny'),
            field: 'apollo_annual_income'
        },
        apolloIdProofType: {
            type: DataTypes.TEXT('tiny'),
            field: 'apollo_id_proof_type'
        },
        apolloIdProofNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'apollo_id_proof_no'
        },
        apolloProfession: {
            type: DataTypes.TEXT('tiny'),
            field: 'apollo_profession'
        },
        apolloPreviousInsurance: {
            type: DataTypes.TEXT('tiny'),
            field: 'apollo_previous_insurance'
        },
        previousInsurancePolicyNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'previous_insurance_policy_no'
        },
        starSocialStatus: {
            type: DataTypes.INTEGER(11),
            field: 'star_social_status',
            defaultValue: '0'
        },
        starSocialStatusInfo: {
            type: DataTypes.TEXT('tiny'),
            field: 'star_social_status_info'
        },
        address: {
            type: DataTypes.TEXT,
            field: 'address'
        },
        areaLocality: {
            type: DataTypes.TEXT,
            field: 'area_locality'
        },
        landmark: {
            type: DataTypes.TEXT,
            field: 'landmark'
        },
        locality: {
            type: DataTypes.TEXT('tiny'),
            field: 'locality'
        },
        country: {
            type: DataTypes.TEXT('tiny'),
            field: 'country'
        },
        stateId: {
            type: DataTypes.TEXT('tiny'),
            field: 'state_id'
        },
        stateName: {
            type: DataTypes.TEXT('tiny'),
            field: 'state_name'
        },
        cityId: {
            type: DataTypes.TEXT('tiny'),
            field: 'city_id'
        },
        cityName: {
            type: DataTypes.TEXT('tiny'),
            field: 'city_name'
        },
        pincodeId: {
            type: DataTypes.BIGINT(20),
            field: 'pincode_id'
        },
        mobile: {
            type: DataTypes.TEXT('tiny'),
            field: 'mobile'
        },
        pancard: {
            type: DataTypes.TEXT('tiny'),
            field: 'pancard'
        },
        email: {
            type: DataTypes.TEXT('tiny'),
            field: 'email'
        },
        termDuration: {
            type: DataTypes.TEXT('tiny'),
            field: 'term_duration'
        },
        nomineeName: {
            type: DataTypes.TEXT('tiny'),
            field: 'nominee_name'
        },
        nomineeRelationship: {
            type: DataTypes.TEXT('tiny'),
            field: 'nominee_relationship'
        },
        nomineeDob: {
            type: DataTypes.DATE,
            field: 'nominee_dob'
        },
        nomineeContactNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'nominee_contact_no'
        },
        nomineeAddress: {
            type: DataTypes.TEXT('tiny'),
            field: 'nominee_address'
        },
        claimPer: {
            type: DataTypes.INTEGER(11),
            field: 'claim_per'
        },
        landline: {
            type: DataTypes.TEXT('tiny'),
            field: 'landline'
        },
        buyId: {
            type: DataTypes.TEXT,
            field: 'buy_id'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        insuranceStatus: {
            type: DataTypes.INTEGER(11),
            field: 'insurance_status'
        },
        amountDiff: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'amount_diff'
        },
        transctionStatus: {
            type: DataTypes.TEXT('tiny'),
            field: 'transction_status'
        },
        paymentStatus: {
            type: DataTypes.INTEGER(11),
            field: 'payment_status'
        },
        policyStatus: {
            type: DataTypes.INTEGER(11),
            field: 'policy_status',
            defaultValue: 0
        },
        policyId: {
            type: DataTypes.TEXT('tiny'),
            field: 'policy_id'
        },
        proposalNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'proposal_no'
        },
        transctionId: {
            type: DataTypes.TEXT('tiny'),
            field: 'transction_id'
        },
        transctionError: {
            type: DataTypes.TEXT,
            field: 'transction_error'
        },
        paymentGatewayResponse: {
            type: DataTypes.TEXT,
            field: 'payment_gateway_response'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        existing: {
            type: DataTypes.BIGINT,
            field: 'Existing'
        },
        altMobile: {
            type: DataTypes.STRING(20),
            field: 'alt_mobile'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_proposal_form'
    })
    healthProposalForm.associate = (models) => {
        healthProposalForm.belongsTo(models.healthEnquiry, { foreignKey: 'healthEnquiryId', as: 'healthEnquiry' })
    }
    return healthProposalForm
}   