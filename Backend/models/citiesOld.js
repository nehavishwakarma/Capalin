module.exports = (sequelize, DataTypes) => {
    const citiesOld = sequelize.define('citiesOld', {
        stateId: {
            type: DataTypes.BIGINT(20),
            field: 'state_id'
        },
        cityName: {
            type: DataTypes.STRING(255),
            field: 'city_name'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'cities_old'
    })
    return citiesOld
}