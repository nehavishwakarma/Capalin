module.exports = (sequelize, DataTypes) => {
    const healthInsuredRelationship = sequelize.define('healthInsuredRelationship', {
        masterSheetHealthId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_health_id'
        },
        proposerCode: {
            type: DataTypes.STRING(255),
            field: 'proposer_code'
        },
        relationshipProposer: {
            type: DataTypes.STRING(255),
            field: 'relationship_proposer',
            defaultValue: '1'
        },
        relationType: {
            type: DataTypes.BOOLEAN,
            field: 'relation_type',
            defaultValue: '0'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_insured_relationship'
    })
    return healthInsuredRelationship
}