module.exports = (sequelize, DataTypes) => {
    const uniqueVisitors = sequelize.define("uniqueVisitors", {
        vendorId: {
            type: DataTypes.BIGINT,
            field: 'vendor_id'
        },
        utmCampaign: {
            type: DataTypes.STRING,
            field: 'utm_campaign'
        },
        ipAddress: {
            type: DataTypes.STRING,
            field: 'ip_address'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'unique_visitors'
    })
    return uniqueVisitors
}