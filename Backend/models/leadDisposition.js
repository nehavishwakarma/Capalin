module.exports = (sequelize, DataTypes) => {
    const leadDisposition = sequelize.define('leadDisposition', {
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        leadId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_id'
        },
        tcDisposition_id: {
            type: DataTypes.BIGINT(20),
            field: 'tc_disposition_id'
        },
        disposition: {
            type: DataTypes.TEXT,
            field: 'disposition'
        },
        followUpDate: {
            type: DataTypes.DATE,
            field: 'follow_up_date'
        },
        followUpTime: {
            type: DataTypes.TIME,
            field: 'follow_up_time'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {   
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        deletedAt: {
            type: DataTypes.DATE,
            field: 'deleted_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_disposition'
    })
    return leadDisposition
}   