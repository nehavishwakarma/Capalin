module.exports = (sequelize, DataTypes) => {
    const serviceRequestDetails = sequelize.define("serviceRequestDetails", {
        serviceRequestId: {
            type: DataTypes.STRING,
            field: 'service_request_id',
        },
        salesReportId: {
            type: DataTypes.BIGINT(20),
            field: 'sales_report_id'
        },
        clientId: {
            type: DataTypes.BIGINT(20),
            field: 'client_id'
        },
        serviceCategoryId: {
            type: DataTypes.BIGINT(20),
            field: 'service_category_id'
        },
        serviceRequestCreatedBy: {
            type: DataTypes.BIGINT(20),
            field: 'service_request_created_by'
        },
        serviceRequestStatus: {
            type: DataTypes.BIGINT(20),
            field: 'service_request_status'
        },
        requestReopenBy: {
            type: DataTypes.INTEGER,
            field: 'request_reopen_by'
        },
        mailSentStatus: {
            type: DataTypes.ENUM('0', '1'),
            field: 'mail_sent_status',
            defaultValue: '0'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at',
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'service_request_details'
    })
    return serviceRequestDetails
}
