module.exports = (sequelize, DataTypes) => {
    const smsServiceProvider = sequelize.define("smsServiceProvider", {
        name: {
            type: DataTypes.STRING,
            field: 'name'
        },
        status:{
            type:DataTypes.ENUM('0','1'),
            field:'status'
        },
        createdAt:{
            type:DataTypes.DATE,
            field:'created_at'
        },
        updatedAt:{
            type:DataTypes.DATE,
            field:'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'sms_service_provider'
    })
    return smsServiceProvider
}
