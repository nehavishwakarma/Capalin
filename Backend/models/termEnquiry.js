module.exports = (sequelize, DataTypes) => {
    const termEnquiry = sequelize.define("termEnquiry", {
        enquiryId: {
            type: DataTypes.STRING,
            field: 'enquiry_id'
        },
        leadInsuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_insurance_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        annualIncome: {
            type: DataTypes.INTEGER,
            field: 'annual_income'
        },
        sa: {
            type: DataTypes.DECIMAL(11,2),
            field: 'sa'
        },
        termPremium: {
            type: DataTypes.DECIMAL(11,2),
            field: 'term_premium'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        age: {
            type: DataTypes.INTEGER(11),
            field: 'age'
        },
        gender: {
            type: DataTypes.ENUM('Male','Female'),
            field: 'gender'
        },
        tobaccoUser: {
            type: DataTypes.STRING,
            field: 'tobacco_user'
        },
        policyTerm: {
            type: DataTypes.INTEGER,
            field: 'policy_term'
        },
        pincode: {
            type: DataTypes.INTEGER,
            field: 'pincode'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        firstName: {
            type: DataTypes.STRING,
            field: 'first_name'
        },
        lastName:{
            type:DataTypes.STRING,
            field:'last_name'
        },
        city: {
            type: DataTypes.STRING,
            field: 'city'
        },
        planType: {
            type: DataTypes.INTEGER,
            field: 'plan_type',
            defaultValue: 1
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.ENUM('0','1'),
            field: 'is_deleted'
        },
        created:{
            type:DataTypes.DATE,
            field:'created'
        },
        modified:{
            type:DataTypes.DATE,
            field:'modified'
        },
        createdAt:{
            type:DataTypes.DATE,
            field:'created_at'
        },
        updatedAt:{
            type:DataTypes.DATE,         
            field:'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'term_enquiry'
    })
    return termEnquiry
}