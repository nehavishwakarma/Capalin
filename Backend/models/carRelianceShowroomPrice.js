module.exports = (sequelize, DataTypes) => {
    const carRelianceShowroomPrice = sequelize.define('carRelianceShowroomPrice', {
        makeIdPk: {
            type: DataTypes.BIGINT,
            field: 'make_id_pk'
        },
        modelIdPk: {
            type: DataTypes.BIGINT,
            field: 'model_id_pk'
        },
        makeName: {
            type: DataTypes.STRING,
            field: 'make_name'
        },
        modelName: {
            type: DataTypes.STRING,
            field: 'model_name'
        },
        variance: {
            type: DataTypes.STRING,
            field: 'variance'
        },
        stateIdFk: {
            type: DataTypes.BIGINT,
            field: 'state_id_fk'
        },
        modelPrice: {
            type: DataTypes.DECIMAL(10, 2),
            field: 'model_price'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'isDeleted',
            defaultValue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_reliance_showroom_price'
    })
    return carRelianceShowroomPrice
}