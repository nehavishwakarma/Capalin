module.exports = (sequelize, DataTypes) => {
    const motorRenewalData = sequelize.define('motorRenewalData', {
        policyCategory: {
            type: DataTypes.BIGINT(20),
            field: 'policy_category'
        },
        lastInsurer: {
            type: DataTypes.BIGINT(20),
            field: 'last_insurer'
        },
        lastPolicyNo: {
            type: DataTypes.STRING(255),
            field: 'last_policy_no'
        },
        firstName: {
            type: DataTypes.STRING(255),
            field: 'first_name'
        },
        mobile: {
            type: DataTypes.BIGINT(20),
            field: 'mobile'
        },
        newNcb: {
            type: DataTypes.INTEGER(11),
            field: 'new_ncb'
        },
        renewalPremium: {
            type: DataTypes.INTEGER(11),
            field: 'renewal_premium'
        },
        expiryDate: {
            type: DataTypes.DATE,
            field: 'expiry_date'
        },
        lastClaimStatus: {
            type: DataTypes.STRING(255),
            field: 'last_claim_status'
        },
        address: {
            type: DataTypes.STRING(255),
            field: 'address'
        },
        lastNcb: {
            type: DataTypes.INTEGER(11),
            field: 'last_ncb'
        },
        existingAgent: {
            type: DataTypes.STRING(255),
            field: 'existing_agent'
        },
        makeModel: {
            type: DataTypes.STRING(255),
            field: 'make_model'
        },
        fuel: {
            type: DataTypes.STRING(255),
            field: 'fuel'
        },
        variant: {
            type: DataTypes.STRING(255),
            field: 'variant'
        },
        rto: {
            type: DataTypes.INTEGER,
            field: 'rto'
        },
        registrationNo: {
            type: DataTypes.STRING(255),
            field: 'registration_no'
        },
        manufacturingYear: {
            type: DataTypes.STRING(255),
            field: 'manufacturing_year'
        },
        registrationDate: {
            type: DataTypes.DATE,
            field: 'registration_date'
        },
        lastName: {
            type: DataTypes.STRING(255),
            field: 'last_name'
        },
        lastPremium: {
            type: DataTypes.INTEGER(11),
            field: 'last_premium'
        },
        email: {
            type: DataTypes.STRING(255),
            field: 'email'
        },
        accuracy: {
            type: DataTypes.INTEGER(11),
            field: 'accuracy'
        },
        leadAssignStatus: {
            type: DataTypes.ENUM('0', '1'),
            field: 'lead_assign_status',
            defaultValue: '0'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        deletedAt: {
            type: DataTypes.DATE,
            field: 'deleted_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'motor_renewal_data'
    })
    return motorRenewalData
}       