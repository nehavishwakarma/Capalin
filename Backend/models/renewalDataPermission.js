module.exports = (sequelize, DataTypes) => {
    const renewalDataPermission = sequelize.define('renewalDataPermission', {
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        policyCategory: {
            type: DataTypes.STRING(255),
            field: 'policy_category'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'renewal_data_permission'
    })
    return renewalDataPermission
}       