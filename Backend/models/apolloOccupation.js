module.exports = (sequelize, DataTypes) => {
    const apolloOccupation = sequelize.define('apolloOccupation', {
        occupationCode: {
            type: DataTypes.STRING,
            field: 'occupation_code'
        },
        occupationName: {
            type: DataTypes.STRING,
            field: 'occupation_name'
        },
        level: {
            type: DataTypes.STRING,
            field: 'level'
        },
        category: {
            type: DataTypes.STRING,
            field: 'category'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        }
    }, {
        freezeTableName: true,
        tableName: 'apollo_occupation'
    })
    return apolloOccupation
}