module.exports = (sequelize, DataTypes) => {
    const seo = sequelize.define("seo", {
        module: {
            type: DataTypes.STRING,
            field: 'module'
        },
        page: {
            type: DataTypes.STRING,
            field: 'page'
        },
        title: {
            type: DataTypes.STRING,
            field: 'title'
        },
        description: {
            type: DataTypes.STRING,
            field: 'description'
        },
        keyword: {
            type: DataTypes.STRING,
            field: 'keyword'
        },
        breadcrumbTitle: {
            type: DataTypes.TEXT,
            field: 'breadcrumb_title'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'seo'
    })
    return seo
}
