module.exports = (sequelize, DataTypes) => {
    const leadManagementSystem = sequelize.define('leadManagementSystem', {
        companyId: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        product: {
            type: DataTypes.BIGINT(20),
            field: 'product'
        },
        startDate: {
            type: DataTypes.DATE,
            field: 'start_date'
        },
        endDate: {
            type: DataTypes.DATE,
            field: 'end_date'
        },
        quantity: {
            type: DataTypes.INTEGER(11),
            field: 'quantity'
        },
        quantitySent: {
            type: DataTypes.INTEGER(11),
            field: 'quantity_sent'
        },
        ageFrom: {
            type: DataTypes.INTEGER(11),
            field: 'age_from'
        },
        ageTo: {
            type: DataTypes.INTEGER(11),
            field: 'age_to'
        },
        income: {
            type: DataTypes.STRING(255),
            field: 'income'
        },
        city: {
            type: DataTypes.TEXT,
            field: 'city'
        },
        pincode: {
            type: DataTypes.STRING(255),
            field: 'pincode'
        },
        gender: {
            type: DataTypes.INTEGER(11),
            field: 'gender'
        },
        emailVerify: {
            type: DataTypes.INTEGER(11),
            field: 'email_verify'
        },
        mobileVerify: {
            type: DataTypes.INTEGER(11),
            field: 'mobile_verify'
        },
        language: {
            type: DataTypes.TEXT,
            field: 'language'
        },
        campaignBy: {
            type: DataTypes.STRING(255),
            field: 'campaign_by'
        },
        smsProvider: {
            type: DataTypes.STRING(255),
            field: 'sms_provider'
        },
        smsServiceUsedBy: {
            type: DataTypes.STRING(255),
            field: 'sms_service_used_by'
        },
        duplicateLeadType: {
            type: DataTypes.STRING(255),
            field: 'duplicate_lead_type'
        },
        duplicateLeadCountDays: {
            type: DataTypes.INTEGER(11),
            field: 'duplicate_lead_count_days'
        },
        leadTransferTime: {
            type: DataTypes.BIGINT(20),
            field: 'lead_transfer_time'
        },
        transferParameter: {
            type: DataTypes.TEXT,
            field: 'transfer_parameter'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_management_system'
    })
    return leadManagementSystem
}   