module.exports = (sequelize, DataTypes) => {
    const healthSubQuestions = sequelize.define('healthSubQuestions', {
        healthQuestionsId: {
            type: DataTypes.BIGINT(20),
            field: 'health_questions_id'
        },
        subQuestions: {
            type: DataTypes.TEXT,
            field: 'sub_questions'
        },
        subQuestionCode: {
            type: DataTypes.STRING(255),
            field: 'sub_question_code'
        },
        subQuestionSetCode: {
            type: DataTypes.STRING(255),
            field: 'sub_question_set_code'
        },
        subQuestionExistingCode: {
            type: DataTypes.STRING(255),
            field: 'sub_question_existing_code'
        },
        subQuestionDescriptionCode: {
            type: DataTypes.STRING(255),
            field: 'sub_question_description_code'
        },
        ansAvailable: {
            type: DataTypes.INTEGER(11),
            field: 'ans_available'
        },
        ansValidation: {
            type: DataTypes.STRING(255),
            field: 'ans_validation'
        },
        ansPlaceholder: {
            type: DataTypes.STRING(255),
            field: 'ans_placeholder'
        },
        ansDescribe: {
            type: DataTypes.STRING(255),
            field: 'ans_describe'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '0'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_sub_questions'
    })
    return healthSubQuestions
}   