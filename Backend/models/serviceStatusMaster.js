module.exports = (sequelize, DataTypes) => {
    const serviceStatusMaster = sequelize.define("serviceStatusMaster", {
        description:{
            type:DataTypes.STRING,
            field:'description'
        },
        statusColor:{
            type:DataTypes.STRING,
            field:'status_color'
        },
        status: {
            type: DataTypes.ENUM('0','1'),
            field: 'status',
            defaultValue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'service_status_master'
    })
    return serviceStatusMaster
}
