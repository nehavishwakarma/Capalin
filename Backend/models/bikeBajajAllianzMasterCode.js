module.exports = (sequelize, DataTypes) => {
    const bikeBajajAllianzMasterode = sequelize.define('bikeBajajAllianzMasterode', {
        bikeManufacturerId: {
            type: DataTypes.BIGINT,
            field: 'bike_manufacturer_id'
        },
        bikeModelId: {
            type: DataTypes.BIGINT,
            field: 'bike_model_id'
        },
        vehicleCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_code'
        },
        vehicleMakeCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_make_code'
        },
        vehicleModelCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_model_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        carryingCapacity: {
            type: DataTypes.INTEGER,
            field: 'carrying_capacity'
        },
        vehicleSubTypeCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_sub_type_code'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_bajaj_allianz_master_code'
    })
    return bikeBajajAllianzMasterode
}