module.exports = (sequelize, DataTypes) => {
    const tcDisposition = sequelize.define("tcDisposition", {
        disposition: {
            type: DataTypes.STRING,
            field: 'disposition'
        },
        rank: {
            type: DataTypes.INTEGER,
            field: 'rank'
        },
        groupId: {
            type: DataTypes.INTEGER,
            field: 'group_id'
        },
        followUpStatus: {
            type: DataTypes.ENUM('0','1'),
            field: 'follow_up_status'
        },
        smsId: {
            type: DataTypes.INTEGER,
            field: 'sms_id'
        },
        status: {
            type: DataTypes.ENUM('0','1'),
            field: 'status',
            defaultValue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'tc_disposition'
    })
    return tcDisposition
}