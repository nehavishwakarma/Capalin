module.exports = (sequelize, DataTypes) => {
    const carShriramGiMasterCode = sequelize.define('carShriramGiMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT,
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT,
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT,
            field: 'car_variant_id'
        },
        vehicleCode: {
            type: DataTypes.STRING,
            field: 'vehicle_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel',
            defaultValue: '1'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 0
        }
    }, {
        freezeTableName: true,
        tableName: 'car_shriram_gi_master_code'
    })
    return carShriramGiMasterCode
}
