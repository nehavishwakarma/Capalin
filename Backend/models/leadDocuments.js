module.exports = (sequelize, DataTypes) => {
    const leadDocuments = sequelize.define('leadDocuments', {
        leadId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_id'
        },
        documentName: {
            type: DataTypes.STRING(255),
            field: 'document_name'
        },
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status',
            defaultvalue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_documents'
    })
    return leadDocuments
}   