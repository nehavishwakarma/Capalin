module.exports = (sequelize, DataTypes) => {
    const companies = sequelize.define('companies', {
        companyName: {
            type: DataTypes.STRING,
            field: 'company_name'
        },
        insuranceType: {
            type: DataTypes.STRING,
            field: 'insurance_type'
        },
        logo: {
            type: DataTypes.STRING,
            field: 'logo'
        },
        logoBig: {
            type: DataTypes.STRING,
            field: 'logo_big'
        },
        companyFullName: {
            type: DataTypes.STRING,
            field: 'company_full_name'
        },
        website: {
            type: DataTypes.STRING,
            field: 'website'
        },
        address: {
            type: DataTypes.TEXT,
            field: 'address'
        },
        contactNo: {
            type: DataTypes.STRING,
            field: 'contact_no'
        },
        emailId: {
            type: DataTypes.STRING,
            field: 'email_id'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '0'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        companyFilter: {
            type: DataTypes.INTEGER,
            field: 'company_filter'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        description: {
            type: DataTypes.TEXT,
            field: 'description'
        },
        companyPlan: {
            type: DataTypes.TEXT,
            field: 'company_plan'
        }
    }, {
        freezeTableName: true,
        tableName: 'companies'
    })
    companies.associate = (models) => {
        companies.hasMany(models.masterSheet, { foreignKey: 'companyId', as: 'masterSheet' })
        companies.hasMany(models.policyCategories, { foreignKey: 'policyCategoryId', as: 'policyCategoryId' })
        companies.hasMany(models.healthInsurance, { foreignKey: 'masterSheetId', as: 'masterSheetId' })
        companies.hasMany(models.healthPremium, { foreignKey: 'healthInsuranceId', as: 'healthInsuranceId' })
    }
    return companies
}