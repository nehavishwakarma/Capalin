module.exports = (sequelize, DataTypes) => {
    const carHdfcErgoMasterCode = sequelize.define('carHdfcErgoMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT,
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT,
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT,
            field: 'car_variant_id'
        },
        vehicleModelCode: {
            type: DataTypes.BIGINT,
            field: 'vehicle_model_code'
        },
        manufacturerCode: {
            type: DataTypes.INTEGER,
            field: 'manufacturer_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        noOfWheels: {
            type: DataTypes.INTEGER,
            field: 'no_of_wheels'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        grossVehicleWeight: {
            type: DataTypes.INTEGER,
            field: 'gross_vehicle_weight'
        },
        seatingCapacity: {
            type: DataTypes.INTEGER,
            field: 'seating_capacity'
        },
        carryingCapacity: {
            type: DataTypes.INTEGER,
            field: 'carrying_capacity'
        },
        vehicleClassCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_class_code'
        },
        bodyTypeCode: {
            type: DataTypes.INTEGER,
            field: 'body_type_code'
        },
        vehicleModelStatus: {
            type: DataTypes.STRING,
            field: 'vehicle_model_status'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        segmentType: {
            type: DataTypes.STRING,
            field: 'segment_type'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultValue: 0
        }
    }, {
        freezeTableName: true,
        tableName: 'car_hdfc_ergo_master_code'
    })
    return carHdfcErgoMasterCode
}