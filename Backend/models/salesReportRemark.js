module.exports = (sequelize, DataTypes) => {
    const salesReportRemark = sequelize.define("salesReportRemark", {
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        salesReportId: {
            type: DataTypes.BIGINT(20),
            field: 'sales_report_id'
        },
        remarkDescription: {
            type: DataTypes.TEXT,
            field: 'remark_description'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'sales_report_remark'
    })
    return salesReportRemark
}
