module.exports = (sequelize, DataTypes) => {
    const states = sequelize.define("states", {
        stateName: {
            type: DataTypes.STRING,
            field: 'state_name'
        },
        country: {
            type: DataTypes.STRING,
            field: 'country'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'states'
    })
    states.associate = (models) => {
        states.hasMany(models.pincode, { foreignKey: 'stateId', as: 'pincode' })
        states.hasMany(models.cities, { foreignKey: 'stateId', as: 'cities' })
    }
    return states
}
