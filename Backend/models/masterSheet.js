const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const masterSheet = sequelize.define("masterSheet", {
        companyId: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        policyCategoryId: {
            type: DataTypes.BIGINT(20),
            field: 'policy_category_id'
        },
        policyName: {
            type: DataTypes.STRING,
            field: 'policy_name'
        },
        policyCode: {
            type: DataTypes.STRING,
            field: 'policy_code'
        },
        policyCode2: {
            type: DataTypes.STRING,
            field: 'policy_code_2'
        },
        agentId: {
            type: DataTypes.STRING,
            field: 'agent_id'
        },
        rider: {
            type: DataTypes.STRING,
            field: 'rider'
        },
        method: {
            type: DataTypes.STRING,
            field: 'method'
        },
        applloPolicyType: {
            type: DataTypes.INTEGER,
            field: 'apollo_policy_type'
        },
        marketLinked: {
            type: DataTypes.STRING,
            field: 'market_linked'
        },
        brochure: {
            type: DataTypes.STRING,
            field: 'brochure'
        },
        policyWordings: {
            type: DataTypes.STRING,
            field: 'policy_wordings'
        },
        claimForm: {
            type: DataTypes.STRING,
            field: 'claim_form'
        },
        gender: {
            type: DataTypes.STRING,
            field: 'gender'
        },
        planType: {
            type: DataTypes.INTEGER,
            field: 'plan_type'
        },
        policyType: {
            type: DataTypes.STRING,
            field: 'policy_type'
        },
        redirectUrl: {
            type: DataTypes.TEXT,
            field: 'redirect_url'
        },
        renewalLink: {
            type: DataTypes.STRING,
            field: 'renewal_link'
        },
        renewalLinkShort: {
            type: DataTypes.STRING,
            field: 'renewal_link_short'
        },
        sortData: {
            type: DataTypes.INTEGER,
            field: 'sort_data'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        comm: {
            type: DataTypes.DECIMAL(11, 6),
            field: 'comm'
        },
        orc: {
            type: DataTypes.DECIMAL(11, 6),
            field: 'orc'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'master_sheet'
    })
    masterSheet.associate = (models) => {
        masterSheet.belongsTo(models.companies, { foreignKey: 'companyId', as: 'companies' })
        masterSheet.belongsTo(models.policyCategories, { foreignKey: 'policyCategoryId', as: 'policyCategories' })
    }
    return masterSheet
}
