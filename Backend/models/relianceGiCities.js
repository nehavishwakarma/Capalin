module.exports = (sequelize, DataTypes) => {
    const relianceGiCities = sequelize.define('relianceGiCities', {
        stateIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'state_id_pk'
        },
        stateName: {
            type: DataTypes.STRING(255),
            field: 'state_name'
        },
        districtIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'district_id_pk'
        },
        districtName: {
            type: DataTypes.STRING(255),
            field: 'district_name'
        },
        cityOrVillageIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'city_or_village_id_pk'
        },
        cityOrVillageName: {
            type: DataTypes.STRING(255),
            field: 'city_or_village_name'
        },
        areaIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'area_id_pk'
        },
        areaName: {
            type: DataTypes.STRING(255),
            field: 'area_name'
        },
        pincode: {
            type: DataTypes.INTEGER(11),
            field: 'pincode'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'reliance_gi_cities'
    })
    return relianceGiCities
}       