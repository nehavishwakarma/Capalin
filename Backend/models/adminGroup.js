module.exports = (sequelize, DataTypes) => {
    const adminGroups = sequelize.define('adminGroups', {
        name: {
            type: DataTypes.STRING,
            field: 'name'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultvalue: '1'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultvalue: 0
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'admin_groups'
    })
    return adminGroups
}