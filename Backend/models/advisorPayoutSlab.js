module.exports = (sequelize, DataTypes) => {
    const advisorPayoutSlab = sequelize.define('advisorPayoutSlab', {
        policyCategory: {
            type: DataTypes.INTEGER,
            field: 'policy_category'
        },
        companyId: {
            type: DataTypes.INTEGER,
            field: 'company_id'
        },
        minPayoutPremium: {
            type: DataTypes.INTEGER,
            field: 'lnmin_payout_premiumame'
        },
        maxPayoutPremium: {
            type: DataTypes.INTEGER,
            field: 'max_payout_premium'
        },
        rate: {
            type: DataTypes.STRING,
            field: 'rate'
        },
        startDate: {
            type: DataTypes.DATE,
            field: 'start_date'
        },
        endDate: {
            type: DataTypes.DATE,
            field: 'end_date'
        },
        slab: {
            type: DataTypes.INTEGER,
            field: 'slab'
        }
    }, {
        freezeTableName: true,
        tableName: 'advisor_payout_slab'
    })
    return advisorPayoutSlab
}