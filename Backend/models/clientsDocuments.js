module.exports = (sequelize, DataTypes) => {
    const clientsDocuments = sequelize.define('clientsDocuments', {
        clientId: {
            type: DataTypes.BIGINT(20),
            field: 'client_id'
        },
        documentCategory: {
            type: DataTypes.INTEGER(11),
            field: 'document_category'
        },
        documentName: {
            type: DataTypes.STRING(255),
            field: 'document_name'
        },
        otherDocName: {
            type: DataTypes.STRING(255),
            field: 'other_doc_name'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.ENUM('1', '0'),
            field: 'is_deleted'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'clients_documents'
    })
    return clientsDocuments
}