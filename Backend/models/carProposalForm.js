module.exports = (sequelize, DataTypes) => {
    const carProposalForm = sequelize.define('carProposalForm', {
        userId: {
            type: DataTypes.BIGINT(20),
            field: 'user_id'
        },
        carEnquiryId: {
            type: DataTypes.BIGINT(20),
            field: 'car_enquiry_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        companyName: {
            type: DataTypes.TEXT('tiny'),
            field: 'company_name'
        },
        pFirstName: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_first_name'
        },
        pLastName: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_last_name'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        carOwner: {
            type: DataTypes.TEXT('tiny'),
            field: 'car_owner',
            defaultValue: 'individual'
        },
        pAadhar: {
            type: DataTypes.TEXT('tiny'),
            field: 'p_aadhar'
        },
        gstNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'gst_no'
        },
        gender: {
            type: DataTypes.TEXT('tiny'),
            field: 'gender',
        },
        maritalStatus: {
            type: DataTypes.TEXT('tiny'),
            field: 'marital_status'
        },
        email: {
            type: DataTypes.TEXT('tiny'),
            field: 'email'
        },
        mobile: {
            type: DataTypes.TEXT('tiny'),
            field: 'mobile'
        },
        nomineeName: {
            type: DataTypes.TEXT('tiny'),
            field: 'nominee_name'
        },
        nomineeDob: {
            type: DataTypes.DATE,
            field: 'nominee_dob'
        },
        nomineeRelationship: {
            type: DataTypes.TEXT('tiny'),
            field: 'nominee_relationship'
        },
        address: {
            type: DataTypes.TEXT,
            field: 'address'
        },
        area: {
            type: DataTypes.TEXT('tiny'),
            field: 'area'
        },
        areaLocality: {
            type: DataTypes.TEXT,
            field: 'area_locality'
        },
        landmark: {
            type: DataTypes.TEXT,
            field: 'landmark',
        },
        country: {
            type: DataTypes.INTEGER,
            field: 'country'
        },
        stateId: {
            type: DataTypes.TEXT('tiny'),
            field: 'state_id'
        },
        cityId: {
            type: DataTypes.TEXT('tiny'),
            field: 'city_id'
        },
        pincodeId: {
            type: DataTypes.TEXT('tiny'),
            field: 'pincode_id'
        },
        regDistrictId: {
            type: DataTypes.TEXT('tiny'),
            field: 'reg_district_id'
        },
        regCityName: {
            type: DataTypes.TEXT('tiny'),
            field: 'reg_city_name'
        },
        regStateName: {
            type: DataTypes.TEXT('tiny'),
            field: 'reg_state_name'
        },
        carRegisterNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'car_register_no'
        },
        engineeNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'enginee_no'
        },
        chassisNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'chassis_no'
        },
        expiryPolicyNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'expiry_policy_no'
        },
        existingInsurance: {
            type: DataTypes.TEXT('tiny'),
            field: 'existing_insurance'
        },
        thirdPartyExpiryDate: {
            type: DataTypes.DATE,
            field: 'third_party_expiry_date'
        },
        isCarFinance: {
            type: DataTypes.ENUM('true', 'false'),
            field: 'is_car_finance'
        },
        loanProvideFinancier: {
            type: DataTypes.TEXT('tiny'),
            field: 'loan_provide_financier'
        },
        loanProvideFinancierName: {
            type: DataTypes.TEXT('tiny'),
            field: 'loan_provide_financier_name'
        },
        loanCity: {
            type: DataTypes.TEXT('tiny'),
            field: 'loan_city'
        },
        loanStartDate: {
            type: DataTypes.DATE,
            field: 'loan_start_date'
        },
        idv: {
            type: DataTypes.TEXT('tiny'),
            field: 'idv'
        },
        buyId: {
            type: DataTypes.TEXT('tiny'),
            field: 'buy_id',
        },
        addCommunicationAddress: {
            type: DataTypes.TEXT('tiny'),
            field: 'add_communication_address'
        },
        communicationAddress: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_address'
        },
        communicationAreaLocality: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_area_locality'
        },
        communicationLandmark: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_landmark'
        },
        communicationCity: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_city'
        },
        communicationDistrict: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_district'
        },
        communicationState: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_state'
        },
        communicationPincode: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_pincode'
        },
        communicationCityName: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_city_name'
        },
        communicationStateName: {
            type: DataTypes.TEXT('tiny'),
            field: 'communication_state_name',
        },
        existingInsurerName: {
            type: DataTypes.TEXT('tiny'),
            field: 'existing_insurer_name'
        },
        inspectionArea: {
            type: DataTypes.TEXT('tiny'),
            field: 'inspection_area'
        },
        paymentMode: {
            type: DataTypes.TEXT('tiny'),
            field: 'payment_mode'
        },
        diffAmount: {
            type: DataTypes.INTEGER,
            field: 'diff_amount'
        },
        proposalNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'proposal_no'
        },
        carProposalRequest: {
            type: DataTypes.TEXT('tiny'),
            field: 'car_proposal_request',
        },
        carProposalResponse: {
            type: DataTypes.TEXT('tiny'),
            field: 'car_proposal_request'
        },
        insuranceStatus: {
            type: DataTypes.INTEGER,
            field: 'insurance_status'
        },
        paymentStatus: {
            type: DataTypes.INTEGER,
            field: 'payment_status'
        },
        policyNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'policy_no'
        },
        policyStatus: {
            type: DataTypes.BOOLEAN,
            field: 'policy_status',
            defaultValue: '0'
        },
        inspectionNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'inspection_no'
        },
        quoteNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'quote_no'
        },
        receiptNo: {
            type: DataTypes.TEXT('tiny'),
            field: 'receipt_no'
        },
        proposalPremium: {
            type: DataTypes.INTEGER,
            field: 'proposal_premium'
        },
        txnId: {
            type: DataTypes.TEXT('tiny'),
            field: 'txn_id'
        },
        contactId: {
            type: DataTypes.TEXT('tiny'),
            field: 'contact_id'
        },
        proposalTimestamp: {
            type: DataTypes.TEXT('tiny'),
            field: 'proposal_timestamp'
        },
        proposalResponse: {
            type: DataTypes.TEXT,
            field: 'proposal_response'
        },
        accountTimestamp: {
            type: DataTypes.TEXT('tiny'),
            field: 'account_timestamp',
        },
        paymentGetwayResponse: {
            type: DataTypes.TEXT,
            field: 'payment_getway_response'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_proposal_form',
    })
    carProposalForm.associate = (models) => {
        carProposalForm.belongsTo(models.carEnquiry, { foreignKey: 'carEnquiryId', targetKey: 'enquiryId', as: 'carEnquiryCarProposalForm' })

    }
    return carProposalForm
}

