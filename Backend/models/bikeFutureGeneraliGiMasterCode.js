module.exports = (sequelize, DataTypes) => {
    const bikeFutureGeneraliGiMasterCode = sequelize.define('bikeFutureGeneraliGiMasterCode', {
        bikeManufacturerId: {
            type: DataTypes.BIGINT,
            field: 'bike_manufacturer_id'
        },
        bikeModelId: {
            type: DataTypes.BIGINT,
            field: 'bike_model_id'
        },
        vehicleCode: {
            type: DataTypes.STRING,
            field: 'vehicle_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        seatingCapacity: {
            type: DataTypes.INTEGER,
            field: 'seating_capacity'
        },
        carryingCapacity: {
            type: DataTypes.INTEGER,
            field: 'carrying_capacity'
        },
        bodyType: {
            type: DataTypes.STRING,
            field: 'body_type'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_future_generali_gi_master_code'
    })
    return bikeFutureGeneraliGiMasterCode
}
