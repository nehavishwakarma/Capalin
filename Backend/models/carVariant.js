const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const carVariant = sequelize.define('carVariant', {
        carModelId: {
            type: DataTypes.BIGINT(20),
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT(20),
            field: 'car_fuel_type_id'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_variant'
    })
    return carVariant
}