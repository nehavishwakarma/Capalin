const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const cities = sequelize.define('cities', {
        companyId: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        stateId: {
            type: DataTypes.BIGINT(20),
            field: 'state_id'
        },
        city: {
            type: DataTypes.STRING(255),
            field: 'city'
        },
        cityCode: {
            type: DataTypes.STRING(255),
            field: 'city_code'
        },
        stateName: {
            type: DataTypes.STRING(255),
            field: 'state_name'
        },
        stateCode: {
            type: DataTypes.STRING(255),
            field: 'state_code'
        },
        districtCode: {
            type: DataTypes.STRING(255),
            field: 'district_code'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1',
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'cities'
    })
    cities.associate = (models) => {
        cities.belongsTo(models.states, { foreignKey: 'stateId', as: 'states' })
    }
    return cities
}