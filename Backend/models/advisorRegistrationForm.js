module.exports = (sequelize, DataTypes) => {
    const advisorRegistrationForm = sequelize.define('advisorRegistrationForm', {
        adminUserId: {
            type: DataTypes.BIGINT,
            field: 'admin_user_id'
        },
        companyName: {
            type: DataTypes.STRING,
            field: 'company_name'
        },
        experience: {
            type: DataTypes.INTEGER,
            field: 'experience'
        },
        fname: {
            type: DataTypes.STRING,
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING,
            field: 'lname'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.BIGINT,
            field: 'mobile'
        },
        address: {
            type: DataTypes.TEXT,
            field: 'address'
        },
        city: {
            type: DataTypes.STRING,
            field: 'city'
        },
        state: {
            type: DataTypes.STRING,
            field: 'state'
        },
        pincode: {
            type: DataTypes.INTEGER,
            field: 'pincode',
        },
        ipAddress: {
            type: DataTypes.STRING,
            field: 'ip_address'
        },
        userAgent: {
            type: DataTypes.STRING,
            field: 'user_agent'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '0'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted',
            defaultValue: '0'
        },
        idProofStatus: {
            type: DataTypes.BOOLEAN,
            field: 'id_proof_status',
            defaultValue: '0'
        },
        addProofStatus: {
            type: DataTypes.BOOLEAN,
            field: 'add_proof_status',
            defaultValue: '0'
        },
        chequeStatus: {
            type: DataTypes.BOOLEAN,
            field: 'cheque_status',
            defaultValue: '0'
        },
        photoStatus: {
            type: DataTypes.BOOLEAN,
            field: 'photo_status',
            defaultValue: '0'
        },
    }, {
        freezeTableName: true,
        tableName: 'advisor_registration_form',
    })
    return advisorRegistrationForm
}