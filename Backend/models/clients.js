module.exports = (sequelize, DataTypes) => {
    const clients = sequelize.define('clients', {
        emailListId: {
            type: DataTypes.INTEGER(11),
            field: 'email_list_id'
        },
        mobileListId: {
            type: DataTypes.INTEGER(11),
            field: 'mobile_list_id'
        },
        clientsId: {
            type: DataTypes.STRING(255),
            field: 'clients_id'
        },
        relationshipsId: {
            type: DataTypes.STRING(255),
            field: 'relationships_id'
        },
        clientCode: {
            type: DataTypes.STRING(255),
            field: 'client_code'
        },
        firstName: {
            type: DataTypes.STRING(255),
            field: 'first_name'
        },
        lastName: {
            type: DataTypes.STRING(255),
            field: 'last_name'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'clients'
    })
    return clients
}