module.exports = (sequelize, DataTypes) => {
    const healthInsurance = sequelize.define('healthInsurance', {
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        sumAssured: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'sum_assured'
        },
        maxEntryAge: {
            type: DataTypes.INTEGER(11),
            field: 'max_entry_age'
        },
        minEntryAgeChild: {
            type: DataTypes.INTEGER(11),
            field: 'min_entry_age_child'
        },
        maxEntryAgeChild: {
            type: DataTypes.INTEGER(11),
            field: 'max_entry_age_child'
        },
        normalRoomLimit: {
            type: DataTypes.STRING(255),
            field: 'normal_room_limit'
        },
        icuRoomLimit: {
            type: DataTypes.STRING(255),
            field: 'icu_room_limit'
        },
        restorationBenefit: {
            type: DataTypes.STRING(255),
            field: 'restoration_benefit'
        },
        copayAgeLimit: {
            type: DataTypes.INTEGER(11),
            field: 'copay_age_limit'
        },
        copayBeforeAgeLimit: {
            type: DataTypes.INTEGER(11),
            field: 'copay_before_age_limit',
            defaultValue: '1',
        },
        copayAfterAgeLimit: {
            type: DataTypes.INTEGER(11),
            field: 'copay_after_age_limit'
        },
        prePolicyCheckUp: {
            type: DataTypes.INTEGER(11),
            field: 'pre_policy_check_up'
        },
        maternityBenefit: {
            type: DataTypes.STRING(255),
            field: 'maternity_benefit'
        },
        preExistingDiseases: {
            type: DataTypes.INTEGER(11),
            field: 'pre_existing_diseases'
        },
        familyMembers: {
            type: DataTypes.STRING(255),
            field: 'family_members'
        },
        renewalAge: {
            type: DataTypes.STRING(255),
            field: 'renewal_age'
        },
        preHospitalisation: {
            type: DataTypes.INTEGER(11),
            field: 'pre_hospitalisation'
        },
        postHospitalisation: {
            type: DataTypes.INTEGER(11),
            field: 'post_hospitalisation'
        },
        dayCareProcedures: {
            type: DataTypes.STRING(255),
            field: 'day_care_procedures'
        },
        emergencyAmbulance: {
            type: DataTypes.INTEGER(11),
            field: 'emergency_ambulance'
        },
        networkHospitals: {
            type: DataTypes.INTEGER(11),
            field: 'network_hospitals'
        },
        initialWaitingPeriod: {
            type: DataTypes.STRING(255),
            field: 'initial_waiting_period'
        },
        specificWaitingPeriod: {
            type: DataTypes.STRING(255),
            field: 'specific_waiting_period'
        },
        taxBenefit: {
            type: DataTypes.STRING(255),
            field: 'tax_benefit'
        },
        worldwideCoverage: {
            type: DataTypes.STRING(255),
            field: 'worldwide_coverage'
        },
        freeLookPeriod: {
            type: DataTypes.INTEGER(11),
            field: 'free_look_period'
        },
        gracePeriod: {
            type: DataTypes.INTEGER(11),
            field: 'grace_period'
        },
        claimBasedLoading: {
            type: DataTypes.STRING(255),
            field: 'claim_based_loading'
        },
        healthCheckUp: {
            type: DataTypes.STRING(255),
            field: 'health_check_up'
        },
        saBonus: {
            type: DataTypes.STRING(255),
            field: 'sa_bonus'
        },
        uniqueFeatures: {
            type: DataTypes.TEXT,
            field: 'unique_features'
        },
        ciRider: {
            type: DataTypes.TEXT,
            field: 'ci_rider'
        },
        paRider: {
            type: DataTypes.TEXT,
            field: 'pa_rider'
        },
        otherRider: {
            type: DataTypes.TEXT,
            field: 'other_rider'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_insurance'
    })
    healthInsurance.associate = (models) => {
        healthInsurance.belongsTo(models.healthInsurance, { foreignKey: 'masterSheetId', as: 'healthInsurance' })
    }
    return healthInsurance
}   