module.exports = (sequelize, DataTypes) => {
    const carRelianceMasterCode = sequelize.define('carRelianceMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT(20),
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT(20),
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT(20),
            field: 'car_variant_id'
        },
        makeIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'make_id_pk'
        },
        makeName: {
            type: DataTypes.STRING(255),
            field: 'make_name'
        },
        modelIdPk: {
            type: DataTypes.INTEGER(11),
            field: 'model_id_pk'
        },
        variance: {
            type: DataTypes.STRING(255),
            field: 'variance'
        },
        wheels: {
            type: DataTypes.STRING(255),
            field: 'wheels'
        },
        operatedBy: {
            type: DataTypes.STRING(255),
            field: 'operated_by'
        },
        cubicCapacity: {
            type: DataTypes.STRING(255),
            field: 'cubic_capacity'
        },
        seatingCapacity: {
            type: DataTypes.STRING(255),
            field: 'seating_capacity'
        },
        carryingCapacity: {
            type: DataTypes.STRING(255),
            field: 'carrying_capacity'
        },
        vehTypeIdFk: {
            type: DataTypes.BIGINT(20),
            field: 'veh_type_id_fk'
        },
        vehTypeName: {
            type: DataTypes.STRING(255),
            field: 'veh_type_name'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_reliance_master_code'
    })
    return carRelianceMasterCode
}