module.exports = (sequelize, DataTypes) => {
    const claim = sequelize.define('claim', {
        emailListId: {
            type: DataTypes.BIGINT(50),
            field: 'email_list_id'
        },
        mobileListId: {
            type: DataTypes.BIGINT(50),
            field: 'mobile_list_id'
        },
        policyNo: {
            type: DataTypes.INTEGER(50),
            field: 'policy_no'
        },
        nameOfInsurer: {
            type: DataTypes.STRING(50),
            field: 'name_of_insurer'
        },
        dateOfAccident: {
            type: DataTypes.DATE,
            field: 'date_of_accident'
        },
        dateOfDischarge: {
            type: DataTypes.DATE,
            field: 'date_of_discharge'
        },
        reasonOfClaim: {
            type: DataTypes.STRING(50),
            field: 'reason_of_claim'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        deletedAt: {
            type: DataTypes.DATE,
            field: 'deleted_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'claim'
    })
    return claim
}