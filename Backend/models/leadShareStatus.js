module.exports = (sequelize, DataTypes) => {
    const leadShareStatus = sequelize.define('leadShareStatus', {
        leadCampaignId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_campaign_id'
        },
        leadManagementId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_management_id'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status'
        },
        statusReport: {
            type: DataTypes.TEXT,
            field: 'status_report'
        },
        edelweissRatioCount: {
            type: DataTypes.INTEGER(11),
            field: 'edelweiss_ratio_count'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_share_status'
    })
    return leadShareStatus
}   