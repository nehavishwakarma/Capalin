module.exports = (sequelize, DataTypes) => {
    const cronJobEmailSentReport = sequelize.define('cronJobEmailSentReport', {
        emailListId: {
            type: DataTypes.BIGINT(20),
            field: 'email_list_id'
        },
        action: {
            type: DataTypes.STRING(255),
            field: 'action'
        },
        emailSentDate: {
            type: DataTypes.DATE,
            field: 'email_sent_date'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'cron_job_email_sent_report'
    })
    return cronJobEmailSentReport
}