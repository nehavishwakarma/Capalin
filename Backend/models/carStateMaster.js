module.exports = (sequelize, DataTypes) => {
    const carStateMaster = sequelize.define('carStateMaster', {
        company_id: {
            type: DataTypes.BIGINT,
            field: 'company_id'
        },
        state_name: {
            type: DataTypes.STRING,
            field: 'state_name'
        },
        state_id: {
            type: DataTypes.BIGINT,
            field: 'state_id'
        },
        group_state_id: {
            type: DataTypes.BIGINT,
            field: 'group_state_id'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        is_deleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_state_master'
    })
    return carStateMaster
}