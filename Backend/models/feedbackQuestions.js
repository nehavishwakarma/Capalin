module.exports = (sequelize, DataTypes) => {
    const feedbackQuestions = sequelize.define('feedbackQuestions', {
        adminGroupId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_group_id'
        },
        questions: {
            type: DataTypes.STRING(5000),
            field: 'questions'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status',
            defaultValue: '1'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'feedback_questions'
    })
    return feedbackQuestions
}