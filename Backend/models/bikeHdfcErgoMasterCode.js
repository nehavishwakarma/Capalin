module.exports = (sequelize, DataTypes) => {
    const bikeHdfcErgoMasterCode = sequelize.define('bikeHdfcErgoMasterCode', {
        bikeManufacturerId: {
            type: DataTypes.BIGINT,
            field: 'bike_manufacturer_id'
        },
        bikeModelId: {
            type: DataTypes.BIGINT,
            field: 'bike_model_id'
        },
        vehicleModelCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_model_code'
        },
        manufacturerCode: {
            type: DataTypes.INTEGER,
            field: 'manufacturer_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        noOfWheels: {
            type: DataTypes.INTEGER,
            field: 'no_of_wheels'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        grossVehicleWeight: {
            type: DataTypes.INTEGER,
            field: 'gross_vehicle_weight'
        },
        seatingCapacity: {
            type: DataTypes.INTEGER,
            field: 'seating_capacity'
        },
        carryingCapacity: {
            type: DataTypes.INTEGER,
            field: 'carrying_capacity'
        },
        vehicleClassCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_class_code'
        },
        bodyTypeCode: {
            type: DataTypes.INTEGER,
            field: 'body_type_code'
        },
        vehicleModelStatus: {
            type: DataTypes.STRING,
            field: 'vehicle_model_status'
        },
        activeFlag: {
            type: DataTypes.STRING,
            field: 'active_flag'
        },
        noOfAxle: {
            type: DataTypes.INTEGER,
            field: 'no_of_axle'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        segmentType: {
            type: DataTypes.STRING,
            field: 'segment_type'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        corporate: {
            type: DataTypes.STRING,
            field: 'corporate'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_hdfc_ergo_master_code'
    })
    return bikeHdfcErgoMasterCode
}
