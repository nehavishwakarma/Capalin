const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const leadInsuranceStatus = sequelize.define('leadInsuranceStatus', {
        statusCode: {
            type: DataTypes.INTEGER(11),
            field: 'status_code'
        },
        statusName: {
            type: DataTypes.STRING(100),
            field: 'status_name'
        },
        statusColor: {
            type: DataTypes.STRING(100),
            field: 'status_color'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status',
            defaultvalue: 1
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_insurance_status'
    })
    return leadInsuranceStatus
}