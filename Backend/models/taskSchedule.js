module.exports = (sequelize, DataTypes) => {
    const taskSchedule = sequelize.define("taskSchedule", {
        advisorId: {
            type: DataTypes.INTEGER,
            field: 'advisor_id'
        },
        fName: {
            type: DataTypes.STRING,
            field: 'fname'
        },
        lName: {
            type: DataTypes.STRING,
            field: 'lname'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        category: {
            type: DataTypes.STRING,
            field: 'category'
        },
        title: {
            type: DataTypes.STRING,
            field: 'title'
        },
        description: {
            type: DataTypes.STRING,
            field: 'description'
        },
        taskColor: {
            type: DataTypes.STRING,
            field: 'task_color'
        },
        taskDate: {
            type: DataTypes.DATE,
            field: 'task_date'
        },
        documentType: {
            type: DataTypes.STRING,
            field: 'document_type'
        },
        attachments: {
            type: DataTypes.STRING,
            field: 'attachments'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'task_schedule'
    })
    return taskSchedule
}
