module.exports = (sequelize, DataTypes) => {
    const clientsDocumentCategories = sequelize.define('clientsDocumentCategories', {
        category: {
            type: DataTypes.STRING(255),
            field: 'category'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'clients_document_categories'
    })
    return clientsDocumentCategories
}