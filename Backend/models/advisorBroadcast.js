module.exports = (sequelize, DataTypes) => {
    const advisorBroadcast = sequelize.define('advisorBroadcast', {
        campaignId: {
            type: DataTypes.INTEGER,
            field: 'campaign_id'
        },
        adminId: {
            type: DataTypes.BIGINT,
            field: 'admin_id'
        },
        clientId: {
            type: DataTypes.BIGINT,
            field: 'client_id'
        },
        body: {
            type: DataTypes.TEXT,
            field: 'body'
        },
        type: {
            type: DataTypes.INTEGER,
            field: 'type'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
    }, {
        freezeTableName: true,
        tableName: 'advisor_broadcast',
    })
    return advisorBroadcast
}