module.exports = (sequelize, DataTypes) => {
    const operation = sequelize.define('operation', {
        emailListOperationId: {
            type: DataTypes.INTEGER(11),
            field: 'email_list_operation_id'
        },
        queryReferenceId: {
            type: DataTypes.BIGINT(20),
            field: 'query_reference_id'
        },
        queryDate: {
            type: DataTypes.DATE,
            field: 'query_date'
        },
        fname: {
            type: DataTypes.STRING(255),
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING(255),
            field: 'lname'
        },
        alternateEmail: {
            type: DataTypes.STRING(255),
            field: 'alternate_email'
        },
        mobile: {
            type: DataTypes.STRING(255),
            field: 'mobile'
        },
        alternateMobile: {
            type: DataTypes.STRING(255),
            field: 'alternate_mobile'
        },
        queryMode: {
            type: DataTypes.STRING(255),
            field: 'query_mode'
        },
        query: {
            type: DataTypes.TEXT,
            field: 'query'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '0'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'operation'
    })
    return operation
}       