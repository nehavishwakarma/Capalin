module.exports = (sequelize, DataTypes) => {
    const customCommissionRate = sequelize.define('customCommissionRate', {
        adminId: {
            type: DataTypes.INTEGER(11),
            field: 'admin_id'
        },
        pcTwRate: {
            type: DataTypes.STRING(255),
            field: 'pc_tw_rate'
        },
        mdPaTrRate: {
            type: DataTypes.STRING(255),
            field: 'md_pa_tr_rate'
        },
        termInsRate: {
            type: DataTypes.STRING(255),
            field: 'term_ins_rate'
        },
        startDate: {
            type: DataTypes.DATE,
            field: 'start_date'
        },
        endDate: {
            type: DataTypes.DATE,
            field: 'end_date'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'custom_commission_rate'
    })
    return customCommissionRate
}