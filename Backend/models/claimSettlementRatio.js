module.exports = (sequelize, DataTypes) => {
    const claimSettlementRatio = sequelize.define('claimSettlementRatio', {
        companyId: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        financialYear: {
            type: DataTypes.STRING(255),
            field: 'financial_year'
        },
        ratio: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'ratio'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'claim_settlement_ratio'
    })
    return claimSettlementRatio
}