module.exports = (sequelize, DataTypes) => {
    const district = sequelize.define('district', {
        company_id: {
            type: DataTypes.INTEGER(11),
            field: 'company_id'
        },
        pincode_id: {
            type: DataTypes.INTEGER(11),
            field: 'pincode_id'
        },
        district_name: {
            type: DataTypes.STRING(255),
            field: 'district_name'
        },
        district_code: {
            type: DataTypes.INTEGER(11),
            field: 'district_code'
        }
    }, {
        freezeTableName: true,
        tableName: 'district'
    })
    return district
}