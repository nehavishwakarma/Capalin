module.exports = (sequelize, DataTypes) => {
    const carNewIndMasterCode = sequelize.define('carNewIndMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT,
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT,
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT,
            field: 'car_variant_id'
        },
        make: {
            type: DataTypes.STRING,
            field: 'make'
        },
        model: {
            type: DataTypes.STRING,
            field: 'model'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        fuelType: {
            type: DataTypes.STRING,
            field: 'fuel_type'
        },
        cubicCapacity: {
            type: DataTypes.BIGINT,
            field: 'cubic_capacity'
        },
        seatingCapacity: {
            type: DataTypes.INTEGER,
            field: 'seating_capacity'
        },
        gvw: {
            type: DataTypes.INTEGER,
            field: 'gvw'
        },
        carryingCapacity: {
            type: DataTypes.INTEGER,
            field: 'carrying_capacity',
        },

        carryingCapacity: {
            type: DataTypes.STRING,
            field: 'carrying_capacity'
        },
        cities: {
            type: DataTypes.STRING,
            field: 'cities'
        },
        invoices: {
            type: DataTypes.STRING,
            field: 'invoices'
        },
        sixMonths: {
            type: DataTypes.STRING,
            field: '6_months'
        },
        twelveMonths: {
            type: DataTypes.STRING,
            field: '12_months'
        },
        twentyFourMonths: {
            type: DataTypes.STRING,
            field: '24_months'
        },
        thirtySixMonths: {
            type: DataTypes.STRING,
            field: '36_months'
        },
        fortyEightMonths: {
            type: DataTypes.STRING,
            field: '48_months'
        },
        sixtyMonths: {
            type: DataTypes.STRING,
            field: '60_months'
        },
        seventyTwoMonths: {
            type: DataTypes.STRING,
            field: '72_months',
        },
        eightyFourMonths: {
            type: DataTypes.STRING,
            field: '84_months'
        },
        ninetySixMonths: {
            type: DataTypes.STRING,
            field: '96_months'
        },
        oneThousandEightMonths: {
            type: DataTypes.STRING,
            field: '108_months'
        },
        oneThousandEightyMonths: {
            type: DataTypes.STRING,
            field: '180_months'
        },
        makeModelCode: {
            type: DataTypes.STRING,
            field: 'make_model_code'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_new_ind_master_code'
    })
    return carNewIndMasterCode
}