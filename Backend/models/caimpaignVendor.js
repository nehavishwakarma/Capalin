module.exports = (sequelize, DataTypes) => {
    const caimpaignVendor = sequelize.define('caimpaignVendor', {
        adminId: {
            type: DataTypes.INTEGER,
            field: 'admin_id'
        },
        vendorName: {
            type: DataTypes.STRING,
            field: 'vendor_name'
        },
        subVendorName: {
            type: DataTypes.STRING,
            field: 'sub_vendor_name'
        },
        stoppedSubVendorCampaign: {
            type: DataTypes.STRING,
            field: 'stopped_sub_vendor_campaign'
        },
        ageFrom: {
            type: DataTypes.INTEGER,
            field: 'age_from'
        },
        ageTo: {
            type: DataTypes.INTEGER,
            field: 'age_to'
        },
        income: {
            type: DataTypes.INTEGER,
            field: 'income'
        },
        city: {
            type: DataTypes.TEXT,
            field: 'city'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultValue: 0
        },
        created: {
            type: DataTypes.DATE,
            field: 'created',
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'caimpaign_vendor'
    })
    return caimpaignVendor
}