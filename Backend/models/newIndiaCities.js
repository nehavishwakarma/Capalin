module.exports = (sequelize, DataTypes) => {
    const newIndiaCities = sequelize.define('newIndiaCities', {
        cityName: {
            type: DataTypes.BIGINT(20),
            field: 'city_name'
        },
        cityCode: {
            type: DataTypes.STRING(255),
            field: 'city_code'
        },
        status: {
            type: DataTypes.BOOLEAN,
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'new_india_cities'
    })
    return newIndiaCities
}       