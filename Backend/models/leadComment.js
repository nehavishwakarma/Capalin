module.exports = (sequelize, DataTypes) => {
    const leadComment = sequelize.define('leadComment', {
        leadInsuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_insurance_id'
        },
        commentEntryDate: {
            type: DataTypes.DATE,
            field: 'comment_entry_date'
        },
        adminId: {
            type: DataTypes.BIGINT(20),
            field: 'admin_id'
        },
        adminEmail: {
            type: DataTypes.STRING(100),
            field: 'admin_email'
        },
        adminFname: {
            type: DataTypes.STRING(100),
            field: 'admin_fname'
        },
        adminLname: {
            type: DataTypes.STRING(100),
            field: 'admin_lname'
        },
        adminDate: {
            type: DataTypes.DATE,
            field: 'admin_date'
        },
        contactMode: {
            type: DataTypes.STRING(100),
            field: 'contact_mode'
        },
        ratedByEmp: {
            type: DataTypes.INTEGER(11),
            field: 'rated_by_emp'
        },
        product: {
            type: DataTypes.STRING(100),
            field: 'product'
        },
        reasons: {
            type: DataTypes.STRING(100),
            field: 'reasons'
        },
        comment: {
            type: DataTypes.TEXT,
            field: 'comment'
        },
        followUp: {
            type: DataTypes.DATE,
            field: 'follow_up'
        },
        remainderStatus: {
            type: DataTypes.INTEGER(11),
            field: 'remainder_status'
        }
    }, {
        freezeTableName: true,
        tableName: 'lead_comment'
    })
    return leadComment
}   