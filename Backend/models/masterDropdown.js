module.exports = (sequelize, DataTypes) => {
    const masterDropdown = sequelize.define('masterDropdown', {
        dropdownType: {
            type: DataTypes.STRING(255),
            field: 'dropdown_type'
        },
        optionName: {
            type: DataTypes.STRING(255),
            field: 'option_name'
        },
        status: {
            type: DataTypes.INTEGER(11),
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER(11),
            field: 'is_deleted',
            defaultValue: 0
        }
    }, {
        freezeTableName: true,
        tableName: 'master_dropdown'
    })
    return masterDropdown
}   