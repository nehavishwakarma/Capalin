module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define("user", {
        emailListId: {
            type: DataTypes.BIGINT,
            field: 'email_list_id'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        userGroup: {
            type: DataTypes.STRING,
            field: 'user_group'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        loginCount: {
            type: DataTypes.INTEGER,
            field: 'login_count'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        password: {
            type: DataTypes.STRING,
            field: 'password'
        },
        salt: {
            type: DataTypes.STRING,
            field: 'salt'
        },
        passwordToken: {
            type: DataTypes.STRING,
            field: 'password_token'
        },
        passwordTokenCreated: {
            type: DataTypes.INTEGER,
            field: 'password_token_created'
        },
        ipAddress: {
            type: DataTypes.STRING,
            field: 'ip_address'
        },
        userAgent: {
            type: DataTypes.TEXT,
            field: 'user_agent'
        },
        signupSource: {
            type: DataTypes.STRING,
            field: 'signup_source'
        },
        fbId: {
            type: DataTypes.STRING,
            field: 'fb_id'
        },
        googleId: {
            type: DataTypes.STRING,
            field: 'google_id'
        },
        randomPasswordGenerate: {
            type: DataTypes.STRING,
            field: 'random_passwod_generate'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted',
            defaultValue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'user'
    })
    return user
}