module.exports = (sequelize, DataTypes) => {
    const healthSumInsured = sequelize.define('healthSumInsured', {
        masterSheetHealthId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_health_id'
        },
        insuredCode: {
            type: DataTypes.STRING(255),
            field: 'insured_code'
        },
        amount: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'amount'
        },
        coverType: {
            type: DataTypes.STRING(255),
            field: 'cover_type'
        },
        termPlan: {
            type: DataTypes.INTEGER(11),
            field: 'term_plan'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_sum_insured'
    })
    return healthSumInsured
}   