module.exports = (sequelize, DataTypes) => {
    const bikeManufacturer = sequelize.define('bikeManufacturer', {
        manufacturer: {
            type: DataTypes.STRING,
            field: 'manufacturer'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_manufacturer'
    })
    return bikeManufacturer
}
