module.exports = (sequelize, DataTypes) => {
    const networkHospital = sequelize.define('networkHospital', {
        companyId: {
            type: DataTypes.BIGINT(20),
            field: 'company_id'
        },
        cityId: {
            type: DataTypes.BIGINT(20),
            field: 'city_id'
        },
        hospitalName: {
            type: DataTypes.STRING(255),
            field: 'hospital_name'
        },
        addressOne: {
            type: DataTypes.TEXT,
            field: 'address_1'
        },
        addressTwo: {
            type: DataTypes.TEXT,
            field: 'address_2'
        },
        area: {
            type: DataTypes.STRING(255),
            field: 'area'
        },
        city: {
            type: DataTypes.STRING(255),
            field: 'city'
        },
        pincode: {
            type: DataTypes.INTEGER(11),
            field: 'pincode'
        },
        district: {
            type: DataTypes.STRING(255),
            field: 'district'
        },
        state: {
            type: DataTypes.STRING(255),
            field: 'state'
        },
        stdCode: {
            type: DataTypes.STRING(255),
            field: 'std_code'
        },
        contactNumberOne: {
            type: DataTypes.STRING(255),
            field: 'contact_number_1'
        },
        contactNumberTwo: {
            type: DataTypes.STRING(255),
            field: 'contact_number_2'
        },
        contactNumberThree: {
            type: DataTypes.STRING(255),
            field: 'contact_number_3'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_deleted',
            defaultValue: '0'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'network_hospital'
    })
    return networkHospital
}       