module.exports = (sequelize, DataTypes) => {
    const bikeShriramGiMasterCode = sequelize.define('bikeShriramGiMasterCode', {
        bikeManufacturerId: {
            type: DataTypes.BIGINT,
            field: 'bike_manufacturer_id'
        },
        bikeModelId: {
            type: DataTypes.BIGINT,
            field: 'bike_model_id'
        },
        vehicleModelCode: {
            type: DataTypes.STRING,
            field: 'vehicle_model_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_shriram_gi_master_code'
    })
    return bikeShriramGiMasterCode
}
