module.exports = (sequelize, DataTypes) => {
    const salesReportLog = sequelize.define("salesReportLog", {
        salesReportId: {
            type: DataTypes.INTEGER,
            field: 'sales_report_id'
        },
        editBy: {
            type: DataTypes.BIGINT(20),
            field: 'edit_by'
        },
        prevPolicyStatus: {
            type: DataTypes.INTEGER,
            field: 'prev_policy_status'
        },
        newPolicyStatus: {
            type: DataTypes.INTEGER,
            field: 'new_policy_status'
        },
        salesUpdateLog: {
            type: DataTypes.STRING,
            field: 'sales_update_log'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        }
    }, {
        freezeTableName: true,
        tableName: 'sales_report_log'
    })
    return salesReportLog
}
