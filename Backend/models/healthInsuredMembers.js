module.exports = (sequelize, DataTypes) => {
    const healthInsuredMembers = sequelize.define('healthInsuredMembers', {
        proposalId: {
            type: DataTypes.BIGINT(20),
            field: 'proposal_id'
        },
        iFirstName: {
            type: DataTypes.STRING(255),
            field: 'i_first_name'
        },
        iMiddleName: {
            type: DataTypes.STRING(255),
            field: 'i_middle_name'
        },
        iLastName: {
            type: DataTypes.STRING(255),
            field: 'i_last_name'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        gender: {
            type: DataTypes.STRING(255),
            field: 'gender'
        },
        relationship: {
            type: DataTypes.STRING(255),
            field: 'relationship'
        },
        relationshipCode: {
            type: DataTypes.STRING(255),
            field: 'relationship_code'
        },
        insuredType: {
            type: DataTypes.STRING(255),
            field: 'insured_type'
        },
        preIllness: {
            type: DataTypes.STRING(255),
            field: 'pre_illness',
            defaultValue: '1'
        },
        hospitalCash: {
            type: DataTypes.INTEGER(11),
            field: 'hospital_cash',
            defaultValue: 0
        },
        personalAccident: {
            type: DataTypes.INTEGER(11),
            field: 'personal_accident',
            defaultValue: 0
        },
        relationshipName: {
            type: DataTypes.STRING(255),
            field: 'relationship_name'
        },
        insuredOccupation: {
            type: DataTypes.STRING(255),
            field: 'insured_occupation'
        },
        insuredOccupationCode: {
            type: DataTypes.STRING(255),
            field: 'insured_occupation_code'
        },
        heightFeet: {
            type: DataTypes.INTEGER(11),
            field: 'height_feet'
        },
        heightInch: {
            type: DataTypes.INTEGER(11),
            field: 'height_inch'
        },
        weight: {
            type: DataTypes.INTEGER(11),
            field: 'weight'
        },
        waistline: {
            type: DataTypes.INTEGER(11),
            field: 'waistline'
        },
        iMobile: {
            type: DataTypes.STRING(255),
            field: 'i_mobile'
        },
        iMaritalStatus: {
            type: DataTypes.STRING(255),
            field: 'i_marital_status'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_insured_members'
    })
    return healthInsuredMembers
}   