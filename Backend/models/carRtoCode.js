const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const carRtoCode = sequelize.define('carRtoCode', {
        rtoCodeId: {
            type: DataTypes.INTEGER,
            field: 'rto_code_id'
        },
        companyId: {
            type: DataTypes.INTEGER,
            field: 'company_id'
        },
        rtoCode: {
            type: DataTypes.STRING(255),
            field: 'rto_code'
        },
        description: {
            type: DataTypes.STRING(255),
            field: 'description'
        },
        developerDesc: {
            type: DataTypes.STRING(255),
            field: 'developer_desc'
        },
        stateId: {
            type: DataTypes.STRING(255),
            field: 'state_id'
        },
        stateName: {
            type: DataTypes.STRING(255),
            field: 'state_name'
        },
        zoneCode: {
            type: DataTypes.STRING(255),
            field: 'zone_code'
        },
        productCode: {
            type: DataTypes.STRING(255),
            field: 'product_code'
        },
        zoneName: {
            type: DataTypes.STRING(255),
            field: 'zone_name'
        },
        productDescription: {
            type: DataTypes.STRING(255),
            field: 'product_description'
        },
        licensePlateOne: {
            type: DataTypes.STRING(255),
            field: 'license_plate_one'
        },
        licensePlateTwo: {
            type: DataTypes.STRING(255),
            field: 'license_plate_two'
        },
        LTGICLBranch: {
            type: DataTypes.STRING(255),
            field: 'LTGICL_branch'
        },
        status: {
            type: DataTypes.STRING,
            field: 'status'
        },
        isDeleted: {
            type: DataTypes.STRING,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.STRING,
            field: 'created'
        },
        modified: {
            type: DataTypes.STRING,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_rto_code'
    })
    carRtoCode.associate = (models) =>{
        carRtoCode.belongsTo(models.carRto,{foreignKey:'rtoCodeId' , as:'RtoCode'})        
    }
    return carRtoCode
}