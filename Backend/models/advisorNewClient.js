module.exports = (sequelize, DataTypes) => {
    const adminRolePermissions = sequelize.define('adminRolePermissions', {
        advisorId: {
            type: DataTypes.INTEGER,
            field: 'advisor_id'
        },
        fname: {
            type: DataTypes.STRING,
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING,
            field: 'lname'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        companyName: {
            type: DataTypes.STRING,
            field: 'company_name'
        },
        policyName: {
            type: DataTypes.STRING,
            field: 'policy_name'
        },
        sumAssured: {
            type: DataTypes.INTEGER,
            field: 'sum_assured'
        },
        premium: {
            type: DataTypes.INTEGER,
            field: 'premium'
        },
        paymentDate: {
            type: DataTypes.DATE,
            field: 'payment_date'
        },
        status: {
            type: DataTypes.ENUM('0', '1', '2'),
            field: 'status',
            defaultValue: '0'
        },
        adminId: {
            type: DataTypes.INTEGER,
            field: 'admin_id'
        },
        reason: {
            type: DataTypes.STRING,
            field: 'reason'
        },
    }, {
        freezeTableName: true,
        tableName: 'admin_role_permissions',
    })
    return adminRolePermissions
}