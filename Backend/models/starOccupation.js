module.exports = (sequelize, DataTypes) => {
    const starOccupation = sequelize.define("starOccupation", {
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        occupationCode: {
            type: DataTypes.STRING,
            field: 'occupation_code'
        },
        occupation: {
            type: DataTypes.STRING,
            field: 'occupation'
        },
        status:{
            type:DataTypes.BOOLEAN,
            field:'status',
            defaultValue: '1'
        },
        isDeleted:{
            type:DataTypes.BOOLEAN,
            field:'is_deleted'
        },
        created:{
            type:DataTypes.DATE,
            field:'created'
        }
    }, {
        freezeTableName: true,
        tableName: 'star_occupation'
    })
    return starOccupation
}
