module.exports = (sequelize, DataTypes) => {
    const advisorBroadcastCampaign = sequelize.define('advisorBroadcastCampaign', {
        campaignName: {
            type: DataTypes.STRING,
            field: 'campaign_name'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'advisor_broadcast_campaign'
    })
    return advisorBroadcastCampaign
}