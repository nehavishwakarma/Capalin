module.exports = (sequelize, DataTypes) => {
    const healthPremium = sequelize.define('healthPremium', {
        healthInsuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'health_insurance_id'
        },
        premiumCode: {
            type: DataTypes.STRING(255),
            field: 'premium_code'
        },
        area: {
            type: DataTypes.STRING(255),
            field: 'area'
        },
        adult: {
            type: DataTypes.INTEGER(11),
            field: 'adult'
        },
        child: {
            type: DataTypes.INTEGER(11),
            field: 'child'
        },
        sa: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'sa'
        },
        minAge: {
            type: DataTypes.INTEGER(11),
            field: 'min_age'
        },
        maxAge: {
            type: DataTypes.INTEGER(11),
            field: 'max_age'
        },
        term: {
            type: DataTypes.INTEGER(11),
            field: 'term'
        },
        premium: {
            type: DataTypes.DECIMAL(11, 2),
            field: 'premium'
        }
    }, {
        freezeTableName: true,
        tableName: 'health_premium'
    })
    healthPremium.associate = (models) => {
        healthPremium.belongsTo(models.healthPremium, { foreignKey: 'healthInsuranceId', as: 'healthPremium' })
    }
    return healthPremium
}   