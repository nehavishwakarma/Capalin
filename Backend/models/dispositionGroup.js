module.exports = (sequelize, DataTypes) => {
    const dispositionGroup = sequelize.define('dispositionGroup', {
        description: {
            type: DataTypes.STRING(255),
            field: 'description'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'disposition_group'
    })
    return dispositionGroup
}