module.exports = (sequelize, DataTypes) => {
    const bikeTataAigMasterCode = sequelize.define('bikeTataAigMasterCode', {
        bikeManufacturerId: {
            type: DataTypes.BIGINT,
            field: 'bike_manufacturer_id'
        },
        bikeModelId: {
            type: DataTypes.BIGINT,
            field: 'bike_model_id'
        },
        vehicleModelCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_model_code'
        },
        manufacturerCode: {
            type: DataTypes.INTEGER,
            field: 'manufacturer_code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        vehicleModelName: {
            type: DataTypes.STRING,
            field: 'vehicle_model_name'
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        vehicleClassCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_class_code'
        },
        bodyTypeCode: {
            type: DataTypes.INTEGER,
            field: 'body_type_code'
        },
        segmentType: {
            type: DataTypes.STRING,
            field: 'segment_type'
        },
        segmentCode: {
            type: DataTypes.INTEGER,
            field: 'segment_code'
        },
        modelParentCode: {
            type: DataTypes.STRING,
            field: 'model_parent_code'
        },
        variant: {
            type: DataTypes.STRING,
            field: 'variant'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_Deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'bike_tata_aig_master_code'
    })
    return bikeTataAigMasterCode
}
