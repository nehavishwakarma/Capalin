module.exports = (sequelize, DataTypes) => {
    const ulipEnquiry = sequelize.define("ulipEnquiry", {
        enquiryId: {
            type: DataTypes.STRING,
            field: 'enquiry_id'
        },
        leadInsuranceId: {
            type: DataTypes.BIGINT(20),
            field: 'lead_insurance_id'
        },
        masterSheetId: {
            type: DataTypes.BIGINT(20),
            field: 'master_sheet_id'
        },
        annualIncome: {
            type: DataTypes.INTEGER,
            field: 'annual_income'
        },
        dob: {
            type: DataTypes.DATE,
            field: 'dob'
        },
        age: {
            type: DataTypes.INTEGER,
            field: 'age'
        },
        gender: {
            type: DataTypes.ENUM('Male','Female'),
            field: 'gender'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.ENUM('0','1'),
            field: 'is_deleted'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'ulip_enquiry'
    })
    return ulipEnquiry
}