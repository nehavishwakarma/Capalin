module.exports = (sequelize, DataTypes) => {
    const admin = sequelize.define('admin', {
        emailListId: {
            type: DataTypes.INTEGER,
            field: 'email_list_id'
        },
        mobileListId: {
            type: DataTypes.INTEGER,
            field: 'mobile_list_id'
        },
        username: {
            type: DataTypes.STRING,
            field: 'username'
        },
        fname: {
            type: DataTypes.STRING,
            field: 'fname'
        },
        lname: {
            type: DataTypes.STRING,
            field: 'lname'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        mobile: {
            type: DataTypes.STRING,
            field: 'mobile'
        },
        adminGroup: {
            type: DataTypes.STRING,
            field: 'admin_group'
        },
        rmId: {
            type: DataTypes.INTEGER,
            field: 'rm_id'
        },
        sourceAdminId: {
            type: DataTypes.BIGINT,
            field: 'source_admin_id'
        },
        isSalesMan: {
            type: DataTypes.INTEGER,
            field: 'is_sales_man',
            defaultvalue: 0
        },
        isTeamLeader: {
            type: DataTypes.INTEGER,
            field: 'is_team_leader'
        },
        panNo: {
            type: DataTypes.STRING,
            field: 'pan_no'
        },
        aadharNo: {
            type: DataTypes.STRING,
            field: 'aadhar_no'
        },
        accountNo: {
            type: DataTypes.STRING,
            field: 'account_no'
        },
        bankName: {
            type: DataTypes.STRING,
            field: 'bank_name'
        },
        bankIfsc: {
            type: DataTypes.STRING,
            field: 'bank_ifsc'
        },
        branch_name: {
            type: DataTypes.STRING,
            field: 'branch_name'
        },
        address: {
            type: DataTypes.TEXT,
            field: 'address'
        },
        city: {
            type: DataTypes.STRING,
            field: 'city'
        },
        state: {
            type: DataTypes.STRING,
            field: 'state'
        },
        pinCode: {
            type: DataTypes.INTEGER,
            field: 'pin_code'
        },
        ipAddress: {
            type: DataTypes.STRING,
            field: 'ip_address'
        },
        userAgent: {
            type: DataTypes.TEXT,
            field: 'user_agent'
        },
        loginCount: {
            type: DataTypes.INTEGER,
            field: 'login_count'
        },
        passwordNew: {
            type: DataTypes.STRING,
            field: 'password_new'
        },
        rememberToken: {
            type: DataTypes.STRING,
            field: 'remember_token'
        },
        mobileOtp: {
            type: DataTypes.STRING,
            field: 'mobile_otp'
        },
        mobileStatus: {
            type: DataTypes.STRING,
            field: 'mobile_status',
            defaultvalue: '0'
        },
        emailConfirm: {
            type: DataTypes.STRING,
            field: 'email_confirm'
        },
        isCustomCommission: {
            type: DataTypes.ENUM('0', '1'),
            field: 'is_custom_commission'           
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        deletedAt: {
            type: DataTypes.DATE,
            field: 'deleted_at'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultvalue: '0'
        }
    }, {
        freezeTableName: true,
        tableName: 'admin',
    })
    return admin
}