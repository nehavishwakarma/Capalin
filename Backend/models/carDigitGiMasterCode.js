module.exports = (sequelize, DataTypes) => {
    const carDigitGiMasterCode = sequelize.define('carDigitGiMasterCode', {
        carModelId: {
            type: DataTypes.STRING,
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.STRING,
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.STRING,
            field: 'car_variant_id'
        },
        vehicleCode: {
            type: DataTypes.STRING,
            field: 'Vehicle Code'
        },
        vehicleManufacturer: {
            type: DataTypes.STRING,
            field: 'vehicle_manufacturer'
        },
        modelName: {
            type: DataTypes.STRING,
            field: 'model_name'
        },
        demoConcat: {
            type: DataTypes.STRING,
            field: 'demo_concat'
        },
        Variant: {
            type: DataTypes.STRING,
            field: 'Variant'
        },
        SeatingCapacity	: {
            type: DataTypes.INTEGER,
            field: 'Seating Capacity'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel'
        },
        CubicCapacity	: {
            type: DataTypes.INTEGER,
            field: 'Cubic Capacity'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_digit_gi_master_code'
    })
    return carDigitGiMasterCode
}