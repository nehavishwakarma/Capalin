module.exports = (sequelize, DataTypes) => {
    const serviceTypeMaster = sequelize.define("serviceTypeMaster", {
        categoryId: {
            type: DataTypes.BIGINT(20),
            field: 'category_id'
        },
        categoryDescription: {
            type: DataTypes.STRING,
            field: 'category_description'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        }
    }, {
        freezeTableName: true,
        tableName: 'service_type_master'
    })
    return serviceTypeMaster
}
