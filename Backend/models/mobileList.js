module.exports = (sequelize, DataTypes) => {
    const mobileList = sequelize.define('mobileList', {
        emailListId: {
            type: DataTypes.BIGINT(20),
            field: 'email_list_id'
        },
        mobile: {
            type: DataTypes.BIGINT(20),
            field: 'mobile'
        },
        mobileStatus: {
            type: DataTypes.ENUM('0', '1'),
            field: 'mobile_status',
            defaultValue: '1'
        },
        mobileOtp: {
            type: DataTypes.STRING,
            field: 'mobile_otp'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.INTEGER,
            field: 'modified'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'mobile_list'
    })
    return mobileList
}