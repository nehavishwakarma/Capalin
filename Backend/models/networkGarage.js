module.exports = (sequelize, DataTypes) => {
    const networkGarage = sequelize.define('networkGarage', {
        garageName: {
            type: DataTypes.STRING(255),
            field: 'garage_name'
        },
        companyId: {
            type: DataTypes.INTEGER(11),
            field: 'company_id'
        },
        cityId: {
            type: DataTypes.STRING(255),
            field: 'city_id'
        },
        addressOne: {
            type: DataTypes.STRING(255),
            field: 'address_1'
        },
        addressTwo: {
            type: DataTypes.STRING(255),
            field: 'address_2'
        },
        addressThree: {
            type: DataTypes.STRING(255),
            field: 'address_3'
        },
        addressFour: {
            type: DataTypes.STRING(255),
            field: 'address_4'
        },
        addressFive: {
            type: DataTypes.STRING(255),
            field: 'address_5'
        },
        cityName: {
            type: DataTypes.STRING(255),
            field: 'city_name'
        },
        stateName: {
            type: DataTypes.STRING(255),
            field: 'state_name'
        },
        pincode: {
            type: DataTypes.STRING(255),
            field: 'pincode'
        },
        contactPerson: {
            type: DataTypes.STRING(255),
            field: 'contact_person'
        },
        mobileNo: {
            type: DataTypes.STRING(255),
            field: 'mobile_no'
        },
        offPhoneNo: {
            type: DataTypes.STRING(255),
            field: 'off_phone_no'
        },
        compName: {
            type: DataTypes.STRING(255),
            field: 'comp_name'
        },
        vehMake: {
            type: DataTypes.STRING(255),
            field: 'veh_make'
        },
        zone: {
            type: DataTypes.STRING(255),
            field: 'zone'
        }
    }, {
        freezeTableName: true,
        tableName: 'network_garage'
    })
    return networkGarage
}       