module.exports = (sequelize, DataTypes) => {
    const modules = sequelize.define('modules', {
        groupBy: {
            type: DataTypes.INTEGER(11),
            field: 'group_by'
        },
        orderBy: {
            type: DataTypes.INTEGER(11),
            field: 'order_by'
        },
        icon: {
            type: DataTypes.STRING(255),
            field: 'icon'
        },
        module: {
            type: DataTypes.STRING(255),
            field: 'module'
        },
        action: {
            type: DataTypes.STRING(255),
            field: 'action'
        },
        actionLabel: {
            type: DataTypes.STRING(255),
            field: 'action_label'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        },
        status: {
            type: DataTypes.ENUM('1', '0'),
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted',
            defaultValue: '0'
        },
        deletedAt: {
            type: DataTypes.DATE,
            field: 'deleted_at'
        }
    }, {
        freezeTableName: true,
        tableName: 'modules'
    })
    return modules
}   