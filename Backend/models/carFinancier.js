module.exports = (sequelize, DataTypes) => {
    const carFinancier = sequelize.define('carFinancier', {
        companyId: {
            type: DataTypes.BIGINT,
            field: 'company_id'
        },
        financierName: {
            type: DataTypes.STRING,
            field: 'financier_name'
        },
        companyCode: {
            type: DataTypes.STRING,
            field: 'company_code'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status',
            defaultValue: 1
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted',
            defaultValue: 0
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.INTEGER,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_financier'
    })
    return carFinancier
}