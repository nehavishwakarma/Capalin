const { DateTime } = require('../utils/date')
module.exports = (sequelize, DataTypes) => {
    const carModel = sequelize.define('carModel', {
        carModelName: {
            type: DataTypes.STRING,
            field: 'car_model_name'
        },
        status: {
            type: DataTypes.ENUM('0', '1'),
            field: 'status',
            defaultValue: '1'
        },
        isDeleted: {
            type: DataTypes.INTEGER,
            field: 'is_deleted'
        },
        created: {
            type: DataTypes.DATE,
            field: 'created'
        },
        modified: {
            type: DataTypes.DATE,
            field: 'modified'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_model'
    })
    return carModel
}