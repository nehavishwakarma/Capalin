module.exports = (sequelize, DataTypes) => {
    const carBajajAllianzMasterCode = sequelize.define('carBajajAllianzMasterCode', {
        carModelId: {
            type: DataTypes.BIGINT,
            field: 'car_model_id'
        },
        carFuelTypeId: {
            type: DataTypes.BIGINT,
            field: 'car_fuel_type_id'
        },
        carVariantId: {
            type: DataTypes.BIGINT,
            field: 'car_variant_id'
        },
        vehicleCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_code'
        },
        vehicleType: {
            type: DataTypes.STRING,
            field: 'vehicle_type'
        },
        vehicleMakeCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_make_code'
        },
        vehicleMake: {
            type: DataTypes.STRING,
            field: 'vehicle_make'
        },
        vehicleModelCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_model_code'
        },
        vehicleModel: {
            type: DataTypes.STRING,
            field: 'vehicle_model'
        },
        vehicleSubtypeCode: {
            type: DataTypes.INTEGER,
            field: 'vehicle_subtype_code'
        },
        vehicleSubtype: {
            type: DataTypes.STRING,
            field: 'vehicle_subtype'
        },
        fuel: {
            type: DataTypes.STRING,
            field: 'fuel',
        },
        cubicCapacity: {
            type: DataTypes.INTEGER,
            field: 'cubic_capacity'
        },
        carryingCapacity: {
            type: DataTypes.INTEGER,
            field: 'carrying_capacity'
        }
    }, {
        freezeTableName: true,
        tableName: 'car_bajaj_allianz_master_code'
    })
    return carBajajAllianzMasterCode
}