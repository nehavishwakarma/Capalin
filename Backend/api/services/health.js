const models = require('../../models')
const { Op } = require('sequelize')
const moment = require('moment')
const { saveLead, emailListId } = require('./helperFunction')
const { Base64 } = require('js-base64')
const { mode } = require('crypto-js')
const { gst } = require('../../config/common')

exports.getStateDetails = async (companyId, pincode) => {
    let result = await models.pincode.findAll({
        include: [
            {
                model: models.states,
                as: 'states',
                require: true,
                include: [
                    {
                        model: models.cities,
                        as: 'cities',
                        where: {
                            companyId: companyId
                        }
                    }
                ]
            }

        ],
        where: {
            pincode: pincode,
        },
        attrubutes: ['pincode', 'state_name', 'company_id', 'city', 'city_code', 'state_code', 'district_code']
    })
    if (result) {
        return { success: true, getState: result }
    } else {
        return { success: false, getState: null }
    }

}
exports.healthEnquiryDetails = async (enquiryId) => {
    let result = await models.healthEnquiry.findAll({
        where: {
            enquiryId: enquiryId
        },
        order: [
            ['createdAt', 'DESC'],
        ],
    })
    return result;
}

exports.updateHealthEnquiryPincode = async (enquiryId, pincodeId) => {
  await models.healthEnquiry.update({
    pincodeId: pincodeId
  }, {
    where: {
      enquiryId: enquiryId
    }
  })
}

exports.saveHealthProposalDetails = async (data) => {
  if (data.dob === "undefined-undefined-undefined") {
    delete data.dob
  }
  data.gender = (data.gender == 0) ? "Male" : "Female"

  if (data.policy_hard_copy) {
    delete data.policy_hard_copy;
  }
  if (data.p_first_name != '') {
    let leadInsuranceId = await models.healthEnquiry.findOne({
      where: {
        enquiryId: data.health_enquiry_id,
      },
    })

    let obj = {};
    let updateLead;
    if (data['email']) {
      obj.emailId = emailListId(email);
      obj.mobileListId = mobileListId(mobile, email);
      updateLead = {
        fname: data.p_first_name,
        lname: data.p_last_name,
        email: data.email,
        mobile: data.mobile,
        emailListId: obj.emailId,
        mobileListId: obj.mobileListId,
      }
    } else {
      updateLead = {
        fname: data.p_first_name,
        lname: data.p_last_name,
      }
    }
    await models.leadInsurance.update(updateLead, {
      where: {
        id: leadInsuranceId.leadInsuranceId,
      },
    })
  }

  if ("reg_district_id" in data) {
    delete data.reg_district_id
  }
  if ("reg_state_name" in data) {
    delete data.reg_state_name
  }

  let checkEnquiryExists = await models.healthProposalForm.findOne({
    where: {
      healthEnquiryId: data.health_enquiry_id,
    },
  })

  if (data.id == "") {
    let saveHealthDetail = await models.healthProposalForm.create(data);
    if (saveHealthDetail) {
      return { id: saveHealthDetail.id }
    } else {
      return res.json({ message: "id not inserdere" });
    }
  } else {
    let updateHealthDetail = await models.healthProposalForm.update(data, {
      where: {
        id: data.id
      }
    })

    return { id: data.id }
  }
}
