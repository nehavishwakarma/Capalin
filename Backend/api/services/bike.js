const session = require('express-session')
const model = require('../../models')
const { saveLead } = require('./helperFunction')
exports.bikeInsuranceAjax = async (table, id) => {
    let result = {}
    if (table == 'bike_manufacturer') {
        result.makeModel = await getTable()

        result.rto = await getBikeRto()
        result.existing_insurance = await companies()

        if (result.makeModel.length == 0 || result.rto.length == 0 || result.existing_insurance.length == 0) {
            result.success = false
            result.status = 404
        } else {
            result.year = getYearMonth().year
            result.month = getYearMonth().month
            result.success = true
            result.status = 200
        }
    } else if (table == 'bike_model' && id != null && !isNaN(id)) {
        result.response = await bikeModel(id)
        if (result.response.length == 0) {
            result.success = false
            result.status = 400
        } else {
            result.success = true
            result.status = 200
        }

    }
    return result
}

const getYearMonth = () => {
    let result = {}
    result.year = []
    result.month = []

    for (i = 2000; i <= new Date().getFullYear(); i++) {
        result.year.push(i)
    }

    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    for (i = 0; i < months.length; i++) {
        result.month.push({ id: i + 1, name: months[i] })
    }
    return result
}

const getBikeRto = async () => {
    return await model.carRto.findAll({
        where: {
            status: '1'
        },
        attributes: [
            'rto_description', 'id', 'rto_code'
        ]
    })
}

const getTable = async () => {
    return await model.bikeManufacturer.findAll({
        where: {
            status: '1'
        },
        attributes: [
            'manufacturer', 'id'
        ]
    })
}
const companies = async () => {
    return await model.companies.findAll({
        where: {
            status: '1',
            insuranceType: 'General',
        },
        attributes: [
            'company_full_name', 'id'
        ]
    })
}
const bikeModel = async (id) => {
    console.log(id);
    return await model.bikeModel.findAll({
        where: {
            status: '1',
            manufacturerId: id
        },
        attributes: [
            'model', 'id'
        ]
    })
}
exports.bikeProposalForm = async (dataValue) => {
    let result = await model.bikeProposalForm.findOne({
        where: {
            bikeEnquiryId: dataValue
        }
    })
    return result
}
exports.getBikeProposalFormData = async (enquiryId, proposalId) => {
    let bikeProposalDetails = null;
    if (proposalId != null) {
        bikeProposalDetails = await getBikeProposalFormData(enquiryId, proposalId);
    } else if (session.ProposalId) {
        bikeProposalDetails = await getBikeProposalFormData(enquiryId, session.ProposalId)
    }
    const bikeEnquiryDetails = await getbikeEnquiryDetails(enquiryId);
    bikeEnquiryProposalDetails = {
        "bikeEnquiryDetails": bikeEnquiryDetails,
        "bikeProposalDetails": bikeProposalDetails
    }
    if (bikeEnquiryProposalDetails) {
        return bikeEnquiryProposalDetails;
    } else {
        res.status(404).send('Data not found')
    }
}
const getBikeProposalFormData = async (enquiryId, proposalId) => {
    let getBikeProposalDetails = await model.bikeProposalForm.findAll({
        include: [{
            model: model.bikeEnquiry,
            as: 'bikeProposalFormToBikeEnquiry',
            require: true,
            where: {
                enquiryId: enquiryId
            },
        }],
        where: {
            id: proposalId
        }
    })
    if (getBikeProposalDetails) {
        return getBikeProposalDetails
    } else {
        return null
    }
}

const getbikeEnquiryDetails = async (enquiryId) => {
    let enquiryDetails = await model.bikeEnquiry.findAll({
        include: [
            {
                model: model.leadInsurance,
                as: 'leId',
                require: true,
                attributes: ['masterSheetId'],
                include: [
                    {
                        model: model.masterSheet,
                        as: 'msId',
                        require: true,
                        attributes: ['policyName']
                    }
                ]
            },
            {
                model: model.carRto,
                as: 'crId',
                require: true,
                attributes: ['rtoCode']
            },
            {
                model: model.bikeModel,
                as: 'bmId',
                require: true,
                attributes: ['model']
            },
            {
                model: model.bikeManufacturer,
                as: 'bikeManufacturerId',
                require: true,
                attributes: ['manufacturer']
            }
        ],
        where: {
            enquiryId: enquiryId
        },
        attributes: ['policyTerm', 'idvMultiyear', 'premiumMultiyear']
    })
    if (enquiryDetails) {
        return enquiryDetails
    } else {
        return null
    }

}

