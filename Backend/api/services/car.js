const model = require('../../models')
const { Op } = require('sequelize')
const moment = require('moment')
const { saveLead, emailListId } = require('./helperFunction')
const { Base64 } = require('js-base64')

exports.carInsuranceAjax = async (table, id) => {
    let result = {}
    if (table == 'car_model' && id == 'car_rto') {
        result.makeModel = await carModel()
        result.rto = await carRto()
        result.existing_insurance = await companies()

        if (result.makeModel.length == 0 || result.rto.length == 0 || result.existing_insurance.length == 0) {
            return { status: 404, success: false, data: "Data Not Found" }
        } else {
            result.year = getYearMonth().year
            result.month = getYearMonth().month
            return { status: 200, success: true, data: result }
        }
    } else if (table == 'car_fuel_type' && id != null && !isNaN(id)) {
        result.response = await carFuelType()
        if (result.response.length) {
            return { success: true, status: 200, data: result.response }
        } else {
            return { success: true, status: 200, data: "Data Not Found" }
        }

    } else if (table == 'car_variant' && id != null && !isNaN(id)) {
        result.response = await carVarient()
        if (result.response.length) {
            return { success: true, status: 200, data: result.response }
        } else {
            return { success: false, status: 404, data: "Data Not Found" }
        }
    } else {
        return { data: "Something Went Wrong", status: 404, succes: false }
    }
}

const getYearMonth = () => {
    let result = {}
    result.year = []
    result.month = []

    for (i = 2000; i <= new Date().getFullYear(); i++) {
        result.year.push(i)
    }

    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    for (i = 0; i < months.length; i++) {
        result.month.push({ id: i + 1, name: months[i] })
    }
    return result
}

const carRto = async () => {
    return await model.carRto.findAll({
        where: {
            status: '1'
        },
        attributes: [
            'rto_description', 'id', 'rto_code'
        ]
    })
}

const carModel = async () => {
    return await model.carModel.findAll({
        where: {
            status: '1'
        },
        attributes: [
            'car_model_name', 'id'
        ]
    })
}

const companies = async () => {
    return await model.companies.findAll({
        where: {
            status: '1',
            insuranceType: 'General',
        },
        attributes: [
            'company_full_name', 'id'
        ]
    })
}

const carFuelType = async () => {
    return await model.carFuelType.findAll({
        where: {
            status: '1',
            car_model_id: id
        },
        attributes: [
            'fuel_name', 'id'
        ]
    })
}

const carVarient = async () => {
    return await model.carVariant.findAll({
        where: {
            status: '1',
            carFuelTypeId: id
        },
        attributes: [
            'variant', 'id'
        ]
    })
}

exports.carProposalData = async (dataValue) => {
    let result = await model.carProposalForm.findOne({
        where: {
            carEnquiryId: dataValue
        },
        order: [
            ['id', 'DESC'],
        ],
    })

    if (result) {
        return { status: 200, data: result, error: "" }
    } else {
        return { status: 404, data: result, error: 'Data Not Found' }
    }
}

exports.carStoreJson = async (reqData) => {
    let result = {}

    let { policyType, leadId, enquiryType, makeModel, fuelType, variant, rto, manufactYear, manufactMonth, regtDate, expiryDate, claimStatus, lastPolicyNcb, selectedMonthYear } = reqData

    let manufacturingDate = manufactYear.concat('-', manufactMonth, '-', '01')
    let registrationDate = regtDate.year.toString().concat('-', regtDate.month, '-', regtDate.day)
    let previousYearPolicyExpiryDate = expiryDate.year.toString().concat('-', expiryDate.month, '-', expiryDate.day)

    let car = {
        modelId: makeModel,
        fuelTypeId: fuelType,
        varientId: variant,
        cityRegistrationId: rto,
        carRegisterDate: registrationDate,
        manufacturingDate: manufacturingDate,
        previousYearPolicyExpiryDate: previousYearPolicyExpiryDate,
        claimPastYear: (claimStatus == 'no') ? 'false' : 'true'
    }

    if (!(new Date(manufacturingDate) <= new Date(registrationDate))) { return "Manufacturing Date is less then Registration Date" }
    if (!(new Date(manufacturingDate).getTime() < new Date().setMonth(new Date().getMonth() - 9))) { return "Manufacturer Month should not be within 9 Month from Current Date" }

    if (leadId) {
        car.ncb = (claimStatus == 'no') ? 'false' : '0'
        car.policyType = (new Date(previousYearPolicyExpiryDate) >= new Date()) ? 'renew_enquiry' : 'expired_enquiry'
        result.data = await model.carEnquiry.findOne({
            where: {
                leadInsuranceId: leadId
            }
        })
        if (result.data) {
            result.data = await result.update(car)
            result.status = 200
            return { status: 200, data: result, error: '' }
        }

    } else {
        leadId = await saveLead(44, '4', '1', '1', 'BI0')
    }

    policyType = (policyType == 'renew_enquiry') ? (new Date(previousYearPolicyExpiryDate) >= new Date()) ? 'renew_enquiry' : 'expired_enquiry' : 'new_enquiry'
    if (policyType != 'renew_enquiry') { previousYearPolicyExpiryDate = "" }
    if (policyType != 'renew_enquiry') { car.claimPastYear = "true" }

    car.leadInsuranceId = leadId
    car.enquiryId = new Date().getTime().toString().substring(0, 10).concat(Math.round(Math.random() * 1000000))
    car.policyType = policyType
    car.ncb = (claimStatus == 'no') ? lastPolicyNcb : '0'
    car.idv = '0'
    car.zeroDepreciation = 'false'
    car.driverCover = '0'
    car.externalBiFuelKit = 'false'
    car.antitheftDevice = 'false'
    car.automobileAssociation = 'false'

    result.data = await model.carEnquiry.create(car)
    result.status = (result.data.length == 0) ? 404 : 200
    return { status: 200, data: result, error: '' }

}

exports.carQuotes = async (enquiryId, queryType) => {

    const carEnquiryData = await getCarEnquiryData(enquiryId)
    if (carEnquiryData) {
        const daysLeftToPolicyExpiry = moment(new Date().toLocaleDateString()).diff(moment(new Date(carEnquiryData.previousYearPolicyExpirtyDate).toLocaleDateString()), 'days')
        const newPolicyStartDate = (daysLeftToPolicyExpiry < 0) ? moment(new Date(carEnquiryData.previousYearPolicyExpirtyDate).toLocaleDateString()).add(1, 'd').format('YYYY-MM-DD') : moment(new Date().toLocaleDateString()).add(2, 'd').format('YYYY-MM-DD')
        const prevYearPolicyStartDate = moment(new Date(carEnquiryData.previousYearPolicyExpirtyDate).toLocaleDateString()).subtract(1, 'Y').add(1, 'd').format('YYYY-MM-DD')
        const newPolicyEndDate = moment(new Date(newPolicyStartDate).toLocaleDateString()).add(1, 'Y').subtract(1, 'd').format('YYYY-MM-DD')
        const ncbBonusObject = { '0': '20', '20': '25', '25': '35', '35': '45', '45': '50', '50': '50' }
        const newNcb = ((carEnquiryData.claimPastYear == 'false') || (carEnquiryData.policyType == 'new_enquiry') || (daysLeftToPolicyExpiry > 90)) ? 0 : ncbBonusObject[carEnquiryData.ncb]
        const vehicleAge = moment(new Date().toLocaleDateString()).diff(moment(new Date(carEnquiryData.manufacturingDate).toLocaleDateString()), 'months')
        const carEnquiryObj = {
            carEnquiryId: carEnquiryData.enquiryId,
            leadId: carEnquiryData.leadInsuranceId,
            biCarModel: carEnquiryData.carModel.carModelName,
            biCarFuel: carEnquiryData.carFuelType.fuelName,
            biCarVariant: carEnquiryData.carVariant.variant,
            biRto: carEnquiryData.carRto.rtoCode,
            biRtoId: carEnquiryData.cityRegistrationId,
            biCarModelId: carEnquiryData.modelId,
            biCarFuelId: carEnquiryData.fuelTypeId,
            biCarVariantId: carEnquiryData.varientId,
            enquiryType: carEnquiryData.policyType,
            regDay: new Date(carEnquiryData.carRegisterDate).getDate(),
            regMonth: new Date(carEnquiryData.carRegisterDate).getMonth() + 1,
            regYear: new Date(carEnquiryData.carRegisterDate).getFullYear(),
            carRegisterDate: new Date(carEnquiryData.carRegisterDate).toLocaleDateString(),
            carExpiryDay: new Date(carEnquiryData.previousYearPolicyExpirtyDate).getDate(),
            carExpiryMonth: new Date(carEnquiryData.previousYearPolicyExpirtyDate).getMonth() + 1,
            carExpiryYear: new Date(carEnquiryData.previousYearPolicyExpirtyDate).getFullYear(),
            previousYearPolicyExpirtyDate: new Date(carEnquiryData.previousYearPolicyExpirtyDate).toLocaleDateString(),
            prevPolicyStartDate: prevYearPolicyStartDate,
            existingInsurercompanyId: carEnquiryData.existingInsuranceCompanyId,
            existingInsurerPolicyNo: carEnquiryData.existingPolicyNo,
            existingCarRegNo: carEnquiryData.carRegistrationNo,
            newPolicyStartDate: newPolicyStartDate,
            newPolicyEndDate: newPolicyEndDate,
            daysLeftToPolicyExpiry: daysLeftToPolicyExpiry,
            manufacturingMonth: new Date(carEnquiryData.manufacturingDate).getMonth() + 1,
            manufacturingYear: new Date(carEnquiryData.manufacturingDate).getFullYear(),
            manufacturingDate: new Date(carEnquiryData.manufacturingDate).getDate(),
            vehicleAgeInMonths: vehicleAge,
            prevYearClaim: carEnquiryData.claimPastYear,
            ncb: carEnquiryData.ncb,
            newNcb: newNcb,
            idv: carEnquiryData.idv,
            relianceZeroDepRate: carEnquiryData.relianceNilDepApplicableRate,
            royalSundaramIdv: carEnquiryData.royalSundaramIdv,
            zeroDepreciation: carEnquiryData.zeroDepreciation,
            invoiceCover: carEnquiryData.invoiceCover,
            rodeSideAssistance: carEnquiryData.rodeSideAssistance,
            engineProtector: carEnquiryData.engineProtector,
            ncbProtection: carEnquiryData.ncbProtection,
            consumable: carEnquiryData.consumable,
            keyProtector: carEnquiryData.keyProtector,
            tyreCover: carEnquiryData.tyreCover,
            paToOwnerDriver: carEnquiryData.paToOwnerDriver,
            paidDriverLegalLiabilityCover: carEnquiryData.paidDriverLegalLiabilityCover,
            passengerCoverAddOn: (carEnquiryData.driverCover > 0) ? 'true' : 'false',
            driverCover: carEnquiryData.driverCover,
            accessoriesAddOn: ((carEnquiryData.electricalAccessories > 0) || (carEnquiryData.nonElectricalAccessories > 0)) ? 'true' : 'false',
            electricalAccessories: carEnquiryData.electricalAccessories,
            nonElectricalAccessories: carEnquiryData.nonElectricalAccessories,
            externalBiFuelKitAddOn: carEnquiryData.externalBiFuelKit,
            externalBiFuelKitValue: carEnquiryData.externalBiFuelKitValue,
            voluntaryDeductible: (carEnquiryData.voluntaryDeductibleValue > 0) ? 'true' : 'false',
            voluntaryDeductibleValue: carEnquiryData.voluntaryDeductibleValue,
            antiTheftDevice: carEnquiryData.antiTheftDevice,
            automobileAssociation: carEnquiryData.automobileAssociation,
            transactionId: carEnquiryData.transactionId,
            buyId: Base64.encode(carEnquiryData.enquiryId),
            // buyId: Buffer.from(carEnquiryData.id).toString('base64'),
            passengerCoverMultiplier: {
                '0': 0,
                '50000': 1,
                '100000': 2,
                '200000': 4
            },
            gst: 1.18
        }
        if (queryType == 'edit') {
            return { status: 200, success: true, data: carEnquiryObj }
        }
        return await getCarQuotes(carEnquiryData, carEnquiryObj)
    } else {
        return { status: 404, success: false, error: "Data Not found" }
    }
}

const getCarEnquiryData = async (enquiryId) => {
    let result = await model.carEnquiry.findOne({
        include: [{
            model: model.leadInsurance,
            as: 'leadInsurance',
            required: true,
        },
        {
            model: model.carModel,
            as: 'carModel',
            required: true,
            attributes: ['carModelName']
        },
        {
            model: model.carFuelType,
            as: 'carFuelType',
            required: true,
            attributes: ['fuelName']
        },
        {
            model: model.carVariant,
            as: 'carVariant',
            required: true,
            attributes: ['variant']
        },
        {
            model: model.carRto,
            as: 'carRto',
            required: true,
            attributes: ['rtoCode']
        }],

        where: {
            enquiryId: enquiryId
        }
    })
    return result

}

const getCarQuotes = async (carEnquiryData, carEnquiryObj) => {
    insurerRenewalIntegration = {
        '8': false,
        '12': false,
        '14': false,
        '38': false,
        '40': true,
        '36': false,
        '45': false
    }
    let whereCondition = {}
    if (insurerRenewalIntegration[carEnquiryData.existingInsurercompanyId] == false) {
        whereCondition = {
            id: {
                [Op.ne]: carEnquiryData.existingInsurercompanyId
            }
        }
    }
    let carQuotes = await model.companies.findAll({
        include: [{
            model: model.masterSheet,
            as: 'masterSheet',
            required: true,
            where: {
                status: '1',
                policyCategoryId: '4'
            },
            attributes: [['id', 'masterSheetId'], 'brochure', 'policyWordings', 'policyCode']
        }],
        order: [
            ['id', 'ASC'],
        ],
        attributes: [['id', 'companyId'], 'companyName', 'logo'],
        where: whereCondition

    })

    let newCarQuotes = JSON.parse(JSON.stringify(carQuotes));
    if (newCarQuotes) {
        let x = 0
        const insurerModelTable = {
            '8': 'carBajajAllianzMasterCode', '12': 'carRelianceMasterCode', '14': 'carNewIndMasterCode',
            '38': 'carTataAigMasterCode', '40': 'carHdfcErgoMasterCode', '36': 'carUnitedIndiaMasterCode', '45': 'carShriramGiMasterCode',
            '53': 'carDigitGiMasterCode', '37': 'carRoyalSundaramGiMasterCode', '35': 'carOrientalGiMasterCode', '43': 'carFutureGeneraliGiMasterCode'
        }
        for await (let carQuote of newCarQuotes) {
            let insurerModelData = await getInsurerMakeModelVariant(insurerModelTable[carQuote.companyId], carEnquiryObj.biCarModelId, carEnquiryObj.biCarFuelId, carEnquiryObj.biCarVariantId)
            if (insurerModelData.success) {
                const verifyCcData = await getVerifyCCdetails(carEnquiryObj.biCarVariant, insurerModelData.response[0].cubicCapacity)
                if (verifyCcData.success) {
                    Object.assign(newCarQuotes[x], { insurerModelData: insurerModelData.response })
                    const insurerRtoData = await insurerRto(carQuote.companyId, carEnquiryObj.biRtoId)
                    if (insurerRtoData.success) {
                        Object.assign(newCarQuotes[x], { insurerRtoData: insurerRtoData.response, minIdv: 0, maxIdv: 0, success: true })
                        if (carEnquiryObj.enquiryType != 'expired_enquiry' || newCarQuotes[x].companyId == 40) {
                            Object.assign(newCarQuotes[x], { expiredPolicyStatus: 'false', errorMsg: '' })
                            x++
                        } else {
                            Object.assign(newCarQuotes[x], { success: false, expiredPolicyStatus: 'true', errorMsg: 'expired Policy can not be bought Online' })
                            x++
                        }
                    } else {
                        Object.assign(newCarQuotes[x], { success: false, errorMsg: 'RTO Location not Serviced' })
                        x++
                    }
                } else {
                    Object.assign(newCarQuotes[x], {
                        success: false,
                        errorMsg: 'Car CC Error',
                        errorData: {
                            leadId: carEnquiryObj.leadId,
                            companyName: carQuote.companyName,
                            carModel: carEnquiryObj.biCarModel,
                            carModelId: carEnquiryObj.biCarModelId,
                            carFuelId: carEnquiryObj.biCarFuelId,
                            carVariantId: carEnquiryObj.biCarVariantId
                        }
                    })
                    x++
                }
            } else {
                Object.assign(newCarQuotes[x], {
                    success: false,
                    errorMsg: 'Model not Found',
                    errorData: {
                        leadId: carEnquiryObj.leadId,
                        companyName: carQuote.companyName,
                        carModel: carEnquiryObj.biCarModel,
                        carModelId: carEnquiryObj.biCarModelId,
                        carFuelId: carEnquiryObj.biCarFuelId,
                        carVariantId: carEnquiryObj.biCarVariantId
                    }
                })
                x++
            }
        }
    }
    return { enquiryData: carEnquiryObj, insurerData: newCarQuotes , status:200 , success:true }

}

const getInsurerMakeModelVariant = async (table, biModelId, biFuelId, biVariantId) => {
    const result = await model[table].findAll({
        where: {
            carModelId: biModelId,
            carFuelTypeId: biFuelId,
            carVariantId: biVariantId
        }
    })
    if (result.length == 0) {
        return { success: false, response: result }
    }
    return { success: true, response: result }
}

const getVerifyCCdetails = async (variantName, insurerCubicCapacity) => {
    let cubicCapacityData = variantName.split("(")[1]
    cubicCapacityData = cubicCapacityData.split(' ')[0]
    const cubicCapacity = cubicCapacityData.match(/[0-9]{3,4}/)
    const sysCc = await compareCubicCapacity(cubicCapacity)
    const insurerCc = await compareCubicCapacity(insurerCubicCapacity)
    if (sysCc == insurerCc) {
        return { success: true }
    } else {
        return { success: false }
    }
}

const compareCubicCapacity = async (cubicCapacity) => {
    if (cubicCapacity >= 0 && cubicCapacity <= 1000) {
        return 1000
    } else if (cubicCapacity >= 1001 && cubicCapacity <= 1500) {
        return 1500
    } else if (cubicCapacity >= 1501 && cubicCapacity <= 10000) {
        return 10000
    } else {
        throw new Error("cubic Capacity Not Matched in Range")
    }
}

const insurerRto = async (companyId, biRtoId) => {
    let result = await model.carRto.findAll({
        include: [{
            model: model.carRtoCode,
            as: 'RtoCode',
            where: {
                companyId: companyId,
                rtoCodeId: biRtoId
            },
            required: true
        }],
    })
    if (result.length == 0) {
        return { success: false }
    } else {
        return { success: true, response: result }
    }
}

exports.editCarQuotes = async ({ parameter, id }, enquiryId) => {
    let carEnquiry = await model.carEnquiry.findOne({
        where: {
            enquiryId: enquiryId
        }
    })
    if (carEnquiry) {
        carEnquiry[parameter] = id
        carEnquiry.save()
        return { success: true, status: 200, data: carEnquiry }
    } else {
        return { success: false, status: 404, error: "Data Not Found" }
    }

}

exports.editCarEnquiry = async (data, enquiryId) => {
    const { parameter, id, voluntaryDeductibleValue, antiTheftDevice, automobileAssociation, electricalAccessories, nonElectricalAccessories, externalBiFuelKitValue, date } = data
    let carEnquiry = await model.carEnquiry.findOne({
        where: {
            enquiryId: enquiryId
        }
    })
    if (parameter == 'ncb') {
        if (id == 'true') {
            carEnquiry.claimPastYear = 'true'
            carEnquiry.ncb = '0'
        } else {
            carEnquiry.claimPastYear = 'false'
            carEnquiry.ncb = id
        }
    } else if (parameter == 'discounts') {
        carEnquiry.voluntaryDeductibleValue = voluntaryDeductibleValue
        carEnquiry.antiTheftDevice = antiTheftDevice
        carEnquiry.automobileAssociation = automobileAssociation
    } else if (parameter == 'additionalCovers') {
        carEnquiry.electricalAccessories = electricalAccessories
        carEnquiry.nonElectricalAccessories = nonElectricalAccessories
        carEnquiry.externalBiFuelKitValue = externalBiFuelKitValue
        carEnquiry.externalBiFuelKit = (externalBiFuelKitValue > 0) ? 'true' : 'false'
    } else if (parameter == 'car_register_date' || parameter == 'previous_year_policy_expirty_date') {
        let currentDate = moment(new Date().toLocaleDateString())
        date = moment(new Date(date).toLocaleDateString())
        carEnquiry[parameter] = date
        if (parameter == 'car_register_date') { carEnquiry.manufacturingDate = date }
        if (parameter == 'previous_year_policy_expirty_date') {
            const policyType = (date >= currentDate) ? 'renew_enquiry' : 'expired_enquiry'
            carEnquiry.policyType = policyType
        }
    } else {
        carEnquiry[parameter] = id
    }
    carEnquiry.save()
    const displayCarQuotes = this.carQuotes(enquiryId, 'edit')
    return { success: true, status: 200, data: displayCarQuotes }
}

exports.carProposalFormSave = async (data) => {
    let { id, dob, nomineeDob, loanStartDate, thirdPartyExpiryDate, email, pFirstName, pLastName, carEnquiryId } = data
    dob = moment(new Date(dob).toLocaleDateString()).format('YYYY-MM-DD')
    loanStartDate = moment(new Date(loanStartDate).toLocaleDateString()).format('YYYY-MM-DD')
    nomineeDob = moment(new Date(nomineeDob).toLocaleDateString()).format('YYYY-MM-DD')
    if (thirdPartyExpiryDate) {
        thirdPartyExpiryDate = moment(new Date(thirdPartyExpiryDate).toLocaleDateString()).format('YYYY-MM-DD')
    }
    const emailList = await emailListId(email)
    if (pFirstName) {
        let leadInsuranceId = await model.carEnquiry.findAll({
            where: {
                enquiryId: carEnquiryId
            }
        })
        updateObj = {
            fname: pFirstName,
            lname: pLastName,
            email: email,
            emailListId: emailList
        }
        await model.leadInsurance.update(updateObj, {
            where: {
                id: leadInsuranceId[0].leadInsuranceId
            }
        })
    }

    if (id == '') {
        const result = await model.carProposalForm.create(data)
        return { status: 200, success: true, data: result }
    } else {
        await model.carProposalForm.update(data, {
            where: {
                id: id
            }
        })
        let result = await model.carProposalForm.findOne({
            where: {
                id: id
            }
        })

        return { success: true, data: result, status: 200 }
    }

}

exports.insurerCityStateDistrict = async (companyId, pincode) => {

    let result = await model.states.findAll({
        include: [
            {
                model: model.cities,
                as: 'cities',
                where: {
                    companyId: companyId,
                    status: "1"
                },
                order: [
                    ['city', 'ASC']
                ],
                required: true,
                attributes: ['id', 'city', 'cityCode', 'stateName', 'stateCode', 'districtCode']
            },
            {
                model: model.pincode,
                as: 'pincode',
                required: true,
                where: {
                    pincode: pincode
                }
            }
        ],
    })

    if (result.length) {
        return { status: 200, success: true, data: result }
    } else {
        return { status: 404, success: false, error: "Data Not Found" }
    }
}

const getCarProposalFormData = async (enquiryId, proposalId) => {
    let result = await model.carProposalForm.findAll({
        include: [
            {
                model: model.carEnquiry,
                as: 'carEnquiryCarProposalForm',
                required: true,
                where: {
                    enquiryId: enquiryId
                }
            }
        ],
        where: {
            id: proposalId
        },
        order: [
            ['id', 'DESC']
        ],
    })
    return result
}

const carEnquiryDetails = async (enquiryId) => {
    let result = await model.carEnquiry.findAll({
        include: [
            {
                model: model.leadInsurance,
                as: 'leadInsurance',
                required: true,
                include: [
                    {
                        model: model.masterSheet,
                        as: 'msId',
                        required: true,
                        include: [
                            {
                                model: model.companies,
                                as: 'companies',
                                required: true,
                                attributes: [['id', 'companyId'], 'companyName', 'insuranceType', 'logo', 'address', 'contactNo']
                                // ['id','companyId'],'companyName','insuranceType','logo','address','contactNo'
                            }
                        ],
                        attributes: ['policyName', 'policyCode']
                    },
                    {
                        model: model.leadInsuranceStatus,
                        as: 'leadInsuranceToLeadInsuranceStatus',
                        attributes: ['statusName']
                    }
                ],
                attributes: ['masterSheetId', ['status', 'leadStatus']]
            },
            {
                model: model.carRto,
                as: 'carRto',
                required: true,
                attributes: ['rtoCode']
            },
            {
                model: model.carModel,
                as: 'carModel',
                required: true,
                attributes: ['carModelName']
            },
            {
                model: model.carFuelType,
                as: 'carFuelType',
                required: true,
                attributes: ['fuelName']
            },
            {
                model: model.carVariant,
                as: 'carVariant',
                required: true,
                attributes: ['variant'],
                where: {},

            }
        ],
        where: {
            enquiryId: enquiryId
        },
        order: [
            ['enquiryId', 'DESC']
        ]
    })
    return result
}

exports.proposalFormSummary = async (proposalId, enquiryId) => {
    let carProposalDetails = await getCarProposalFormData(enquiryId, proposalId)
    let carEnquiryDetail = await carEnquiryDetails(enquiryId)
    if (carProposalDetails.length && carEnquiryDetail.length) {
        return { data: { carProposalDetails, carEnquiryDetail }, status: 200, success: true }
    } else {
        return { data: {}, status: 404, success: false }
    }
}



exports.proposalForm = async (enquiryId) => {
    let result = await carEnquiryDetails(enquiryId)
    if (result.length) {
        return { data: result, status: 200, success: true }
    } else {
        return { data: result, status: 404, success: false }
    }

}

exports.insuranceAndFinanceDetails = async (companyId) => {
    let existingInsurance = await model.carExistingInsurance.findAll({
        where: {
            companyId: companyId,
            status: '1'
        },
        attributes: ['id', 'insuranceCompany', 'companyCode'],
        order: [
            ['insuranceCompany', 'ASC']
        ]
    })
    let carFinancier = await model.carFinancier.findAll({
        where: {
            companyId: companyId,
            status: '1'
        },
        attributes: ['id', 'financierName', 'companyCode'],
        order: [
            ['financierName', 'ASC']
        ]
    })
    if (existingInsurance.length && carFinancier.length) {
        return { success: true, status: 200, data: { existingInsurance, carFinancier } }
    } else {
        return { success: false, status: 404, data: { existingInsurance, carFinancier } }
    }
}

exports.addProposalForm = async (data) => {
    let updateLead = await updateLeadByMasterId(data)
    let updateIdvPermium = await updateIdvPremium(data)
    if (updateIdvPermium && updateLead) {
        return { success: true, status: 200, data: { updateLead, updateIdvPermium } }
    } else {
        return { success: false, status: 404, data: { updateLead, updateIdvPermium } }
    }
}

const updateLeadByMasterId = async (data) => {
    const { leadId, masterSheetId } = data
    const updateData = {
        masterSheetId: masterSheetId,
        status: "5"
    }
    let result = await model.leadInsurance.update(updateData, {
        where: {
            id: leadId
        }
    })
    if (result) {
        result = await model.leadInsurance.findOne({
            where: {
                id: leadId
            }
        })
    }

    return result
}

const updateIdvPremium = async (data) => {
    const { idv, premium, enquiryId, transactionId, odPremium, tpPremium, paToOwnerDriverPremium } = data
    const updateData = {
        idv: idv,
        finalPremium: premium,
        transactionId: transactionId,
        odPremium: odPremium,
        paToOwnerDriverPremium: paToOwnerDriverPremium,
        tpPremium
    }
    let result = await model.carEnquiry.update(updateData, {
        enquiryId: enquiryId
    })
    if (result) {
        result = await model.carEnquiry.findOne({
            where: {
                enquiryId: enquiryId
            }
        })
    }
    return result
}

exports.updateLeadStatus = async (leadInsuranceId, statusCode) => {
    let result = await model.leadInsurance.update(
        {
            status: statusCode
        },
        {
            where: {
                id: leadInsuranceId
            }
        }
    )
    if (result) {
        return { success: true, status: 200, data: "Data Updated" }
    } else {
        return { success: false, status: 404, data: "Data Not Updated" }
    }
}
