const model = require('../../models')
const MD5 = require('md5')
const { DateTime } = require('../../utils/date')


const saveLead = async (partnerCode, policyType, medium, vendor, subVendor) => {
    let result = {}
    let advisor = {}
    let leadInsurance = {}

    result.admin = await model.admin.findOne({
        where: {
            username: partnerCode
        }
    })
    if (result.admin) {
        advisor.fname = result.admin.fname
        advisor.lname = result.admin.lname
        advisor.email = result.admin.email
        advisor.mobile = result.admin.mobile
        advisor.partnerCodeFinal = partnerCode
        advisor.vendorId = vendor
    } else {
        advisor.fname = 'Insurance'
        advisor.lname = 'Broker'
        advisor.email = 'care@abc.com'
        advisor.mobile = '9999999999'
        advisor.partnerCodeFinal = 'booninsurance'
        advisor.vendorId = vendor
    }

    advisor.emailId = await emailListId(advisor.email)
    advisor.mobileId = await mobileListId(advisor.mobile, advisor.email)

    leadInsurance.emailListId = advisor.emailId
    leadInsurance.mobileListId = advisor.mobileId
    leadInsurance.enquiryType = 'Lead'
    leadInsurance.policyType = policyType
    leadInsurance.fname = advisor.fname
    leadInsurance.lname = advisor.lname
    leadInsurance.email = advisor.email
    leadInsurance.mobile = advisor.mobile
    leadInsurance.source = advisor.partnerCodeFinal
    leadInsurance.medium = medium
    leadInsurance.created = await DateTime().getDateTime()

    result.leadInsurance = await model.leadInsurance.create(leadInsurance)
    if (result.leadInsurance.id) {
        const data = { leadId: result.leadInsurance.id, campaignType: medium, vendorId: advisor.vendor, utmCampaign: subVendor, ipAddress: '::1 ' }
        let res = await model.leadCaimpaignDetails.create(data)
        if (!res) { return "data not Stored" }
    }
    return result.leadInsurance.id

}

const emailListId = async (email, pageUrl = '', source = '') => {
    let result = await model.emailList.findOne({
        where: {
            email: email
        }
    })
    if (result) { return result.id }
    const data = { email, pageUrl, source, emailConfirm: MD5(email) }
    result = await model.emailList.create(data)
    if (result.length == 0) { return "Error" }
    return result[0].id
}

const mobileListId = async (mobile, email) => {
    let result = await model.mobileList.findOne({
        where: {
            mobile: mobile
        }
    })
    if (result) { return result.id }
    const emailListId = emailListId(email)
    const data = { mobile, emailListId }
    result = await model.mobileList.create(data)
    if (result.length == 0) { return "Error" }
    return result[0].id
}

const fetchTableData = async (table, condition) => {
    const result = await model[table].findAll({
        where: condition
    })
    return result
}

module.exports = { saveLead, emailListId, mobileListId, fetchTableData }