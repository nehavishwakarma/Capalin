const { bikeInsuranceAjax, bikeProposalForm, getBikeProposalFormData } = require('../services/bike')

exports.getBikeInsuranceAjax = async (req, res) => {
    const { id, table } = req.params
    let result = await bikeInsuranceAjax(table, id)
    return res.status(result.status).json(result)
}
exports.getBikeProposalForm = async (req, res) => {
    let result = await bikeProposalForm(req.params.enquiryId)
    if (result.lenth != 0) {
        res.status(200).json(result)
    } else {
        res.status(404).json(result)
    }
}
exports.getProposalFormSummary = async (req, res) => {
    let { enquiryId, proposalId } = req.params
    proposalId = (proposalId) ? proposalId : req.session.proposalId
    let result = await getBikeProposalFormData(enquiryId, proposalId)
    return res.json(result)
} 