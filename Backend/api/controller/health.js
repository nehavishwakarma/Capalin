const models= require('../../models')
const { healthEnquiryDetails, getStateDetails,updateHealthEnquiryPincode,saveHealthProposalDetails} = require('../services/health')

exports.getStateDetails = async (req, res) => {
    const { companyId, pincode } = req.params
    let getState = await getStateDetails(companyId, pincode)
    return res.json(getState);
}
exports.getHealthEnquiry = async (req, res) => {
    let result = await healthEnquiryDetails(req.params.enquiryId)
    return res.json(result)
}

exports.getProposalFormSave = async(req,res)=>{
    const {health_enquiry_id,pincode_id } = req.body
    await updateHealthEnquiryPincode(health_enquiry_id, pincode_id)
    let saveHealthProposalDetail = await saveHealthProposalDetails(req.body)
    req.session.health_proposal_id = saveHealthProposalDetail.id
    return res.status(200).json(saveHealthProposalDetail)
}


