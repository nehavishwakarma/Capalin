const { carInsuranceAjax, carStoreJson, carProposalData, carQuotes, editCarQuotes, editCarEnquiry, carProposalFormSave, insurerCityStateDistrict, proposalFormSummary, proposalForm, addProposalForm, insuranceAndFinanceDetails, carDownloadPdf, updateLeadStatus } = require('../services/car')

exports.getCarInsuranceAjax = async (req, res) => {
    const { id, table } = req.params
    let result = await carInsuranceAjax(table, id)
    return res.status(result.status).json(result)
}

exports.getCarStoreJson = async (req, res) => {
    let result = await carStoreJson(req.body)
    return res.status(result.status).json(result)
}

exports.getCarProposalData = async (req, res) => {
    let result = await carProposalData(req.params.enquiryId)
    return res.status(result.status).json(result)
}

exports.getCarQuotes = async (req, res) => {
    let result = await carQuotes(req.params.enquiryId, 'new')
    return res.status(result.status).json(result)
}

exports.getEditCarQuotes = async (req, res) => {
    const { parameter, id } = req.body
    const enquiryId = req.params.enquiryId
    let result = await editCarQuotes({ parameter, id }, enquiryId)
    return res.status(result.status).json(result)
}

exports.getEditCarEnquiry = async (req, res) => {
    let result = await editCarEnquiry(req.body, req.params.enquiryId)
    return res.status(result.status).json(result)
}

exports.getCarProposalFormSave = async (req, res) => {
    let result = await carProposalFormSave(req.body)
    return res.status(result.status).json(result)
}

exports.getInsurerCityStateDistrict = async (req, res) => {
    const { companyId, pincode } = req.body
    let result = await insurerCityStateDistrict(companyId, pincode)
    return res.status(result.status).json(result)
}

exports.getProposalFormSummary = async (req, res) => {
    const { proposalId, enquiryId } = req.params
    let result = await proposalFormSummary(proposalId, enquiryId)
    return res.status(result.status).json(result)
}

exports.getProposalForm = async (req, res) => {
    const { enquiryId } = req.params
    let result = await proposalForm(enquiryId)
    return res.status(result.status).json(result)
}

exports.postProposalForm = async (req, res) => {
    let result = await addProposalForm(req.body)
    return res.status(result.status).json(result)
}

exports.getInsuranceAndFinanceDetails = async (req, res) => {
    const { companyId } = req.params
    let result = await insuranceAndFinanceDetails(companyId)
    return res.status(result.status).json(result)
}
exports.getUpdateLeadStatus = async (req, res) => {
    const { leadInsuranceId, statusCode } = req.params
    let result = await updateLeadStatus(leadInsuranceId, statusCode)
    return res.status(result.status).json(result)
}