const express = require('express')
const route = express.Router()
const { wrapper } = require('../middlewares/error')
const { getStateDetails, getHealthEnquiry ,getProposalFormSave} = require('../controller/health')

route.get('/health-insurer-city-state-district/:companyId/:pincode', wrapper(getStateDetails)) 
route.get('/health-enquiery_details/:enquiryId', wrapper(getHealthEnquiry))
route.post('/health-proposal-form-save',wrapper(getProposalFormSave))



module.exports = route
