const express = require('express')
const route = express.Router()
const car = require('./car')
const health = require('./health')
const bike = require('./bike')

route.use('/car',car)
route.use('/health',health)
route.use('/bike',bike)

module.exports = route