const express = require('express')
const route = express.Router()
const { wrapper } = require('../middlewares/error')
const { getBikeInsuranceAjax, getBikeProposalForm, getProposalFormSummary} = require('../controller/bike')

route.get('/bike-insurance-ajax/:table/:id', wrapper(getBikeInsuranceAjax))
route.get('/fetch-bike-proposal/:enquiryId', wrapper(getBikeProposalForm))
route.get('/bike-proposal-form-summary/:enquiryId/:proposalId', wrapper(getProposalFormSummary))

module.exports = route