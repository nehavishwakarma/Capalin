const express = require('express')
const route = express.Router()
const { wrapper } = require('../middlewares/error')
const { getCarInsuranceAjax,getCarStoreJson , getCarProposalData , getCarQuotes ,getEditCarQuotes ,getEditCarEnquiry ,getCarProposalFormSave ,getInsurerCityStateDistrict,getProposalFormSummary,getProposalForm,postProposalForm,getInsuranceAndFinanceDetails ,postCarDownloadPdf ,getUpdateLeadStatus} = require('../controller/car')

route.get('/car-insurance-ajax/:table/:id', wrapper(getCarInsuranceAjax))
route.get('/fetch-car-proposal/:enquiryId', wrapper(getCarProposalData))
route.post('/car-insurance', wrapper(getCarStoreJson))
route.post('/car-quotes/:enquiryId', wrapper(getCarQuotes))
route.post('/car-quotes/edit/:enquiryId', wrapper(getEditCarQuotes))
route.post('/car-quotes/edit-insurer-api/:enquiryId', wrapper(getEditCarEnquiry))
route.post('/car-proposal-form-save', wrapper(getCarProposalFormSave))
route.get('/car-insurer-city-state-district/:companyId/:pincode', wrapper(getInsurerCityStateDistrict))
route.get('/car-proposal-form-summary/:enquiryId/:proposalId', wrapper(getProposalFormSummary))
route.get('/car-proposal-form/:enquiryId', wrapper(getProposalForm))
route.post('/car-proposal-form', wrapper(postProposalForm))
route.post('/car-proposal-insurance-finance/:companyId', wrapper(getInsuranceAndFinanceDetails))
route.get('/update-car-lead-status/:leadInsuranceId/:statusCode',wrapper(getUpdateLeadStatus))


module.exports = route