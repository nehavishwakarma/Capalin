const express = require('express')
const app = express()
const indexRoute = require('./api/routes/index')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan'); 
const cors = require('cors'); 
const session = require('express-session')

app.use(logger('dev'));
app.use(cors());
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(session({
    // It holds the secret key for session
    secret: process.env.SESSION_SECRET_KEY,

    // Forces the session to be saved
    // back to the session store
    resave: true,

    // Forces a session that is "uninitialized"
    // to be saved to the store
    saveUninitialized: true
}))

app.use('/api', indexRoute)

app.use((req, res) => {
    res.status(404).json({
        message: "Url not found"
    })
});


app.use((err, req, res, next) => {
    console.log(err, 'errr');
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};    
    res.status(err.status || 500);
    res.render('error');
});

app.use((err, req, res, next) => {
    res.status(500).send({message:'server error'})   
});

module.exports = app;

